{
	"LangCode":
			{
				"E":"0",
				"F":"1",
				"CT":"2",
				"CS":"3",
				"J":"4",
				"G":"5",
				"K":"6",
				"R":"7",
				"P":"8",
				"A":"9",
				"S":"10"
			},
			

	"Room":
			{
				"E":"ROOM",
				"F":"CHAMBRE",
				"CT":"房號",
				"CS":"房号",
				"J":"部屋",
				"G":"ZIMMER",
				"K":"객실",
				"R":"КОМНАТА",
				"P":"QUARTO",
				"A":"غرفة",
				"S":"HABITACIÓN"
			},
			
			
	"Bedroom Control":
			{
				"E":"BEDROOM",
				"F":"CHAMBRE",
				"CT":"臥室",
				"CS":"卧室",
				"J":"寝室",
				"G":"ZIMMER",
				"K":"침실",
				"R":"СПАЛЬНЯ",
				"P":"QUARTO",
				"A":"مراقبة غرفة نوم",
				"S":"DORMITORIO"
			},
			
			
	"Living Room Control":
			{
				"E":"LIVING ROOM",
				"F":"SALON",
				"CT":"客廳",
				"CS":"客厅",
				"J":"居間",
				"G":"LIVING ROOM",
				"K":"거실",
				"R":"ГОСТИНАЯ",
				"P":"SALA DE ESTAR",
				"A":"غرفة المعيشة",
				"S":"SALA DE ESTAR"
			},
						
	
	"RC": 
			{
				"E":"ROOM CONTROL",
				"F":"COMMANDES",
				"CT":"房間調控",
				"CS":"房间调控",
				"J":"ルームコントロール",
				"G":"ZIMMERREGLER",
				"K":"객실 제어",
				"R":"СИСТЕМА",
				"P":"CONTROLOS DO QUARTO",
				"A":"وسائل تحكم الغرفة",
				"S":"CONTROLES DE LA"
			},
			
	"TV":
			{
				"E":"TV",
				"F":"TV",
				"CT":"電視",
				"CS":"电视",
				"J":"テレビ",
				"G":"FERNSEHER",
				"K":"TV",
				"R":"ТВ",
				"P":"TV",
				"A":"تلفزيون",
				"S":"TV"
			},
			
	"Radio":
			{
				"E":"RADIO",
				"F":"RADIO",
				"CT":"電台",
				"CS":"电台",
				"J":"ラジオ",
				"G":"RADIO",
				"K":"라디오",
				"R":"РАДИО",
				"P":"RÁDIO",
				"A":"راديو",
				"S":"RADIO"
			},
			
	"Lang":
			{
				"E":"LANGUAGES",
				"F":"LANGUES",
				"CT":"語言",
				"CS":"语言",
				"J":"言語",
				"G":"SPRACHEN",
				"K":"언어",
				"R":"ЯЗЫКИ",
				"P":"IDIOMAS",
				"A":"اللغات",
				"S":"IDIOMAS"
			},
			
	"Weather":
			{
				"E":"WEATHER",
				"F":"MÉTÉO",
				"CT":"天氣",
				"CS":"天气",
				"J":"天気",
				"G":"WETTER",
				"K":"날씨",
				"R":"ПОГОДА",
				"P":"TEMPO",
				"A":"الطقس",
				"S":"TIEMPO"
			},
			
	"MasterSwitch":
			{
				"E":"MASTER SWITCH",
				"F":"INTERRUPTEUR PRINCIPAL",
				"CT":"總開關",
				"CS":"总开关",
				"J":"主電源",
				"G":"HAUPTSCHALTER",
				"K":"마스터 스위치",
				"R":"ГЛАВНЫЙ ВЫКЛЮЧАТЕЛЬ",
				"P":"INTERRUPTOR PRINCIPAL",
				"A":"المفتاح الرئيسي",
				"S":"INTERRUPTOR PRINCIPAL"			
			},
			
	"BedroomLight":
			{
				"E":"ROOM LIGHT",
				"F":"ECLAIRAGE CHAMBRE",
				"CT":"房燈",
				"CS":"房灯",
				"J":"照明",
				"G":"ZIMMERBELEUCHTUNG",
				"K":"객실 조명",
				"R":"ОСВЕЩЕНИЕ НОМЕРА",
				"P":"LUZ DO QUARTO",
				"A":"إضاءة الغرفة",
				"S":"ILUMINACIÓN CUARTO"			
			},
			
	"Temperature":
			{
				"E":"TEMPERATURE",
				"F":"TEMPÉRATURE",
				"CT":"溫度",
				"CS":"温度",
				"J":"温度",
				"G":"TEMPERATUR",
				"K":"온도",
				"R":"ТЕМПЕРАТУРА",
				"P":"TEMPERATURA",
				"A":"درجة الحرارة",
				"S":"TEMPERATURA"
			},
			
	"NightLight":
			{
				"E":"NIGHT LIGHT",
				"F":"VEILLEUSES",
				"CT":"夜燈",
				"CS":"夜灯",
				"J":"常夜灯",
				"G":"NACHTLICHT",
				"K":"종야등",
				"R":"НОЧНИК",
				"P":"NIGHT LIGHT",
				"A":"الليل الخفيفة",
				"S":"LAMPARILLA"
			},
			
	"Curtain":
			{
				"E":"CURTAIN",
				"F":"RIDEAUX",
				"CT":"窗簾",
				"CS":"窗帘",
				"J":"カーテン",
				"G":"VORHÄNGE",
				"K":"커튼",
				"R":"ШТОРЫ",
				"P":"CORTINAS",
				"A":"الستائر",
				"S":"CORTINAS"
			},
			
	"Fan":
			{
				"E":"FAN SPEED",
				"F":"VITESSE DU VENTILATEUR",
				"CT":"風速",
				"CS":"风速",
				"J":"風速",
				"G":"VENTILATORSTUFE",
				"K":"팬 속도",
				"R":"СКОРОСТЬ ВЕНТИЛЯТРА",
				"P":"VELOCIDADE DO VENTILADOR",
				"A":"سرعة المروحة",
				"S":"VELOCIDAD DEL VENTILADOR"				
			},
			
	"VC":
			{
				"E":"VALET",
				"F":"VALET",
				"CT":"服務員",
				"CS":"服务员",
				"J":"バレー",
				"G":"VALET",
				"K":"발레",
				"R":"ОБСЛУЖИВАНИЕ",
				"P":"MALAS",
				"A":"خادم",
				"S":"SERVICIO DE VALET"
			},
			
	"MUR":
			{
				"E":"MAKE UP ROOM",
				"F":"SERVICE DE CHAMBRE",
				"CT":"清潔房間",
				"CS":"清洁房间",
				"J":"客室清掃",
				"G":"BITTE ZIMMER REINIGEN",
				"K":"메이크업 룸",
				"R":"УБОРНАЯ",
				"P":"SERVIÇO DE QUARTO",
				"A":"غرفة التجميل",
				"S":"SERVICIO DE HABITACIONES"
			},
			
	"DND":
			{
				"E":"PRIVACY PLEASE",
				"F":"INTIMITÉ S'IL VOUS",
				"CT":"請勿打擾",
				"CS":"请勿打扰",
				"J":"プライバシーください",
				"G":"BITTE NICHT STÖREN",
				"K":"방해하지 마십시오",
				"R":"ПРОСЬБА НЕ",
				"P":"PRIVACIDADE POR",
				"A":"الرجاء عدم الإزعاج",
				"S":"PRIVACIDAD POR FAVOR"
			},
			
	"SetAlarm":
			{
				"E":"ENABLE ALARM",
				"F":"ACTIVER ALARME",
				"CT":"啟動響鬧",
				"CS":"启动响闹",
				"J":"アラームを設定",
				"G":"WECKER AKTIVIEREN",
				"K":"알람 사용",
				"R":"ВКЛЮЧИТЬ",
				"P":"ACTIVAR ALARME",
				"A":"تمكين التنبيه",
				"S":"ACTIVAR LA ALARMA"
			},
			
	"Snooze":
			{
				"E":"SNOOZE",
				"F":"SNOOZE",
				"CT":"小睡",
				"CS":"小睡",
				"J":"スヌーズ",
				"G":"SCHLUMMERN",
				"K":"졸다",
				"R":"КОРОТКИЙ СОН",
				"P":"SNOOZE",
				"A":"قيلل",
				"S":"SNOOZE"
			},
			
	"SetHour":
			{
				"E":"HOURS",
				"F":"HEURE",
				"CT":"時",
				"CS":"时",
				"J":"時",
				"G":"STUNDEN",
				"K":"시간",
				"R":"ЧАСОВ",
				"P":"HORAS",
				"A":"ساعات",
				"S":"HORAS"
			},
			
	"SetMinute":
			{
				"E":"MINUTES",
				"F":"MINUTE",
				"CT":"分",
				"CS":"分",
				"J":"分",
				"G":"MINUTEN",
				"K":"분",
				"R":"МИНУТ",
				"P":"ATAS",
				"A":"دقيقة",
				"S":"ACTA"
			},
			
	"RadioCountry":
			{
				"E":"Countries",
				"F":"Pays",
				"CT":"國家",
				"CS":"国家",
				"J":"国々",
				"G":"Länder",
				"K":"국가",
				"R":"Страны",
				"P":"Países",
				"A":"البلدان",
				"S":"Países"
			},
			
	"RadioGenre":
			{
				"E":"Genres",
				"F":"Genres",
				"CT":"類型",
				"CS":"类型",
				"J":"ジャンル",
				"G":"Genres",
				"K":"장르",
				"R":"Жанры",
				"P":"Gêneros",
				"A":"اكتشف الموسيقى",
				"S":"Géneros"
			},
			
	"RadioStreamToRoom":
			{
				"E":"Audio Output",
				"F":"Sortie Audio",
				"CT":"音響輸出",
				"CS":"音响输出",
				"J":"音声出力",
				"G":"Audio-Ausgang",
				"K":"오디오 출력",
				"R":"Аудио выход",
				"P":"saída de Áudio",
				"A":"خرج الصوت",
				"S":"Salida de audio"
			},
			
	"wwTemperature":
			{
				"E":"Temperature",
				"F":"Température",
				"CT":"溫度",
				"CS":"温度",
				"J":"温度",
				"G":"Temperatur",
				"K":"온도",
				"R":"ТЕМПЕРАТУРА",
				"P":"Temperatura",
				"A":"درجة الحرارة",
				"S":"Temperatura"
			},
			
	"wwHumidity":
			{
				"E":"Humidity",
				"F":"Humidité",
				"CT":"濕度",
				"CS":"湿度",
				"J":"湿度",
				"G":"Luftfeuchtigkeit",
				"K":"습도",
				"R":"ВЛАЖНОСТЬ",
				"P":"Umidade",
				"A":"الرطوبة",
				"S":"Humedad"
			},
			
	"wwWind":
			{
				"E":"Wind",
				"F":"Vent",
				"CT":"風",
				"CS":"风",
				"J":"風",
				"G":"Wind",
				"K":"바람",
				"R":"ВЕТЕР",
				"P":"Vento",
				"A":"الرياح",
				"S":"Viento"
			},
			
	"wwUV":
			{
				"E":"UV Index",
				"F":"Index UV",
				"CT":"紫外線指數",
				"CS":"紫外线指数",
				"J":"紫外線指数",
				"G":"UV-Index",
				"K":"자외선 지수",
				"R":"УФ-ИНДЕКС",
				"P":"Índice UV",
				"A":"مؤشر الأشعة فوق البنفسجية",
				"S":"Índice UV"
			},
			
	"Mon":
			{
				"E":"Monday",
				"F":"Lundi",
				"CT":"星期一",
				"CS":"星期一",
				"J":"月曜日",
				"G":"Montag",
				"K":"월요일",
				"R":"Понедельник",
				"P":"Segunda",
				"A":"الاثنين",
				"S":"Lunes"
			},
			
	"Tue":
			{
				"E":"Tuesday",
				"F":"Mardi",
				"CT":"星期二",
				"CS":"星期二",
				"J":"火曜日",
				"G":"Dienstag",
				"K":"화요일",
				"R":"Вторник",
				"P":"Terça",
				"A":"الثلاثاء",
				"S":"Martes"
				
			},
				
	"Wed":
			{
				"E":"Wednesday",
				"F":"Mercredi",
				"CT":"星期三",
				"CS":"星期三",
				"J":"水曜日",
				"G":"Mittwoch",
				"K":"수요일",
				"R":"Среда",
				"P":"Quarta",
				"A":"الأربعاء",
				"S":"Miércoles"
				
			},
			
	"Thu":
			{
				"E":"Thursday",
				"F":"Jeudi",
				"CT":"星期四",
				"CS":"星期四",
				"J":"木曜日",
				"G":"Donnerstag",
				"K":"목요일",
				"R":"Четверг",
				"P":"Quinta",
				"A":"الخميس",
				"S":"Jueves"
				
			},	
			
	"Fri":
			{
				"E":"Friday",
				"F":"Vendredi",
				"CT":"星期五",
				"CS":"星期五",
				"J":"金曜日",
				"G":"Freitag",
				"K":"금요일",
				"R":"Пятница",
				"P":"Sexta-feira",
				"A":"الجمعة",
				"S":"Viernes"
				
			},
			
	"Sat":
			{
				"E":"Saturday",
				"F":"Samedi",
				"CT":"星期六",
				"CS":"星期六",
				"J":"土曜日",
				"G":"Samstag",
				"K":"토요일",
				"R":"Суббота",
				"P":"Sábado",
				"A":"السبت",
				"S":"Sábado"
				
			},			
			
	"Sun":
			{
				"E":"Sunday",
				"F":"Dimanche",
				"CT":"星期日",
				"CS":"星期日",
				"J":"日曜日",
				"G":"Sonntag",
				"K":"일요일",
				"R":"Воскресенье",
				"P":"Domingo",
				"A":"الأحد",
				"S":"Domingo"
				
			}			
			
					
			
			
}
		
	