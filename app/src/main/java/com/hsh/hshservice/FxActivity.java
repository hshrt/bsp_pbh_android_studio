package com.hsh.hshservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Time;
import android.util.Log;
import android.view.Window;
import android.widget.GridView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.FxRateAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.model.FxRate;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetFxRateParser;
import com.hsh.hshservice.parser.GetFxRateParser.GetFxRateParserInterface;

public class FxActivity extends BaseActivity implements GetFxRateParserInterface {

	private static final String TAG = "FxActivity";
	private ArrayList<FxRate> items = new ArrayList<FxRate>();
	private FxRateAdapter adapter;
	private MyTextView disclaimer_text;
	
	public enum ApiCallType {
		GET_FX
	}

	ApiCallType apiCallType;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.cms_fxrate);
		
		MainApplication.setCurrentActivity(this);
		
		mDialog = new ProgressDialog(this);
		
		adapter = new FxRateAdapter(this, 0, items);
		
		String titleId = getIntent().getStringExtra("titleId");
		
		MyTextView disclaimer = (MyTextView) findViewById(R.id.disclaimer);

		GridView gridView = (GridView) findViewById(R.id.fxRateGridView);
		gridView.setAdapter(adapter);
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        
		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);
		
		BottomBarFragment bottomBarFragment = new BottomBarFragment();
		
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);
			
		// Commit the transaction
		transaction.commit();
		
		
		getFxRate();
		
		//marco test
	}

	private void getFxRate() {
		
		apiCallType = ApiCallType.GET_FX;
		
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));

		showDialog();
		
		Log.i(TAG, "getFX start");
		
		if (MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"fxrate.json")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
		else{
			try {

				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();

				URL url = null;
				
				url = new URL(
						this.getString(R.string.api_base)+this.getString(R.string.cms_api)+this.getString(R.string.get_all_fx));
								
				Log.e(TAG, "url = " + url);

				/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

				Bundle bundle = new Bundle();

				ApiRequest.request(this, url, "get", bundle);

			} catch (MalformedURLException e) {

				e.printStackTrace();
				onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
			} catch (Exception e) {
				e.printStackTrace();
				onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
			}
		}
	}
	
	@Override
	public void postExecute(String json) {
		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);
		
		GetFxRateParser parser = new GetFxRateParser(json, this);
		parser.startParsing();
		
	}


	@Override
	public void onGetFxParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}

	@Override
	public void onGetFxFinishParsing(ArrayList<FxRate> _fxArray) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
		
		items.clear();
		items.addAll(_fxArray);
		
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onGetFxError() {
		// TODO Auto-generated method stub
		mDialog.dismiss();
		
		//try to get from cache data
	}
}
