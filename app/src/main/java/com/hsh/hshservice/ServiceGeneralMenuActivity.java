package com.hsh.hshservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.adapter.MainMenuItemAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.model.GeneralItem;

public class ServiceGeneralMenuActivity extends BaseActivity {
	
	private String TAG = "ServiceGeneralMenuActivity";
	private GridView menu_gridView;
	
	private MainMenuItemAdapter mAdapter;
	
	private ArrayList<GeneralItem> itemArray;
	
	private String parentId;
	private String titleId;
	
	private RelativeLayout loadingProgressBar;
	
	String[] keyArr = {"nightlife_club","shopping","culinary_adventures","peninsula_arcade","peninsula_academy"
            };
	
	@Override
	protected void onResume() {
		MainApplication.setCurrentActivity(this);

		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.cms_main);
		
		MainApplication.setCurrentActivity(this);
		
		initUI();
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        
		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);
		
		BottomBarFragment bottomBarFragment = new BottomBarFragment();
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);
		
		// Commit the transaction
		transaction.commit();
		
		if(itemArray == null){
			itemArray = new ArrayList<GeneralItem>();
		}
		
		/*String[] nameArr = {"NightLife and Clubs","Shopping","Culinary Adventures","The Peninsula Arcade",
		                    "The Peninsula Academy"};
		
		for(int x = 0; x < 5; x++){
			GeneralItem m = new GeneralItem(nameArr[x]);
			itemArray.add(m);
		}*/
		
		
		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		
		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(parentId)){
				itemArray.add(item);
			}		
		}
		
		if(mAdapter == null){
			mAdapter= new MainMenuItemAdapter(this, itemArray);
			mAdapter.setHasLeftIcon(false);
		}
		
		menu_gridView.setAdapter(mAdapter);

	}
	public void onItemclick(int position){
		
		GeneralItem item = itemArray.get(position);
		
		//for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		map.put("Choice", MainApplication.getEngLabel(itemArray.get(position).getTitleId()));
		
		FlurryAgent.logEvent("ServiceSubMenuChoice", map);
		
		String type = item.getType();
		String title = MainApplication.getEngLabel(item.getTitleId());
		Log.i(TAG, "type = " + type);
		Log.i(TAG, "title = " + title);
				
		
		if (title.compareToIgnoreCase("Press Reader") == 0) {

			Intent intent = new Intent(ServiceGeneralMenuActivity.this,
					NewsActivity.class);

			intent.putExtra("titleId", item.getTitleId());

			startActivity(intent);
		}
		else if (type.compareToIgnoreCase("NY Times") == 0) {

			Intent intent = new Intent(ServiceGeneralMenuActivity.this,
					NewsActivity.class);

			intent.putExtra("titleId", item.getTitleId());
			intent.putExtra("url", "http://www.nytimes.com/");
			intent.putExtra("hasWebControl",true);

			startActivity(intent);
		}
		else if (type.compareToIgnoreCase("Web") == 0) {

			Intent intent = new Intent(ServiceGeneralMenuActivity.this,
					NewsActivity.class);

			intent.putExtra("titleId", item.getTitleId());
			intent.putExtra("url", item.getCommand());
			intent.putExtra("hasWebControl",true);

			startActivityForResult(intent, 0);
		}
		else if (type.compareToIgnoreCase("Information (with sub menu)") == 0) {

			Intent intent = new Intent(ServiceGeneralMenuActivity.this,
					ServiceGeneralMenuActivity.class);

			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		}
		else{
		
			Intent intent = new Intent(ServiceGeneralMenuActivity.this,ServiceGeneralItemActivity.class);
	
			intent.putExtra("parentId", itemArray.get(position).getItemId());
			intent.putExtra("titleId", itemArray.get(position).getTitleId());
			
			startActivityForResult(intent, 0);
		}

		/*else if(position<=6){
			Intent intent = new Intent(ServiceGeneralMenuActivity.this,
					ServiceHouseKeepingActivity.class);
			
			intent.putExtra("parentId", keyArr[position]);
			startActivityForResult(intent, 0);
		}*/
	}
	
	private void initUI(){
		menu_gridView = (GridView) findViewById(R.id.menu_gridView);
		
		loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
		loadingProgressBar.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.cms_service_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}