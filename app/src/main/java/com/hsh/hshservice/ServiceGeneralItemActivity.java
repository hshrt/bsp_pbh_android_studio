package com.hsh.hshservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.adapter.GeneralItemAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.customclass.EnlargeImageView;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetGeneralItemParser;
import com.hsh.hshservice.parser.GetGeneralItemParser.GetGeneralItemParserInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ServiceGeneralItemActivity extends BaseActivity implements
		GetGeneralItemParserInterface {

	private String TAG = "ServiceGeneralItemActivity";
	private ListView leftMenuListView;
	private WebView descriptionText;

	private GeneralItemAdapter mAdapter;

	private Button scaleUpBtn;
	private Button scaleDownBtn;
	private Button arrowBtn;

	private FrameLayout scroller;

	private Boolean scrollerClose = false;

	private int textSize = 25;

	private int animation_duration = 500;

	private EnlargeImageView bgPhoto;

	private String parentId;
	private String titleId;

	private ArrayList<GeneralItem> leftItemArray;

	// a key for what is the current topic of this page and what to get from DB
	private String objectString;

	public enum ApiCallType {
		GET_GENERAL_ITEMS
	}

	ApiCallType apiCallType;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, TAG + "onCreate");
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.cms_two_column_general);

		MainApplication.setCurrentActivity(this);

		mDialog = new ProgressDialog(this);
		
		textSize = (int) getResources().getDimension(R.dimen.default_text_size);

		initUI();

		leftMenuListView.setVerticalScrollBarEnabled(false);

		Handler handler = new Handler();
		final Runnable r = new Runnable() {
			public void run() {
				leftMenuListView.setVerticalScrollBarEnabled(true);
			}
		};
		handler.postDelayed(r, 1000);

		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);

		BottomBarFragment bottomBarFragment = new BottomBarFragment();

		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);

		// Commit the transaction
		transaction.commit();

		descriptionText.setBackgroundColor(0x00000000);
		descriptionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

		// descriptionText.setVisibility(View.INVISIBLE);

		/*
		 * if(objectString.equalsIgnoreCase("hotel_restaurants")){
		 * bgPhoto.setImageResource(R.drawable.restaurant); }
		 */

		if (leftItemArray == null) {
			leftItemArray = new ArrayList<GeneralItem>();
		}

		/*
		 * String[] nameArr =
		 * {"In Room Dinning","Hotel Restaurants","Concierge","Wellness",
		 * "Guest Services","Transportation","Housekeeping","Airport"};
		 * 
		 * for(int x = 0; x < 8; x++){ GeneralItem m = new
		 * GeneralItem(nameArr[x]); leftItemArray.add(m); }
		 */

		// get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance()
				.getAllItemArray();

		// Log.i(TAG, "temp Array length = " + tempArray.size());

		for (GeneralItem item : tempArray) {
			// Log.i(TAG, "temp Arary item = " + item.getItemId());
			if (item.getParentId().equalsIgnoreCase(parentId)) {
				leftItemArray.add(item);
			}
		}

		Log.i(TAG, "length = " + leftItemArray.size());

		if (mAdapter == null) {
			mAdapter = new GeneralItemAdapter(this, leftItemArray);
			leftMenuListView.setAdapter(mAdapter);
		}

		mAdapter.notifyDataSetChanged();

		/*
		 * Handler handler = new Handler();
		 * 
		 * handler.postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { if(mAdapter.getFirstButton() != null){
		 * mAdapter.getFirstButton().performClick(); } }
		 * 
		 * },1000);
		 */

		descriptionText.getSettings().setDefaultFontSize((int) getResources().getDimension(R.dimen.default_text_size));

		changeDescriptionText(0);

		// getGeneralItems();

		descriptionText.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Log.d(TAG, "descriptionText webView Error: " + errorCode + ": "
						+ description + " @ " + failingUrl);
			}
		});

		descriptionText.setWebChromeClient(new WebChromeClient() {
			public void onConsoleMessage(String message, int lineNumber,
					String sourceID) {
				Log.d("MyApplication", message + " -- From line " + lineNumber
						+ " of " + sourceID);
			}
		});

	}

	private void initUI() {
		leftMenuListView = (ListView) findViewById(R.id.list);
		descriptionText = (WebView) findViewById(R.id.descriptionText);
		scaleUpBtn = (Button) findViewById(R.id.scaleUpBtn);
		scaleDownBtn = (Button) findViewById(R.id.scaleDownBtn);

		// scaleUpBtn.setVisibility(View.INVISIBLE);
		// scaleDownBtn.setVisibility(View.INVISIBLE);

		bgPhoto = (EnlargeImageView) findViewById(R.id.bgPhoto);
		// bgPhoto.setVisibility(View.INVISIBLE);

		// Helper.setFonts(descriptionText, getString(R.string.app_font));

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.scaleUpBtn:
					if (textSize < 40)
						textSize += 5;

					// for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room",
							MainApplication.getMAS().getData("data_myroom"));

					FlurryAgent.logEvent("ServiceZoomIn", map);

					break;
				case R.id.scaleDownBtn:
					if (textSize > (int) getResources().getDimension(R.dimen.default_text_size))
						textSize -= 5;

					// for Flurry log
					final Map<String, String> map2 = new HashMap<String, String>();
					map2.put("Room",
							MainApplication.getMAS().getData("data_myroom"));

					FlurryAgent.logEvent("ServiceZoomOut", map2);

					break;
				default:
					break;
				}
				// descriptionText.setTextScaleX(textScale);
				// descriptionText.setTextSize(textSize);
				// descriptionTextwebSettings.setDefaultFontSize
				descriptionText.getSettings().setDefaultFontSize(textSize);
			}
		};
		scaleUpBtn.setOnClickListener(onClickListener);
		scaleDownBtn.setOnClickListener(onClickListener);

		leftMenuListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i(TAG, "wei!");
				// descriptionText.setText(Html.fromHtml(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),null,
				// new MyTagHandler()).toString());
				// descriptionText.setHtmlFromString(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),true);
				// descriptionText.setHtmlFromString(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
				// false);
				// descriptionText.loadData(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
				// "text/html", null);
				// descriptionText.loadDataWithBaseURL("file:///android_asset/style.css",
				// MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
				// "text/html","utf-8", null);

				/*
				 * String pish =
				 * "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/minion.otf\")}body {font-family: MyFont; color:#fff}</style></head><body>"
				 * ; String pas = "</body></html>"; String myHtmlString = pish +
				 * MainApplication
				 * .getLabel(leftItemArray.get(position).getDescriptionId()) +
				 * pas;
				 * 
				 * descriptionText.loadDataWithBaseURL(null,myHtmlString,
				 * "text/html", "UTF-8", null);
				 */
			}

		});

		scroller = (FrameLayout) findViewById(R.id.scroller);
		arrowBtn = (Button) findViewById(R.id.arrowBtn);

		// arrowBtn.setVisibility(View.INVISIBLE);

		arrowBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Display display = getWindowManager().getDefaultDisplay();
				int width = display.getWidth();
				
				//to differentiate the high density display galaxy tab S2 and other tablet, 1 = normal, 2 = high density
				int divider = 1;
				
				if(width == 2048){
					divider = 2;
				}

				Log.i(TAG, "scroll distance =" + Helper.getPixelFromDp((int) getResources().getDimension(R.dimen.scroller_width)));
				
				// for Flurry log
				final Map<String, String> map = new HashMap<String, String>();
				map.put("Room", MainApplication.getMAS().getData("data_myroom"));
				map.put("Close", scrollerClose == true ? 1 + "" : 0 + "");
				FlurryAgent.logEvent("ServiceItemDescription", map);
				
				

				if (!scrollerClose) {
					ObjectAnimator scrollerAnim = ObjectAnimator.ofFloat(
							scroller, "translationX", 0,
							Helper.getPixelFromDp((int) getResources().getDimension(R.dimen.scroller_width))/divider);
					ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(arrowBtn,
							"translationX", 0, Helper.getPixelFromDp((int) getResources().getDimension(R.dimen.scroller_width))/divider);
					scrollerAnim.setDuration(animation_duration);
					scrollerAnim.start();
					arrowAnim.setDuration(animation_duration);
					arrowAnim.addListener(new AnimatorListener() {

						@Override
						public void onAnimationStart(Animator animation) {
						}

						@Override
						public void onAnimationRepeat(Animator animation) {
						}

						@Override
						public void onAnimationEnd(Animator animation) {
							arrowBtn.setBackgroundResource(R.drawable.info_open);
						}

						@Override
						public void onAnimationCancel(Animator animation) {
						}
					});
					arrowAnim.start();
					scrollerClose = true;
				} else {
					ObjectAnimator scollerAnim = ObjectAnimator.ofFloat(
							scroller, "translationX",
							Helper.getPixelFromDp((int) getResources().getDimension(R.dimen.scroller_width)/divider), 0);
					ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(arrowBtn,
							"translationX", Helper.getPixelFromDp((int) getResources().getDimension(R.dimen.scroller_width)/divider), 0);
					scollerAnim.setDuration(animation_duration);
					scollerAnim.start();
					arrowAnim.setDuration(animation_duration);
					arrowAnim.addListener(new AnimatorListener() {

						@Override
						public void onAnimationStart(Animator animation) {
						}

						@Override
						public void onAnimationRepeat(Animator animation) {
						}

						@Override
						public void onAnimationEnd(Animator animation) {
							arrowBtn.setBackgroundResource(R.drawable.info_close);
						}

						@Override
						public void onAnimationCancel(Animator animation) {
						}
					});
					arrowAnim.start();
					scrollerClose = false;
				}
			}
		});
	}

	public void changeDescriptionText(int position) {

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		map.put("Choice", MainApplication.getEngLabel(leftItemArray.get(
				position).getTitleId()));

		FlurryAgent.logEvent("ServiceItemChoice", map);

		// descriptionText.setText(leftItemArray.get(position).getDescriptionId());
		// descriptionText.setText(Html.fromHtml(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),null,
		// new MyTagHandler()).toString());
		// descriptionText.setHtmlFromString(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
		// false);
		// descriptionText.loadData("<font color= \"#FFF\">" +
		// MainApplication.getLabel(leftItemArray.get(position).getDescriptionId())+
		// "</font>", "text/html", null);

		String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/minion.otf\")}body {font-family: MyFont;  color:#fff}</style></head><body>";
		String pas = "</body></html>";

		/*
		 * This section make sure the html align the right hand side for the
		 * language Arabic
		 */
		String currLang = "";
		currLang = MainApplication.getMAS().getData("data_language") == "" ? "E"
				: MainApplication.getMAS().getData("data_language");

		// if PHK roll royce app, get the lang setting locally
		if (MainApplication.getCurrentActivity().getString(R.string.hotel)
				.equalsIgnoreCase("PHK_R")) {
			currLang = DataCacheManager.getInstance().getLang();
		}

		if (currLang.equalsIgnoreCase("")) {
			currLang = "E";
		}

		if (currLang.equalsIgnoreCase("A")) {
			pish += "<span dir=\"rtl\">";
			pas = "</span>" + pas;
		}
		/* *********************** */

		String myHtmlString = pish
				+ MainApplication.getLabel(leftItemArray.get(position)
						.getDescriptionId()) + pas;

		descriptionText.loadDataWithBaseURL(null, myHtmlString, "text/html",
				"UTF-8", null);

		Log.i(TAG, "html text = " + myHtmlString);

		bgPhoto.setImageResource(R.drawable.bbg);

		if (leftItemArray.get(position).getImageName() != null
				&& !leftItemArray.get(position).getImageName()
						.equalsIgnoreCase("")
				&& !leftItemArray.get(position).getImageName()
						.equalsIgnoreCase("null")) {

			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.bbg)
					.showImageForEmptyUri(R.drawable.bbg)
					.showImageOnFail(R.drawable.bbg).cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();

			String imageLink = this.getString(R.string.api_base)
					+ this.getString(R.string.cms_upload)
					+ leftItemArray.get(position).getImageName() + "_m.jpg";
			ImageLoader.getInstance().displayImage(imageLink, bgPhoto, options);
		}

		// descriptionText.loadDataWithBaseURL("file:///android_asset/style.css",
		// MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
		// "text/html","utf-8", null);
		;
	}

	private void getGeneralItems() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getGeneralItems start");
		apiCallType = ApiCallType.GET_GENERAL_ITEMS;

		if (MainApplication.useLocalFile) {
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						objectString + ".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String myjsonstring = sb.toString();

			postExecute(myjsonstring);
		}

		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.get_table) + "?table="
					+ objectString);
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideDialog();
		if (apiCallType == ApiCallType.GET_GENERAL_ITEMS) {
			GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
			parser.startParsing();
		}
	}

	@Override
	public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}

	@Override
	public void onGetGeneralItemFinishParsing(
			ArrayList<GeneralItem> generalItemArray) {
		// TODO Auto-generated method stub
		mDialog.dismiss();

		leftItemArray.clear();
		leftItemArray.addAll(generalItemArray);
		Log.i(TAG, leftItemArray.toString());
		mAdapter.notifyDataSetChanged();

		// descriptionText.setText(leftItemArray.get(0).getDescription());

		if (mAdapter.getFirstButton() != null) {
			// mAdapter.getFirstButton().performClick();
		}

	}

	@Override
	public void onGetGeneralItemError() {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.cms_service_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

}