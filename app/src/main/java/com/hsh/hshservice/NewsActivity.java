package com.hsh.hshservice;

import java.net.URI;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.Helper;

public class NewsActivity extends BaseActivity {

	private static final String TAG = "NewsActivity";
	private WebView webView;
	private ProgressBar webViewProgressIndicator;
	private View viewRootHTML;
	private static int iViewRootHTMLHeightDifferent;
	private String[] whitelistDomain = { "pressreader.com", "pressdisplay.com",
			"opentable.com", "nytimes.com", "peninsula.com", "" };

	private LinearLayout mainContainer;
	private RelativeLayout subContainer;
	
	private String url = "http://www.pressreader.com/";

	private Button backBtn;
	private Button forwardBtn2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.news);

		url = getIntent().getStringExtra("url");
		Log.i(TAG, "Url = " + url);

		if (url == null) {
			url = "http://www.pressreader.com/";
		}


		backBtn = (Button) findViewById(R.id.backBtn);
		forwardBtn2 = (Button) findViewById(R.id.forwardBtn2);

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.backBtn:
					if (webView.canGoBack()) {
						webView.goBack();
					}
					break;
				case R.id.forwardBtn2:
					if (webView.canGoForward()) {
						webView.goForward();
					}
					break;
				}
			}
		};

		backBtn.setOnClickListener(onClickListener);
		forwardBtn2.setOnClickListener(onClickListener);

		LinearLayout webControlContainer = (LinearLayout) findViewById(R.id.webControlContainer);
		webControlContainer.setVisibility(View.GONE);

		if (getIntent().getBooleanExtra("hasWebControl", false)) {
			webControlContainer.setVisibility(View.VISIBLE);
		}
		webControlContainer.bringToFront();
		
		mainContainer = (LinearLayout) findViewById(R.id.mainContainer);
		subContainer = (RelativeLayout) findViewById(R.id.subContainer);
		
		if(url.equalsIgnoreCase("http://www.nytimes.com/")){
			subContainer.removeView(webControlContainer);
			mainContainer.addView(webControlContainer,1);
		}
		

		viewRootHTML = findViewById(R.id.rootContainer);
		//viewRootHTML.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);

		MainApplication.setCurrentActivity(this);

		Helper.showInternetWarningToast();

		String titleId = getIntent().getStringExtra("titleId");

		webViewProgressIndicator = (ProgressBar) findViewById(R.id.webViewProgressIndicator);

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);

		BottomBarFragment bottomBarFragment = new BottomBarFragment();

		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);

		// Commit the transaction
		transaction.commit();

		webViewProgressIndicator.setVisibility(View.GONE);

		webView = (WebView) findViewById(R.id.newsWebView);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.getSettings().setRenderPriority(RenderPriority.HIGH);
		webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Log.d(TAG, "webView Error: " + errorCode + ": " + description
						+ " @ " + failingUrl);
			}

			@Override
			public void onPageFinished(WebView view, String url) {

				super.onPageFinished(view, url);
				webViewProgressIndicator.setVisibility(View.GONE);
				
				/*if (webView.canGoBack()){
					backBtn.setAlpha(1);
					backBtn.setEnabled(true);
				}
				else{
					backBtn.setAlpha(0.5f);
					backBtn.setEnabled(false);
				}
				
				if (webView.canGoForward()){
					forwardBtn2.setAlpha(1);
					forwardBtn2.setEnabled(true);
				}
				else{
					forwardBtn2.setAlpha(0.5f);
					forwardBtn2.setEnabled(false);
				}*/

			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

				super.onPageStarted(view, url, favicon);
				webViewProgressIndicator.setVisibility(View.VISIBLE);
				
				if(NewsActivity.this.url.equalsIgnoreCase("http://www.nytimes.com/")){
					webViewProgressIndicator.setVisibility(View.GONE);
				}
				
				

			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				boolean shouldLoad = true;
				// check white list
				URI uri = URI.create(url);
				if (uri != null) {
					for (String domain : whitelistDomain) {
						if (uri.getHost().endsWith(domain)) {
							shouldLoad = true;
							break;
						}
					}
				}
				if (shouldLoad) {
					view.loadUrl(url);
				}
				Log.d(TAG, "webView soul " + (shouldLoad ? "ok! " : "ng. ")
						+ url);
				return true;
			}
		});

		webView.setWebChromeClient(new WebChromeClient() {
			public void onConsoleMessage(String message, int lineNumber,
					String sourceID) {
				Log.d("MyApplication", message + " -- From line " + lineNumber
						+ " of " + sourceID);
			}
		});

		webView.loadUrl(url);

		webView.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// The code of the hiding goest here, just call
				// hideSoftKeyboard(View v);
				hideSoftKeyboard(v);
				return false;
			}
		});
	}

	public void hideSoftKeyboard(View v) {
		Activity activity = (Activity) v.getContext();
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
				.getWindowToken(), 0);
	}

	@Override
	public void onBackPressed() {
		Log.i(TAG, "News onBackPressed fire");
		super.onBackPressed();
	}

	@Override
	public void onDestroy() {
		Log.i(TAG, "news onDestroy");
		
		webView.clearHistory();
		webView.clearCache(true);
		webView.destroy();
		super.onDestroy();

	}

	/*@Override
	public void onWindowFocusChanged(boolean eFocus) {
		super.onWindowFocusChanged(eFocus);
		if (eFocus == false) {
			fKeyboardClose();

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Instrumentation inst = new Instrumentation();
						inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
					} catch (Exception e) {
					}
				}
			}).start();
		}
	}*/

	private void fKeyboardClose() {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
				.getWindowToken(), 0);
	}

	public OnGlobalLayoutListener onGlobalLayoutListener = new OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			Rect rect = new Rect();
			viewRootHTML.getWindowVisibleDisplayFrame(rect);
			iViewRootHTMLHeightDifferent = viewRootHTML.getRootView()
					.getHeight() - (rect.bottom - rect.top);
			if (iViewRootHTMLHeightDifferent > 50)
				fKeyboardClose();
		}
	};
}
