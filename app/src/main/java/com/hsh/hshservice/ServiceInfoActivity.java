package com.hsh.hshservice;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.adapter.GeneralItemAdapter;
import com.hsh.hshservice.adapter.InfoAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetGeneralItemParser;
import com.hsh.hshservice.parser.GetGeneralItemParser.GetGeneralItemParserInterface;


public class ServiceInfoActivity extends BaseActivity implements GetGeneralItemParserInterface{
	
	private String TAG = "ServiceInfoActivity";
	private ListView infoListView;
	
	private InfoAdapter mAdapter;

	
	private String parentId;
	private String titleId;
	
	private ArrayList<GeneralItem> leftItemArray;
	
	//a key for what is the current topic of this page and what to get from DB
	private String objectString;
	
	public enum ApiCallType {
		GET_GENERAL_ITEMS
	}
	
	ApiCallType apiCallType;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, TAG+"onCreate");
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.cms_info);
		
		MainApplication.setCurrentActivity(this);
		
		mDialog = new ProgressDialog(this);
		
		initUI();
		
		infoListView.setVerticalScrollBarEnabled(false);	
		
		Handler handler = new Handler();
		final Runnable r = new Runnable(){
		    public void run() {
		    	infoListView.setVerticalScrollBarEnabled(true);
		    }
		};
		handler.postDelayed(r, 1000);
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        
		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);
		
		BottomBarFragment bottomBarFragment = new BottomBarFragment();
		
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);
			
		// Commit the transaction
		transaction.commit();

		
		//descriptionText.setVisibility(View.INVISIBLE);

		/*if(objectString.equalsIgnoreCase("hotel_restaurants")){
			bgPhoto.setImageResource(R.drawable.restaurant);
		}*/
		
		if(leftItemArray == null){
			leftItemArray = new ArrayList<GeneralItem>();
		}
		
		/*String[] nameArr = {"In Room Dinning","Hotel Restaurants","Concierge","Wellness",
		                    "Guest Services","Transportation","Housekeeping","Airport"};
		
		for(int x = 0; x < 8; x++){
			GeneralItem m = new GeneralItem(nameArr[x]);
			leftItemArray.add(m);
		}*/
		
		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		
		//Log.i(TAG, "temp Array length = " + tempArray.size());
		
		for(GeneralItem item: tempArray){
			//Log.i(TAG, "temp Arary item = " + item.getItemId());
			if(item.getParentId().equalsIgnoreCase(parentId)){
				leftItemArray.add(item);
			}		
		}
		
		Log.i(TAG, "length = " + leftItemArray.size());
		
		if(mAdapter == null){
			mAdapter= new InfoAdapter(this, leftItemArray);
			infoListView.setAdapter(mAdapter);
		}
		
		mAdapter.notifyDataSetChanged();
		
		/*Handler handler = new Handler();
		
		handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            	if(mAdapter.getFirstButton() != null){
        			mAdapter.getFirstButton().performClick();
        		}
            }

        },1000);*/

		
		//getGeneralItems();
		
	
	}
	
	private void initUI(){
		infoListView = (ListView) findViewById(R.id.infoList);

		

	}
	
	
	private void getGeneralItems() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getGeneralItems start");
		apiCallType = ApiCallType.GET_GENERAL_ITEMS;
		
		
		if(MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						objectString+".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
				
		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.get_table) + "?table="+objectString);
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);  

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideDialog();
		if (apiCallType == ApiCallType.GET_GENERAL_ITEMS) {
			GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
			parser.startParsing();
		} 
	}
	
	@Override
	public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}

	@Override
	public void onGetGeneralItemFinishParsing(
			ArrayList<GeneralItem> generalItemArray) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
		
		leftItemArray.clear();
		leftItemArray.addAll(generalItemArray);
		Log.i(TAG, leftItemArray.toString());
		mAdapter.notifyDataSetChanged();
		
		//descriptionText.setText(leftItemArray.get(0).getDescription());
		
		
	}

	@Override
	public void onGetGeneralItemError() {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.cms_service_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

}