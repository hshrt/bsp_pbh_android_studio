package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetTransportationParser {

	public interface GetTransportationParserInterface {
		public void onGetTransportationParsingError(int failMode, boolean isPostExecute);
		public void onGetTransportationFinishParsing(ArrayList<GeneralItem> restaurantArray);
		public void onGetTransportationError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetTransportationParser";
	private String json;
	private GetTransportationParserInterface getTransportationParserInterface;
	
	private ArrayList<GeneralItem> restaurantArray;

	public GetTransportationParser(String json, GetTransportationParserInterface theInterface) {

		this.getTransportationParserInterface = theInterface;
		this.json = json;
		restaurantArray = new ArrayList<GeneralItem>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetTransportationParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("transportation");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject restaurantObj = jArray.getJSONObject(x);
				GeneralItem generalItem = new GeneralItem("");
				generalItem.setItemId(restaurantObj.getString("transportation"));
				generalItem.setDescriptionId(restaurantObj.getString("description"));
				
				restaurantArray.add(generalItem);
			}

			if (restaurantArray.size() > 0) {
				getTransportationParserInterface.onGetTransportationFinishParsing(restaurantArray);
			} 
			else {
				getTransportationParserInterface.onGetTransportationError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getTransportationParserInterface.onGetTransportationParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
