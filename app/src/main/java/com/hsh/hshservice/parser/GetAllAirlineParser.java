package com.hsh.hshservice.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airline;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllAirportParser.GetAllAirportParserInterface;

public class GetAllAirlineParser {

	public interface GetAllAirlineParserInterface {
		public void onGetAirlineParsingError(int failMode, boolean isPostExecute);
		public void onGetAirlineFinishParsing(ArrayList<Airline> airlineArray);
		public void onGetAirlineError();
	}
	
	private String TAG = "GetAllAirlineParser";
	private String json;
	private GetAllAirlineParserInterface getAllAirlineParserInterface;
	
	private ArrayList<Airline> airlineArray;

	public GetAllAirlineParser(String json, GetAllAirlineParserInterface theInterface) {

		this.getAllAirlineParserInterface = theInterface;
		this.json = json;
		airlineArray = new ArrayList<Airline>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetAllAirlineParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject airlineObj = jArray.getJSONObject(x);
				Airline airline = new Airline();
				airline.setName(airlineObj.getString("name"));
				airline.setIATA(airlineObj.getString("IATA"));

				airlineArray.add(airline);
			}
			if (airlineArray.size() > 0) {
				Log.i(TAG, "good");
				getAllAirlineParserInterface.onGetAirlineFinishParsing(airlineArray);
			} 
			else {
				getAllAirlineParserInterface.onGetAirlineError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getAllAirlineParserInterface.onGetAirlineParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
