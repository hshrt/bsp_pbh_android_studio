package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FoodSubCategory;
import com.hsh.hshservice.network.XMLCaller;

public class GetFoodSubCatParser {

	public interface GetFoodSubCatParserInterface {
		public void onGetFoodSubCatParsingError(int failMode, boolean isPostExecute);
		public void onGetFoodSubCatFinishParsing(ArrayList<FoodSubCategory> foodSubCatArray);
		public void onGetFoodSubCatError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetFoodSubCatParser";
	private String json;
	private String objectName;
	private GetFoodSubCatParserInterface getFoodSubCatParserInterface;
	
	private ArrayList<FoodSubCategory> foodSubCatArray;

	public GetFoodSubCatParser(String json, GetFoodSubCatParserInterface theInterface, String objectName) {

		this.getFoodSubCatParserInterface = theInterface;
		this.json = json;
		this.objectName = objectName;
		foodSubCatArray = new ArrayList<FoodSubCategory>();
	}

	@SuppressWarnings("unused")
	public void startParsing() { 
		
		Log.i("GetFoodSubCatParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray(objectName);
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject obj = jArray.getJSONObject(x);
				FoodSubCategory foodSubCategory = new FoodSubCategory("");
				foodSubCategory.setName(obj.getString("name_en"));
				foodSubCategory.setId(obj.getString("id"));
				foodSubCategory.setCatId(obj.getString("food_cat_id"));
				
				foodSubCatArray.add(foodSubCategory);
			}

			if (foodSubCatArray.size() > 0) {
				Log.i(TAG, "googo");
				getFoodSubCatParserInterface.onGetFoodSubCatFinishParsing(foodSubCatArray);
			} 
			else {
				getFoodSubCatParserInterface.onGetFoodSubCatError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getFoodSubCatParserInterface.onGetFoodSubCatParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
