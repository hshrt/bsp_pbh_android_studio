package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetFlightTrackParser {

	public interface GetFlightTrackParserInterface {
		public void onGetFlightTrackParsingError(int failMode, boolean isPostExecute);
		public void onGetFlightTrackFinishParsing(ArrayList<FlightInfo> flightArrivalArray);
		public void onGetFlightTrackError();
		public void onGetAirportsLatLon(ArrayList<String> latlonArray);
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetFlightTrackParser";
	private String json;
	private GetFlightTrackParserInterface getFlightTrackParserInterface;
	private int type;
	
	private ArrayList<FlightInfo> flightTrackArray;
	
	private ArrayList<String> lonlatArray;

	public GetFlightTrackParser(String json, GetFlightTrackParserInterface theInterface) {

		this.getFlightTrackParserInterface = theInterface;
		this.json = json;
		flightTrackArray = new ArrayList<FlightInfo>();
		lonlatArray = new ArrayList<String>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetFlightTrackParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			
			JSONArray jArray = new JSONArray();
			JSONObject jObject = new JSONObject();
			
			if(restObject.has("flightTracks")){
				 jArray = restObject.getJSONArray("flightTracks");
				 
				//it is possible that there is no result returned
					if(jArray.length() == 0){
						getFlightTrackParserInterface.onGetFlightTrackFinishParsing(flightTrackArray);
						return;
					}
			}
			else if(restObject.has("flightTrack")){
				jObject = restObject.getJSONObject("flightTrack");
				
				jArray.put(jObject);
			}
			
			
			
			JSONArray airlines = restObject.getJSONObject("appendix").getJSONArray("airlines");
			JSONArray airports = restObject.getJSONObject("appendix").getJSONArray("airports");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject itemObj = jArray.getJSONObject(x);
				FlightInfo flightInfo = new FlightInfo("");
				flightInfo.setFlightId(itemObj.getString("flightId"));
				flightInfo.setCarrierCode(itemObj.getString("carrierFsCode"));
				flightInfo.setFlightNumber(itemObj.getString("flightNumber"));
				flightInfo.setDepartureAirportCode(itemObj.getString("departureAirportFsCode"));
				flightInfo.setArrivalAirportCode(itemObj.getString("arrivalAirportFsCode"));
				
				JSONArray positions = itemObj.getJSONArray("positions");
				
				
				//setup the actually route lat/lon for FlightInfo
				int position_length = positions.length();
				
				String lons = "";
				String lats = "";
				
				for(int y = 0; y < position_length ; y++){
					JSONObject position =positions.getJSONObject(y);
					Log.i(TAG,"position = " + position.toString());
					lats += position.getDouble("lat")+",";
					lons += position.getDouble("lon")+",";
				}
				
				if(lats.length() > 0)lats = lats.substring(0, lats.length()-1);
				if(lons.length() > 0) lons = lons.substring(0, lons.length()-1);
				
				flightInfo.setLons(lons);
				flightInfo.setLats(lats);
				
				
				// WayPoint lat/lon (The Original flight plan)
				JSONArray wayPoints = null;
				
				//not every feed returned will have flight plan waypoint
				if(itemObj.has("waypoints")){
					wayPoints = itemObj.getJSONArray("waypoints");	
					
					//setup the actually route lat/lon for FlightInfo
					int waypoints_length = wayPoints.length();
					
					String waypoint_lats = "";
					String waypoint_lons = "";
					
					for(int y = 0; y < waypoints_length ; y++){
						JSONObject waypoint =wayPoints.getJSONObject(y);
						Log.i(TAG,"waypoint = " + waypoint.toString());
						waypoint_lats += waypoint.getDouble("lat")+",";
						waypoint_lons += waypoint.getDouble("lon")+",";
					}
					
					if(waypoint_lats.length() > 0) waypoint_lats = waypoint_lats.substring(0, waypoint_lats.length()-1);
					if(waypoint_lons.length() > 0) waypoint_lons = waypoint_lons.substring(0, waypoint_lons.length()-1);
					
					flightInfo.setPlaned_lons(waypoint_lats);
					flightInfo.setPlaned_lats(waypoint_lons);
				}
				
				
				
				
				//Airport info				
				int airport_length = airports.length();
				
				Log.i(TAG, "airport length = " + airport_length);
				
				lonlatArray.clear();
				
				for(int y = 0; y < airport_length ; y++){
					
					Log.i(TAG, "airport length = " + airport_length);
					JSONObject airport = airports.getJSONObject(y);
					
					String latlon = airport.getString("latitude")+","+airport.getString("longitude");
					lonlatArray.add(latlon);
					
					if(type == ServiceAirportActivity.ARRIVAL){
						if(airport.getString("iata").equalsIgnoreCase(flightInfo.getDepartureAirportCode())){
							flightInfo.setPlace(airport.getString("name"));
							
							break;
						}
					}
					else if(type == ServiceAirportActivity.DEPARTURE){
						if(airport.getString("iata").equalsIgnoreCase(flightInfo.getArrivalAirportCode())){
							flightInfo.setPlace(airport.getString("name"));
							
							break;
						}
					}					
				}
				
				int airline_length = airlines.length();
				for(int z = 0; z < airline_length ; z++){
					JSONObject airline = airlines.getJSONObject(z);
					if(airline.getString("iata").equalsIgnoreCase(flightInfo.getCarrierCode())){
						flightInfo.setAirline(airline.getString("name"));					
						break;
					}
					if(airline.getString("icao").equalsIgnoreCase(flightInfo.getCarrierCode())){
						flightInfo.setAirline(airline.getString("name"));				
						break;
					}					
				}
				
				if(itemObj.getJSONObject("departureDate")!=null)
				flightInfo.setDepartureDate(itemObj.getJSONObject("departureDate").getString("dateLocal").substring(11, 16));
				if(itemObj.has("arrivalDate"))
				flightInfo.setArrivalDate(itemObj.getJSONObject("arrivalDate").getString("dateLocal").substring(11, 16));
				
				if( itemObj.has("status")){
					String statusCode = itemObj.getString("status");
					String status = "";
					switch(statusCode){
						case  "A" : status = "Active";break;
						case  "C" : status = "Canceled";break;
						case  "D" : status = "Diverted";break;
						case  "DN" : status = "Data source needed";break;
						case  "L" : status = "Landed";break;
						case  "NO" : status = "Not Operational";break;
						case  "R" : status = "Redirected";break;
						case  "S" : status = "Scheduled";break;
						case  "U" : status = "Unknown";break;
						
						default: status = ""; break;
					}
					flightInfo.setStatus(status);
				}
				
				flightTrackArray.add(flightInfo);
				
			}

			if (flightTrackArray.size() > 0) {
				Log.i(TAG, "good for flightTrack Array");
				getFlightTrackParserInterface.onGetAirportsLatLon(lonlatArray);
				getFlightTrackParserInterface.onGetFlightTrackFinishParsing(flightTrackArray);
			} 
			else {
				getFlightTrackParserInterface.onGetFlightTrackError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getFlightTrackParserInterface.onGetFlightTrackParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
