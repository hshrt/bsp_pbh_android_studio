package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetFlightStatusParser {

	public interface GetFlightStatusParserInterface {
		public void onGetFlightStatusParsingError(int failMode, boolean isPostExecute);
		public void onGetFlightStatusFinishParsing(ArrayList<FlightInfo> flightArrivalArray);
		public void onGetFlightStatusError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetFlightStatusParser";
	private String json;
	private GetFlightStatusParserInterface getFlightStatusParserInterface;
	private int type;
	
	private ArrayList<FlightInfo> flightArrivalArray;

	public GetFlightStatusParser(String json, GetFlightStatusParserInterface theInterface) {

		this.getFlightStatusParserInterface = theInterface;
		this.json = json;
		flightArrivalArray = new ArrayList<FlightInfo>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetFlightArrivalParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("flightStatuses");
			
			JSONArray airlines = new JSONArray();
			JSONArray airports = new JSONArray();
			
			if(restObject.getJSONObject("appendix").has("airlines")){
				airlines = restObject.getJSONObject("appendix").getJSONArray("airlines");
			}
			if(restObject.getJSONObject("appendix").has("airports")){
				airports = restObject.getJSONObject("appendix").getJSONArray("airports");
			}
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject itemObj = jArray.getJSONObject(x);
				FlightInfo flightInfo = new FlightInfo("");
				flightInfo.setFlightId(itemObj.getString("flightId"));
				flightInfo.setCarrierCode(itemObj.getString("carrierFsCode"));
				flightInfo.setFlightNumber(itemObj.getString("flightNumber"));
				flightInfo.setDepartureAirportCode(itemObj.getString("departureAirportFsCode"));
				flightInfo.setArrivalAirportCode(itemObj.getString("arrivalAirportFsCode"));
				
				int airport_length = airports.length();
				for(int y = 0; y < airport_length ; y++){
					JSONObject airport = airports.getJSONObject(y);
					
					if(type == ServiceAirportActivity.ARRIVAL){
						if(airport.has("iata") && airport.getString("iata").equalsIgnoreCase(flightInfo.getDepartureAirportCode())){
							flightInfo.setPlace(airport.getString("name"));
							
							break;
						}
					}
					else if(type == ServiceAirportActivity.DEPARTURE){
						if(airport.has("iata") && airport.getString("iata").equalsIgnoreCase(flightInfo.getArrivalAirportCode())){
							flightInfo.setPlace(airport.getString("name"));
							
							break;
						}
					}					
				}
				
				int airline_length = airlines.length();
				for(int z = 0; z < airline_length ; z++){
					JSONObject airline = airlines.getJSONObject(z);
					if(airline.has("iata") && airline.getString("iata").equalsIgnoreCase(flightInfo.getCarrierCode())){
						flightInfo.setAirline(airline.getString("name"));					
						break;
					}
					if(airline.has("icao") && airline.getString("icao").equalsIgnoreCase(flightInfo.getCarrierCode())){
						flightInfo.setAirline(airline.getString("name"));				
						break;
					}					
				}
				
				if(itemObj.getJSONObject("departureDate").has("dateLocal")){
					flightInfo.setDepartureDate(itemObj.getJSONObject("departureDate").getString("dateLocal").substring(11, 16));
				}
				if(itemObj.getJSONObject("arrivalDate").has("dateLocal")){
					flightInfo.setArrivalDate(itemObj.getJSONObject("arrivalDate").getString("dateLocal").substring(11, 16));
				}
				
				String statusCode = itemObj.getString("status");
				String status = "";
				switch(statusCode){
					case  "A" : status = "Active";break;
					case  "C" : status = "Canceled";break;
					case  "D" : status = "Diverted";break;
					case  "DN" : status = "Data source needed";break;
					case  "L" : status = "Landed";break;
					case  "NO" : status = "Not Operational";break;
					case  "R" : status = "Redirected";break;
					case  "S" : status = "Scheduled";break;
					case  "U" : status = "Unknown";break;
					
					default: status = ""; break;
				}
				flightInfo.setStatus(status);
				flightArrivalArray.add(flightInfo);
				
			}

			if (flightArrivalArray.size() > 0) {
				Log.i(TAG, "good for flightArrival Array");
				getFlightStatusParserInterface.onGetFlightStatusFinishParsing(flightArrivalArray);
			} 
			else {
				getFlightStatusParserInterface.onGetFlightStatusError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getFlightStatusParserInterface.onGetFlightStatusParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
