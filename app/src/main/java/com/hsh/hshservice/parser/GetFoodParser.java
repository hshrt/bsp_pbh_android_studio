package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Food;
import com.hsh.hshservice.model.FoodSubCategory;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetFoodSubCatParser.GetFoodSubCatParserInterface;

public class GetFoodParser {

	public interface GetFoodParserInterface {
		public void onGetFoodParsingError(int failMode, boolean isPostExecute);
		public void onGetFoodFinishParsing(ArrayList<Food> foodArray);
		public void onGetFoodError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetFoodParser";
	private String json;
	private String objectName;
	private GetFoodParserInterface getFoodParserInterface;
	
	private ArrayList<Food> foodArray;

	public GetFoodParser(String json, GetFoodParserInterface theInterface, String objectName) {

		this.getFoodParserInterface = theInterface;
		this.json = json;
		this.objectName = objectName;
		foodArray = new ArrayList<Food>();
	}

	@SuppressWarnings("unused")
	public void startParsing() { 
		
		Log.i("GetFoodCatParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray(objectName);
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject obj = jArray.getJSONObject(x);
				Food food = new Food("");
				food.setName(obj.getString("name_en"));
				food.setId(obj.getString("id"));
				food.setFood_sub_cat_id(obj.getString("food_sub_cat_id"));
				food.setPrice(obj.getString("price"));
				food.setCode(obj.getString("code"));
				food.setDescription(obj.getString("description"));
				
				foodArray.add(food);
			}

			if (foodArray.size() > 0) {
				Log.i(TAG, "googo");
				getFoodParserInterface.onGetFoodFinishParsing(foodArray);
			} 
			else {
				getFoodParserInterface.onGetFoodError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getFoodParserInterface.onGetFoodParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
