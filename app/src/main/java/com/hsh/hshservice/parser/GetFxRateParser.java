package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FxRate;
import com.hsh.hshservice.model.MessageItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetFxRateParser {
	public interface GetFxRateParserInterface {
		public void onGetFxParsingError(int failMode, boolean isPostExecute);
		public void onGetFxFinishParsing(ArrayList<FxRate> generalItemArray);
		public void onGetFxError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetAllFxParser";
	private String json;
	private GetFxRateParserInterface getfxParserInterface;
	
	private ArrayList<FxRate> fxArray;

	public GetFxRateParser(String json, GetFxRateParserInterface theInterface) {

		this.getfxParserInterface = theInterface;
		this.json = json;
		fxArray = new ArrayList<FxRate>();
	}

	public void startParsing() {
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject dictObj = jArray.getJSONObject(x);
				FxRate item = new FxRate();
				item.setCurrency(dictObj.getString("currency"));
				item.setRate(dictObj.getString("rate"));
				
				fxArray.add(item);
				
				if (fxArray.size() > 0) {
					Log.i(TAG, "good");
					getfxParserInterface.onGetFxFinishParsing(fxArray);
				} 
				else {
					getfxParserInterface.onGetFxError();
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getfxParserInterface.onGetFxParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}
	}
}
