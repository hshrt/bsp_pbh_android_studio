package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.model.Dictionary;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllDictParser.GetAllDictParserInterface;

public class GetAllAirportParser {

	public interface GetAllAirportParserInterface {
		public void onGetAirportParsingError(int failMode, boolean isPostExecute);
		public void onGetAirportFinishParsing(ArrayList<Airport> airportArray);
		public void onGetAirportError();
	}
	
	private String TAG = "GetAllAirportParser";
	private String json;
	private GetAllAirportParserInterface getAllAirportParserInterface;
	
	private ArrayList<Airport> airportArray;

	public GetAllAirportParser(String json, GetAllAirportParserInterface theInterface) {

		this.getAllAirportParserInterface = theInterface;
		this.json = json;
		airportArray = new ArrayList<Airport>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetAllAirportParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject airportObj = jArray.getJSONObject(x);
				Airport airport = new Airport();
				airport.setName(airportObj.getString("name"));
				airport.setIATA(airportObj.getString("IATA"));

				airportArray.add(airport);
			}
			if (airportArray.size() > 0) {
				Log.i(TAG, "good");
				getAllAirportParserInterface.onGetAirportFinishParsing(airportArray);
			} 
			else {
				getAllAirportParserInterface.onGetAirportError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getAllAirportParserInterface.onGetAirportParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
