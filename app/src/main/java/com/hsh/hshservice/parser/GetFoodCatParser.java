package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FoodCategory;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetFoodCatParser {

	public interface GetFoodCatParserInterface {
		public void onGetFoodCatParsingError(int failMode, boolean isPostExecute);
		public void onGetFoodCatFinishParsing(ArrayList<FoodCategory> foodCatArray);
		public void onGetFoodCatError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetFoodCatParser";
	private String json;
	private String objectName;
	private GetFoodCatParserInterface getFoodCatParserInterface;
	
	private ArrayList<FoodCategory> foodCatArray;

	public GetFoodCatParser(String json, GetFoodCatParserInterface theInterface, String objectName) {

		this.getFoodCatParserInterface = theInterface;
		this.json = json;
		this.objectName = objectName;
		foodCatArray = new ArrayList<FoodCategory>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetFoodCatParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray(objectName);
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject restaurantObj = jArray.getJSONObject(x);
				FoodCategory foodCategory = new FoodCategory("");
				foodCategory.setName(restaurantObj.getString("name_en"));
				foodCategory.setId(restaurantObj.getString("id"));
				
				foodCatArray.add(foodCategory);
			}

			if (foodCatArray.size() > 0) {
				Log.i(TAG, "dius");
				getFoodCatParserInterface.onGetFoodCatFinishParsing(foodCatArray);
			} 
			else {
				getFoodCatParserInterface.onGetFoodCatError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getFoodCatParserInterface.onGetFoodCatParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
