package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.MessageItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetMessageParser {
	public interface GetMessageParserInterface {
		public void onGetDictParsingError(int failMode, boolean isPostExecute);
		public void onGetDictFinishParsing(ArrayList<MessageItem> generalItemArray);
		public void onGetDictError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetAllDictParser";
	private String json;
	private GetMessageParserInterface getMessageParserInterface;
	
	private ArrayList<MessageItem> dictArray;

	public GetMessageParser(String json, GetMessageParserInterface theInterface) {

		this.getMessageParserInterface = theInterface;
		this.json = json;
		dictArray = new ArrayList<MessageItem>();
	}

	public void startParsing() {
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject dictObj = jArray.getJSONObject(x);
				MessageItem item = new MessageItem();
				item.setId(dictObj.getString("id"));
				if(dictObj.has("messageId")){
					item.setMessageId(dictObj.getString("messageId"));
				}
		
				item.setSubject(dictObj.getString("subject"));
				item.setDescription(dictObj.getString("description"));
				item.setPriority(dictObj.getString("priority"));
				
				if(dictObj.has("read")){
					item.setRead(dictObj.getString("read"));
				}
				item.setSource(dictObj.getString("source"));
				
				if(dictObj.has("status")){
					item.setStatus(dictObj.getString("status"));
				} 
				item.setStartDate(dictObj.getString("startDate"));
				item.setEndDate(dictObj.getString("endDate"));
				item.setLastUpdate(dictObj.getString("lastUpdate"));
				item.setLastUpdateBy(dictObj.getString("lastUpdateBy"));
				
				dictArray.add(item);
				
				

			}
			
			if (dictArray.size() >= 0) {
				Log.i(TAG, "good");
				getMessageParserInterface.onGetDictFinishParsing(dictArray);
			} 
			else {
				getMessageParserInterface.onGetDictError();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, e.toString());
			e.printStackTrace();
			getMessageParserInterface.onGetDictParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}
	}
}
