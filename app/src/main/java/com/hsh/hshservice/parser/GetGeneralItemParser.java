package com.hsh.hshservice.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.XMLCaller;

public class GetGeneralItemParser {

	public interface GetGeneralItemParserInterface {
		public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute);
		public void onGetGeneralItemFinishParsing(ArrayList<GeneralItem> generalItemArray);
		public void onGetGeneralItemError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetGeneralItemParser";
	private String json;
	private GetGeneralItemParserInterface getGeneralItemParserInterface;
	
	private ArrayList<GeneralItem> generalItemArray;

	public GetGeneralItemParser(String json, GetGeneralItemParserInterface theInterface) {

		this.getGeneralItemParserInterface = theInterface;
		this.json = json;
		generalItemArray = new ArrayList<GeneralItem>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetGeneralItemParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject itemObj = jArray.getJSONObject(x);
				GeneralItem generalItem = new GeneralItem("");
				generalItem.setItemId(itemObj.getString("id"));
				generalItem.setTitleId(itemObj.getString("titleId"));
				generalItem.setDescriptionId(itemObj.getString("descriptionId"));
				generalItem.setParentId(itemObj.getString("parentId"));
				generalItem.setType(itemObj.getString("type"));
				generalItem.setOrder(Integer.parseInt(itemObj.getString("order")));
				generalItem.setLayout(Integer.parseInt(itemObj.getString("layout")));
				if(itemObj.has("command") == true)
				{generalItem.setCommand(itemObj.getString("command"));}
				if(itemObj.has("availTime") == true)
				{generalItem.setAvailTime(itemObj.getString("availTime"));}
				generalItem.setImageName(itemObj.getString("image"));
				generalItem.setPrice(Double.parseDouble(itemObj.getString("price")));
				
				generalItemArray.add(generalItem);
				
				//Log.i(TAG, "the layout = " + Integer.parseInt(itemObj.getString("layout")));
				
			}

			if (generalItemArray.size() > 0) {
				Log.i(TAG, "dius");
				getGeneralItemParserInterface.onGetGeneralItemFinishParsing(generalItemArray);
			} 
			else {
				getGeneralItemParserInterface.onGetGeneralItemError();
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getGeneralItemParserInterface.onGetGeneralItemParsingError(
					XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
