package com.hsh.hshservice;

import java.io.IOException;
import java.util.ArrayList;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.Log;

public class MusicActivity extends BaseActivity {
	private static String TAG = "MusicActivity";
	private String[] songList = {
			"Windswept-synth-guitar-and-soft-strings.mp3",
			"Bird-sounds-at-night.wav",
			"Chris-zabriskie-nirvana-relaxing-piano-music.mp3",
			"Slow-ambient-new-age-song.mp3",
			"Slow-ambient-new-age-song.mp3" };
	
	private int numOfTrack = 3;
	private ArrayList<MediaPlayer> mediaPlayerList = new ArrayList<MediaPlayer>();

	private SeekBar seekBar1, seekBar2, seekBar3, seekBar4, seekBar5;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.music);

		MainApplication.setCurrentActivity(this);
		
		//init the mediaPlayers
		for(int x = 0; x < numOfTrack;x++){
			MediaPlayer mp = new MediaPlayer();
			mediaPlayerList.add(mp);
		}
		
		setupUI();

		setupMusic();
	}

	public void setupUI() {

		seekBar1 = (SeekBar) this.findViewById(R.id.seekBar1);
		seekBar2 = (SeekBar) this.findViewById(R.id.seekBar2);
		seekBar3 = (SeekBar) this.findViewById(R.id.seekBar3);
		seekBar4 = (SeekBar) this.findViewById(R.id.seekBar4);
		seekBar5 = (SeekBar) this.findViewById(R.id.seekBar5);

		OnSeekBarChangeListener listener = new OnSeekBarChangeListener() {

			int progress = 0;
			float progressF = 0f;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progresValue,
					boolean fromUser) {

				progress = progresValue;
				
				progressF = (float)progress/100;
				Log.i(TAG, "progressF = " + progressF);
				/*Toast.makeText(getApplicationContext(),
						"Changing seekbar's progress, value = " + progresValue, Toast.LENGTH_SHORT)
						.show();*/
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

				/*Toast.makeText(getApplicationContext(),
						"Started tracking seekbar", Toast.LENGTH_SHORT).show();*/

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				Log.i(TAG, "onStopTrackingTouch");
				switch (seekBar.getId()){
				case R.id.seekBar1:mediaPlayerList.get(0).setVolume(progressF, progressF);Log.i(TAG, "Diu");break;
				case R.id.seekBar2:mediaPlayerList.get(1).setVolume(progressF, progressF);break;
				case R.id.seekBar3:mediaPlayerList.get(2).setVolume(progressF, progressF);break;
				case R.id.seekBar4:mediaPlayerList.get(3).setVolume(progressF, progressF);break;
				case R.id.seekBar5:mediaPlayerList.get(4).setVolume(progressF, progressF);break;					
				}
				
				// textView.setText("Covered: " + progress + "/" +
				// seekBar.getMax());

				/*Toast.makeText(getApplicationContext(),
						"Stopped tracking seekbar", Toast.LENGTH_SHORT).show();*/

			}

		};

		seekBar1.setOnSeekBarChangeListener(listener);
		seekBar2.setOnSeekBarChangeListener(listener);
		seekBar3.setOnSeekBarChangeListener(listener);
		seekBar4.setOnSeekBarChangeListener(listener);
		seekBar5.setOnSeekBarChangeListener(listener);

	}

	public void setupMusic() {

		for (int x = 0; x < numOfTrack; x++) {
			/*Uri myUri = Uri
					.parse("http://ec2-52-25-223-207.us-west-2.compute.amazonaws.com/test/mp3/"
							+ songList[x]); // initialize Uri here
*/			MediaPlayer mediaPlayer = mediaPlayerList.get(x);
			


			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setWakeMode(getApplicationContext(),
					PowerManager.PARTIAL_WAKE_LOCK);
			mediaPlayer.setLooping(true);

			try {
				AssetFileDescriptor descriptor = getAssets().openFd(songList[x]);
				mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
		        descriptor.close();
				//mediaPlayer.setDataSource(getApplicationContext(), myUri);
				mediaPlayer.prepareAsync();

				mediaPlayer.setOnPreparedListener(new OnPreparedListener() {

					@Override
					public void onPrepared(MediaPlayer mp) {
						// TODO Auto-generated method stub
						Log.i(TAG, "mediaPlayer onPrepared fire");
						mp.start();
					}
				});

				mediaPlayer.setVolume(0.5f, 0.5f);

			} catch (IllegalArgumentException | SecurityException
					| IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
