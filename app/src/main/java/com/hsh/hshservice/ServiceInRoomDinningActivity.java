package com.hsh.hshservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.dinning.adapter.FoodAdapter;
import com.hsh.hshservice.dinning.adapter.FoodCatAdapter;
import com.hsh.hshservice.dinning.adapter.FoodSubCatAdapter;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ServiceInRoomDinningActivity extends BaseActivity {
		
	private String TAG = "ServiceInRoomDinningActivity";	
	private ListView leftMenuListView;	
	private ListView rightMenuListView;
	private LinearLayout top_list_container;
	
	private FoodCatAdapter foodCatAdapter;
	
	private FoodSubCatAdapter foodSubCatAdapter;
	
	private FoodAdapter foodAdapter;
	
	private ArrayList<GeneralItem> allItemArray;
	
	private ArrayList<GeneralItem> foodCatArray;
	
	private ArrayList<GeneralItem> foodSubCatArray;//this is array storing all foodSubCat
	
	private ArrayList<GeneralItem> filter_foodSubCatArray;//this array storing foodSubCat for selected Food Category
	
	private ArrayList<GeneralItem> foodArray;
	private ArrayList<GeneralItem> filter_foodArray;
	private ArrayList<TextView> catMenuArray;
	
	private ImageView topFoodImage;
	private TextView timeAvailText;
	
	private MyTextView tipsText;
	
	private String parentId;
	private String titleId;
	
	//UI for the popup detail View
	private RelativeLayout detailBox; // detail View root container
	private MyTextView foodTitle;
	private MyTextView foodPrice;
	private WebView foodDes;
	
	private Button closeBtn;
	
	private LinearLayout layout_drink_container;
	
	private int layoutType;
	
	private ViewGroup header;
	
	private HorizontalScrollView horizontal_scrollView;
	
	public enum ApiCallType {
		GET_FOOD_CAT, GET_FOOD_SUB_CAT, GET_FOOD
	}
	
	ApiCallType apiCallType;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.cms_two_column_food);
		
		MainApplication.setCurrentActivity(this);
		
		mDialog = new ProgressDialog(this);
		
		initUI();
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        
		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);
		
		BottomBarFragment bottomBarFragment = new BottomBarFragment();
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);
		

		// Commit the transaction
		transaction.commit();
		
		foodDes.setBackgroundColor(0x00000000);
		foodDes.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		foodDes.setHorizontalScrollBarEnabled(true);
		foodDes.getSettings().setDefaultFontSize(20);
		
		foodDes.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		
		// disable scroll on touch
		/*
		foodDes.setOnTouchListener(new View.OnTouchListener() {
	
		    public boolean onTouch(View v, MotionEvent event) {
		      return (event.getAction() == MotionEvent.ACTION_MOVE);
		    }
		});
		*/
		
		//foodDes.setScrollContainer(false);
		
		closeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				detailBox.setVisibility(View.GONE);
			}
			
		});
		
		parentId = getIntent().getStringExtra("parentId");

		if(foodCatArray == null){
			foodCatArray = new ArrayList<GeneralItem>();
		}
		
		if(foodSubCatArray == null){
			foodSubCatArray = new ArrayList<GeneralItem>();
		}
		
		if(foodArray == null){
			foodArray = new ArrayList<GeneralItem>();
		}
		
		if(filter_foodSubCatArray == null){
			filter_foodSubCatArray = new ArrayList<GeneralItem>();
		}
		
		if(filter_foodArray == null){
			filter_foodArray = new ArrayList<GeneralItem>();
		}
		
		//get itemFromCacheArray
		allItemArray = GlobalValue.getInstance().getAllItemArray();
		
		for(GeneralItem item: allItemArray){
			Log.i(TAG, "temp Arary item = " + item.getItemId());
			if(item.getParentId().equalsIgnoreCase(parentId)){
				foodCatArray.add(item);
				Log.i(TAG, "Add ITEM");
			}		
		}
		foodCatAdapter = new FoodCatAdapter(this, foodCatArray);
		
		
		
		//top_list.setAdapter(foodCatAdapter);
		
		foodSubCatAdapter = new FoodSubCatAdapter(this, filter_foodSubCatArray);
		
		leftMenuListView.setAdapter(foodSubCatAdapter);
		
		foodAdapter = new FoodAdapter(this, filter_foodArray);
		rightMenuListView.setAdapter(foodAdapter);
		
		
		rightMenuListView.setVerticalScrollBarEnabled(false);	
		leftMenuListView.setVerticalScrollBarEnabled(false);
		horizontal_scrollView.setHorizontalScrollBarEnabled(false);
		
		Handler handler = new Handler();
		final Runnable r = new Runnable(){
		    public void run() {
		    	rightMenuListView.setVerticalScrollBarEnabled(true);
		    	leftMenuListView.setVerticalScrollBarEnabled(true);
		    	horizontal_scrollView.setHorizontalScrollBarEnabled(true);
		    }
		};
		handler.postDelayed(r, 1000);
		
		setUpTopList();
		//getFoodCat();
	}
	
	private void initUI(){
		leftMenuListView = (ListView)findViewById(R.id.list);
		rightMenuListView = (ListView)findViewById(R.id.right_list3);  
		
		horizontal_scrollView = (HorizontalScrollView)findViewById(R.id.horizontal_scrollView);
		top_list_container = (LinearLayout) findViewById(R.id.top_list_container);
		
		LayoutInflater inflater = getLayoutInflater();
		header = (ViewGroup)inflater.inflate(R.layout.cms_food_list_header, rightMenuListView, false);
		rightMenuListView.addHeaderView(header, null, false);
		
		topFoodImage = (ImageView)findViewById(R.id.topFoodImage);
		timeAvailText = (MyTextView)findViewById(R.id.timeAvailText);
		
		tipsText = (MyTextView)findViewById(R.id.tips);
		
		if(getString(R.string.hotel).equalsIgnoreCase("pch")){
			timeAvailText.setVisibility(View.VISIBLE);
			tipsText.setVisibility(View.VISIBLE);
		}
		else{
			tipsText.setVisibility(View.INVISIBLE);
		}
		
		//topFoodImage = (EnlargeImageView)findViewById(R.id.food_bg);
		//topFoodImage.setVisibility(View.INVISIBLE);
		
		foodTitle = (MyTextView) findViewById(R.id.foodTitle);
		foodPrice = (MyTextView) findViewById(R.id.foodPrice);
		foodDes = (WebView) findViewById(R.id.foodDes);
		
		detailBox = (RelativeLayout) findViewById(R.id.detailBox);
		closeBtn = (Button) findViewById(R.id.closeBtn);
		
		layout_drink_container = (LinearLayout) findViewById(R.id.layout_drink_container);
	}
	
	private void setUpTopList(){
		catMenuArray = new ArrayList<TextView>();
		
		String firstTag = "";
		for(int x = 0; x < foodCatArray.size();x++){
			final TextView tv = new TextView(this);
			tv.setText(MainApplication.getLabel(foodCatArray.get(x).getTitleId()).toUpperCase());
			tv.setTag(foodCatArray.get(x).getItemId());
			
			if(x == 0){
				firstTag = foodCatArray.get(x).getItemId();
			}
			catMenuArray.add(tv);
			
			LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
			layout.setMargins(6, 3, 36, 3);
			layout.gravity = Gravity.CENTER;
			tv.setLayoutParams(layout);
			tv.setTextSize(30);

			Helper.setFonts(tv, getString(R.string.app_font));
			
			tv.setTextColor(getResources().getColor(R.color.white));;
			
			if(x == 0){
				tv.setTextColor(getResources().getColor(R.color.bsp_gold));
			}
			
			tv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickCatButton(tv.getTag().toString());
					
					for(int x = 0; x < catMenuArray.size();x++){
						TextView t = catMenuArray.get(x);
						t.setTextColor(getResources().getColor(R.color.white));
					}
					
					tv.setTextColor(getResources().getColor(R.color.bsp_gold));
					
					
				}
			});
			top_list_container.addView(tv);
			
			this.onClickCatButton(firstTag);
		}
	}
	public void clickOnFood(int position){
		
		detailBox.setVisibility(View.VISIBLE);
		
		GeneralItem food = filter_foodArray.get(position);
		
		//for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		map.put("Choice", MainApplication.getEngLabel(filter_foodArray.get(position).getTitleId()));
		FlurryAgent.logEvent("DinItem", map);
		
		foodTitle.setText(MainApplication.getLabel(food.getTitleId()));
		foodPrice.setText("US $"+Helper.processPrice(food.getPrice()+""));
		
		if(food.getPrice() == 0){
			foodPrice.setText("");
		}
		
		
		String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/minion.otf\")}body {font-family: MyFont; color:#fff}</style></head><body>";
		String pas = "</body></html>";
		String myHtmlString = pish + MainApplication.getLabel(food.getDescriptionId()) + pas;
		
		foodDes.loadDataWithBaseURL(null,myHtmlString, "text/html", "UTF-8", null);
	}
	public void clickOnFoodSubCat(int position){
		String first_sub_cat_id = "";
		try{
			first_sub_cat_id = filter_foodSubCatArray.get(0).getItemId().toString();
		}
		catch (Exception e){
			e.printStackTrace();
			return;
		}
    	//first_sub_cat_id = "1";
    	
		//**this is just temporary solution for hardcode data, will remove
    	if(position%2 == 0){
    		first_sub_cat_id = "1";
    	}
    	else{
    		first_sub_cat_id = "2";
    	}
    	//**
    	//layout_drink_container
    	//int layoutType = filter_foodSubCatArray.get(position).getLayout();
    	
    	Log.i(TAG,  "layout type = " + layoutType);
    	
    	//horizontal food layout
    	if(layoutType == 0){
    		layout_drink_container.setVisibility(View.GONE);
    		
    		topFoodImage = (ImageView)findViewById(R.id.topFoodImage);   		
    		
    		rightMenuListView = (ListView)findViewById(R.id.right_list3); 
    		rightMenuListView.setVisibility(View.VISIBLE);
    		
    		if(rightMenuListView.getHeaderViewsCount()== 0){
    			rightMenuListView.addHeaderView(header, null, false);
    		}
    		
    		
    		
    		foodAdapter.setLayoutType(layoutType);
    		//rightMenuListView.setVerticalScrollBarEnabled(false);
    		
    		rightMenuListView.setAdapter(foodAdapter);
    		
    		//rightMenuListView.setVerticalScrollBarEnabled(true);
    	}
    	else if(layoutType == 1){                                                                                                                                                                                                                                                                                                                                                           
    		layout_drink_container.setVisibility(View.VISIBLE);
    		
    		topFoodImage = (ImageView)findViewById(R.id.food_bg);
    		
    		rightMenuListView.setVisibility(View.GONE);
    		rightMenuListView = (ListView)findViewById(R.id.right_list3_2);
    		rightMenuListView.setVisibility(View.VISIBLE);
    		
    		rightMenuListView.removeHeaderView(header);
    		
    		foodAdapter.setLayoutType(layoutType);
    		
    		rightMenuListView.setAdapter(foodAdapter);
    	}
    	
    	filter_foodArray.clear();
    	
    	for(GeneralItem food : allItemArray){
    		if(food.getParentId().equalsIgnoreCase(filter_foodSubCatArray.get(position).getItemId())){
    			filter_foodArray.add(food);
    		}
    	}
    	
    	foodAdapter.notifyDataSetChanged();
    	
    	//set up image
    	//topFoodImage.setImageResource(R.drawable.bbg); 
    	
    	if(filter_foodSubCatArray.get(position).getImageName() != null && !filter_foodSubCatArray.get(position).getImageName().equalsIgnoreCase("") && !filter_foodSubCatArray.get(position).getImageName().equalsIgnoreCase("null")){
			
			ImageLoader.getInstance().displayImage(this.getString(R.string.api_base)+"cms/upload/"+filter_foodSubCatArray.get(position).getImageName()+"_m.jpg", topFoodImage);
		}
    	
    	if(filter_foodSubCatArray.get(position).getAvailTime()!=null){
    		timeAvailText.setText(filter_foodSubCatArray.get(position).getAvailTime());
    		timeAvailText.setVisibility(View.VISIBLE);
    		
    		if(filter_foodSubCatArray.get(position).getAvailTime().length()==0){
    			timeAvailText.setVisibility(View.GONE);
    		}
    	}
    	else{
    		timeAvailText.setVisibility(View.GONE);
    	}
    	
    	//for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		map.put("Choice", MainApplication.getEngLabel(filter_foodSubCatArray.get(position).getTitleId()));
		FlurryAgent.logEvent("DinSubCategory", map);
	}
    private void onClickCatButton(String tag){
    	Log.i(TAG, tag);
    	
 	
    	filter_foodSubCatArray.clear();
    	
    	//can correponding layout type of current food cat
    	for (GeneralItem item: foodCatArray){
    		if(item.getItemId().equalsIgnoreCase(tag)){
    			layoutType = item.getLayout();  			
    			Log.i(TAG,  "layout type = " + layoutType);
    			
    			//for Flurry log
    			final Map<String, String> map = new HashMap<String, String>();
    			map.put("Room", MainApplication.getMAS().getData("data_myroom"));
    			map.put("Choice", MainApplication.getEngLabel(item.getTitleId()));
    			FlurryAgent.logEvent("DinCategory", map);
    		}
    	}
    	
    	for (GeneralItem foodSubCategory : allItemArray){
    		if(foodSubCategory.getParentId().equalsIgnoreCase(tag)){
    			filter_foodSubCatArray.add(foodSubCategory);
    		}
    	}
    	
    	foodSubCatAdapter.setCurrentPosition(0);
    	foodSubCatAdapter.notifyDataSetChanged();
    	
    	leftMenuListView.setSelectionAfterHeaderView();

    	
    	if(filter_foodSubCatArray.size() == 0){
    		return;
    	}
    	
    	if(foodSubCatAdapter.getFirstButton() != null){
    		//foodSubCatAdapter.getFirstButton().performClick();
    	}
    	else{
    		Handler handler = new Handler();
    		final Runnable r = new Runnable(){
    		    public void run() {
    		    	//foodSubCatAdapter.getFirstButton().performClick();
    		    }
    		};
    		handler.postDelayed(r, 2000);
    	}
    	
    	String first_sub_cat_id = filter_foodSubCatArray.get(0).getItemId().toString();
    	first_sub_cat_id = "1";
    	
    	
    	filter_foodArray.clear();
    	for(GeneralItem food : foodArray){
    		if(food.getParentId().equalsIgnoreCase(filter_foodSubCatArray.get(0).getItemId())){
    			filter_foodArray.add(food);
    		}
    	}
    	
    	foodAdapter.notifyDataSetChanged();
    	
    	clickOnFoodSubCat(0);
    }
	
	private void getFoodCat() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getFoodCat start");
		apiCallType = ApiCallType.GET_FOOD_CAT;
		
		if(MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"food_category"+".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
		
		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.get_table) + "?table="+"food_category");
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	private void getFoodSubCat() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getFoodCat start");
		
		apiCallType = ApiCallType.GET_FOOD_SUB_CAT;
		
		if(MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"food_sub_category"+".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
		
		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.get_table) + "?table="+"food_sub_category");
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	private void getFood() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getFoodCat start");
		
		apiCallType = ApiCallType.GET_FOOD;
		
		if(MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"food"+".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
		
		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.get_table) + "?table="+"food");
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideDialog();
		/*if (apiCallType == ApiCallType.GET_FOOD_CAT) {
			GetFoodCatParser parser = new GetFoodCatParser(json, this, "food_category");
			parser.startParsing();
		}
		else if(apiCallType == ApiCallType.GET_FOOD_SUB_CAT) {
			GetFoodSubCatParser parser = new GetFoodSubCatParser(json, this, "food_sub_category");
			parser.startParsing();
		}
		else if(apiCallType == ApiCallType.GET_FOOD) {
			GetFoodParser parser = new GetFoodParser(json, this, "food");
			parser.startParsing();
		}*/
	}
	
	/*@Override
	public void onGetFoodCatParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodCatFinishParsing(ArrayList<GeneralItem> _foodCatArray) {
		
		//mDialog.dismiss();
		
		foodCatArray.clear();
		
		foodCatArray.addAll(_foodCatArray);
		Log.i(TAG, foodCatArray.toString());
		
		
		catMenuArray = new ArrayList<TextView>();
		
		for(int x = 0; x < foodCatArray.size();x++){
			final TextView tv = new TextView(this);
			tv.setText(foodCatArray.get(x).getName().toUpperCase());
			tv.setTag(foodCatArray.get(x).getItemId());
			catMenuArray.add(tv);
			
			LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
			layout.setMargins(6, 3, 36, 3);
			layout.gravity = Gravity.CENTER;
			tv.setLayoutParams(layout);
			tv.setTextSize(30);

			Helper.setFonts(tv, getString(R.string.app_font));
			
			tv.setTextColor(getResources().getColor(R.color.white));;
			
			if(x == 0){
				tv.setTextColor(getResources().getColor(R.color.bsp_gold));
			}
			
			
			tv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickCatButton(tv.getTag().toString());
					
					for(int x = 0; x < catMenuArray.size();x++){
						TextView t = catMenuArray.get(x);
						t.setTextColor(getResources().getColor(R.color.white));
					}
					
					tv.setTextColor(getResources().getColor(R.color.bsp_gold));
					
					
				}
			});
			top_list_container.addView(tv);
			
			
		}
		
		//foodCatAdapter.notifyDataSetChanged();
		
		getFoodSubCat();
	}

	@Override
	public void onGetFoodCatError() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onGetFoodSubCatParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodSubCatFinishParsing(
			ArrayList<FoodSubCategory> _foodSubCatArray) {
		// TODO Auto-generated method stub
		//mDialog.dismiss();
		
		foodSubCatArray.clear();
		foodSubCatArray.addAll(_foodSubCatArray);
		
		//foodSubCatAdapter.notifyDataSetChanged();
		
		Log.i(TAG, foodCatArray.toString());	
		
		getFood();
	}

	@Override
	public void onGetFoodSubCatError() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onGetFoodParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodFinishParsing(ArrayList<Food> _foodArray) {
		mDialog.dismiss();
		
		foodArray.clear();
		foodArray.addAll(_foodArray);
		Log.i(TAG, foodArray.toString());	
		
		onClickCatButton("1");
	}

	@Override
	public void onGetFoodError() {
		// TODO Auto-generated method stub
		
	}*/
	
}