package com.hsh.hshservice;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.ESDDeviceAdminReceiver;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.LangActivity;
import com.hsh.esdbsp.activity.RadioActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.activity.WeatherActivity;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.MainMenuItemAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.model.Dictionary;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllDictParser;
import com.hsh.hshservice.parser.GetAllDictParser.GetAllDictParserInterface;
import com.hsh.hshservice.parser.GetGeneralItemParser;
import com.hsh.hshservice.parser.GetGeneralItemParser.GetGeneralItemParserInterface;

import android.app.enterprise.knoxcustom.KnoxCustomManager;

public class ServiceMainActivity extends BaseActivity implements
		GetGeneralItemParserInterface, GetAllDictParserInterface {

	String myLOG = "ServiceMainActivity";
	private String TAG = "ServiceMainActivity";
	private GridView menu_gridView;

	private MainMenuItemAdapter mAdapter;

	private ArrayList<GeneralItem> itemArray;

	MyTextView rcHeadClock;

	private MyTextView headRC;
	private MyTextView headService;
	private MyTextView headTV;
	private MyTextView headRadio;
	private MyTextView headLang;
	private MyTextView headWeather;

	private String parentId;
	private String titleId;

	private RelativeLayout loadingProgressBar;

	MyTextView footRoom; // room number
	MyTextView footRoomArea; // zone

	LinearLayout layoutPage;

	// ////////////////////////////////////////////////////

	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5 * 1000;

	// ////////////////////////////////////////////////////

	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30 * 1000;

	// ////////////////////////////////////////////////////

	String myCMD = "";

	// share perferences
	public static final String PREFS_NAME = "MyPrefsFile";
	private SharedPreferences settings;

	public enum ApiCallType {
		GET_ALL_ITEMS, GET_ALL_DICT
	}

	ApiCallType apiCallType;

	String[] keyArr = { "hotel_restaurants", "hotel_restaurants",
			"hotel_restaurants", "wellness", "guest_services", "transportation" };

	static final int ACTIVATION_REQUEST = 47; // identifies our request id
	DevicePolicyManager devicePolicyManager;
	ComponentName myDeviceAdmin;

	private KnoxCustomManager kcm;

	static float myBrightness = 0.01f; // for testing, normal is 0.01 full dark

	// ////////////////////////////////////////////////////////

	@Override
	protected void onResume() {
		Log.i(TAG, "OnResume");
		MainApplication.setCurrentActivity(this);

		addRunnableCall();

		Window mWindow = MainApplication.getCurrentActivity().getWindow();
		WindowManager.LayoutParams lp = mWindow.getAttributes();
		lp.screenBrightness = (float) 1.0f;
		mWindow.setAttributes(lp);

		super.onResume();
	}

	@Override
	protected void onPause() {
		clearReferences();

		removeRunnableCall();

		super.onPause();
	}

	@Override
	protected void onDestroy() {
		clearReferences();

		removeRunnableCall();

		super.onDestroy();
	}

	private void clearReferences() {
		try {
			Activity currActivity = MainApplication.getCurrentActivity();

			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {
		}
	}

	// ////////////////////////////////////////////////////////

	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext()
					.getString(R.string.timeoutvalue)) * 1000;

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);

		} catch (Exception e) {

		}
	}

	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);

		} catch (Exception e) {
		}
	}

	// ////////////////////////////////////////////////////////

	private void handleSleep() {
		try {

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

		} catch (Exception e) {
		}

	}

	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				sleepBSP();
				goMain();

			} catch (Exception e) {
			}

		}
	};

	// ////////////////////////////////////////////////////////

	public void goMain() {
		try {
			// we don't need to have alarm function and go back to main for PHK
			if (this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {
				return;
			}

			Log.i(TAG, "ServiceMaingoMain");
			Intent intent = new Intent(MainApplication.getContext(),
					RoomControlActivity.class);
			MainApplication.getCurrentActivity().startActivity(intent);

		} catch (Exception e) {
		}
	}

	// ////////////////////////////////////////////////////////

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "Service Main fire");

		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_service_main);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.cms_main);

		settings = MainApplication.getContext().getSharedPreferences(
				PREFS_NAME, 0);

		if (this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {
			try {
				// Initialize Device Policy Manager service and our receiver
				// class
				devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
				myDeviceAdmin = new ComponentName(this,
						ESDDeviceAdminReceiver.class);

				// Activate device administration
				if (!devicePolicyManager.isAdminActive(myDeviceAdmin)) {
					Log.v(myLOG,
							"Samsung: enabling application as device admin");
					try {
						Intent intent = new Intent(
								DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
						intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
								myDeviceAdmin);
						intent.putExtra(
								DevicePolicyManager.EXTRA_ADD_EXPLANATION,
								"ESD BSP");
						startActivityForResult(intent, ACTIVATION_REQUEST);

					} catch (Exception e) {
						Log.v(myLOG, "Exception:" + e);
					}
				} else {
					Log.v(myLOG,
							"Samsung: Application already has device admin privileges");
				}

			} catch (Exception e) {
			}
		}

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));

		FlurryAgent.logEvent("Service", map);

		MainApplication.setCurrentActivity(this);

		// loadPageItem();

		// addRunnableCall();

		// loadPageLabel();

		mDialog = new ProgressDialog(this);

		Helper.showInternetWarningToast();

		// initHeadUI();

		initUI();

		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		// bundle.putString("titleId", "");
		bundle.putBoolean("hideBackBtn", true);
		bundle.putString("hightLightChoice", "service");

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);

		BottomBarFragment bottomBarFragment = new BottomBarFragment();

		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);

		// Commit the transaction
		transaction.commit();

		if (itemArray == null) {
			itemArray = new ArrayList<GeneralItem>();
		}

		// itemArray = GlobalValue.getInstance().getAllItemArray();
		if (GlobalValue.getInstance().getAllItemArray() == null) {
			GlobalValue.getInstance().setAllItemArray(
					new ArrayList<GeneralItem>());
		} else {
			ArrayList<GeneralItem> generalItemArray = GlobalValue.getInstance()
					.getAllItemArray();

			for (GeneralItem item : generalItemArray) {
				if (item.getParentId().equalsIgnoreCase("0")) {
					itemArray.add(item);
				}
			}
		}

		if (mAdapter == null) {
			mAdapter = new MainMenuItemAdapter(this, itemArray);
		}

		menu_gridView.setAdapter(mAdapter);

		// a defense approach to make sure No data in service is not allow
		if (itemArray.size() == 0
				&& !this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {

			// we will force an get data from server
			settings.edit().putLong("lastItemUpdateTime", 0).commit();

			DataCacheManager.getInstance().setStartLoop(false);
			DataCacheManager.getInstance().startLoadData();

			Intent intent = new Intent(MainApplication.getContext(),
					RoomControlActivity.class);

			MainApplication.getCurrentActivity().startActivity(intent);
			this.finish();

		}

		if (this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {
			// DataCacheManager.getInstance().setStartLoop(false);

			Log.i(TAG, "hard show loading!");
			showLoading();
			DataCacheManager.getInstance().startLoadData();

			if (itemArray.size() > 0) {
				hideLoading();
			}
		}

		// getAllItems();

	}

	private void loadPageLabel() {
		try {

			// change all label
			headRC.setText(MainApplication.getLabel("RC").toUpperCase());
			headService.setText(MainApplication.getLabel("Services")
					.toUpperCase());
			headTV.setText(MainApplication.getLabel("TV").toUpperCase());
			headRadio.setText(MainApplication.getLabel("Radio").toUpperCase());
			headLang.setText(MainApplication.getLabel("Lang").toUpperCase());
			headWeather.setText(MainApplication.getLabel("Weather")
					.toUpperCase());

			footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room")
					+ "<br>" + MainApplication.getMAS().getData("data_myroom")));
			footRoom.setText("obobobobobobobobobo");

		} catch (Exception e) {
		}
	}

	private void loadPageItem() {

		try {

			layoutPage = (LinearLayout) findViewById(R.id.layoutPage);
			layoutPage.setVisibility(View.INVISIBLE); // hide first

			layoutPage.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						handleSleep();

					} catch (Exception e) {
					}

					return false;
				}
			});

			// ////////////////////////////////////////////////////////

			headRC = (MyTextView) findViewById(R.id.headRoomControl);
			headRC.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						MainApplication.playClickSound(headRC);

						Intent intent = new Intent(getApplicationContext(),
								RoomControlActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);

					} catch (Exception e) {
					}

					return false;
				}

			});

			headTV = (MyTextView) findViewById(R.id.headTV);
			headTV.setVisibility(View.GONE);

			headTV.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						MainApplication.playClickSound(headTV);

					} catch (Exception e) {
					}

					return false;
				}

			});

			headRadio = (MyTextView) findViewById(R.id.headRadio);
			headRadio.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						MainApplication.playClickSound(headRadio);

						Intent intent = new Intent(getApplicationContext(),
								RadioActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);

					} catch (Exception e) {
					}

					return false;
				}

			});

			headLang = (MyTextView) findViewById(R.id.headLang);
			headLang.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						MainApplication.playClickSound(headLang);

						Intent intent = new Intent(
								MainApplication.getContext(),
								LangActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);

					} catch (Exception e) {
					}

					return false;
				}

			});

			// ///////////////

			headWeather = (MyTextView) findViewById(R.id.headWeather);
			headWeather.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						MainApplication.playClickSound(headWeather);

					} catch (Exception e) {
					}

					return false;
				}

			});

			// /////////////////////////////////////////////////////////

			rcHeadClock = (MyTextView) findViewById(R.id.headClock);
			rcHeadClock.setText("");

			try {
				String tmpHeadClock[] = MainApplication.getMAS()
						.getData("data_curtime").split(":");
				int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
				int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
				String myHeadClockSign = "am";

				if (myHeadClockHour == 0) {
					myHeadClockHour = 12;
					myHeadClockSign = "am";
				} else if (myHeadClockHour == 12) {
					myHeadClockHour = myHeadClockHour;
					myHeadClockSign = "pm";
				} else if (myHeadClockHour > 12) {
					myHeadClockHour = myHeadClockHour - 12;
					myHeadClockSign = "pm";
				} else {
					myHeadClockSign = "am";
				}

				rcHeadClock.setText(Html.fromHtml("<font>"
						+ MainApplication.twoDigitMe(myHeadClockHour) + ":"
						+ MainApplication.twoDigitMe(myHeadClockMin)
						+ "</font><small>" + myHeadClockSign + "</small>"));

			} catch (Exception e) {
			}

			footRoom = (MyTextView) findViewById(R.id.footRoomNum);
			footRoom.setText("");

			// /////////////////////////////////////////////////////////

		} catch (Exception e) {
		}

	}

	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {

				guiHandler.removeCallbacks(guiRunnable);

				try {
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources()
							.getDrawable(getResources().getIdentifier(
									"drawable/battery_"
											+ MainApplication
													.getBatteryChecker()
													.getBatteryLevel(),
									"drawable", getPackageName()))));

				} catch (Exception e) {
				}

				try {

					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();

						Log.v(myLOG, "alarm wake");
					}

				} catch (Exception e) {
				}

				try {
					try {
						String tmpHeadClock[] = MainApplication.getMAS()
								.getData("data_curtime").split(":");
						int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
						int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
						String myHeadClockSign = "am";

						if (myHeadClockHour == 0) {
							myHeadClockHour = 12;
							myHeadClockSign = "am";
						} else if (myHeadClockHour == 12) {
							myHeadClockHour = myHeadClockHour;
							myHeadClockSign = "pm";
						} else if (myHeadClockHour > 12) {
							myHeadClockHour = myHeadClockHour - 12;
							myHeadClockSign = "pm";
						} else {
							myHeadClockSign = "am";
						}

						rcHeadClock.setText(Html.fromHtml("<font>"
								+ MainApplication.twoDigitMe(myHeadClockHour)
								+ ":"
								+ MainApplication.twoDigitMe(myHeadClockMin)
								+ "</font><small>" + myHeadClockSign
								+ "</small>"));

					} catch (Exception e) {
					}

				} catch (Exception e) {
				}

				// ////////////////////////////////////////////////////////

				// ////////////////////////////////////////////////////////

				loadPageLabel();

				layoutPage.setVisibility(View.VISIBLE); //

				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);

			} catch (Exception e) {
			}
		}
	};

	public void updateUI() {

		Log.i(TAG, "updateUI fire");
		if (GlobalValue.getInstance().getAllItemArray() == null) {
			GlobalValue.getInstance().setAllItemArray(
					new ArrayList<GeneralItem>());
		} else {
			ArrayList<GeneralItem> generalItemArray = GlobalValue.getInstance()
					.getAllItemArray();

			itemArray.clear();

			for (GeneralItem item : generalItemArray) {
				if (item.getParentId().equalsIgnoreCase("0")) {
					itemArray.add(item);
				}
			}
		}

		if (mAdapter == null) {
			mAdapter = new MainMenuItemAdapter(this, itemArray);
		}

		mAdapter.notifyDataSetChanged();
	}

	public void onItemclick(GeneralItem item) {
		String type = item.getType();
		String title = MainApplication.getEngLabel(item.getTitleId());
		Log.i(TAG, "type = " + type);
		Log.i(TAG, "title = " + title);

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		map.put("Choice", MainApplication.getEngLabel(item.getTitleId()));

		FlurryAgent.logEvent("ServiceMainMenuChoice", map);

		if (type.compareToIgnoreCase("Information (with top sub menu)") == 0
				|| type.compareToIgnoreCase("Information (with two sub menu)") == 0) {
			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceInRoomDinningActivity.class);

			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("Information (with sub menu)") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceGeneralMenuActivity.class);

			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("Guest request") == 0
				|| title.compareToIgnoreCase("HouseKeeping") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceHouseKeepingActivity.class);
			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		} else if (title.compareToIgnoreCase("news") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					NewsActivity.class);
			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());

			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("NY Times") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					NewsActivity.class);

			intent.putExtra("titleId", item.getTitleId());
			intent.putExtra("url", "http://www.nytimes.com/");
			intent.putExtra("hasWebControl", true);

			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("Web") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					NewsActivity.class);

			intent.putExtra("titleId", item.getTitleId());
			intent.putExtra("url", item.getCommand());
			intent.putExtra("hasWebControl", true);

			if (item.getCommand().equalsIgnoreCase("https://hsh.blazeloop.com")) {
				intent.putExtra("hasWebControl", false);
			}

			startActivityForResult(intent, 0);
		} else if (title.compareToIgnoreCase("airport") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceAirportActivity.class);
			startActivityForResult(intent, 0);
		} else if (title.compareToIgnoreCase("FX Currency") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					FxActivity.class);

			intent.putExtra("titleId", item.getTitleId());

			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("Mini Bar") == 0) {

			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceInfoActivity.class);

			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		} else if (type.compareToIgnoreCase("Information") == 0) {
			Intent intent = new Intent(ServiceMainActivity.this,
					ServiceGeneralItemActivity.class);

			intent.putExtra("parentId", item.getItemId());
			intent.putExtra("titleId", item.getTitleId());
			startActivityForResult(intent, 0);
		}

	}

	private void initHeadUI() {

		headRC = (MyTextView) findViewById(R.id.headRoomControl);

		headRC.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				try {
					MainApplication.playClickSound(headRC);
					Intent intent = new Intent(MainApplication.getContext(),
							RoomControlActivity.class);
					MainApplication.getCurrentActivity().startActivity(intent);

				} catch (Exception e) {
				}

				return false;
			}

		});

		headTV = (MyTextView) findViewById(R.id.headTV);
		headTV.setVisibility(View.GONE);

		headTV.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				try {

					MainApplication.playClickSound(headTV);

					// Intent intent = new Intent(getApplicationContext(),
					// com.hshgroup.tv.TVActivity.class);
					// startActivity(intent);

				} catch (Exception e) {
				}

				return false;
			}

		});

		headRadio = (MyTextView) findViewById(R.id.headRadio);
		headRadio.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				try {

					MainApplication.playClickSound(headRadio);

					Intent intent = new Intent(MainApplication.getContext(),
							RadioActivity.class);
					MainApplication.getCurrentActivity().startActivity(intent);

				} catch (Exception e) {
				}

				return false;
			}

		});

		headLang = (MyTextView) findViewById(R.id.headLang);
		headLang.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				try {

					MainApplication.playClickSound(headLang);

					Intent intent = new Intent(MainApplication.getContext(),
							LangActivity.class);
					MainApplication.getCurrentActivity().startActivity(intent);

				} catch (Exception e) {
				}

				return false;
			}

		});

		// ///////////////

		headWeather = (MyTextView) findViewById(R.id.headWeather);
		headWeather.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				try {

					MainApplication.playClickSound(headWeather);

					Intent intent = new Intent(MainApplication.getContext(),
							WeatherActivity.class);
					MainApplication.getCurrentActivity().startActivity(intent);

				} catch (Exception e) {
				}

				return false;
			}

		});

		footRoom = (MyTextView) findViewById(R.id.footRoomNum);
		footRoom.setText("");

		/*
		 * headRC.setText(MainApplication.getLabel("RC"));
		 * headTV.setText(MainApplication.getLabel("TV"));
		 * headRadio.setText(MainApplication.getLabel("Radio"));
		 * headLang.setText(MainApplication.getLabel("Lang"));
		 * headWeather.setText(MainApplication.getLabel("Weather"));
		 */
	}

	private void initUI() {
		menu_gridView = (GridView) findViewById(R.id.menu_gridView);

		loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
		loadingProgressBar.setVisibility(View.GONE);

	}

	private void getAllItems() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getAllItems start");
		apiCallType = ApiCallType.GET_ALL_ITEMS;

		if (MainApplication.useLocalFile) {
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"items.json")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String myjsonstring = sb.toString();

			postExecute(myjsonstring);
		}

		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.cms_api)
					+ this.getString(R.string.get_item_list));
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			bundle.putInt("getAll", 1);
			ApiRequest.request(this, url, "post", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	private void getAllDict() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showDialog();
		Log.i(TAG, "getAllDict start");
		apiCallType = ApiCallType.GET_ALL_DICT;

		if (MainApplication.useLocalFile) {
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"dict.json")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String myjsonstring = sb.toString();

			postExecute(myjsonstring);
		}

		try {
			URL url = null;

			url = new URL(this.getString(R.string.api_base)
					+ this.getString(R.string.cms_api)
					+ this.getString(R.string.get_lang_key));
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			bundle.putInt("getAll", 1);
			ApiRequest.request(this, url, "post", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public void onBackPressed() {
		Log.i(TAG, "ServiceMainActiviy onBackPressed fire");
		super.onBackPressed();
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		if (apiCallType == ApiCallType.GET_ALL_ITEMS) {
			GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
			parser.startParsing();
		}

		else if (apiCallType == ApiCallType.GET_ALL_DICT) {
			GetAllDictParser parser = new GetAllDictParser(json, this);
			parser.startParsing();

			String filename = "dict.json";
			FileOutputStream outputStream;

			try {
				outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
				outputStream.write(json.getBytes());
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			int ch;
			StringBuffer fileContent = new StringBuffer("");
			FileInputStream fis;
			try {
				fis = this.openFileInput("dict.json");
				try {
					while ((ch = fis.read()) != -1)
						fileContent.append((char) ch);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			/*
			 * String data = new String(fileContent);
			 * 
			 * Log.i(TAG, "i am data = " + data);
			 * 
			 * GlobalValue.getInstance().setDict(data);
			 */

		}
	}

	public void showLoading() {
		Log.i(TAG, "showLoading fire");
		loadingProgressBar.setVisibility(View.VISIBLE);
		// loadingProgressBar.bringToFront();
		// btnTouchWake.bringToFront();
	}

	public void hideLoading() {
		Log.i(myLOG, "hideLoading fire");
		loadingProgressBar.setVisibility(View.GONE);

		if (MainApplication.getCurrentActivity().getString(R.string.hotel)
				.equalsIgnoreCase("PHK_R")) {
			((ServiceMainActivity) MainApplication.getCurrentActivity())
					.updateUI();
		}

		// for the case that no internet, we have to show "no interest" after we
		// got the dictionary from local file
		Helper.showInternetWarningToast();
	}

	@Override
	public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
	}

	@Override
	public void onGetGeneralItemFinishParsing(
			ArrayList<GeneralItem> generalItemArray) {
		// TODO Auto-generated method stub
		mDialog.dismiss();
		GlobalValue.getInstance().getAllItemArray().clear();
		GlobalValue.getInstance().getAllItemArray().addAll(generalItemArray);

		itemArray.clear();
		// itemArray.addAll(generalItemArray);

		for (GeneralItem item : generalItemArray) {
			if (item.getParentId().equalsIgnoreCase("0")) {
				itemArray.add(item);
			}
		}

		Log.i(TAG, "the itemArray = " + itemArray.toString());

		/*
		 * if(mAdapter.getFirstButton() != null){
		 * //mAdapter.getFirstButton().performClick(); }
		 */

		getAllDict();

	}

	@Override
	public void onGetGeneralItemError() {

		mDialog.dismiss();

		// try to get from cache data
	}

	@Override
	public void onGetDictParsingError(int failMode, boolean isPostExecute) {

		mDialog.dismiss();

		// try to get from cache data
	}

	@Override
	public void onGetDictFinishParsing(ArrayList<Dictionary> dictArray) {
		mDialog.dismiss();

		GlobalValue.getInstance().getAllDictArray().clear();
		GlobalValue.getInstance().getAllDictArray().addAll(dictArray);

		hideDialog();
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetDictError() {

		mDialog.dismiss();

		// try to get from cache data

	}

	private void sleepBSP() {
		Log.i(TAG, "sleepBSP fire");
		try {

			MainApplication.setScreenSta(0);

			Log.v(myLOG, "************************");
			Log.v(myLOG, "sleeping fire");
			Log.v(myLOG, "************************");

			try {

				devicePolicyManager.lockNow();

				// samsung part

				try {

					kcm = KnoxCustomManager.getInstance();

					kcm.setWakeUpMode(true);

				} catch (Exception e) {
				}

			} catch (Exception e) {
			}

			// try {
			// MainApplication.getWakeLock().release();
			//
			// } catch (Exception e) {}

			/*
			 * try { int_AlarmPop = 0;
			 * 
			 * alarmPopup.setVisibility(View.GONE);
			 * 
			 * showRCLayout();
			 * 
			 * } catch (Exception e) {}
			 * 
			 * 
			 * try { if (btnTouchWake == null) { btnTouchWake =
			 * (Button)findViewById(R.id.btnTouchWake); }
			 * 
			 * btnTouchWake.setVisibility(View.VISIBLE);
			 * btnTouchWake.bringToFront();
			 * 
			 * } catch (Exception e) {}
			 */

			Window mWindow = MainApplication.getCurrentActivity().getWindow();
			WindowManager.LayoutParams lp = mWindow.getAttributes();
			lp.screenBrightness = (float) myBrightness;
			// mWindow.setAttributes(lp);

			// Grace Samsung
			/*
			 * try {
			 * 
			 * kcm = KnoxCustomManager.getInstance();
			 * 
			 * kcm.setOverlayTransparency(1.0f); // add overlay
			 * kcm.setLcdBacklightState(false); // turn off backlight
			 * 
			 * Log.v(myLOG, "lcd:" + kcm.getLcdBacklightState() +"");
			 * 
			 * 
			 * } catch (Exception e) { Log.v(myLOG,"samsung:" + e.toString()); }
			 */

		} catch (Exception e) {
			Log.i(myLOG, "sleeping issue = " + e.toString());
		}
	}

}
