package com.hsh.hshservice.global;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

public class ImageCacheManager extends Object implements
		XMLCaller.InternetCallbacks {

	private String TAG = "ImageCacheManager";
	private ArrayList<String> fileNameList;

	private int loadIndex = 0;
	private int totalNum = 0;

	private int retryTime = 0;

	private int inBetweenImageTime = 250;

	private static int RETRY_LIMIT = 5;

	// share perferences
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;

	private ImageView testIV = new ImageView(MainApplication.getContext());

	public ImageCacheManager() {
		fileNameList = new ArrayList<String>();
	}

	private static ImageCacheManager imageCacheManager = new ImageCacheManager();

	public static synchronized ImageCacheManager getInstance() {
		return imageCacheManager;
	}

	public void getImageFileList() {
		try {
			loadIndex = 0;

			settings = MainApplication.getContext().getSharedPreferences(
					PREFS_NAME, 0);

			URL url = null;

			url = new URL(MainApplication.getContext().getString(
					R.string.api_base)
					+ MainApplication.getCurrentActivity().getString(
							R.string.cms_api)
					+ MainApplication.getCurrentActivity().getString(
							R.string.get_all_photo) + "?getAllName=true");

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		// no need to handle there, as the "universalimageloader" library has
		// its own error handling Method

		Log.i(TAG, "ImageCachaeManager error = " + failMode);

		retryTime++;
		if (retryTime < RETRY_LIMIT) {
			getImageFileList();
		} else {
			hideLoading();
		}
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, json);
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);

			if (restObject.has("data")) {
				JSONArray jArray = restObject.getJSONArray("data");

				totalNum = jArray.length();

				for (int x = 0; x < totalNum; x++) {
					JSONObject fileObj = jArray.getJSONObject(x);
					fileNameList.add(fileObj.getString("fn"));
				}

				Log.i(TAG, fileNameList.size() + "");

				String filename;
				String imageLink = "";

				if (fileNameList.size() > 0) {
					retryTime = 0;
					imageLink = MainApplication.getContext().getString(
							R.string.api_base)
							+ MainApplication.getContext().getString(
									R.string.cms_upload)
							+ fileNameList.get(0)
							+ "_m.jpg";
				} else {
					hideLoading();
					return;
				}
				try {
					loadImage(imageLink, testIV);
				} catch (Exception e) {
					Log.i(TAG, "the load Image error is = " + e.toString());
					hideLoading();
				}
			}

		} catch (JSONException e1) {

			e1.printStackTrace();
			hideLoading();

		}
	}

	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub

	}

	private void loadImage(String imageLink, ImageView imageView) {
		// Load image, decode it to Bitmap and display Bitmap in ImageView (or
		// any other view
		// which implements ImageAware interface)

		Log.i(TAG, "loadImage 's imageLink = " + imageLink);
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.bbg)
				.showImageForEmptyUri(R.drawable.bbg)
				.showImageOnFail(R.drawable.bbg).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		/*
		 * ImageLoader.getInstance().loadImage(imageLink, options, new
		 * SimpleImageLoadingListener() {
		 * 
		 * @Override public void onLoadingComplete(String imageUri, View view,
		 * Bitmap loadedImage) { // Do whatever you want with Bitmap } });
		 */

		String tempString = imageLink;
		try {
			ImageLoader.getInstance().displayImage(tempString, imageView,
					options, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							Log.i(TAG, "failed");
							Log.i(TAG, imageUri);
							Log.i(TAG, failReason.toString());

							retryTime++;
							if (retryTime >= RETRY_LIMIT) {
								Log.i(TAG, "Skip to next Image");
								// skip current image, go to download next image
								loadIndex++;

								if (loadIndex >= totalNum) {

									hideLoading();
									return;
								}

								retryTime = 0;
							}
							try {
								/*
								 * String imageLink =
								 * MainApplication.getContext(
								 * ).getString(R.string
								 * .api_base)+"cms/upload/"+fileNameList
								 * .get(loadIndex)+"_m.jpg";
								 * loadImage(imageLink, testIV);
								 */

								Handler handler = new Handler();

								handler.postDelayed(new Runnable() {
									@Override
									public void run() {
										String imageLink = MainApplication
												.getContext().getString(
														R.string.api_base)
												+ MainApplication
														.getContext()
														.getString(
																R.string.cms_upload)
												+ fileNameList.get(loadIndex)
												+ "_m.jpg";
										loadImage(imageLink, testIV);
									}

								}, inBetweenImageTime);
							} catch (Exception e) {

								retryTime++;
								if (retryTime >= RETRY_LIMIT) {
									// skip current image, go to download next
									// image
									loadIndex++;

									retryTime = 0;

									if (loadIndex >= totalNum) {
										hideLoading();
										return;
									}
								}

								Handler handler = new Handler();

								handler.postDelayed(new Runnable() {
									@Override
									public void run() {
										String imageLink = "wrongLink";
										if (fileNameList.size() > loadIndex) {
											imageLink = MainApplication
													.getContext().getString(
															R.string.api_base)
													+ MainApplication
															.getContext()
															.getString(
																	R.string.cms_upload)
													+ fileNameList
															.get(loadIndex)
													+ "_m.jpg";

										} else {
											Log.i(TAG,
													"use wrong Link in onLoading Failed!!");
										}
										loadImage(imageLink, testIV);
									}

								}, inBetweenImageTime);

							}

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							Log.i(TAG, "Loading complete");
							Log.i(TAG, "Loading index = " + loadIndex);

							// set retryTime to 0, to give next image has enough
							// retry time
							retryTime = 0;

							loadIndex++;

							Log.i(TAG, "loadIndex after ++ = " + loadIndex);
							Log.i(TAG, "totalNum = " + totalNum);

							if (loadIndex < totalNum) {
								try {

									Handler handler = new Handler();

									handler.postDelayed(new Runnable() {
										@Override
										public void run() {
											String imageLink = MainApplication
													.getContext().getString(
															R.string.api_base)
													+ MainApplication
															.getContext()
															.getString(
																	R.string.cms_upload)
													+ fileNameList
															.get(loadIndex)
													+ "_m.jpg";
											loadImage(imageLink, testIV);
										}
									}, inBetweenImageTime);

								} catch (Exception e) {

									retryTime++;
									if (retryTime >= RETRY_LIMIT) {
										// skip current image, go to download
										// next image
										loadIndex++;

										retryTime = 0;

										// handle the last image case
										if (loadIndex >= totalNum) {
											hideLoading();
											return;
										}
									}

									Log.i(TAG, "No Return works");

									Handler handler = new Handler();

									handler.postDelayed(new Runnable() {
										@Override
										public void run() {
											String imageLink = "wrongLink";
											if (fileNameList.size() > loadIndex) {
												imageLink = MainApplication
														.getContext()
														.getString(
																R.string.api_base)
														+ MainApplication
																.getContext()
																.getString(
																		R.string.cms_upload)
														+ fileNameList
																.get(loadIndex)
														+ "_m.jpg";

											} else {
												Log.i(TAG,
														"use wrong Link in onLoadingComplete!");
											}
											loadImage(imageLink, testIV);
										}

									}, inBetweenImageTime);

								}
							}

							else {
								long finishTime = System.currentTimeMillis();
								long downloadTime = finishTime
										- DataCacheManager.getInstance().startTime;
								Log.i(TAG, "download time = " + downloadTime);

								// for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS()
										.getData("data_myroom"));
								map.put("time", downloadTime + "");
								map.put("MAC address", Helper
										.getMacAddress(MainApplication
												.getContext()));

								FlurryAgent.logEvent(
										"ImageAndDataDownloadTime", map);

								settings.edit()
										.putLong("lastMediaUpdateTime",
												System.currentTimeMillis())
										.commit();

								hideLoading();
								
								
							}
						}

						@Override
						public void onLoadingCancelled(String imageUri,
								View view) {

						}
					}, new ImageLoadingProgressListener() {
						@Override
						public void onProgressUpdate(String imageUri,
								View view, int current, int total) {

						}
					});
		} catch (Exception e) {
			e.printStackTrace();
			stop();
			hideLoading();
		}
	}

	public void stop() {
		ImageLoader.getInstance().stop();
	}

	private void hideLoading() {

		if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
			((RoomControlActivity) MainApplication.getCurrentActivity())
					.hideLoading();
		}

		if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
			((ServiceMainActivity) MainApplication.getCurrentActivity())
					.hideLoading();
			
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				((ServiceMainActivity) MainApplication.getCurrentActivity()).updateUI();
			}
		}
	}

}
