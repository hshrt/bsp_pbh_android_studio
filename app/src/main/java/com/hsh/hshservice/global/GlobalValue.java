package com.hsh.hshservice.global;

import java.util.ArrayList;

import com.hsh.hshservice.model.Dictionary;
import com.hsh.hshservice.model.GeneralItem;

public class GlobalValue {

	private String notificationCountLastUpdateTime;
	private String password;

	private ArrayList<GeneralItem> allItemArray;
	private ArrayList<Dictionary> allDictArray;
	
	private String dict;
	
	private GlobalValue() {
		allItemArray = new ArrayList<GeneralItem>();
		allDictArray = new ArrayList<Dictionary>();
	}

	private static GlobalValue mGlobalValueObj = new GlobalValue();

	public static synchronized GlobalValue getInstance() {
		return mGlobalValueObj;
	}

	public String getNotificationCountLastUpdateTime() {
		return notificationCountLastUpdateTime;
	}

	public void setNotificationCountLastUpdateTime(
			String notificationCountLastUpdateTime) {
		this.notificationCountLastUpdateTime = notificationCountLastUpdateTime;
	}

	public String getPw() {
		return password;
	}

	public ArrayList<GeneralItem> getAllItemArray() {
		return allItemArray;
	}

	public void setAllItemArray(ArrayList<GeneralItem> allItemArray) {
		this.allItemArray = allItemArray;
	}

	public String getDict() {
		return dict;
	}

	public void setDict(String dict) {
		this.dict = dict;
	}

	public ArrayList<Dictionary> getAllDictArray() {
		return allDictArray;
	}

	public void setAllDictArray(ArrayList<Dictionary> allDictArray) {
		this.allDictArray = allDictArray;
	}
}
