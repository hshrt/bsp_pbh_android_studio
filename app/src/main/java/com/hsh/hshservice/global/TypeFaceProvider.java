package com.hsh.hshservice.global;

import java.util.Hashtable;

import com.hsh.esdbsp.MainApplication;

import android.content.Context;
import android.graphics.Typeface;

public class TypeFaceProvider {

	public static final String TYPEFACE_FOLDER = "fonts";
	public static final String TYPEFACE_EXTENSION = ".ttf";

	private static Hashtable<String, Typeface> sTypeFaces = new Hashtable<String, Typeface>(
			4);

	public static Typeface getTypeFace(Context context, String fontPath) {
		Typeface tempTypeface = sTypeFaces.get(fontPath);

		if (tempTypeface == null) {

			tempTypeface = Typeface.createFromAsset(context.getAssets(),
					fontPath);
			sTypeFaces.put(fontPath, tempTypeface);
		}
		return tempTypeface;
	}
}