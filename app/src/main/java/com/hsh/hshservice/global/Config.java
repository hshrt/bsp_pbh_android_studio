package com.hsh.hshservice.global;

import android.os.Parcel;
import android.os.Parcelable;

public class Config {
	public static String GOOGLE_LOGIN_SUCCESS = "google_login_success";
	public static String GOOGLE_LOGIN_FAIL = "google_login_fail";
	public static String TWITTER_LOGIN_SUCCESS = "twitter_login_success";
	public static String TWITTER_LOGIN_FAIL = "twitter_login_fail";
	
	public static String FACEBOOK_LOGIN_SUCCESS = "facebook_login_success";
	
public static enum StampType implements Parcelable {
		
		WEATHER, LOCATION, WEATHERLOCATION, NONE;

		public static final Parcelable.Creator<StampType> CREATOR = new Parcelable.Creator<StampType>() {

			public StampType createFromParcel(Parcel in) {
				return StampType.values()[in.readInt()];
			}

			public StampType[] newArray(int size) {
				return new StampType[size];
			}

		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			out.writeInt(ordinal());
		}
	}
	
	public static enum StampCategory implements Parcelable{
		FORECAST, FOOD, HOTSPOT, GREETING, OTHER;
		
		public static final Parcelable.Creator<StampCategory> CREATOR = new Parcelable.Creator<StampCategory>() {

			public StampCategory createFromParcel(Parcel in) {
				return StampCategory.values()[in.readInt()];
			}

			public StampCategory[] newArray(int size) {
				return new StampCategory[size];
			}

		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			out.writeInt(ordinal());
		}
	}
	
	public static enum ShadowDirection implements Parcelable {
		HORIZONTAL, VERTICAL;
		
		public static final Parcelable.Creator<ShadowDirection> CREATOR = new Parcelable.Creator<ShadowDirection>() {

			public ShadowDirection createFromParcel(Parcel in) {
				return ShadowDirection.values()[in.readInt()];
			}

			public ShadowDirection[] newArray(int size) {
				return new ShadowDirection[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			out.writeInt(ordinal());
		}
	}
}


