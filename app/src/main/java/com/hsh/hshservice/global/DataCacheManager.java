package com.hsh.hshservice.global;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.model.Dictionary;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllDictParser;
import com.hsh.hshservice.parser.GetAllDictParser.GetAllDictParserInterface;
import com.hsh.hshservice.parser.GetGeneralItemParser;
import com.hsh.hshservice.parser.GetGeneralItemParser.GetGeneralItemParserInterface;

public class DataCacheManager extends Object implements
		XMLCaller.InternetCallbacks, GetGeneralItemParserInterface,
		GetAllDictParserInterface {

	private String TAG = "DataCacheManager";

	// share perferences
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;

	private Handler dataHandler = new Handler();

	private int dataDelay = 30 * 60 * 1000;

	private AlertDialog alertDialog;

	// private int dataInterval = 15*60*1000;

	private Boolean networkError = false;
	private Boolean useLocal = false;

	private Date dictLastupdateDate = null;
	private Date itemLastupdateDate = null;
	private Date mediaLastupdateDate = null;

	private Boolean startLoop = false;

	private Boolean showingDialog = false; // whether the no internet box is
											// shown
	private String lang = "E";

	public enum ApiCallType {
		GET_ALL_LAST_UPDATE_TIME, GET_ALL_ITEMS, GET_ALL_DICT
	}

	ApiCallType apiCallType;

	private int retryTime = 0;

	public static long startTime;
	private long finishTime;

	private Runnable loadDataRunnable = new Runnable() {

		public void run() {
			try {

				// if showing popup, don't do anything
				if (showingDialog) {
					return;
				}

				dataHandler.removeCallbacks(loadDataRunnable);

				Log.i(TAG,
						"tester first= "
								+ settings.getLong("lastItemUpdateTime2", 0));
				Log.i(TAG,
						"lastItemUpdateTime = "
								+ settings.getLong("lastItemUpdateTime", 0));

				getallLastUpdateTime();

				if (MainApplication.getCurrentActivity() instanceof RoomControlActivity
						|| (MainApplication.getCurrentActivity() instanceof ServiceMainActivity && MainApplication
								.getCurrentActivity().getString(R.string.hotel)
								.equalsIgnoreCase("PHK_R"))) {

					settings.edit().putString("tester", "jamison").commit();
					settings.edit()
							.putLong("lastItemUpdateTime2",
									System.currentTimeMillis()).commit();

					Log.i(TAG,
							"tester = "
									+ settings.getString("tester",
											"default value"));

					startLoop = true;

					dataHandler.removeCallbacks(loadDataRunnable);
					dataHandler.postDelayed(loadDataRunnable, dataDelay);
				}
				/*
				 * else{ try{ Log.i(TAG, "not in RoomControlAc"); //transfer
				 * back to RoomControlActivity first, then it will automatically
				 * load data again startLoop = false;
				 * 
				 * Intent intent = new Intent(MainApplication.getContext(),
				 * RoomControlActivity.class);
				 * //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				 * Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 * MainApplication.getCurrentActivity().startActivity(intent); }
				 * catch(Exception e){ e.printStackTrace();
				 * dataHandler.removeCallbacks(loadDataRunnable);
				 * dataHandler.postDelayed(loadDataRunnable, dataDelay); } }
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public DataCacheManager() {

		// fileNameList = new ArrayList<String>();
	}

	private static DataCacheManager dataCacheManager = new DataCacheManager();

	public static synchronized DataCacheManager getInstance() {
		return dataCacheManager;
	}

	public void startLoadData() {

		try {
			settings = MainApplication.getContext().getSharedPreferences(
					PREFS_NAME, 0);

			// start the data loading looping, the reason to delay half second
			// is because need wait for setup Wifi first

			if (!startLoop) {
				dataHandler.postDelayed(loadDataRunnable, 500);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Handler h = new Handler();

			final Runnable r = new Runnable() {
				public void run() {
					startLoadData();

				}
			};

			h.postDelayed(r, 1000);
		}
	}

	@Override
	public boolean hasInternet() {

		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {

		Log.i(TAG, "failMode =" + failMode);

		/*Toast toast = Toast.makeText(MainApplication.getContext(), "failMode =" + failMode, Toast.LENGTH_SHORT);
		toast.show();*/

		String errorMsg = "";

		if (apiCallType == ApiCallType.GET_ALL_LAST_UPDATE_TIME) {
			retryTime++;
			if (retryTime < 3) {
				getallLastUpdateTime();
				return;
			}
		} else if (apiCallType == ApiCallType.GET_ALL_ITEMS) {
			retryTime++;
			if (retryTime < 3) {
				getAllItems();
				return;
			}
		} else if (apiCallType == ApiCallType.GET_ALL_DICT) {
			retryTime++;
			if (retryTime < 3) {
				getAllDict();
				return;
			}
		}

		if (failMode == 1) {
			errorMsg = "no internet";
			networkError = true;
		} else if (failMode == XMLCaller.FAIL_MODE_PARSE_ERROR) {
			useLocal = false;
			networkError = false;
		}

		// Log.i(TAG, "failMode = " + errorMsg);
		Log.i(TAG, "onerror");

		// if no internet then we get item locally
		retryTime++;
		if (retryTime < 5) {
			getAllItems();
		} else {
			if (alertDialog != null) {
				Log.i(TAG, "Dismiss AlertDialog");
				alertDialog.dismiss();
			}

			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						// Yes button clicked
						retryTime = 0;
						showingDialog = false;
						getAllItems();
						break;

					case DialogInterface.BUTTON_NEGATIVE:
						// No button clicked
						break;
					}
				}
			};

			try {
				if (!showingDialog) {
					showingDialog = true;
					Helper.showAlertBox(
							MainApplication.getCurrentActivity(),
							dialogClickListener,
							MainApplication.getCurrentActivity().getString(
									R.string.error_no_internet), "Try Again");
				}
			} catch (Exception e) {
				Log.i(TAG, e.toString());
			}
		}

	}

	private void getallLastUpdateTime() {
		Log.i(TAG, "getallLastUpdateTime start!!!");
		apiCallType = ApiCallType.GET_ALL_LAST_UPDATE_TIME;

		try {
			URL url = null;

			url = new URL(MainApplication.getContext().getString(
					R.string.api_base)
					+ MainApplication.getContext().getString(R.string.cms_api)
					+ MainApplication.getContext().getString(
							R.string.get_all_last_update_time));
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
		}
	}

	private void getAllItems() {
		// just in case
		try {

			if (showingDialog) {
				return;
			}

			if (MainApplication.getCurrentActivity() instanceof RoomControlActivity
					|| (MainApplication.getCurrentActivity() instanceof ServiceMainActivity && MainApplication
							.getCurrentActivity().getString(R.string.hotel)
							.equalsIgnoreCase("PHK_R"))) {
				Log.i(TAG, "show Loading");

				if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
					((RoomControlActivity) MainApplication.getCurrentActivity())
							.showLoading();
				} else if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
					((ServiceMainActivity) MainApplication.getCurrentActivity())
							.showLoading();
				}
			} else {
				Log.i(TAG, "not show Loading");
			}

			startTime = System.currentTimeMillis();

			Log.i(TAG, "getAllItems start");
			apiCallType = ApiCallType.GET_ALL_ITEMS;

			if (networkError || useLocal) {
				Log.i(TAG, "getAllItems with LocalFile");

				String filename = "item.json";
				String read_data = null;

				try {
					FileInputStream fis = MainApplication.getContext()
							.openFileInput(filename);
					byte[] dataArray = new byte[fis.available()];
					while (fis.read(dataArray) != -1) {
						read_data = new String(dataArray);
					}
					fis.close();
				} catch (FileNotFoundException e) {
					networkError = false;
					useLocal = false;
					getAllItems();
					e.printStackTrace();
				} catch (IOException e) {
					networkError = false;
					useLocal = false;
					getAllItems();
					e.printStackTrace();
				}

				Log.i(TAG, "read_data = " + read_data);

				// String myjsonstring = sb.toString();

				if (read_data != null && read_data.length() > 0) {
					postExecute(read_data);
				} else {
					networkError = false;
					useLocal = false;
					getAllItems();
				}
			} else { // get from server
				try {
					URL url = null;

					url = new URL(MainApplication.getContext().getString(
							R.string.api_base)
							+ MainApplication.getContext().getString(
									R.string.cms_api)
							+ MainApplication.getContext().getString(
									R.string.get_item_list));
					Log.e(TAG, "url = " + url);

					Bundle bundle = new Bundle();
					bundle.putInt("getAll", 1);
					ApiRequest.request(this, url, "post", bundle);

				} catch (MalformedURLException e) {
					e.printStackTrace();
					onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
				} catch (Exception e) {
					e.printStackTrace();
					onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
				}
			}

		} catch (Exception e) {
		}
	}

	private void getAllDict() {

		try {

			Log.i(TAG, "getAllDict start");

			apiCallType = ApiCallType.GET_ALL_DICT;

			if (networkError || useLocal) {

				String filename = "dict.json";
				String read_data = null;

				try {
					FileInputStream fis = MainApplication.getContext()
							.openFileInput(filename);
					byte[] dataArray = new byte[fis.available()];
					while (fis.read(dataArray) != -1) {
						read_data = new String(dataArray);
					}
					fis.close();
				} catch (FileNotFoundException e) {
					networkError = false;
					useLocal = false;
					getAllItems();
					e.printStackTrace();
				} catch (IOException e) {
					networkError = false;
					useLocal = false;
					getAllItems();
					e.printStackTrace();
				}

				Log.i(TAG, "read_data = " + read_data);

				// String myjsonstring = sb.toString();

				if (read_data != null && read_data.length() > 0) {
					postExecute(read_data);
				} else {
					networkError = false;
					useLocal = false;
					getAllItems();
				}
			} else {
				try {
					URL url = null;

					url = new URL(MainApplication.getContext().getString(
							R.string.api_base)
							+ MainApplication.getContext().getString(
									R.string.cms_api)
							+ MainApplication.getContext().getString(
									R.string.get_lang_key));
					Log.e(TAG, "url = " + url);

					Bundle bundle = new Bundle();
					bundle.putInt("getAll", 1);
					ApiRequest.request(this, url, "post", bundle);

				} catch (MalformedURLException e) {
					e.printStackTrace();
					onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
				} catch (Exception e) {
					e.printStackTrace();
					onError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, false);
				}
			}

		} catch (Exception e) {
		}
	}

	public void forceReload() {
		Log.i(TAG, "forceReload fire");
		settings = MainApplication.getContext().getSharedPreferences(
				PREFS_NAME, 0);
		settings.edit().putLong("lastItemUpdateTime", 0).commit();

		DataCacheManager.getInstance().setStartLoop(false);
		DataCacheManager.getInstance().startLoadData();

		Intent intent = new Intent(MainApplication.getContext(),
				RoomControlActivity.class);

		MainApplication.getCurrentActivity().startActivity(intent);
		// MainApplication.getCurrentActivity().finish();
	}

	@Override
	public void onGetDictParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		onError(failMode, isPostExecute);
	}

	@Override
	public void onGetDictFinishParsing(ArrayList<Dictionary> dictArray) {
		// TODO Auto-generated method stub
		GlobalValue.getInstance().getAllDictArray().clear();
		GlobalValue.getInstance().getAllDictArray().addAll(dictArray);

		try {
			networkError = false;
			useLocal = false;

			settings.edit()
					.putLong("lastItemUpdateTime", System.currentTimeMillis())
					.commit();

			Log.i(TAG, "setting = " + settings.getLong("lastItemUpdateTime", 0));

			Log.i(TAG, "tester = " + settings.getLong("lastItemUpdateTime2", 0));
			finishTime = System.currentTimeMillis();

			if (mediaLastupdateDate != null) {
				Log.i(TAG,
						"mediaLastupdateDate = "
								+ mediaLastupdateDate.getTime() + "");
				Log.i(TAG,
						"settings lastMediaUpdateTime = "
								+ settings.getLong("lastMediaUpdateTime", 0)
								+ "");

				Log.i(TAG, "the Item and dict loading time = "
						+ (finishTime - startTime) + "");

				Boolean needUpdate = mediaLastupdateDate.getTime() > settings
						.getLong("lastMediaUpdateTime", 0);
				Log.i(TAG, "we need update Images? : " + needUpdate);
			}

			// if there is image update
			if (mediaLastupdateDate != null
					&& mediaLastupdateDate.getTime() > settings.getLong(
							"lastMediaUpdateTime", 0)) {
				ImageCacheManager.getInstance().getImageFileList();
			}
			// if not turn off loading
			else {
				Log.i(TAG, "say something here");
				if (MainApplication.getCurrentActivity() instanceof RoomControlActivity
						|| (MainApplication.getCurrentActivity() instanceof ServiceMainActivity && MainApplication
								.getCurrentActivity().getString(R.string.hotel)
								.equalsIgnoreCase("PHK_R"))) {
					Log.i(TAG, "hide Loading");

					if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
						((RoomControlActivity) MainApplication
								.getCurrentActivity()).hideLoading();
					}

					if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
						((ServiceMainActivity) MainApplication
								.getCurrentActivity()).hideLoading();

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// set the retryTime back to 0
		retryTime = 0;

	}

	private void runOnUiThread(Runnable runnable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetDictError() {
		// TODO Auto-generated method stub
		onError(1, false);
	}

	@Override
	public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {

		// onError(failMode,isPostExecute);

	}

	@Override
	public void onGetGeneralItemFinishParsing(
			ArrayList<GeneralItem> generalItemArray) {

		try {
			GlobalValue.getInstance().getAllItemArray().clear();
			GlobalValue.getInstance().getAllItemArray()
					.addAll(generalItemArray);

			// set the retryTime back to 0
			retryTime = 0;

		} catch (Exception e) {
		}
	}

	@Override
	public void onGetGeneralItemError() {

		onError(1, false);
	}

	@Override
	public void postExecute(final String json) {

		try {

			// for safe reason
			dataHandler.removeCallbacks(loadDataRunnable);
			dataHandler.postDelayed(loadDataRunnable, dataDelay);
			//

			settings = MainApplication.getContext().getSharedPreferences(
					PREFS_NAME, 0);

			JSONObject timeObject = null;
			Log.i(TAG, "postExecute");
			// Log.i("XMLContent", "api xml =" + json);

			JSONObject jo;
			try {
				jo = new JSONObject(json);

				JSONArray jArray = jo.getJSONArray("data");

				timeObject = (JSONObject) jArray.get(0);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (apiCallType == ApiCallType.GET_ALL_LAST_UPDATE_TIME) {

				SimpleDateFormat formatter;
				dictLastupdateDate = null;
				itemLastupdateDate = null;
				mediaLastupdateDate = null;

				if (timeObject == null) {
					onError(1, false);
					return;
				}

				formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				try {
					dictLastupdateDate = (Date) formatter.parse(timeObject
							.getString("dictLastUpdate"));
					itemLastupdateDate = (Date) formatter.parse(timeObject
							.getString("itemLastUpdate"));
					mediaLastupdateDate = (Date) formatter.parse(timeObject
							.getString("mediaLastUpdate"));

					if (MainApplication.getCurrentActivity()
							.getString(R.string.hotel)
							.equalsIgnoreCase("PHK_R")) {
						Calendar cal = Calendar.getInstance(); // creates
																// calendar
						cal.setTime(dictLastupdateDate); // sets calendar
															// time/date
						cal.add(Calendar.HOUR_OF_DAY, 8); // adds one hour
						dictLastupdateDate = cal.getTime(); // returns new date
															// object, one hour
															// in the future

						cal.setTime(itemLastupdateDate); // sets calendar
															// time/date
						cal.add(Calendar.HOUR_OF_DAY, 8); // adds one hour
						itemLastupdateDate = cal.getTime(); // returns new date
															// object, one hour
															// in the future

						cal.setTime(mediaLastupdateDate); // sets calendar
															// time/date
						cal.add(Calendar.HOUR_OF_DAY, 8); // adds one hour
						mediaLastupdateDate = cal.getTime(); // returns new date
																// object, one
																// hour in the
																// future
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					onError(1, false);
					return;
				}

				if (dictLastupdateDate == null || itemLastupdateDate == null
						|| mediaLastupdateDate == null) {
					onError(1, false);
					return;
				}

				Log.i(TAG,
						"date timestamp dict= " + dictLastupdateDate.getTime()
								+ "");
				Log.i(TAG,
						"date timestamp item = " + itemLastupdateDate.getTime()
								+ "");

				Log.i(TAG, "date timestamp dict date format= "
						+ dictLastupdateDate.toString() + "");
				Log.i(TAG, "date timestamp item date format= "
						+ itemLastupdateDate.toString() + "");
				if (mediaLastupdateDate != null)
					Log.i(TAG,
							"date timestamp media = "
									+ mediaLastupdateDate.getTime() + "");

				long dv = Long.valueOf(settings
						.getLong("lastItemUpdateTime", 0));// its need to be in
															// milisecond
				Date df = new java.util.Date(dv);
				String vv = new SimpleDateFormat("MM dd, yyyy hh:mma")
						.format(df);

				Log.i(TAG, "last update Date Formated = " + vv + "");

				Log.i(TAG,
						"last update Date = "
								+ settings.getLong("lastItemUpdateTime", 0)
								+ "");

				Log.i(TAG, "screen status" + MainApplication.getScreenSta()
						+ " ");
				// if need update
				if (dictLastupdateDate.getTime() > settings.getLong(
						"lastItemUpdateTime", 0)
						|| itemLastupdateDate.getTime() > settings.getLong(
								"lastItemUpdateTime", 0)) {

					if (MainApplication.getCurrentActivity() instanceof RoomControlActivity
							|| (MainApplication.getCurrentActivity()
									.getString(R.string.hotel)
									.equalsIgnoreCase("PHK_R") && MainApplication
									.getCurrentActivity() instanceof ServiceMainActivity)) {

						getAllItems();
					} else {
						try {
							Log.i(TAG, "not in RoomControlAc");
							// transfer back to RoomControlActivity first, then
							// it
							// will automatically load data again
							/*
							 * startLoop = false;
							 * 
							 * Intent intent = new
							 * Intent(MainApplication.getContext(),
							 * RoomControlActivity.class);
							 * 
							 * MainApplication.getCurrentActivity().startActivity
							 * (intent );
							 */
						} catch (Exception e) {
							e.printStackTrace();
							dataHandler.removeCallbacks(loadDataRunnable);
							dataHandler
									.postDelayed(loadDataRunnable, dataDelay);
						}
					}
				}
				// if there is no update from Server
				else {
					if (MainApplication.getCurrentActivity() instanceof RoomControlActivity
							|| (MainApplication.getCurrentActivity()
									.getString(R.string.hotel)
									.equalsIgnoreCase("PHK_R") && MainApplication
									.getCurrentActivity() instanceof ServiceMainActivity)) {

						// if there is no data in memory
						if (GlobalValue.getInstance().getAllItemArray().size() == 0) {

							Log.i(TAG, "use local");
							useLocal = true;
							getAllItems();
						} else {
							Log.i(TAG,
									"there is already item data in memory, no need load");
							useLocal = true;
							getAllItems();
						}
					} else {
						Log.i(TAG,
								"No need update, Current Activity is not in Room Control Activity or not sleep");

						// we still need keep looping
						dataHandler.removeCallbacks(loadDataRunnable);
						dataHandler.postDelayed(loadDataRunnable, dataDelay);
					}
				}

			} else if (apiCallType == ApiCallType.GET_ALL_ITEMS) {
				GetGeneralItemParser parser = new GetGeneralItemParser(json,
						this);
				parser.startParsing();

				Handler h2 = new Handler();

				final Runnable r2 = new Runnable() {
					public void run() {
						// write items inside internal storage
						String filename = "item.json";
						FileOutputStream outputStream;

						try {
							outputStream = MainApplication.getContext()
									.openFileOutput(filename,
											Context.MODE_PRIVATE);
							outputStream.write(json.getBytes());
							outputStream.close();
						} catch (Exception e) {

							Log.i(TAG,
									"error of writing item.file = "
											+ e.toString());
						}

						String read_data = null;

						try {
							FileInputStream fis = MainApplication.getContext()
									.openFileInput(filename);
							byte[] dataArray = new byte[fis.available()];
							while (fis.read(dataArray) != -1) {
								read_data = new String(dataArray);
							}
							fis.close();
						} catch (FileNotFoundException e) {

							e.printStackTrace();
						} catch (IOException e) {

							e.printStackTrace();
						}

						Log.i(TAG, "read_data = " + read_data);

					}
				};

				// h2.postDelayed(r2, 500);
				new Thread(r2).start();

				Handler h = new Handler();

				final Runnable r = new Runnable() {
					public void run() {
						getAllDict();

					}
				};

				h.postDelayed(r, 500);

			}

			else if (apiCallType == ApiCallType.GET_ALL_DICT) {
				GetAllDictParser parser = new GetAllDictParser(json, this);
				parser.startParsing();

				Handler h = new Handler();

				final Runnable r = new Runnable() {
					public void run() {
						String filename = "dict.json";
						FileOutputStream outputStream;

						try {
							outputStream = MainApplication.getContext()
									.openFileOutput(filename,
											Context.MODE_PRIVATE);
							outputStream.write(json.getBytes());
							outputStream.close();
						} catch (Exception e) {

							Log.i(TAG,
									"error of writing dict.file = "
											+ e.toString());
						}

						int ch;
						StringBuffer fileContent = new StringBuffer("");
						FileInputStream fis = null;
						try {
							fis = MainApplication.getContext().openFileInput(
									"dict.json");
							try {
								while ((ch = fis.read()) != -1)
									fileContent.append((char) ch);
							} catch (IOException e) {
								e.printStackTrace();
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}

						try {
							fis.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				};

				// h.postDelayed(r, 500);
				new Thread(r).start();

				/*
				 * String data = new String(fileContent);
				 * 
				 * Log.i(TAG, "i am data = " + data);
				 * 
				 * GlobalValue.getInstance().setDict(data);
				 */

			}

		} catch (Exception e) {
		}

	}

	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub

	}

	public Boolean getStartLoop() {
		return startLoop;
	}

	public void setStartLoop(Boolean startLoop) {
		this.startLoop = startLoop;
	}

	public AlertDialog getAlertDialog() {
		return alertDialog;
	}

	public void setAlertDialog(AlertDialog alertDialog) {
		this.alertDialog = alertDialog;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
