package com.hsh.hshservice.global;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Creates helper object to retreive an image from the network to be placed into
 * the view. This retreival will be done in a thread seperate from the UI thread
 * to prevent a network exception from occurring. This class is designed to help
 * provide a default common method to display an network image without having to
 * worry too much about how to get the image.
 * <p>
 * <b>Notes:</b> Please see <code>TODO</code> item on line 161 if utilizing a
 * default image
 * 
 * @version 0.1.0
 * @author Eric Lee
 */
public class NetworkImage {

	/**
	 * An interface a class may need to implement to provide additional
	 * processing of the retrieved image after the image has been fetched.
	 */
	public interface InternetCallbacks {
		/**
		 * This method triggers the processing of the network image after it has
		 * been retreived. The object passed in may be null, thus checking for a
		 * null pointer is strongly advised.
		 * 
		 * @param image
		 *            the Bitmap image fetched
		 * @param position
		 *            the adapter position that needs to be updated
		 */
		public void postExecute(Bitmap image, int position, Boolean fail,
				int _item_id, int image_id, int cache_id);
	}

	private static String TAG = "NetworkImage";
	private URL mURL;
	private Bitmap mImage;
	private InternetCallbacks mCallbacks;
	private ImageView mImageView;
	private Integer mDefaultImage;
	private int mPosition;
	private Boolean canGetImage = true;
	private int item_id;
	
	private int image_id = -1; //this is an id for recognize which images when there are more than one image in the same item row
	
	private int cache_id = -1;
	
	private ImageFetcher fetcher;

	/**
	 * Retreive and render an image from the network.
	 * <p>
	 * Synonym for
	 * {@link #NetworkImage(String, InternetCallbacks, ImageView, Integer)} with
	 * null {@link ImageView} and default image resource ID.
	 * 
	 * @param url
	 *            the image URL
	 * @param callbacks
	 *            an implementation of the {@link InternetCallbacks} interface
	 *            that will do additional processing, if necessary
	 * @throws MalformedURLException
	 *             if <code>url</code> cannot be parsed into a URL
	 */
	public NetworkImage(String url, InternetCallbacks callbacks)
			throws MalformedURLException {
		this(url, callbacks, null, null);
	}

	/**
	 * Retreive and render an image from the network.
	 * <p>
	 * Synonym for
	 * {@link #NetworkImage(String, InternetCallbacks, ImageView, Integer)} with
	 * null callbacks implementation and default image resource ID.
	 * 
	 * @param url
	 *            the image URL
	 * @param image
	 *            an {@link ImageView} to append the image into once retreived
	 * @throws MalformedURLException
	 *             if <code>url</code> cannot be parsed into a URL
	 */
	public NetworkImage(String url, ImageView image)
			throws MalformedURLException {
		this(url, null, image, null);
	}

	/**
	 * Retreive and render an image from the network.
	 * <p>
	 * Synonym for
	 * {@link #NetworkImage(String, InternetCallbacks, ImageView, Integer)} with
	 * null default image resource ID.
	 * 
	 * @param url
	 *            the image URL
	 * @param callbacks
	 *            an implementation of the {@link InternetCallbacks} interface
	 *            that will do additional processing, if necessary
	 * @param image
	 *            an {@link ImageView} to append the image into once retreived
	 * @throws MalformedURLException
	 *             if <code>url</code> cannot be parsed into a URL
	 */
	public NetworkImage(String url, InternetCallbacks callbacks, ImageView image)
			throws MalformedURLException {
		this(url, callbacks, image, null);
	}

	/**
	 * Retreive and render an image from the network. This will assign the
	 * provided default image resource if any network issues occur (i.e., no
	 * connection available, server connection error).
	 * <p>
	 * With the exception of <b>url</b>, all other parameters are optional and
	 * will be ignored if set to null.
	 * 
	 * @param url
	 *            the image URL
	 * @param callbacks
	 *            an implementation of the {@link InternetCallbacks} interface
	 *            that will do additional processing, if necessary
	 * @param image
	 *            an {@link ImageView} to append the image into once retreived
	 * @param defaultImage
	 *            the default image resource ID if the retrieval failed
	 * @throws MalformedURLException
	 *             if <code>url</code> cannot be parsed into a URL
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public NetworkImage(String url, InternetCallbacks callbacks,
			ImageView image, Integer defaultImage) throws MalformedURLException {
		mURL = new URL(url);
		mImageView = image;
		mCallbacks = callbacks;
		mDefaultImage = defaultImage;
		mImageView.setImageResource(mDefaultImage);
		fetcher = new ImageFetcher();
		
		//fetcher.execute(mURL);
		if (Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB) {
			  //myTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			  fetcher.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mURL);
			 // fetcher.execute(mURL);
		}
		else {
			fetcher.execute(mURL);
		}
	}
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public NetworkImage(String url, InternetCallbacks callbacks,
			ImageView image, Integer defaultImage, int position, int Id)
			throws MalformedURLException {
		mURL = new URL(url);
		mImageView = image;
		mCallbacks = callbacks;
		mDefaultImage = defaultImage;
		
		if(mDefaultImage != null && mImageView != null)
		mImageView.setImageResource(mDefaultImage);
		/*
		 * mImageView.invalidate(); LinearLayout.LayoutParams pl = new
		 * LinearLayout.LayoutParams(mImageView.getMeasuredWidth(),
		 * mImageView.getMeasuredWidth()); //lp.setMargins(left, top, right,
		 * bottom); mImageView.setLayoutParams(pl);
		 */
		mPosition = position;
		item_id = Id;
		fetcher = new ImageFetcher();
		if (Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB) {
			  //myTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			  fetcher.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mURL);
			  //fetcher.execute(mURL);
		}
		else {
			fetcher.execute(mURL);
		}
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public NetworkImage(String url, InternetCallbacks callbacks,
			ImageView image, Integer defaultImage, int position, int Id, int imageId, int cacheId)
			throws MalformedURLException {
		mURL = new URL(url);
		mImageView = image;
		mCallbacks = callbacks;
		mDefaultImage = defaultImage;
		mImageView.setImageResource(mDefaultImage);
		/*
		 * mImageView.invalidate(); LinearLayout.LayoutParams pl = new
		 * LinearLayout.LayoutParams(mImageView.getMeasuredWidth(),
		 * mImageView.getMeasuredWidth()); //lp.setMargins(left, top, right,
		 * bottom); mImageView.setLayoutParams(pl);
		 */
		mPosition = position;
		item_id = Id;
		image_id = imageId;
		cache_id = cacheId;
		
		fetcher = new ImageFetcher();
		if (Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB) {
			  //myTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			  fetcher.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mURL);
			  //fetcher.execute(mURL);
		}
		else {
			fetcher.execute(mURL);
		}
	}

	/**
	 * Retreives the image's network path
	 * 
	 * @return the URL
	 */
	public String getPath() {
		return mURL.toExternalForm();
	}

	/**
	 * Retreives the image bitmap
	 * 
	 * @return the image as a {@link Bitmap} object
	 */
	public Bitmap getBitmapImage() {
		return mImage;
	}

	/**
	 * Creates and returns an {@link ImageView} with the retrieved network image
	 * embedded.
	 * 
	 * @param context
	 *            the context the ImageView belongs to
	 * @return the view embedded
	 */
	public View createImageView(Context context) {
		ImageView img = new ImageView(context);
		img.setImageBitmap(mImage);
		return img;
	}

	public void cancelImageFetcher() {
//		Log.i(TAG, "cancelImageFetcher");
		fetcher.cancel(true);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}

	private class ImageFetcher extends AsyncTask<URL, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(URL... url) {
			Bitmap img = null;
			try {
				for (int len = url.length; len > 0; len--) {
					HttpURLConnection conn = (HttpURLConnection) url[(len - 1)]
							.openConnection();
					conn.setConnectTimeout(15000);
					conn.setReadTimeout(20000);
					int response = conn.getResponseCode();

					if (response == HttpURLConnection.HTTP_OK) {
						InputStream stream = conn.getInputStream();
						img = BitmapFactory.decodeStream(stream);
						
						/*BitmapFactory.Options options = new BitmapFactory.Options();
						options.inJustDecodeBounds = false;
						options.inSampleSize = 2;
						img = BitmapFactory.decodeStream(stream, null, options);*/
						
						
						// img = Helper.drawShadow(img, 2, 0, 0);
					} else {
						throw new Exception("Cannot connect to server.");
					}

//					Log.i("NetworkImage",
//							"ImageFetcher from network for position "
//									+ mPosition);
				}
			} catch (Exception e) {
//				Log.d("NetworkImage", "Error reading data: " + e.getMessage()
//						+ " (" + e.getClass() + ")");
				if (mDefaultImage != null) {
					// TODO: Ensure that this code works as advertised: use the
					// provided default image if a failure occurs
					// img =
					// BitmapFactory.decodeResource(mImageView.getResources(),
					// mDefaultImage);
					canGetImage = false;
				} else {
					return null;
				}
			}

			return img;
		}

		@Override
		protected void onPostExecute(Bitmap img) {
//			Log.i("network image", "NetworkImage postExecute" + mPosition);
			if (img != null) {
				mImage = img;
//				Log.i("network image", "set mImage" + mPosition);
			}
			// mImageView.setImageBitmap(img);
			if (mCallbacks != null) {
//				Log.i("network image", "mCallbacks.postExecute" + mPosition);
				mCallbacks.postExecute(mImage, mPosition, canGetImage, item_id, image_id, cache_id);
			}
		}
	}
}
