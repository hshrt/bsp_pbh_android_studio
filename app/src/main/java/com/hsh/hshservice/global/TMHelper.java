package com.hsh.hshservice.global;

import com.hsh.esdbsp.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;

import com.hsh.hshservice.global.Log;

import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.hsh.esdbsp.MainApplication;


public class TMHelper {

	
	public static String getCountryByCode(int countryCode) { 
		SharedPreferences settings;
		settings = MainApplication.getContext().getSharedPreferences(
				MainApplication.getContext().getResources().getString(R.string.share_peferene_file), 0);
		
		String countryStrings = settings.getString("towns", "");
		
		if(countryCode == 0){
			return "";
		}
		
		if(countryStrings == ""){
			return "";
		}
		else{
			String[] parts = countryStrings.split(":");
			/*for(int x = 0; x < parts.length-1; x++){
				Log.e("Country", "country = " + parts[x]);
			}*/
			
			String idString = settings.getString("ids", "");
			String[] ids = idString.split(":");
			
			int position = -1;
			for(int x = 0; x < ids.length;x++){
				
				if(countryCode == Integer.valueOf(ids[x])){
					position = x;
				}
			}
			//townSpinner.setSelection(position+1, true);
			
			
			/*for (int i = 0; i < parts.length; i++) {

				Log.i("TMHelper", "town = "+i + " " + parts[i]);
			}
			Log.i("TMHelper", " final town = " +  parts[countryCode-1]);*/
			
			return parts[position];
		}
	}
	public static String getCurrentTimeString(){
		
		
		SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date myDate = new Date();
		String timeString = timeStampFormat.format(myDate);
		return timeString;
	}
/*	
	public static String getTimeDifferentString(String time){
		
		SharedPreferences settings;
		settings = MainApplication.getContext().getSharedPreferences(
				MainApplication.getContext().getResources().getString(R.string.share_peferene_file), 0);
		
		
		Log.e("TMHELPER", "getTimeDifferentString fire");
		Log.e("TMHELPER", "time = "+ time);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		
		Date date = null;//the passed in date
		if(time == null){
			return "";
		}
		try {  
		    date = format.parse(time);  
		   	Log.e("TMHELPER", "date = " + date);
		} catch (ParseException e) {  
		    e.printStackTrace();  
		}
		
		Date currentDate = new Date();
		SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date myDate = new Date();
		String timeString = timeStampFormat.format(currentDate);
		
		Log.i("TMHELPER", "timeString = " + timeString);
		
		String photoTimeSTring = timeStampFormat.format(date);
		Log.i("TMHELPER", "photo timeString = " + photoTimeSTring);
		
		//timeInterval in second
		int interval = (int) ((currentDate.getTime() - date.getTime())/1000);
		
		Log.i("TMHelper", "timeInterval = "+ interval);
		
		int sec = -1;
		int min = -1;
		int hour = -1;
		int day = -1;
		int month = -1;
		int year = -1;
		
		int secondsInYear = 365*24*60*60;
		int secondsInMonth = 30*24*60*60;
		int secondsInDay = 24*60*60;
		int secondsInHour = 60*60;
		int secondsInMin = 60;
		
		if(interval <= 0){
			return  MainApplication.getContext().getResources().getString(R.string.Just_Now);
		}

	    if (interval <=60 ) {
	        sec = interval;
	        interval -= sec;
	    }else{
	        sec = interval % 60;
	        interval -= sec;
	    }

	    
	    if (interval > 0) {
	        if (interval/secondsInMin <=60) {
	            min = interval/secondsInMin;
	            interval -= min * secondsInMin;
	        }else{
	            min = (interval/secondsInMin) % 60;
	            interval -= min * secondsInMin;
	        }
	    }

	    
	    if (interval > 0) {
	        if (interval/secondsInHour <= 24) {
	            hour = interval/secondsInHour;
	            interval -= hour * secondsInHour;
	        }else{
	            hour = (interval/secondsInHour) % 24;
	            interval -= hour * secondsInHour;
	        }
	    }
   
	    
	    if (interval > 0) {
	        if (interval/secondsInDay <= 30) {
	            day = interval/secondsInDay;
	            interval -= day * secondsInDay;
	        }else{
	            day = (interval/secondsInDay) % 30;
	            interval -= day * secondsInDay;
	        }
	    }
 
	    
	    if (interval > 0) {
	        if (interval/secondsInMonth <= 12) {
	            month = interval/secondsInMonth;
	            interval -= month * secondsInMonth;
	        }else{
	            month = (interval/secondsInMonth) % 12;
	            interval -= month * secondsInMonth;
	        }
	    }

	    if (interval>0) {
	        year = interval/secondsInYear;
	    }

	    
	    String result = "";
	    int displayValue = 0;
	    
	    if(year>0){
	    	Calendar c = Calendar.getInstance();
	    	c.setTime(date);
	    	
	    	Log.i("time debug", "month = " + c.get(Calendar.MONTH));
	    	String monthText = convertMonthToString(c.get(Calendar.MONTH));
	    	String dayText = c.get(Calendar.DAY_OF_MONTH)+"";
	    	String yearText = c.get(Calendar.YEAR)+"";
	    	
	    	result = monthText+dayText+ " " +yearText;
	    }
	    else{
	    	if(month>0){
	    		result = month+" "+MainApplication.getContext().getResources().getString(R.string.month);
	    		displayValue = month;
	    	}
	    	else if(day>0){
	    		result = day+" "+MainApplication.getContext().getResources().getString(R.string.day);
	    		displayValue = day;
	    	}
	    	else if(hour>0){
	    		result = hour+" "+MainApplication.getContext().getResources().getString(R.string.hour);
	    		displayValue = hour;
	    	}
	    	else if(min>0){
	    		result = min+" "+MainApplication.getContext().getResources().getString(R.string.minute);
	    		displayValue = min;
	    	}
	    	else if(sec>0){
	    		result = sec+" "+MainApplication.getContext().getResources().getString(R.string.second);
	    		displayValue = sec;
	    	}
	    	
//	    	String currentLang = settings.getString("appLanguage", "lang_error");
//	    	if(currentLang.equalsIgnoreCase("lang_error")){
//				Log.i("yiu", "lang_error");
//			}

//			if(lang.equalsIgnoreCase("en")){
//				if(displayValue > 1){
//		    		result = result + "s ";
//		    	}
//		    	else{
//		    		result = result + " ";
//		    	}
//			}
	    	
	    	Log.i("yiu", "Locale.getDefault().getDisplayLanguage() = " + Locale.getDefault().getDisplayLanguage());
	    	
			if(Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("english")){
				if(displayValue > 1){
		    		result = result + "s ";
		    	}
	    	else{
	    		result = result + " ";
	    	}
		}
	    	
	    	result = result+MainApplication.getContext().getResources().getString(R.string.ago);
	    }
		
	    Log.i("time debug", "time result" + result);
		return result;
	}
	
	public static String convertMonthToString(int month){
	    String result = "";
	    switch (month) {
	        case 0:
	            result = MainApplication.getContext().getResources().getString(R.string.JAN);
	            break;
	        case 1:
	        	result = MainApplication.getContext().getResources().getString(R.string.FEB);
	            break;
	        case 2:
	        	result = MainApplication.getContext().getResources().getString(R.string.MAR);
	            break;
	        case 3:
	        	result = MainApplication.getContext().getResources().getString(R.string.APR);
	            break;
	        case 4:
	        	result = MainApplication.getContext().getResources().getString(R.string.MAY);
	            break;
	        case 5:
	        	result = MainApplication.getContext().getResources().getString(R.string.JUN);
	            break;
	        case 6:
	        	result = MainApplication.getContext().getResources().getString(R.string.JUL);
	            break;
	        case 7:
	        	result = MainApplication.getContext().getResources().getString(R.string.AUG);
	            break;
	        case 8:
	        	result = MainApplication.getContext().getResources().getString(R.string.SEP);
	            break;
	        case 9:
	        	result = MainApplication.getContext().getResources().getString(R.string.OCT);
	            break;
	        case 10:
	        	result = MainApplication.getContext().getResources().getString(R.string.NOV);
	            break;
	        case 11:
	        	result = MainApplication.getContext().getResources().getString(R.string.DEC);
	            break;
	        default:
	            break;
	    }
	    
	    return result;
	}
	
	public static  String getProfilePicUrlBase(){
		SharedPreferences settings;
		settings = MainApplication.getContext().getSharedPreferences(
				MainApplication.getContext().getResources().getString(R.string.share_peferene_file), 0);
		return MainApplication.getContext().getString(R.string.api_base) + settings.getString("userprofilefolder","");
	}
	
	public static  String getCoverPicUrlBase(){
		SharedPreferences settings;
		settings = MainApplication.getContext().getSharedPreferences(
				MainApplication.getContext().getResources().getString(R.string.share_peferene_file), 0);
		return MainApplication.getContext().getString(R.string.api_base) + settings.getString("usercoverfolder","");
	}
	
	public static String getNotificationSentenceWithType(int type){
		String result = "";
	    switch (type) {
	    	case 0:
	    		result = MainApplication.getContext().getResources().getString(R.string.is_following_you);
	    		break;
	        case 1:
	            result = MainApplication.getContext().getResources().getString(R.string.likes_your_photo);
	            break;
	        case 2:
	        	result = MainApplication.getContext().getResources().getString(R.string.left_a_message);
	            break;
	        case 3:
	        	result = MainApplication.getContext().getResources().getString(R.string.bookmark_photo);
	            break;
	        case 4:
	        	result = MainApplication.getContext().getResources().getString(R.string.has_been_to_photo);
	            break;
	        case 5:
	        	result = MainApplication.getContext().getResources().getString(R.string.left_a_message_same_photo);
	            break;
	        case 6:
	        	result = MainApplication.getContext().getResources().getString(R.string.likes_comment);
	            break;
	        case 7:
	        	result = MainApplication.getContext().getResources().getString(R.string.uploaded_photo);
	            break;
	        case 8:
	        	result = MainApplication.getContext().getResources().getString(R.string.friend_just_joined);
	            break;
	        default:
	            break;
	    }
	    
	    return result;
	}*/
	
	@SuppressLint("NewApi")
	public static int getHeight(Context mContext){
	    int height=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){               
	        Point size = new Point();
	        display.getSize(size);
	        height = size.y;
	    }else{          
	        height = display.getHeight();  // deprecated
	    }
	    return height;      
	}
	
	@SuppressLint("NewApi")
	public static int getWidth(Context mContext){
	    int width=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){               
	        Point size = new Point();
	        display.getSize(size);
	        width = size.x;
	    }else{          
	    	width = display.getWidth();  // deprecated
	    }
	    return width;      
	}
}
