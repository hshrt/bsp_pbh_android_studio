package com.hsh.hshservice.global;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class CheckMessageUpdateManager extends Object implements XMLCaller.InternetCallbacks{
	
	private String TAG = "CheckMessageUpdateManager";
	
	//share perferences
	public static final String PREFS_NAME = "MyPrefsFile";
	
	private SharedPreferences settings;
	
	private Handler dataHandler = new Handler();
	
	private int dataDelay = 30*1000;
	
	private Date messageLastupdateDate = null ;
	
	
	private Boolean networkError = false;
	private Boolean useLocal = false;
	
	private Boolean startLoop = false;
	
	private String lastMsgSource = null;
	private Boolean isGold = false;
	
	private AsyncHttpClient client;
	
	public enum ApiCallType {
		CHECK_MSG_UPDATE
	}
	
	ApiCallType apiCallType;

	
	private Runnable loadDataRunnable = new Runnable() {

		public void run() {
			try {
				dataHandler.removeCallbacks(loadDataRunnable);
				
				//Log.i(TAG, "tester first= " + settings.getLong("lastItemUpdateTime2", 0));
				//Log.i(TAG, "lastItemUpdateTime = " + settings.getLong("lastItemUpdateTime", 0));
				
				checkMessageLastUpdate();

			} catch (Exception e) { e.printStackTrace();}
		}
	};
	
	public CheckMessageUpdateManager(){
		
		//fileNameList = new ArrayList<String>();
	}
	
	private static CheckMessageUpdateManager checkRoomStatusManager = new CheckMessageUpdateManager();

	public static synchronized CheckMessageUpdateManager getInstance() {
		return checkRoomStatusManager;
	}
	
	public void startLoadData(){
		
		try{
		settings = MainApplication.getContext().getSharedPreferences(PREFS_NAME, 0);
		
		//start the data loading looping, the reason to delay half second is because need wait for setup Wifi first
		
			if(!startLoop){
				dataHandler.postDelayed(loadDataRunnable, 500);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		     Handler h = new Handler();
		     
		     final Runnable r = new Runnable()
		     {
		         public void run() 
		         {
		        	 startLoadData();
		             
		         }
		     };
		     
		     h.postDelayed(r, 1000);
		}
	}
	
	
	
	@Override
	public boolean hasInternet() {
    	
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		
		String errorMsg = "";
		
		if(failMode == 1){
			errorMsg = "no internet";
		}
		
		Log.i(TAG, "failMode = " + errorMsg);
		Log.i(TAG, "onerror");
		//networkError = true;
		
		//for safe reason
		dataHandler.removeCallbacks(loadDataRunnable);
		dataHandler.postDelayed(loadDataRunnable, dataDelay);
		
	}
	
    private void checkMessageLastUpdate(){
    	Log.i(TAG, "checkMessageLastUpdate start!!!");
    	
		apiCallType = ApiCallType.CHECK_MSG_UPDATE;
		
		try {
			URL url = null;

			url = new URL(MainApplication.getContext().getString(R.string.api_base)
					+ MainApplication.getContext().getString(R.string.cms_api)
					+ MainApplication.getContext().getString(R.string.get_message_last_update_time)
					+ "?roomId="
					/*+"1708");*/
					+MainApplication.getMAS().getData("data_myroom"));
			
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			
			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
    }
    
public void returnMsgSignalToMAS(int index){
		
		
		client = new AsyncHttpClient();
		
		String url = "";
		
		if(index ==1){
			url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20HAVEMSG%20ON";
		}
		else{
			url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20HAVEMSG%20OFF";
		}
		Log.i(TAG, "URL = " + url);
		
		client.get(url, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }
		    
		    @Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);			
				Log.i(TAG,"success response = " +  s);
				
			}
		    
			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {
				
				if(response != null){
					String s = new String(response);	
					Log.i(TAG, s);
				}
				
				Log.i(TAG, "Error = " + e.toString());
				
			}

		    @Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
		    	
		    	Log.i(TAG,"retrying number = " + retryNo);
			}
		
		});
	}

	@Override
	public void postExecute(String json) {
		
		//for safe reason
		dataHandler.removeCallbacks(loadDataRunnable);
		dataHandler.postDelayed(loadDataRunnable, dataDelay);
		
		settings = MainApplication.getContext().getSharedPreferences(PREFS_NAME, 0);
		
		JSONObject msgObject = null;
		
		int status = 0; 
		
		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);
		
		JSONObject jo;
		JSONArray jArray = null;
		try {
			jo = new JSONObject(json);
		 
			jArray = jo.getJSONArray("data");
			
			if(jArray.length() > 0){
				msgObject= (JSONObject)jArray.get(0);
			}
			
			status = jo.getInt("status");
		}
		catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try{
		String fax = MainApplication.getMAS().getData("data_fax");
		String msg = MainApplication.getMAS().getData("data_msg");
		Log.i(TAG, "fax = " + fax);
		Log.i(TAG, "msg = " + msg);
		
		
			if(isGold && msg.equalsIgnoreCase("OFF") && fax.equalsIgnoreCase("OFF")){
				Log.i(TAG, "is gold but both msg and fax = OFF");
				Intent intent = new Intent("change_white_msg");
				LocalBroadcastManager.getInstance(MainApplication.getCurrentActivity()).sendBroadcast(
						intent);
				
				String dateString = MainApplication.getMAS().getData("year") +"-"+ MainApplication.getMAS().getData("month")+"-"+MainApplication.getMAS().getData("day")+" "+MainApplication.getMAS().getData("hour")+":"+MainApplication.getMAS().getData("minute");
				Log.i(TAG, "dateString = " + dateString);
				
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
				Date date = null;
				
				try {  
				    date = format.parse(dateString);  
				    System.out.println(date);  
				} catch (Exception e) {  
				    // TODO Auto-generated catch block  
				    e.printStackTrace();  
				}
						
				if(date != null){
					settings.edit().putLong("lastMessageVisitTime",date.getTime()).commit();
					
					Log.i(TAG, "stored UpdateTime =" + date.getTime());
				}
			}
			
			
			//for cutting off the FAX flag
			else if(fax.equalsIgnoreCase("OFF") && msg.equalsIgnoreCase("OFF")){
				Log.i(TAG, "both msg and fax = OFF. for cutting off the FAX flag");
				Intent intent = new Intent("change_white_msg");
				LocalBroadcastManager.getInstance(MainApplication.getCurrentActivity()).sendBroadcast(
						intent);
			}
		}
		catch(Exception e){
			Log.i(TAG, e.toString());
		}
		
		if(apiCallType == ApiCallType.CHECK_MSG_UPDATE && status == 1 && jArray!=null && jArray.length()>0){
			
			try {
				String messageLastUpdate = (String) msgObject.get("messageLastUpdate");
				
				Log.i(TAG,"messageLastUpdate = " + messageLastUpdate);
				
				SimpleDateFormat formatter; 
				messageLastupdateDate = null;

			    
			    formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			    try {
			    	messageLastupdateDate = (Date)formatter.parse(msgObject.getString("messageLastUpdate"));
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			    Context context = MainApplication.getCurrentActivity().getApplicationContext();
				CharSequence text = "The last message date = " + messageLastupdateDate;
				int duration = Toast.LENGTH_LONG;

				Toast toast = Toast.makeText(context, text, duration);
				//toast.show();
			    
			    //Log.i(TAG,"date timestamp message= " + messageLastupdateDate.getTime()+"");

			    Log.i(TAG,"last message Page Visit Date = " + createDate(settings.getLong("lastMessageVisitTime",0)) +"");
			    
			    //if need update
			    if(messageLastupdateDate.getTime() > settings.getLong("lastMessageVisitTime",0)){
			    	//change icon here
			    	Intent intent = new Intent("change_gold_msg");
					LocalBroadcastManager.getInstance(MainApplication.getCurrentActivity()).sendBroadcast(
							intent);
					
					lastMsgSource = msgObject.getString("source");
					if(lastMsgSource.equalsIgnoreCase("CMS") || lastMsgSource.equalsIgnoreCase("HOTEL")){
						
							returnMsgSignalToMAS(1);
						
					}
			    }
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    
		}
		else if(jArray!=null && jArray.length()==0){
			//this case we have no message return
			Log.i(TAG, "no message at all");
			
			Intent intent = new Intent("change_white_msg");
			LocalBroadcastManager.getInstance(MainApplication.getCurrentActivity()).sendBroadcast(
					intent);
		}
		
		
		
		
		
		
	}
	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub
		
	}

	public String getLastMsgSource() {
		return lastMsgSource;
	}

	public void setLastMsgSource(String lastMsgSource) {
		this.lastMsgSource = lastMsgSource;
	}

	public Boolean getIsGold() {
		return isGold;
	}

	public void setIsGold(Boolean isGold) {
		this.isGold = isGold;
	}
	
	public CharSequence createDate(long timestamp) {
	    Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(timestamp);
	    Date d = c.getTime();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    return sdf.format(d);
	  }

	
}
