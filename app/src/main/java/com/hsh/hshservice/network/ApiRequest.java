package com.hsh.hshservice.network;

import java.net.URL;

import org.w3c.dom.Document;

import android.os.Bundle;

public class ApiRequest {

	public static XMLCaller mCaller;


	/**
	 * Triggers the fetching of the listing
	 * @param type use either AppListings.TAB_SHOWS or AppListings.TAB_MERCH
	 * @param path the XML file path
	 * @throws Exception 
	 */
	public static void request(XMLCaller.InternetCallbacks callbacks, URL url , String method, Bundle param) throws Exception {
		mCaller = new XMLCaller(callbacks,method,param);
		mCaller.fetchOnlineXml(url);
	}
	public static void request(XMLCaller.InternetCallbacks callbacks, URL url,String method,Bundle param, int position) throws Exception {
		mCaller = new XMLCaller(callbacks, method, param, position);
		mCaller.fetchOnlineXml(url);
	}
	public static Document getDoc(String xml) throws Exception {
		return mCaller.fetchParsedXml(xml);
	}
	
	
}
