package com.hsh.hshservice.network;

public class MissingCallbacksException extends Exception {

	private static final long serialVersionUID = 7613067885754580258L;

	public MissingCallbacksException () {
	}
	
	public MissingCallbacksException(String msg) {
		super (msg);
	}
}
