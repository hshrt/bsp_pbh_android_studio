package com.hsh.hshservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.MessageItemAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.CheckMessageUpdateManager;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.model.MessageItem;
import com.hsh.hshservice.parser.GetMessageParser;
import com.hsh.hshservice.parser.GetMessageParser.GetMessageParserInterface;

public class MessageActivity extends BaseActivity implements
		GetMessageParserInterface {

	private static final String TAG = "MessageActivity";
	private ArrayList<MessageItem> items = new ArrayList<MessageItem>();
	private MessageItemAdapter adapter;
	private MyTextView messageSubjectText;
	private MyTextView messageTimeText;
	private WebView messageWebView;
	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.cms_message);
		
		CheckMessageUpdateManager.getInstance().returnMsgSignalToMAS(0);
		
		MainApplication.setCurrentActivity(this);
		if (CheckMessageUpdateManager.getInstance().getLastMsgSource() != null) {
			CheckMessageUpdateManager.getInstance().returnMsgSignalToMAS(0);
			/*if (CheckMessageUpdateManager.getInstance().getLastMsgSource()
					.equalsIgnoreCase("CMS")
					|| CheckMessageUpdateManager.getInstance()
							.getLastMsgSource().equalsIgnoreCase("HOTEL")) {
				CheckMessageUpdateManager.getInstance().returnMsgSignalToMAS(0);
			} else if (CheckMessageUpdateManager.getInstance()
					.getLastMsgSource().equalsIgnoreCase("FAX")) {
				//MainApplication.getMAS().setData("fax", "OFF");
			}*/
		}

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));

		FlurryAgent.logEvent("Message", map);

		// send out the boardcast to change to msg icon back to white
		Intent intent = new Intent("change_white_msg");
		LocalBroadcastManager.getInstance(MainApplication.getCurrentActivity())
				.sendBroadcast(intent);

		Helper.showInternetWarningToast();

		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);

		String titleId = getIntent().getStringExtra("titleId");

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);

		BottomBarFragment bottomBarFragment = new BottomBarFragment();

		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);

		// Commit the transaction
		transaction.commit();

		mDialog = new ProgressDialog(this);

		adapter = new MessageItemAdapter(this, 0, items);

		listView = (ListView) findViewById(R.id.messageListView);
		listView.setAdapter(adapter);
		/*
		 * listView.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) {
		 * 
		 * 
		 * 
		 * }
		 * 
		 * });
		 */

		messageSubjectText = (MyTextView) findViewById(R.id.messageSubjectText);
		messageTimeText = (MyTextView) findViewById(R.id.messageTimeText);
		messageWebView = (WebView) findViewById(R.id.messageWebView);
		messageWebView.setBackgroundColor(0x00000000);

		messageWebView.setHorizontalScrollBarEnabled(false);
		messageWebView.setVerticalScrollBarEnabled(false);

		getMessage();
	}

	public void onPressMessage(int position) {
		messageSubjectText.setText(items.get(position).getSubject());
		messageTimeText.setText(items.get(position).getLastUpdate());

		// HTMLData = [HTMLData
		// stringByReplacingOccurrencesOfString:@"\\n"withString:@"<br />"];

		String message = items.get(position).getDescription();

		StringBuffer localStringBuffer = new StringBuffer(message);

		// only do this for raw Text from TC3
		if (items.get(position).getLastUpdateBy().equalsIgnoreCase("TC3")) {
			localStringBuffer = new StringBuffer(message.replace("%", "&#37;")
					.replace("'", "&#39;").replace("\n", "<br/>")
					.replace("\r", "<br/>"));
		}

		localStringBuffer
				.insert(0,
						"<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><body><font style='font-family: serif; color: white; font-size: 24px; line-height: 36px'>");
		localStringBuffer.append("</font></body>");
		messageWebView.loadDataWithBaseURL(null, localStringBuffer.toString(),
				"text/html", "utf-8", null);
	}

	public void getMessage() {
		mDialog.setCancelable(false);
		mDialog.setMessage(this.getString(R.string.loading));
		showProgress();
		Log.i(TAG, "getMessage start");

		if (MainApplication.useLocalFile) {
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"message.json")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String myjsonstring = sb.toString();

			postExecute(myjsonstring);
		} else {

			String currentTime = "";

			String year = MainApplication.getMAS().getData("year");
			String month = MainApplication.getMAS().getData("month");
			String day = MainApplication.getMAS().getData("day");

			if (year.equalsIgnoreCase("")) {
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();

				year = today.year + "";
				month = (today.month + 1) + "";
				day = today.monthDay + "";

			}

			currentTime = year + "-" + month + "-" + day + " 00:00:00";

			// TODO: replace hardcoded roomID;
			// MainApplication.getMAS().getData("data_myroom")
			// new MessageAsyncFeed().execute(new String[] { "402" });
			/*
			 * new MessageAsyncFeed().execute(new String[]
			 * {MainApplication.getMAS().getData("data_myroom"),
			 * MainApplication.getMAS().getData("data_language") == "" ? "E" :
			 * MainApplication.getMAS().getData("data_language").toString()
			 * ,currentTime});
			 */
			
			/*MainApplication.getMAS().getData("data_myroom")*/
			new MessageAsyncFeed()
					.execute(new String[] {
							MainApplication.getMAS().getData("data_myroom"),
							MainApplication.getMAS().getData("data_language") == "" ? "E"
									: MainApplication.getMAS()
											.getData("data_language")
											.toString(), currentTime });
		}
	}

	@Override
	public void postExecute(String json) {
		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		if (json != null) {
			GetMessageParser parser = new GetMessageParser(json, this);
			parser.startParsing();
		}

	}

	@Override
	public void onGetDictParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		hideProgress();
	}

	@Override
	public void onGetDictFinishParsing(ArrayList<MessageItem> generalItemArray) {
		// TODO Auto-generated method stub
		hideProgress();

		items.clear();
		items.addAll(generalItemArray);

		adapter.notifyDataSetChanged();

		// TODO: Replace hardcoded "No Messages" message with multilingual
		// support
		if (items.size() == 0) {
			String noMessage = "<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><body><div style=\"display:-webkit-box;-webkit-box-pack:center;-webkit-box-align:center;\"><font style='font-family: serif; color: white; font-size: 24px; line-height: 36px'>"
					+ "No Messages" + "</div></font></body></html>";
			messageWebView.loadDataWithBaseURL(null, noMessage, "text/html",
					"utf-8", null);
		} else {
			listView.performItemClick(
					listView.getAdapter().getView(0, null, null), 0, 0);
		}

		if (generalItemArray.size() > 0)
			onPressMessage(0);
	}

	@Override
	public void onGetDictError() {
		// TODO Auto-generated method stub
		hideProgress();

		// try to get from cache data
	}

	public class MessageAsyncFeed extends AsyncTask<String, String, String> {

		// static String myLOG = "MessageAsyncFeed";

		// static Handler mWeatherHandler = new Handler();
		// static int mWeatherDelay = 15*60*1000;

		// static MessageAsyncFeed mMessgaeFeed;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				Context context = MainApplication.getContext();
				URL ww = new URL(context.getString(R.string.api_base)
						+ context.getString(R.string.cms_api)
						+ String.format(
								context.getString(R.string.get_message),
								params[0], params[1], params[2]));
				URLConnection tc = ww.openConnection();

				Log.i("Message", ww.toString());

				BufferedReader in = new BufferedReader(new InputStreamReader(
						tc.getInputStream()));

				String line = "";
				String fullJSON = "";
				while ((line = in.readLine()) != null) {
					fullJSON = fullJSON + line;
				}

				return fullJSON;

			} catch (Exception e) {

				return null;
			}

			// return null;
		}

		@Override
		protected void onPostExecute(String result) {
			postExecute(result);
		}
	}

}
