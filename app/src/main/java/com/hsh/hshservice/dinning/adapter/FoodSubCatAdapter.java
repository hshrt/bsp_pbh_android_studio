package com.hsh.hshservice.dinning.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.ServiceInRoomDinningActivity;
import com.hsh.hshservice.ServiceInRoomPCHDinningActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;

public class FoodSubCatAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> foodSubCatItems;

	private static String TAG = "FoodSubCatAdapter";
	private View mParent;
	
	private Context mContext;
	
	private Button firstButton;
	
	private int currentPosition = 0;
	
	private String parentName = "";
	
	public FoodSubCatAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.foodSubCatItems = items;
		mInflater = LayoutInflater.from(context);
		mContext = context;
	}

	@Override
	public int getCount() {
		Log.i(TAG, "getCount = " + foodSubCatItems.size());
		return foodSubCatItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return foodSubCatItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;
		
		final ViewHolder holder;
		
		String itemName = MainApplication.getLabel(foodSubCatItems.get(position).getTitleId());
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_in_room_pch_left_menu_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 

			holder.textView = (TextView) v.findViewById(R.id.text);
			Helper.setFonts(holder.textView, mContext.getString(R.string.app_font));
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		if(position == 0){
			Log.i(TAG, "assign firstButton");
			firstButton = holder.button;
			holder.textView.setText("");
		}
		
		//holder.button.setText(itemName);
		holder.textView.setText(itemName.toUpperCase());
		if(position == 0){
			//holder.textView.setText("");
		}
		holder.textView.setMaxLines(2);
		holder.button.setBackgroundResource(R.drawable.base_btn_selector);
		
		if( position == currentPosition){
			holder.button.setBackgroundResource(R.drawable.button_on);
		}
		
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i(TAG, ((ViewGroup)parent).getChildCount()+"");
				
				for(int i=0; i<((ViewGroup)parent).getChildCount(); i++) {
				    View nextChild = ((ViewGroup)parent).getChildAt(i);
				    Log.i(TAG, nextChild.getClass().toString());
				    if (nextChild instanceof Button){
				    	Log.i(TAG, "done");
				    	nextChild.setBackgroundResource(R.drawable.base_btn_selector);
				    }
				    for(int y=0; y<((ViewGroup)nextChild).getChildCount(); y++) {
				    	View nextChild2 = ((ViewGroup)nextChild).getChildAt(y);
				    	if (nextChild2 instanceof Button){
					    	Log.i(TAG, "done");
					    	nextChild2.setBackgroundResource(R.drawable.base_btn_selector);
					    }	
				    }
				}
				
				holder.button.setBackgroundResource(R.drawable.button_on);
				
				if(mContext instanceof ServiceInRoomDinningActivity){
					((ServiceInRoomDinningActivity)mContext).clickOnFoodSubCat(position);
				}
				else if(mContext instanceof ServiceInRoomPCHDinningActivity){
					Log.i(TAG,"ServiceInRoomPCHDinningActivity gogogo");
					if(!parentName.equalsIgnoreCase("InRoomDinItemFragment")){
						((ServiceInRoomPCHDinningActivity)mContext).clickOnFoodSubCat(position);
					}
					else{
						((ServiceInRoomPCHDinningActivity)mContext).clickOnFood(position);
					}
				}
				
				currentPosition = position;
			}
		});
		
		return v;
	}
	public Button getFirstButton() {
		return firstButton;
	}

	public void setFirstButton(Button firstButton) {
		this.firstButton = firstButton;
	}
	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}
	static class ViewHolder {
		Button button;
		TextView textView;
	}
	public void setParentName(String s){
		parentName = s;
	}

}
