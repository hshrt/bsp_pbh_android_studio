package com.hsh.hshservice.dinning.adapter;

import com.hsh.esdbsp.R;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FoodCategory;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.model.MainMenuItem;

public class FoodCatAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> foodItems;

	private static String TAG = "FoodCatAdapter";
	private View mParent;
	
	private Context mContext;
	
	public FoodCatAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.foodItems = items;
		mInflater = LayoutInflater.from(context);
		mContext = context;
	}

	@Override
	public int getCount() {
		return foodItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return foodItems.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;
		
		ViewHolder holder;
		
		String itemName = foodItems.get(position).getItemId();
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_two_column_left_menu_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			Helper.setFonts(holder.button, mContext.getString(R.string.app_font));
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.button.setText(itemName.toUpperCase());
		
		return v;
	}
	static class ViewHolder {
		Button button;
	}

}
