package com.hsh.hshservice.dinning.adapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.hshservice.ServiceInRoomDinningActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Food;
import com.hsh.hshservice.model.GeneralItem;

public class FoodAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> foodItems;

	private static String TAG = "FoodAdapter";
	private View mParent;
	
	private Context mContext;
	
	private int layoutType;
	
	public FoodAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.foodItems = items;
		mInflater = LayoutInflater.from(context);
		mContext = context;
	}
	
	private String processPrice(String _price){
		Log.i(TAG, "Index of . = " + _price.indexOf("."));
		
		if(_price.indexOf(".") != _price.length()-3){
			Log.i(TAG, "process Price do!");
			_price= _price + "0";
		}
		return _price;
	}

	@Override
	public int getCount() {
		return foodItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return foodItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;
		
		ViewHolder holder;
		
		String itemName = foodItems.get(position).getItemId();
		
		GeneralItem food = foodItems.get(position);
		
		if (v == null) {
			mParent = parent;
			
			if(layoutType == 0){
				v = mInflater.inflate(R.layout.cms_food_item, parent,
					false);
			}
			else if(layoutType == 1){
				v = mInflater.inflate(R.layout.cms_food_item_short, parent,
						false);
			}
			
			holder = new ViewHolder();
			holder.codeTV = (TextView) v.findViewById(R.id.food_id);
			holder.nameTV = (TextView) v.findViewById(R.id.food_name);
			holder.priceTV = (TextView) v.findViewById(R.id.food_price);
			holder.desTV = (TextView) v.findViewById(R.id.food_des);
			
			holder.itemButton = (Button) v.findViewById(R.id.itemButton);
			
			Helper.setFonts(holder.codeTV, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.nameTV, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.priceTV, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.desTV, mContext.getString(R.string.app_font));
			
			holder.codeTV.setTextColor(Color.parseColor("#ffffff"));
			holder.nameTV.setTextColor(Color.parseColor("#ffffff")); 
			holder.priceTV.setTextColor(Color.parseColor("#ffffff")); 
			holder.desTV.setTextColor(Color.parseColor("#ffffff")); 
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.itemButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(mContext instanceof ServiceInRoomDinningActivity){
					((ServiceInRoomDinningActivity)mContext).clickOnFood(position);
				}
				
				//currentPosition = position;
			}
		});

		
		holder.codeTV.setText(MainApplication.getLabel(food.getTitleId()));
		//holder.nameTV.setText(food.getName());
		
		Log.i(TAG, "the price is = " + food.getPrice());
		
		holder.priceTV.setText("US $"+ Helper.processPrice(food.getPrice()+""));
		if(food.getPrice() == 0){
			holder.priceTV.setText("");
		}
		
		/*if(position == 0){
			holder.codeTV.setText("");
			holder.priceTV.setText("");
		}*/
		String description = MainApplication.getLabel(food.getDescriptionId()).replaceAll("\\<.*?\\>", "");
		description = description.replaceAll("&nbsp;","");
		holder.desTV.setText(description);
		
		return v;
	}
	public int getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(int layoutType) {
		this.layoutType = layoutType;
	}
	static class ViewHolder {
		Button itemButton;
		TextView codeTV;
		TextView nameTV;
		TextView priceTV;
		TextView desTV;
	}

}
