package com.hsh.hshservice.customclass;

import com.hsh.hshservice.customclass.SettingsRow;

public class SettingsOptionItem implements SettingsRow{
	public final String title;

	public SettingsOptionItem(String title) {
		this.title = title;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}
}
