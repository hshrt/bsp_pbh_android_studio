package com.hsh.hshservice.customclass;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class CustomCheckBox extends CheckBox {

	private int row;
	
	public CustomCheckBox(Context context, AttributeSet attrs){
		super(context, attrs);
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
	
	

}
