package com.hsh.hshservice.customclass;

import com.hsh.hshservice.customclass.SettingsRow;

public class SettingsSectionItem implements SettingsRow{
	private final String title;
	
	public SettingsSectionItem(String title) {
		this.title = title;
	}
	
	public String getTitle(){
		return title;
	}
	
	@Override
	public boolean isSection() {
		return true;
	}
}
