package com.hsh.hshservice.customclass;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.global.NetworkImage;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.network.XMLCaller;

public class BaseFragment extends Fragment implements XMLCaller.InternetCallbacks{
	public ProgressBar progressBar;
	protected TextView errorText;
	protected Button retryBtn;
	protected Button cancelBtn;
	protected AsyncTask<URL, Void, String> currentTask;
	protected Map<Integer,NetworkImage> networkImageMap = new HashMap<Integer,NetworkImage>();
	
	//The errorPopup Container
	public RelativeLayout errorPopup;
	
	public void hideError(){
		errorPopup.setVisibility(View.GONE);
	}
	public void showError(){
		errorPopup.setVisibility(View.VISIBLE);
	}
	public void  hideProgress(){
		progressBar.setVisibility(View.GONE);
	}
	public void showProgress(){
		progressBar.setVisibility(View.VISIBLE);
	}
	protected int getOrientation(){
		return this.getActivity().getResources().getConfiguration().orientation;
	}
	public void setCurrentTask(AsyncTask<URL, Void, String> task){
		currentTask = task;
	}
	protected void cancelCurrentTask(){
		if (currentTask != null
				&& (currentTask.getStatus().equals(AsyncTask.Status.RUNNING) || currentTask.getStatus().equals(AsyncTask.Status.FINISHED))) {
			currentTask.cancel(true);
			Log.i("BaseFragment", "cancel currentTask");
			currentTask = null;
		}
	}
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}
	@Override
	public void onError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void postExecute(String xml) {
		// TODO Auto-generated method stub
		hideProgress();
	}
	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub
		
	}
}
