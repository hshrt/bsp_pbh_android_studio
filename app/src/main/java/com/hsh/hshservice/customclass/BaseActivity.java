package com.hsh.hshservice.customclass;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.global.NetworkImage;
import com.hsh.hshservice.network.XMLCaller;


public class BaseActivity extends CustomFragmentActivity implements XMLCaller.InternetCallbacks{
	
	protected ProgressBar progressBar;
	protected ProgressDialog mDialog ;
	protected TextView errorText;
	protected Button retryBtn;
	//The errorPopup Container
	protected Button cancelBtn;
	public RelativeLayout errorPopup;
	
	protected TopBarFragment topBarFragment;
	
	private AsyncTask<URL, Void, String> currentTask;
	protected Map<Integer,NetworkImage> networkImageMap = new HashMap<Integer,NetworkImage>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//mDialog = new ProgressDialog(this);
	}
	public MainApplication getAppContext(){
		return ((MainApplication)getApplicationContext());
	}
	@Override
	public void onUserInteraction(){
		Log.i("BaseActivity", "onUserInteraction fire");
		if(topBarFragment != null)
		topBarFragment.resetBackMainTimer();
	}
	
	public void hideError(){
		errorPopup.setVisibility(View.GONE);
	}
	public void showError(){
		errorPopup.setVisibility(View.VISIBLE);
	}
	protected void  hideProgress(){
		progressBar.setVisibility(View.GONE);
	}
	protected void showProgress(){
		progressBar.setVisibility(View.VISIBLE);
	}
	public ProgressBar getProgressBar(){
		return progressBar;
	}
	public void  showDialog(){
		Log.i("damn","mDialog = " + mDialog);
		mDialog.show();
	}
	public void hideDialog(){
		mDialog.hide();
	}
	public ProgressDialog getDialog(){
		return mDialog;
	}
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}
	@Override
	public void onError(int failMode, boolean isPostExecute){
		if(progressBar != null){
			hideProgress();
		}
		if(mDialog != null){
			hideDialog();
		}
		
		//this is general connection error
		errorText.setText(this.getString(R.string.error_network_api));
		
		//this is timeout connection error
		if(failMode == XMLCaller.FAIL_MODE_DISPLAY_RETRY_TIMEOUT){
			errorText.setText(this.getString(R.string.error_network_timeout));
		}
		
		showError();
	
		Log.i("BaseActivity", "onerror");
//		this.showError();
//		
//		try{
//		//this is general connection error
//		errorText.setText(getActivity().getString(R.string.error_network_api));
//		
//		//this is timeout connection error
//		if(failMode == XmlParser.FAIL_MODE_DISPLAY_RETRY_TIMEOUT){
//			errorText.setText(getActivity().getString(R.string.error_network_timeout));
//		}
//		}
//		catch(Exception e){
//			
//		}
	}


	@Override
	public void postExecute(String xml) {
		if(progressBar != null){
			hideProgress();
		}
		if(mDialog != null){
			hideDialog();
		}
	}
	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub
		
	}
	public AsyncTask<URL, Void, String> getCurrentTask() {
		return currentTask;
	}
	public void setCurrentTask(AsyncTask<URL, Void, String> currentTask) {
		this.currentTask = currentTask;
	}
}
