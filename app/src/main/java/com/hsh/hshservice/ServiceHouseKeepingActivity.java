package com.hsh.hshservice;

import java.util.ArrayList;








import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.HouseKeepingItemAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.model.MainMenuItem;
import com.loopj.android.http.*;

public class ServiceHouseKeepingActivity extends BaseActivity {
	
	private GridView menu_gridView;
	
	private HouseKeepingItemAdapter mAdapter;
	
	private ArrayList<GeneralItem> itemArray;
	
	private String TAG = "ServiceHouseKeepingActivity";
	
	private TextView staticText;
	private Button submitBtn;
	
	private String parentId;
	private String titleId;
	
	private View bgView;
	
	String[] keyArr = {"hotel_restaurants","hotel_restaurants","hotel_restaurants","wellness",
            "guest_services","transportation"};
	
	Boolean[] requestChecker; //a boolean array to check if the request of the service sent out successfully
	String[] commandList;
	
	private int currentIndex;
	private int retryNum = 0;
	
	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;
	
	private AsyncHttpClient client;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.cms_housekeeping);
		
		
		MainApplication.setCurrentActivity(this);
		initUI();
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");
		
		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);


		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);

		// Commit the transaction
		transaction.commit();
		
		
		if(itemArray == null){
			itemArray = new ArrayList<GeneralItem>();
		}
		
		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		
		//Log.i(TAG, "temp Array length = " + tempArray.size());
		
		for(GeneralItem item: tempArray){
			//Log.i(TAG, "temp Arary item = " + item.getItemId());
			if(item.getParentId().equalsIgnoreCase(parentId)){
				itemArray.add(item);
			}		
		}
		
		Log.i(TAG, itemArray.toString());
		
		if(mAdapter == null){
			mAdapter= new HouseKeepingItemAdapter(this, itemArray);
		}
		
		menu_gridView.setAdapter(mAdapter);
		
		

	}
	public void onItemclick(int position){
		if(position == 0){
			Intent intent = new Intent(ServiceHouseKeepingActivity.this,
					ServiceInRoomDinningActivity.class);
			
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
		else if(position==2){
			
			Intent intent = new Intent(ServiceHouseKeepingActivity.this,
					ServiceGeneralMenuActivity.class);
			
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
		else if(position<=5){
			Intent intent = new Intent(ServiceHouseKeepingActivity.this,
					ServiceGeneralItemActivity.class);
			
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
	}
	
	private void initUI(){
		menu_gridView = (GridView) findViewById(R.id.menu_gridView);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		staticText = (TextView) findViewById(R.id.static_text);
		
		bgView = (View) findViewById(R.id.bgView);
		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
		
		errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);
		
		staticText.setText(MainApplication.getLabel("housekeeping.selectservice.msg"));
		submitBtn.setText(MainApplication.getLabel("housekeeping.submit.label"));
		
		Helper.setFonts(submitBtn, getString(R.string.app_font));
		Helper.setFonts(staticText, getString(R.string.app_font));
		
		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.errorMessageDialogCloseImage:
					errorMessageDialogLayout.setVisibility(View.GONE);
					
					break;
				
				default:
					break;
				}
			}
		};

		errorMessageDialogCloseImage.setOnClickListener(onClickListener);
		
		submitBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG,mAdapter.getClickArr().toString());
				
				
				
				int clickCount = 0;
				
				for(int x = 0; x < mAdapter.getClickArr().length ; x++){
					
					if(mAdapter.getClickArr()[x] == true){						
						clickCount++;
					}					
				}
				
				commandList = new String[clickCount];
				
				int temp_clickCount = 0;
				
				for(int x = 0; x < mAdapter.getClickArr().length ; x++){
					
					if(mAdapter.getClickArr()[x] == true){						
						commandList[temp_clickCount] = itemArray.get(x).getCommand();
						temp_clickCount++;
					}					
				}
				
				Log.i(TAG,"commandList = " + commandList.toString());
				
				requestChecker = new Boolean[clickCount];
				for(int x = 0; x < requestChecker.length ; x++){
					requestChecker[x] = false;
					
				}
				
				if(commandList.length > 0){
					//send out Order Serice Request to MAS	
					showProgress();
					bgView.setVisibility(View.VISIBLE);
					requestService(currentIndex);
				}
			}
		});
	}
	
	private void showMessage(String msg) {
		hideProgress();
		bgView.setVisibility(View.GONE);
		errorMessageDialogLayout.setVisibility(View.VISIBLE);
		errorMessageDialogLabel.setText(msg);
	}
	
	private void requestService(final int index){
		
		
		client = new AsyncHttpClient();
		
		client.setMaxRetriesAndTimeout(0, 10000);
		
		String url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20"+commandList[index]+"%20ON";
		
		Log.i(TAG, "URL = " + url);
		
		client.get(url, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }
		    
		    @Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);			
				Log.i(TAG,"success response = " +  s);
				
				//every request should have 3 time retry
				retryNum = 0;
				
				if(currentIndex != commandList.length-1){
					currentIndex++;
					requestService(currentIndex);
				}
				else{
					currentIndex = 0;
					Log.i(TAG,"finish the loop");
					//finish, hide the loading
					hideProgress();
					showMessage(MainApplication.getLabel("housekeeping.thankyou.msg"));
					
				}
			}
		    
			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {
				
				if(response != null){
					String s = new String(response);	
					Log.i(TAG, s);
				}
				
				Log.i(TAG, "Error = " + e.toString());
				
				if(retryNum<2){
					retryNum++;
					requestService(currentIndex);
					
				}
				else{
					retryNum = 0;
					Log.i(TAG, "Stop Retry");
					//notice the user check the network
					showMessage(MainApplication.getLabel("housekeeping.error.msg"));
				}
			}

		    @Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
		    	
		    	Log.i(TAG,"retrying number = " + retryNo);
			}
		
		});
	}
	
	
	@Override
	public void onDestroy() {
		Log.i(TAG, "HouseKeepingActivituy onDestroy");
		
		if(client != null){
			client.cancelAllRequests(true);	
		}
		
		super.onDestroy();

	}
	
	@Override
	public void onPause() {
		
		Log.i(TAG, "HouseKeepingActivituy onPause");
		
		if(client != null){
			client.cancelAllRequests(true);	
		}
		
		super.onPause();
	}
	
	@Override
	public void onStop() {
		Log.i(TAG, "HouseKeepingActivituy onStop");
		
		if(client != null){
			client.cancelAllRequests(true);	
		}
		
		super.onStop();
	}
	
	//MAS IP http://<masip>/mas.php?cmd=SET%20EXTRAWATER%20ON
	
	

}