package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.GeneralItem;

public class InfoAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> infoItems;

	private static String TAG = "InfoAdapter";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public InfoAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.infoItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return infoItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return infoItems.get(position);
	}
	
	private String processPrice(String _price){
		Log.i(TAG, "Index of . = " + _price.indexOf("."));
		
		if(_price.indexOf(".") != _price.length()-3){
			Log.i(TAG, "process Price do!");
			_price= _price + "0";
		}
		return _price;
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		GeneralItem info = infoItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.info_item, parent,
					false);
			holder = new ViewHolder();		
			
			holder.name = (TextView) v.findViewById(R.id.name);
			holder.price = (TextView) v.findViewById(R.id.price);	
			
			Helper.setFonts(holder.name, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.price, mContext.getString(R.string.app_font));

			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		if(position % 2 == 0){	
			v.setBackgroundResource(R.color.transparent_black);
		}
		else{
			v.setBackgroundResource(R.color.transparent);
		}
		
		//holder.button.setText(itemName);
		holder.name.setText(MainApplication.getLabel(info.getTitleId()));
		holder.price.setText(processPrice("US $"+info.getPrice()));

		
		/*if(position == 0){
			firstButton = holder.button;
		}*/

		return v;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	static class ViewHolder {
		TextView name;
		TextView price;
	}

}
