package com.hsh.hshservice.adapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import com.hsh.hshservice.ServiceGeneralMenuActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.model.MainMenuItem;

public class HouseKeepingItemAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> mainMenuItems;

	private static String TAG = "HouseKeepingItemAdapter";
	private View mParent;
	
	private Context mContext;
	
	private Boolean[] clickArr;

	public HouseKeepingItemAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.mainMenuItems = items;
		mContext = context;
		mInflater = LayoutInflater.from(context);
		
		clickArr = new Boolean[items.size()];
		for(int x = 0; x < items.size() ; x++){
			clickArr[x] = false;
		}
	}

	@Override
	public int getCount() {
		return mainMenuItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return mainMenuItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;
		
		final ViewHolder holder;
		
		String itemName = MainApplication.getLabel(mainMenuItems.get(position).getTitleId());
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_housekeeping_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			holder.checker = (Button) v.findViewById(R.id.clickBtn);
			
			Helper.setFonts(holder.button, mContext.getString(R.string.app_font));
			
			
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.button.setText(itemName);
		
		holder.checker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickArr[position]){
					clickArr[position] = false;
					holder.checker.setBackgroundResource(R.drawable.checkmark_off);
				}
				else{
					clickArr[position] = true;
					holder.checker.setBackgroundResource(R.drawable.checkmark_on);
				}
			}
		});
		
		if(clickArr[position] == false)
		{
			holder.checker.setBackgroundResource(R.drawable.checkmark_off);
		}
		else{
			holder.checker.setBackgroundResource(R.drawable.checkmark_on);
		}
		
		

		return v;
	}
	public Boolean[] getClickArr() {
		return clickArr;
	}

	public void setClickArr(Boolean[] clickArr) {
		this.clickArr = clickArr;
	}
	static class ViewHolder {
		Button button;
		Button checker;
	}

}
