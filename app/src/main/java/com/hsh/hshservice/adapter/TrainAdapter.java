package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.R;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.Train;

public class TrainAdapter extends ArrayAdapter<Train> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<Train> trainItems;

	private static String TAG = "TrainAdapter";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public TrainAdapter(Context context, ArrayList<Train> items) {
		super(context, 0, items);
		this.trainItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return trainItems.size();
	}

	@Override
	public Train getItem(int position){
		return trainItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		Train trainInfo = trainItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.train_item, parent,
					false);
			holder = new ViewHolder();	
			
			holder.depart = (TextView) v.findViewById(R.id.depart);
			holder.arrive = (TextView) v.findViewById(R.id.arrive);
			holder.train = (TextView) v.findViewById(R.id.train);
			holder.depTime = (TextView) v.findViewById(R.id.depTime);
			holder.arrTime = (TextView) v.findViewById(R.id.arrTime);	
			holder.interval = (TextView) v.findViewById(R.id.interval);
			holder.type = (TextView) v.findViewById(R.id.type);
			
			Helper.setFonts(holder.depart, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.arrive, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.train, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.depTime, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.arrTime, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.interval, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.type, mContext.getString(R.string.app_font));
			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		if(position % 2 == 0){	
			v.setBackgroundResource(R.color.transparent_black);
		}
		else{
			v.setBackgroundResource(R.color.transparent);
		}
		
		//holder.button.setText(itemName);
		
		holder.depart.setText(trainInfo.getDeptStation());
		holder.arrive.setText(trainInfo.getArriStation());
		holder.train.setText(trainInfo.getCode());
		holder.depTime.setText(trainInfo.getDeptTime());
		holder.arrTime.setText(trainInfo.getArriTime());
		holder.interval.setText(trainInfo.getInterval());
		holder.type.setText(trainInfo.gettType());
		
		return v;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	static class ViewHolder {
		TextView depart;
		TextView arrive;
		TextView train;
		TextView depTime;
		TextView arrTime;
		TextView interval;
		TextView type;
	}

}
