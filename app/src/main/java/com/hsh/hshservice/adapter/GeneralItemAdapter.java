package com.hsh.hshservice.adapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;

public class GeneralItemAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> generalItems;

	private static String TAG = "GeneralItemAdapter";
	private View mParent;
	private Context mContext;
	
	private Button firstButton;
	
	private int currentPosition = 0;
	
	public GeneralItemAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.generalItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}

	@Override
	public int getCount() {
		return generalItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return generalItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		    String itemName = MainApplication.getLabel(generalItems.get(position).getTitleId());
		//String itemName = "fuck";
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_two_column_left_menu_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			holder.textview = (TextView) v.findViewById(R.id.text);
			
			Helper.setFonts(holder.textview, mContext.getString(R.string.app_font));
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.textview.setText(itemName);
		
		//this line is very important that solve the bug of textView will automatically change back to 1 when reusing textview
		holder.textview.setMaxLines(2);
		
		if(position == 0){
			firstButton = holder.button;
			//holder.textview.setText("");
		}
		
		holder.button.setBackgroundResource(R.drawable.base_btn_selector);
		
		if( position == currentPosition){
			holder.button.setBackgroundResource(R.drawable.button_on);
		}
		
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i(TAG, ((ViewGroup)parent).getChildCount()+"");
				
				for(int i=0; i<((ViewGroup)parent).getChildCount(); i++) {
				    View nextChild = ((ViewGroup)parent).getChildAt(i);
				    Log.i(TAG, nextChild.getClass().toString());
				    if (nextChild instanceof Button){
				    	Log.i(TAG, "done");
				    	nextChild.setBackgroundResource(R.drawable.base_btn_selector);
				    }
				    for(int y=0; y<((ViewGroup)nextChild).getChildCount(); y++) {
				    	View nextChild2 = ((ViewGroup)nextChild).getChildAt(y);
				    	if (nextChild2 instanceof Button){
					    	Log.i(TAG, "done");
					    	nextChild2.setBackgroundResource(R.drawable.base_btn_selector);
					    }	
				    }
				}
				
				holder.button.setBackgroundResource(R.drawable.button_on);
				
				if(mContext instanceof ServiceGeneralItemActivity){
					((ServiceGeneralItemActivity)mContext).changeDescriptionText(position);
				}
				
				currentPosition = position;
			}
		});
		
		return v;
	}
	public Button getFirstButton() {
		return firstButton;
	}

	public void setFirstButton(Button firstButton) {
		this.firstButton = firstButton;
	}
	static class ViewHolder {
		Button button;
		TextView textview;
	}

}
