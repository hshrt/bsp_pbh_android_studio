package com.hsh.hshservice.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.ServiceGeneralMenuActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.GeneralItem;

public class MainMenuItemAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> mainMenuItems;

	private static String TAG = "MainMenuItemAdapter";
	private View mParent;
	
	private Context mContext;
	
	Map<String, String> map = new HashMap<String, String>();
	
	private String[] imgNameArr = {"ser_inroom","ser_restaurant","ser_concierge","ser_spa",
            "ser_frontdesk","ser_transportation","ser_attractions","ser_news","ser_airport","ser_fx"};

	private Boolean hasLeftIcon;
	
	public MainMenuItemAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.mainMenuItems = items;
		mContext = context;
		mInflater = LayoutInflater.from(context);
		
		hasLeftIcon = true;
		
		map.put("In Room Dining", "ser_inroom");
		map.put("Hotel Restaurants", "ser_restaurant");
		map.put("Concierge", "ser_concierge");
		map.put("Wellness", "ser_spa");
		map.put("Guest Services", "ser_frontdesk");
		map.put("Transportation", "ser_transportation");
		map.put("Attractions", "ser_attractions");
		map.put("News", "ser_news");
		map.put("Airport", "ser_airport");
		map.put("FX Currency", "ser_fx");
		map.put("Housekeeping", "ser_housekeeping");
		map.put("Mini Bar", "ser_minibar");
		map.put("Relaxing Music", "ser_relaxing_music");
		map.put("Restaurants Reservation", "ser_restaurants_reservation");
		map.put("The New York Times", "ser_the_new_york_times");
		map.put("Peninsula Moments", "p_icon");
		map.put("Pen Cities", "p_icon");
		map.put("Special Offers", "p_icon");
		map.put("Peninsula Magazine", "p_icon");
		map.put("Guest Messaging", "message_out");
		
		//map.get(key)
	}

	@Override
	public int getCount() {
		return mainMenuItems.size();
	}

	@Override
	public GeneralItem getItem(int position){
		return mainMenuItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;
		
		ViewHolder holder;
		
		String itemName = MainApplication.getLabel(mainMenuItems.get(position).getTitleId());
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_main_menu_item, parent,false);
			
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff"));
			
			Helper.setFonts(holder.button, mContext.getString(R.string.app_font));
			
			
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.button.setText(itemName);
		
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(mContext instanceof ServiceMainActivity){
					Log.i(TAG,"on click position = " + position);
					((ServiceMainActivity)mContext).onItemclick(mainMenuItems.get(position));
				}
				
				if(mContext instanceof ServiceGeneralMenuActivity)
				((ServiceGeneralMenuActivity)mContext).onItemclick(position);
			}
		});	
		
		if(hasLeftIcon && map.get(MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()))!=null){
			//String imageName = imgNameArr[position%10];
			try{
			String imageName = map.get(MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()));
			int resourceId = mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
			holder.button.setCompoundDrawablesWithIntrinsicBounds( resourceId, 0, 0, 0);
			}
			catch(Exception e){
				
			}
		}
		else{
			Log.i(TAG, "use reserve icon");
			//Dont do this at this moment, we don't want the sub-level button have the P icon
			
			//int resourceId2 = mContext.getResources().getIdentifier("p_icon", "drawable", mContext.getPackageName());
			//holder.button.setCompoundDrawablesWithIntrinsicBounds( resourceId2, 0, 0, 0);
		}
		
	    return v;
	}
	public Boolean getHasLeftIcon() {
		return hasLeftIcon;
	}

	public void setHasLeftIcon(Boolean hasLeftIcon) {
		this.hasLeftIcon = hasLeftIcon;
	}
	static class ViewHolder {
		Button button;
	}

}
