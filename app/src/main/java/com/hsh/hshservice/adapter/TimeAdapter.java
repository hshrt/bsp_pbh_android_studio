package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;

public class TimeAdapter extends ArrayAdapter<String> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<String> timeItems;

	private static String TAG = "String";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public TimeAdapter(Context context, ArrayList<String> items) {
		super(context, 0, items);
		this.timeItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return timeItems.size();
	}

	@Override
	public String getItem(int position){
		return timeItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		String timeInfo = timeItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.flight_time_item, parent,
					false);
			holder = new ViewHolder();		
			
			holder.timeRange = (Button) v.findViewById(R.id.timeButton);
	
			
			Helper.setFonts(holder.timeRange, mContext.getString(R.string.app_font));

			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		
		//holder.button.setText(itemName);
		holder.timeRange.setText(timeInfo);
		
		
		/*if(position == 0){
			firstButton = holder.button;
		}*/

		return v;
	}

	static class ViewHolder {
		TextView timeRange;

	}

}
