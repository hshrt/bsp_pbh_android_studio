package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.R;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airport;

public class AirportAdapter extends ArrayAdapter<Airport> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<Airport> airports;

	private static String TAG = "AirportAdapter";
	private View mParent;
	private Context mContext;
	
	private Button firstButton;
	
	private Boolean setOnClickInAdapter = true;
	
	private int currentPosition = -1;
	
	public AirportAdapter(Context context, ArrayList<Airport> airports) {
		super(context, 0, airports);
		this.airports = airports;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}

	@Override
	public int getCount() {
		return airports.size();
	}

	@Override
	public Airport getItem(int position){
		return airports.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		    String itemName = "("+airports.get(position).getIATA()+") " + airports.get(position).getName();
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.airport_airline_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			holder.textview = (TextView) v.findViewById(R.id.text);
			
			Helper.setFonts(holder.textview, mContext.getString(R.string.app_font));
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		//holder.button.setText(itemName);
		holder.textview.setText(itemName);
		
		if(position == 0){
			firstButton = holder.button;
		}
		
		holder.button.setBackgroundResource(R.drawable.dining_item_selector);
		
		if( position == currentPosition){
			holder.button.setBackgroundResource(R.drawable.dining_item_selected);
		}
		if(setOnClickInAdapter){
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i(TAG, ((ViewGroup)parent).getChildCount()+"");
				
				for(int i=0; i<((ViewGroup)parent).getChildCount(); i++) {
				    View nextChild = ((ViewGroup)parent).getChildAt(i);
				    Log.i(TAG, nextChild.getClass().toString());
				    if (nextChild instanceof Button){
				    	Log.i(TAG, "done");
				    	nextChild.setBackgroundResource(R.drawable.dining_item_selector);
				    }
				    for(int y=0; y<((ViewGroup)nextChild).getChildCount(); y++) {
				    	View nextChild2 = ((ViewGroup)nextChild).getChildAt(y);
				    	if (nextChild2 instanceof Button){
					    	Log.i(TAG, "done");
					    	nextChild2.setBackgroundResource(R.drawable.dining_item_selector);
					    }	
				    }
				}
				
				holder.button.setBackgroundResource(R.drawable.dining_item_selected);
				
				if(mContext instanceof ServiceAirportActivity){
					((ServiceAirportActivity)mContext).onAirportPressed(position,airports.get(position));
				}
				
				currentPosition = position;
			}
		});
		}
		
		return v;
	}
	public Button getFirstButton() {
		return firstButton;
	}

	public void setFirstButton(Button firstButton) {
		this.firstButton = firstButton;
	}
	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}
	public Boolean getSetOnClickInAdapter() {
		return setOnClickInAdapter;
	}

	public void setSetOnClickInAdapter(Boolean setOnClickInAdapter) {
		this.setOnClickInAdapter = setOnClickInAdapter;
	}
	static class ViewHolder {
		Button button;
		TextView textview;
	}

}
