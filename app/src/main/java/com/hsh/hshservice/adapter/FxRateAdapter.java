package com.hsh.hshservice.adapter;

import java.util.List;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.model.FxRate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class FxRateAdapter extends ArrayAdapter<FxRate> {

	private LayoutInflater mInflater;
	
	public FxRateAdapter(Context context, int resource, List<FxRate> objects) {
		super(context, resource, objects);
		mInflater = LayoutInflater.from(context);
	}

	private class ViewHolder {
		MyTextView currencyText;
		MyTextView rateText;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;
		View view = convertView;
		if (view == null) {
			view = mInflater.inflate(R.layout.fxrate_item_layout, null);
			vh = new ViewHolder();
			vh.currencyText = (MyTextView) view.findViewById(R.id.currencyText);
			vh.rateText = (MyTextView) view.findViewById(R.id.rateText);
			view.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh.currencyText.setText(getItem(position).getCurrency());
		vh.rateText.setText(getItem(position).getRate());
		return view;
	}

	
}
