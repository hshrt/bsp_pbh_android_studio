package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;

public class FlightRouteAdapter extends ArrayAdapter<FlightInfo> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<FlightInfo> flightItems;

	private static String TAG = "AirportAdapter";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public FlightRouteAdapter(Context context, ArrayList<FlightInfo> items) {
		super(context, 0, items);
		this.flightItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return flightItems.size();
	}

	@Override
	public FlightInfo getItem(int position){
		return flightItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		FlightInfo flightInfo = flightItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.flight_route_item_layout, parent,
					false);
			holder = new ViewHolder();		
			
			holder.flightNum = (TextView) v.findViewById(R.id.flightRouteItemNumLabel);
			holder.airline = (TextView) v.findViewById(R.id.flightRouteItemAirlineLabel);
			holder.departTime = (TextView) v.findViewById(R.id.flightRouteItemDepartureLabel);
			holder.arriveTime = (TextView) v.findViewById(R.id.flightRouteItemArrivalLabel);
			holder.status = (TextView) v.findViewById(R.id.flightRouteItemStatsLabel);		
			
			Helper.setFonts(holder.flightNum, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.airline, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.departTime, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.arriveTime, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.status, mContext.getString(R.string.app_font));
			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}

		Log.i(TAG, "flightID = " + flightInfo.getFlightId());
		//holder.button.setText(itemName);
		holder.flightNum.setText(flightInfo.getCarrierCode()+ " " +flightInfo.getFlightNumber());
		holder.airline.setText(flightInfo.getAirline());
		holder.departTime.setText(flightInfo.getDepartureAirportCode() + "-" + flightInfo.getDepartureDate());
		holder.arriveTime.setText(flightInfo.getArrivalAirportCode() + "-" + flightInfo.getArrivalDate());
		
		holder.status.setText(flightInfo.getStatus());
		
		/*if(position == 0){
			firstButton = holder.button;
		}*/

		return v;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	static class ViewHolder {
		TextView flightNum;
		TextView airline;
		TextView departTime;
		TextView arriveTime;
		TextView status;
	}

}
