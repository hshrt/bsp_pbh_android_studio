package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.FlightInfo;

public class FlightAdapter extends ArrayAdapter<FlightInfo> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<FlightInfo> flightItems;

	private static String TAG = "AirportAdapter";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public FlightAdapter(Context context, ArrayList<FlightInfo> items) {
		super(context, 0, items);
		this.flightItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return flightItems.size();
	}

	@Override
	public FlightInfo getItem(int position){
		return flightItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		FlightInfo flightInfo = flightItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.flight_item, parent,
					false);
			holder = new ViewHolder();		
			
			holder.place = (TextView) v.findViewById(R.id.place);
			holder.flight = (TextView) v.findViewById(R.id.flight);
			holder.airline = (TextView) v.findViewById(R.id.airline);
			holder.time = (TextView) v.findViewById(R.id.time);
			holder.status = (TextView) v.findViewById(R.id.status);		
			
			Helper.setFonts(holder.place, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.flight, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.airline, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.time, mContext.getString(R.string.app_font));
			Helper.setFonts(holder.status, mContext.getString(R.string.app_font));
			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		if(position % 2 == 0){	
			v.setBackgroundResource(R.color.transparent_black);
		}
		else{
			v.setBackgroundResource(R.color.transparent);
		}
		
		//holder.button.setText(itemName);
		holder.place.setText(flightInfo.getPlace());
		holder.flight.setText(flightInfo.getFlightNumber());
		holder.airline.setText(flightInfo.getAirline());
		
		if(type == ServiceAirportActivity.ARRIVAL){
			holder.time.setText(flightInfo.getArrivalDate());
		}
		else{
			holder.time.setText(flightInfo.getDepartureDate());
		}
		
		holder.status.setText(flightInfo.getStatus());
		
		/*if(position == 0){
			firstButton = holder.button;
		}*/

		return v;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	static class ViewHolder {
		TextView place;
		TextView flight;
		TextView airline;
		TextView time;
		TextView status;
	}

}
