package com.hsh.hshservice.adapter;

import java.net.URL;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.network.XMLCaller;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hsh.hshservice.global.Helper;

public class CustomBaseAdapter extends BaseAdapter implements XMLCaller.InternetCallbacks{

	protected BaseFragment masterFragment;
	protected AsyncTask<URL, Void, String> currentTask;
	
	/////////////Adapter Methods////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	/////////////Getters Setters////////////////////////////////////////////////////////////////////////////////////////////////////////
	public Fragment getMasterFragment() {
		return masterFragment;
	}

	public void setMasterFragment(BaseFragment masterFragment) {
		this.masterFragment = masterFragment;
	}

	/////////////XML caller callback////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postExecute(String xml) {
		hideProgress();
		
	}
	
	/////////////Public Method////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void hideError(){
		masterFragment.errorPopup.setVisibility(View.GONE);
	}
	public void showError(){
		masterFragment.errorPopup.setVisibility(View.VISIBLE);
	}
	public void  hideProgress(){
		masterFragment.progressBar.setVisibility(View.GONE);
	}
	public void showProgress(){
		masterFragment.progressBar.setVisibility(View.VISIBLE);
	}
	protected int getOrientation(){
		return masterFragment.getActivity().getResources().getConfiguration().orientation;
	}
	public void setCurrentTask(AsyncTask<URL, Void, String> task){
		currentTask = task;
	}
	protected void cancelCurrentTask(){
		Log.i("Custom Base Adapter", "cancelCurrentTask");
		if (currentTask != null
				&& (currentTask.getStatus().equals(AsyncTask.Status.RUNNING) || currentTask.getStatus().equals(AsyncTask.Status.FINISHED))) {
			currentTask.cancel(true);
			Log.i("CustomBaseAdapter", "cancel currentTask");
			currentTask = null;
		}
	}

	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub
		
	}

}
