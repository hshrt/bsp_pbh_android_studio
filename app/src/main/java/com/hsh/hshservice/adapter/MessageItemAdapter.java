package com.hsh.hshservice.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.hsh.esdbsp.R;
import com.hsh.hshservice.MessageActivity;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.MessageItem;

public class MessageItemAdapter extends ArrayAdapter<MessageItem> {

	/**
	 * 
	 */
	private final LayoutInflater mInflater;
	
	private static String TAG = "MessageItemAdapter";
	
	private Button firstButton;	
	private View mParent;
	private Context mContext;
	private int currentPosition = 0;
	
	private ArrayList<MessageItem> messageItems;
		
	private class ViewHolder {
		public TextView textView;
		Button button;
	}
	
	public MessageItemAdapter(Context context, int resource, ArrayList<MessageItem> items) {
		super(context, resource, items);
		mInflater = LayoutInflater.from(context);
		messageItems = items;
		mContext = context;
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final ViewHolder holder;
		View v = convertView;
		
		String itemSubject = messageItems.get(position).getSubject();
		if (v == null) {
			
			mParent = parent;
			
			v = mInflater.inflate(R.layout.msg_list_item_layout, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			holder.textView = (TextView) v.findViewById(R.id.messageListItemTitle);
			
			Helper.setFonts(holder.textView, mContext.getString(R.string.app_font));
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.textView.setText(itemSubject);
		
		if(position == 0){
			firstButton = holder.button;
		}
		
		holder.button.setBackgroundResource(R.drawable.info_list_item_selector);
		
		if( position == currentPosition){
			holder.button.setBackgroundResource(R.drawable.listview_button_active);
		}
		
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i(TAG, ((ViewGroup)parent).getChildCount()+"");
				
				for(int i=0; i<((ViewGroup)parent).getChildCount(); i++) {
				    View nextChild = ((ViewGroup)parent).getChildAt(i);
				    Log.i(TAG, nextChild.getClass().toString());
				    if (nextChild instanceof Button){
				    	Log.i(TAG, "done");
				    	nextChild.setBackgroundResource(R.drawable.info_list_item_selector);
				    }
				    for(int y=0; y<((ViewGroup)nextChild).getChildCount(); y++) {
				    	View nextChild2 = ((ViewGroup)nextChild).getChildAt(y);
				    	if (nextChild2 instanceof Button){
					    	Log.i(TAG, "done");
					    	nextChild2.setBackgroundResource(R.drawable.info_list_item_selector);
					    }	
				    }
				}
				
				holder.button.setBackgroundResource(R.drawable.listview_button_active);
				
				if(mContext instanceof MessageActivity){
					((MessageActivity)mContext).onPressMessage(position);
				}
				
				currentPosition = position;
			}
		});
		
		return v;
	}
	public Button getFirstButton() {
		return firstButton;
	}

	public void setFirstButton(Button firstButton) {
		this.firstButton = firstButton;
	}
}