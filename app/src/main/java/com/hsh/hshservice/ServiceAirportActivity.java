package com.hsh.hshservice;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.FlightTrackFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.fragment.TrainFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.AirportAdapter;
import com.hsh.hshservice.adapter.FlightAdapter;
import com.hsh.hshservice.adapter.TimeAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airline;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetFlightStatusParser;
import com.hsh.hshservice.parser.GetFlightStatusParser.GetFlightStatusParserInterface;

public class ServiceAirportActivity extends BaseActivity implements
		GetFlightStatusParserInterface {

	private String TAG = "ServiceAirportActivity";
	private static int NUM_OF_HOUR_RANGE = 4;
	private static int CURRENT_TIME_CODE = 7;

	public static int ARRIVAL = 1;
	public static int DEPARTURE = 2;

	private int current_choice;// choose arrvial or departure
	private int current_time_choice;

	private int current_airport_choice = 0;

	private String current_time;

	private LinearLayout container;
	private LinearLayout flightTrackContainer;

	private ListView planeList;
	private ListView timeList;
	private ListView airportList;

	private FlightAdapter flightAdapter;
	private TimeAdapter timeAdapter;
	private AirportAdapter airportAdapter;

	private Button arrialButton;
	private Button departButton;
	private Button airportButton;
	private Button timeButton;

	private MyTextView placeHead;
	private MyTextView flightHead;
	private MyTextView airlineHead;
	private MyTextView timeHead;
	private MyTextView statusHead;

	private String parentId;

	private ArrayList<FlightInfo> flightInfoArray;
	private ArrayList<String> timeRangeArray;
	private ArrayList<Airport> airportArray;

	private FlightTrackFragment flightTrackFragment;

	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;

	// a key for what is the current topic of this page and what to get from DB
	private String objectString;

	public enum ApiCallType {
		GET_FLIGHT_INFO
	}

	ApiCallType apiCallType;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, TAG + "onCreate");
		super.onCreate(savedInstanceState);

		Log.i(TAG, "date = " + MainApplication.getMAS().getData("data_curdate"));
		Log.i(TAG, "time = " + MainApplication.getMAS().getData("data_curtime"));

		Log.i(TAG, "year = " + MainApplication.getMAS().getData("year"));
		Log.i(TAG, "month = " + MainApplication.getMAS().getData("month"));
		Log.i(TAG, "day = " + MainApplication.getMAS().getData("day"));
		Log.i(TAG, "hour = " + MainApplication.getMAS().getData("hour"));
		Log.i(TAG, "minute = " + MainApplication.getMAS().getData("minute"));

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.airport_flight_info);

		mDialog = new ProgressDialog(this);

		MainApplication.setCurrentActivity(this);

		Helper.showInternetWarningToast();

		initUI();

		parentId = getIntent().getStringExtra("parentId");

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", "Flight");

		// Create new fragment and transaction
		topBarFragment = new TopBarFragment();
		topBarFragment.setArguments(bundle);

		BottomBarFragment bottomBarFragment = new BottomBarFragment();

		FragmentTransaction transaction = fragmentManager.beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
		transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);

		// Commit the transaction
		transaction.commit();

		/*
		 * if(objectString.equalsIgnoreCase("hotel_restaurants")){
		 * bgPhoto.setImageResource(R.drawable.restaurant); }
		 */

		if (flightInfoArray == null) {
			flightInfoArray = new ArrayList<FlightInfo>();
		}

		if (timeRangeArray == null) {
			timeRangeArray = new ArrayList<String>();
		}
		if (airportArray == null) {
			airportArray = new ArrayList<Airport>();
		}
		if (ServiceAirportActivity.this.getString(R.string.hotel)
				.equalsIgnoreCase("pch")) {
			
			  Airport airport0 = new Airport(); airport0.setIATA("ORD");
			  airport0.setName("O'Hare International Airport"); Airport
			  airport1 = new Airport(); airport1.setIATA("MDW");
			  airport1.setName("Chicago Midway International Airport"); Airport
			  airport2 = new Airport(); airport2.setIATA("RFD");
			  airport2.setName("Chicago Rockford International Airport");
			  
			 airportArray.add(airport0); airportArray.add(airport1);
			 airportArray.add(airport2);
			 
		}

		Log.i(TAG, "length = " + flightInfoArray.size());

		if (flightAdapter == null) {
			flightAdapter = new FlightAdapter(this, flightInfoArray);
			planeList.setAdapter(flightAdapter);
		}

		if (timeAdapter == null) {
			timeAdapter = new TimeAdapter(this, timeRangeArray);
			timeList.setAdapter(timeAdapter);
		}

		if (airportAdapter == null) {
			airportAdapter = new AirportAdapter(this, airportArray);
			airportAdapter.setSetOnClickInAdapter(false);
			airportList.setAdapter(airportAdapter);
		}

		airportList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> myAdapter, View myView,
					int pos, long mylng) {

				current_airport_choice = pos;

				if (current_choice == ARRIVAL) {
					arrialButton.performClick();
				} else {
					departButton.performClick();
				}

				airportButton.setText(airportArray.get(pos).getName());
			}

		});

		timeList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> myAdapter, View myView,
					int pos, long mylng) {

				if (pos == 0) {
					current_time_choice = CURRENT_TIME_CODE;
				} else {
					current_time_choice = (pos - 1) * 4;

					Log.i(TAG, "current_time_Choice = " + current_time_choice);
				}

				if (current_choice == ARRIVAL) {
					arrialButton.performClick();
				} else {
					departButton.performClick();
				}

				timeButton.setText(timeRangeArray.get(pos));
			}

		});

		current_time_choice = CURRENT_TIME_CODE;
		arrialButton.performClick();

		if (timeRangeArray.size() > 0)
			timeButton.setText(timeRangeArray.get(0));

		setupTimeRange();

	}

	private void initUI() {

		arrialButton = (Button) findViewById(R.id.arrialButton);
		departButton = (Button) findViewById(R.id.departButton);
		airportButton = (Button) findViewById(R.id.airportButton);
		timeButton = (Button) findViewById(R.id.timeButton);

		placeHead = (MyTextView) findViewById(R.id.placeHead);
		flightHead = (MyTextView) findViewById(R.id.flightHead);
		airlineHead = (MyTextView) findViewById(R.id.airlineHead);
		timeHead = (MyTextView) findViewById(R.id.timeHead);
		statusHead = (MyTextView) findViewById(R.id.statusHead);

		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);

		planeList = (ListView) findViewById(R.id.planeList);

		LayoutInflater inflater = getLayoutInflater();

		timeList = (ListView) findViewById(R.id.timeList);
		airportList = (ListView) findViewById(R.id.airportList);

		errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);

		Helper.setFonts(arrialButton, getString(R.string.app_font));
		Helper.setFonts(departButton, getString(R.string.app_font));
		Helper.setFonts(airportButton, getString(R.string.app_font));
		Helper.setFonts(timeButton, getString(R.string.app_font));

		arrialButton.setText(MainApplication
				.getLabel("flightstats.arrivals.label"));
		departButton.setText(MainApplication
				.getLabel("flightstats.departures.label"));

		if (ServiceAirportActivity.this.getString(R.string.hotel)
				.equalsIgnoreCase("pch")) {
			 airportButton.setText("("+this.getString(R.string.pch_local_airport)+") "
			 + "O'Hare International Airport");
			/*airportButton.setText("("
					+ this.getString(R.string.pbj_local_airport) + ") "
					+ "Beijing Capital International Airport");*/
		} else if (ServiceAirportActivity.this.getString(R.string.hotel)
				.equalsIgnoreCase("PHK_R")) {
			airportButton.setText("("
					+ this.getString(R.string.phk_local_airport) + ") "
					+ "Hong Kong International Airport");
		}

		placeHead.setText(MainApplication
				.getLabel("flightstats.destination.label"));
		flightHead
				.setText(MainApplication.getLabel("flightstats.flight.label"));
		airlineHead.setText(MainApplication
				.getLabel("flightstats.airline.label"));
		timeHead.setText(MainApplication.getLabel("flightstats.time.label"));
		statusHead
				.setText(MainApplication.getLabel("flightstats.status.label"));

		container = (LinearLayout) findViewById(R.id.container);
		// container.setVisibility(View.GONE);

		flightTrackContainer = (LinearLayout) findViewById(R.id.flightTrackContainer);
		// flightTrackContainer.setVisibility(View.VISIBLE);

		// Helper.setFonts(descriptionText, getString(R.string.app_font));

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.arrialButton:

					// for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("Arrivals", map);

					current_choice = ARRIVAL;
					flightAdapter.setType(ARRIVAL);

					setupTimeRange();

					flightInfoArray.clear();
					flightAdapter.notifyDataSetChanged();

					planeList.setVisibility(View.VISIBLE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.GONE);

					departButton
							.setBackgroundResource(R.drawable.base_btn_selector);
					arrialButton.setBackgroundResource(R.drawable.button_on);

					getFlightInfo("arrival", current_time_choice);
					break;
				case R.id.departButton:

					// for Flurry log
					final Map<String, String> map2 = new HashMap<String, String>();
					map2.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("Departures", map2);

					setupTimeRange();
					current_choice = DEPARTURE;
					flightAdapter.setType(DEPARTURE);

					flightInfoArray.clear();
					flightAdapter.notifyDataSetChanged();

					planeList.setVisibility(View.VISIBLE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.GONE);

					departButton.setBackgroundResource(R.drawable.button_on);
					arrialButton
							.setBackgroundResource(R.drawable.base_btn_selector);

					getFlightInfo("departure", current_time_choice);
					break;
				case R.id.airportButton:
					// for Flurry log
					final Map<String, String> airportRange = new HashMap<String, String>();
					airportRange.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("TimeRange", airportRange);

					/* setupTimeRange(); */
					planeList.setVisibility(View.GONE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.VISIBLE);
					break;
				case R.id.timeButton:

					// for Flurry log
					final Map<String, String> mapTimeRange = new HashMap<String, String>();
					mapTimeRange.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("TimeRange", mapTimeRange);

					setupTimeRange();
					planeList.setVisibility(View.GONE);
					timeList.setVisibility(View.VISIBLE);
					airportList.setVisibility(View.GONE);
					break;
				case R.id.errorMessageDialogCloseImage:
					errorMessageDialogLayout.setVisibility(View.GONE);
					break;
				default:
					break;
				}
			}
		};

		arrialButton.setOnClickListener(onClickListener);
		departButton.setOnClickListener(onClickListener);
		airportButton.setOnClickListener(onClickListener);
		timeButton.setOnClickListener(onClickListener);
		errorMessageDialogCloseImage.setOnClickListener(onClickListener);

	}

	public void onFlightInfoPressed() {
		container.setVisibility(View.VISIBLE);
		flightTrackContainer.setVisibility(View.GONE);

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		FlurryAgent.logEvent("FlightInformation", map);

		if (flightTrackFragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = fragmentManager
					.beginTransaction();

			transaction.remove(flightTrackFragment);

			// Commit the transaction
			transaction.commit();

			flightTrackFragment = null;
		}

		arrialButton.performClick();
	};

	public void onFlightTrackPressed() {
		container.setVisibility(View.GONE);
		flightTrackContainer.setVisibility(View.VISIBLE);

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		FlurryAgent.logEvent("FlightTracking", map);

		if (getCurrentTask() != null)
			getCurrentTask().cancel(true);

		hideProgress();

		if (flightTrackFragment == null) {

			FragmentManager fragmentManager = getSupportFragmentManager();

			FragmentTransaction transaction = fragmentManager
					.beginTransaction();

			flightTrackFragment = new FlightTrackFragment();
			transaction.replace(R.id.flightTrackContainer, flightTrackFragment);

			// Commit the transaction
			transaction.commit();
		}
	}

	public void onAirlinePressed(int position, Airline _airline) {
		if (flightTrackFragment != null) {
			flightTrackFragment.onAirlinePressed(position, _airline);
		}
	}

	public void onAirportPressed(int position, Airport _airport) {
		if (flightTrackFragment != null) {
			flightTrackFragment.onAirportPressed(position, _airport);
		}
	}

	private void setupTimeRange() {
		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();

		try {
			// String additionString =
			// MainApplication.getMAS().getData("minute");

			String supply0 = "";
			String time0 = "";

			Log.i(TAG,
					"the hour length = "
							+ MainApplication.getMAS().getData("hour").length());

			if (MainApplication.getMAS().getData("hour").length() > 0) {

				if ((Integer.parseInt(MainApplication.getMAS().getData("hour")) + 5) % 24 < 10) {
					supply0 = "0";
				}
				time0 = MainApplication.getMAS().getData("hour")
						+ ":"
						+ "00"
						+ " - "
						+ supply0
						+ (Integer.parseInt(MainApplication.getMAS().getData(
								"hour")) + 5) % 24 + ":" + "00";
			} else {
				if ((today.hour + 5) % 24 < 10) {
					supply0 = "0";
				}
				time0 = today.hour + ":" + "00" + " - " + supply0
						+ (today.hour + 5) % 24 + ":" + "00";
			}

			// timeButton.setText(time0);

			timeRangeArray.clear();
			timeRangeArray.add(time0);
			timeRangeArray.add("00:00 - 04:00");
			timeRangeArray.add("04:00 - 08:00");
			timeRangeArray.add("08:00 - 12:00");
			timeRangeArray.add("12:00 - 16:00");
			timeRangeArray.add("16:00 - 20:00");
			timeRangeArray.add("20:00 - 24:00");

			timeAdapter.notifyDataSetChanged();
		} catch (Exception e) {

		}
	}

	private void showError(String msg) {
		errorMessageDialogLayout.setVisibility(View.VISIBLE);
		errorMessageDialogLabel.setText(msg);
	}

	private void getFlightInfo(String type, int _startHour) {
		/*
		 * mDialog.setCancelable(false);
		 * mDialog.setMessage(MainApplication.getLabel("loading"));
		 * 
		 * showDialog();
		 */
		showProgress();

		Log.i(TAG, "getFlightInfo start");
		apiCallType = ApiCallType.GET_FLIGHT_INFO;

		try {

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();

			String apiType = "";
			int startHour = 0;

			if (type.equalsIgnoreCase("arrival")) {
				apiType = this.getString(R.string.arrival);
			} else {
				apiType = this.getString(R.string.departure);
			}

			startHour = _startHour;

			if (_startHour == CURRENT_TIME_CODE
					&& MainApplication.getMAS().getData("hour").length() > 0) {
				startHour = Integer.parseInt(MainApplication.getMAS().getData(
						"hour"));
			} else {
				startHour = today.hour;
			}

			URL url = null;
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/HKG/arr/2014/10/11/23?appId=1b5bbebb&appKey=0bd343eb43861dbd7c82996a3f8d07ac&utc=false&numHours=1&maxFlights=5

			String airportCode = this.getString(R.string.pbh_local_airport);

			if (this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {
				airportCode = this.getString(R.string.phk_local_airport);
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
				airportCode = this.getString(R.string.pbj_local_airport);
				// airportCode =
				// airportArray.get(current_airport_choice).getIATA();
			}
			Log.i("PLANE", MainApplication.getMAS().getData("year"));
			Log.i("PLANE", MainApplication.getMAS().getData("month"));
			Log.i("PLANE", MainApplication.getMAS().getData("day"));
			Log.i("PLANE", MainApplication.getMAS().getData("hour"));

			url = new URL(
					this.getString(R.string.flightstat_api_base)
							+ "airport/"
							+ "status/"
							+ airportCode
							+ "/"
							+ apiType
							+ "/"
							+ MainApplication.getMAS().getData("year")
							+ "/"
							+ MainApplication.getMAS().getData("month")
							+ "/"
							+ MainApplication.getMAS().getData("day")
							+ "/"
							+ startHour
							+ "/"
							+ "?appId="
							+ this.getString(R.string.flightstat_app_id)
							+ "&appKey="
							+ this.getString(R.string.flightstat_app_token)
							+ "&utc=false&numHours="
							+ (int) (current_time_choice == CURRENT_TIME_CODE ? NUM_OF_HOUR_RANGE + 1
									: NUM_OF_HOUR_RANGE)
							+ "&maxFlights=150"
							+ "&extendedOptions=languageCode:"
							+ Helper.changeLangCode(
									MainApplication.getMAS().getData(
											"data_language") == "" ? "E"
											: MainApplication.getMAS().getData(
													"data_language"))
									.toString());

			if (MainApplication.getMAS().getData("year").equalsIgnoreCase("")) {
				Log.i("PLANE", "use local");

				url = new URL(
						this.getString(R.string.flightstat_api_base)
								+ "airport/"
								+ "status/"
								+ airportCode
								+ "/"
								+ apiType
								+ "/"
								+ today.year
								+ "/"
								+ (today.month + 1)
								+ "/"
								+ today.monthDay
								+ "/"
								+ startHour
								+ "/"
								+ "?appId="
								+ this.getString(R.string.flightstat_app_id)
								+ "&appKey="
								+ this.getString(R.string.flightstat_app_token)
								+ "&utc=false&numHours="
								+ (int) (current_time_choice == CURRENT_TIME_CODE ? NUM_OF_HOUR_RANGE + 1
										: NUM_OF_HOUR_RANGE)
								+ "&maxFlights=150"
								+ "&extendedOptions=languageCode:"
								+ Helper.changeLangCode(
										MainApplication.getMAS().getData(
												"data_language") == "" ? "E"
												: MainApplication
														.getMAS()
														.getData(
																"data_language"))
										.toString());
			}

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		hideProgress();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideProgress();
		if (apiCallType == ApiCallType.GET_FLIGHT_INFO) {
			GetFlightStatusParser parser = new GetFlightStatusParser(json, this);
			parser.setType(current_choice);
			parser.startParsing();
		}
	}

	@Override
	public void onGetFlightStatusParsingError(int failMode,
			boolean isPostExecute) {

		showError(MainApplication.getLabel("flightstats.empty.label"));
	}

	@Override
	public void onGetFlightStatusFinishParsing(
			ArrayList<FlightInfo> _flightArrivalArray) {
		// TODO Auto-generated method stub
		hideProgress();

		flightInfoArray.clear();
		flightInfoArray.addAll(_flightArrivalArray);
		Log.i(TAG, flightInfoArray.toString());

		if (current_choice == ARRIVAL) {
			// Now sort by the flight info base on arrival date.
			Collections.sort(flightInfoArray, new Comparator<FlightInfo>() {
				public int compare(FlightInfo one, FlightInfo other) {
					return one.getArrivalDate().compareTo(
							other.getArrivalDate());
				}
			});
		} else if (current_choice == DEPARTURE) {
			// Now sort by the flight info base on departure date.
			Collections.sort(flightInfoArray, new Comparator<FlightInfo>() {
				public int compare(FlightInfo one, FlightInfo other) {
					return one.getDepartureDate().compareTo(
							other.getDepartureDate());
				}
			});
		}

		flightAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetFlightStatusError() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		
		  if (flightTrackFragment != null && flightTrackFragment.isMapShown())
		  { flightTrackFragment.hideMap(); } else { super.onBackPressed(); }
		 
	}

}