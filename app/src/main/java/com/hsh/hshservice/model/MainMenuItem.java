package com.hsh.hshservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MainMenuItem implements Parcelable {
	private String itemName;

	public MainMenuItem(String name) {
		itemName = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(itemName);
	}

	public static final Parcelable.Creator<MainMenuItem> CREATOR = new Parcelable.Creator<MainMenuItem>() {
		public MainMenuItem createFromParcel(Parcel in) {
			return new MainMenuItem(in);
		}

		public MainMenuItem[] newArray(int size) {
			return new MainMenuItem[size];
		}
	};

	private MainMenuItem(Parcel in) {
		itemName = in.readString();
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	

}
