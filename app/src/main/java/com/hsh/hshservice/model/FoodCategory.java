package com.hsh.hshservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FoodCategory implements Parcelable {
	private String id;
	private String name_en;

	public FoodCategory(String _name) {
		name_en = _name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(id);
		out.writeString(name_en);
	}

	public static final Parcelable.Creator<FoodCategory> CREATOR = new Parcelable.Creator<FoodCategory>() {
		public FoodCategory createFromParcel(Parcel in) {
			return new FoodCategory(in);
		}

		public FoodCategory[] newArray(int size) {
			return new FoodCategory[size];
		}
	};

	private FoodCategory(Parcel in) {
		id = in.readString();
		name_en = in.readString();
	}

	public String getName() {
		return name_en;
	}

	public void setName(String _name) {
		this.name_en = _name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	

}
