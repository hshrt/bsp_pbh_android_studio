package com.hsh.hshservice.model;

public class Airline {
	
	private String IATA;
	private String name;
	
	public Airline() {
		
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getIATA() {
		return IATA;
	}


	public void setIATA(String iATA) {
		IATA = iATA;
	}
	
	
}