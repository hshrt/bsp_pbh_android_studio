package com.hsh.hshservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Dictionary implements Parcelable {
	private String dictId;
	private String key;
	private String en;
	private String zh_cn;
	private String fr;
	private String jp;
	private String ar;
	private String es;
	private String de;
	private String ko;
	private String ru;
	private String pt;
	private String zh_hk;

	public Dictionary(String name) {
		dictId = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(dictId);
		out.writeString(key);
		out.writeString(en);
		out.writeString(zh_cn);
		out.writeString(fr);
		out.writeString(jp);
		out.writeString(ar);
		out.writeString(es);
		out.writeString(de);
		out.writeString(ko);
		out.writeString(ru);
		out.writeString(pt);
		out.writeString(zh_hk);

	}

	public static final Parcelable.Creator<Dictionary> CREATOR = new Parcelable.Creator<Dictionary>() {
		public Dictionary createFromParcel(Parcel in) {
			return new Dictionary(in);
		}

		public Dictionary[] newArray(int size) {
			return new Dictionary[size];
		}
	};

	private Dictionary(Parcel in) {
		
		dictId = in.readString();
		key = in.readString();
		en = in.readString();
		zh_cn = in.readString();
		fr = in.readString();
		jp = in.readString();
		ar = in.readString();
		es = in.readString();
		de = in.readString();
		ko = in.readString();
		ru = in.readString();
		pt = in.readString();
		zh_hk = in.readString();
		
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEn() {
		return en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public String getZh_cn() {
		return zh_cn;
	}

	public void setZh_cn(String zh_cn) {
		this.zh_cn = zh_cn;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getJp() {
		return jp;
	}

	public void setJp(String jp) {
		this.jp = jp;
	}

	public String getAr() {
		return ar;
	}

	public void setAr(String ar) {
		this.ar = ar;
	}

	public String getEs() {
		return es;
	}

	public void setEs(String es) {
		this.es = es;
	}

	public String getDe() {
		return de;
	}

	public void setDe(String de) {
		this.de = de;
	}

	public String getKo() {
		return ko;
	}

	public void setKo(String ko) {
		this.ko = ko;
	}

	public String getRu() {
		return ru;
	}

	public void setRu(String ru) {
		this.ru = ru;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}

	public String getZh_hk() {
		return zh_hk;
	}

	public void setZh_hk(String zh_hk) {
		this.zh_hk = zh_hk;
	}
	
	public String getLang(String code){
		String r = "";
		switch (code){
			case "E": r = getEn();break;
			case "CT": r = getZh_hk();break;
			case "CS": r = getZh_cn();break;
			case "J": r = getJp();break;
			case "F": r = getFr();break;
			case "A": r = getAr();break;
			case "S": r = getEs();break;
			case "G": r = getDe();break;
			case "K": r = getKo();break;
			case "R": r = getRu();break;
			case "P": r = getPt();break;
			 default:r = getEn();break;
		}
		return r;
	}
}
