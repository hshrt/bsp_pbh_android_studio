package com.hsh.hshservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GeneralItem implements Parcelable {
	private String itemId;
	private String titleId;
	private String descriptionId;
	private String parentId;
	private String type;
	private double price;
	private int order;
	private int layout; //type of layout appear in "In Room Dining", only for item in In Room Dining
	private String command; //the command that needed to be send to HOTSOS
	private String availTime; //temporary variable for PCH in Room Dinning, will delete when develop a full In Room Dinning
	private String imageName;

	public GeneralItem(String name) {
		itemId = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(itemId);
		out.writeString(titleId);
		out.writeString(descriptionId);
		out.writeString(parentId);
		out.writeString(type);
		out.writeInt(order);
		out.writeInt(layout);
		out.writeDouble(price);
		out.writeString(imageName);
		out.writeString(command);
		out.writeString(availTime);
	}

	public static final Parcelable.Creator<GeneralItem> CREATOR = new Parcelable.Creator<GeneralItem>() {
		public GeneralItem createFromParcel(Parcel in) {
			return new GeneralItem(in);
		}

		public GeneralItem[] newArray(int size) {
			return new GeneralItem[size];
		}
	};

	private GeneralItem(Parcel in) {
		itemId = in.readString();
		titleId = in.readString();
		descriptionId = in.readString();
		price = in.readInt();
		setParentId(in.readString());
		setType(in.readString());
		setOrder(in.readInt());
		setImageName(in.readString());
		command = in.readString();
		availTime = in.readString();
		layout = in.readInt();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDescriptionId() {
		return descriptionId;
	}

	public void setDescriptionId(String descriptionId) {
		this.descriptionId = descriptionId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAvailTime() {
		return availTime;
	}

	public void setAvailTime(String availTime) {
		this.availTime = availTime;
	}

	

}
