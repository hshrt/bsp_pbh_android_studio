package com.hsh.hshservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Food implements Parcelable {
	private String id;
	private String food_sub_cat_id;
	private String code;
	private String price;
	private String name;
	private String description;

	public Food(String _name) {
		name = _name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(id);
		out.writeString(food_sub_cat_id);
		out.writeString(code);
		out.writeString(price);
		out.writeString(name);
		out.writeString(description);
	}

	public static final Parcelable.Creator<Food> CREATOR = new Parcelable.Creator<Food>() {
		public Food createFromParcel(Parcel in) {
			return new Food(in);
		}

		public Food[] newArray(int size) {
			return new Food[size];
		}
	};

	private Food(Parcel in) {
		id = in.readString();
		food_sub_cat_id = in.readString();
		code = in.readString();
		price = in.readString();
		name = in.readString();
		description = in.readString();
	}

	public String getName() {
		return name;
	}

	public void setName(String _name) {
		this.name = _name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFood_sub_cat_id() {
		return food_sub_cat_id;
	}

	public void setFood_sub_cat_id(String food_sub_cat_id) {
		this.food_sub_cat_id = food_sub_cat_id;
	}

}
