package com.hsh.hshservice.model;

public class Train {
	
	private String code;
	private String arriTime;
	private int sort;
	private String interval;
	private String station;
	private String arriCity;
	private String deptTimeRange;
	private String arriStation;
	private String dayAfter;
	private int intervalSort;
	private String deptTime;
	private String deptStation;
	private String arriTimeRange;
	private String deptCity;
	private String trainType;
	private String arriCity_py;
	private String deptCity_py;
	private String stationType;
	private String tType;
	
	public Train() {
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getArriTime() {
		return arriTime;
	}

	public void setArriTime(String arriTime) {
		this.arriTime = arriTime;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getArriCity() {
		return arriCity;
	}

	public void setArriCity(String arriCity) {
		this.arriCity = arriCity;
	}

	public String getDeptTimeRange() {
		return deptTimeRange;
	}

	public void setDeptTimeRange(String deptTimeRange) {
		this.deptTimeRange = deptTimeRange;
	}

	public String getArriStation() {
		return arriStation;
	}

	public void setArriStation(String arriStation) {
		this.arriStation = arriStation;
	}

	public String getDayAfter() {
		return dayAfter;
	}

	public void setDayAfter(String dayAfter) {
		this.dayAfter = dayAfter;
	}

	public int getIntervalSort() {
		return intervalSort;
	}

	public void setIntervalSort(int intervalSort) {
		this.intervalSort = intervalSort;
	}

	public String getDeptTime() {
		return deptTime;
	}

	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}

	public String getDeptStation() {
		return deptStation;
	}

	public void setDeptStation(String deptStation) {
		this.deptStation = deptStation;
	}

	public String getArriTimeRange() {
		return arriTimeRange;
	}

	public void setArriTimeRange(String arriTimeRange) {
		this.arriTimeRange = arriTimeRange;
	}

	public String getDeptCity() {
		return deptCity;
	}

	public void setDeptCity(String deptCity) {
		this.deptCity = deptCity;
	}

	public String getTrainType() {
		return trainType;
	}

	public void setTrainType(String trainType) {
		this.trainType = trainType;
	}

	public String getArriCity_py() {
		return arriCity_py;
	}

	public void setArriCity_py(String arriCity_py) {
		this.arriCity_py = arriCity_py;
	}

	public String getDeptCity_py() {
		return deptCity_py;
	}

	public void setDeptCity_py(String deptCity_py) {
		this.deptCity_py = deptCity_py;
	}

	public String getStationType() {
		return stationType;
	}

	public void setStationType(String stationType) {
		this.stationType = stationType;
	}

	public String gettType() {
		return tType;
	}

	public void settType(String tType) {
		this.tType = tType;
	}

	
	
}