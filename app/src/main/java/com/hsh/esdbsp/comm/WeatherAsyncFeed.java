package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;


public class WeatherAsyncFeed extends AsyncTask<Void, String, Void> {
	
	static String myLOG = "WeatherAsyncFeed";
	
	static int myWindDir = 0;
	
	static Handler mWeatherHandler = new Handler();
	static int mWeatherDelay = 15*60*1000;
	
	static WeatherAsyncFeed mWeatherFeed;
	
 
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub

        try {    
        	
        	URL ww = new URL("http://" + MainApplication.getContext().getString(R.string.weather_path) + "/wdata.json");             
        	URLConnection tc = ww.openConnection();             
        	
        	BufferedReader in = new BufferedReader(new InputStreamReader(                     
        			tc.getInputStream()));               
        	
        	String line = "";
        	String fullJSON = "";
        	while ((line = in.readLine()) != null) {  
        		fullJSON = fullJSON + line;    
        	}     
        	
        	MainApplication.getWeather().setJSON(fullJSON);
        	
        	in.close();

        } catch (Exception e) {
        	
        	MainApplication.getWeather().getWeatherFeed();
        }
		
		return null;
	}

 }

