package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.Menu;

public class MASComm {

	private static String myLOG = "MASComm";

	private static String mMASBase = "";
	private static String mMASPath = "";

	private static String mBSPBase = "";

	private static String data_myroom = "";
	private static String data_roomtype = "";

	private static String data_curdate = "";
	private static String data_curtime = "";
	private static String data_language = "";
	private static String data_mur = "";
	private static String data_dnd = "";
	private static String data_valet = "";
	private static String data_valetbox = "";
	private static String data_tdnd = "";
	private static String data_roommasterlight = "";
	private static String data_roomlight = "";
	private static String data_nightlight = "";
	private static String data_temperature = "";
	private static String data_temperature_cf = "";
	private static String data_fan = "";
	private static String data_alarm = "";
	private static String data_alarm_hr = "";
	private static String data_alarm_min = "";
	private static String data_alarm_snooze = "";
	private static String data_alarm_time = "";
	private static String data_alarm_settime = "";
	
	private static String data_fax = "";
	private static String data_msg = "";

	private static String data_havemsg = "";
	private static String data_soundbarmute = "";

	private static boolean data_die = false;

	private static MASAsyncDataFeed mMASFeed;
	private static MASAsyncDataSend mMASSend;

	static Handler mMASSendHandler = new Handler();
	static int mMASSendDelay = 300;

	static Handler mMASHandler = new Handler();
	static int mMASDelay = 5 * 60 * 1000; // 5 min will clear once and do again

	static String mCMD = "";

	static int myCurRoom = 100; // 100 - 104
	static int myCurSpinnerPos = 0; 
	
	
	static String curMASIPPref = "192.168.255.";
	
	// define to getIpAddr or not 
	static int isFirstTime = 1;
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
//////////////////////////////////////////////////////////////////////////////////////
	
	
	public static void specialSend(String cmd){
		InputStream inputStream = null;
		String result = "";

		String url = "http://" + mMASPath + "/mas.php?cmd=" + cmd;

		try {

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();
	
			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
	
			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();
	


		} catch (Exception e) {
			Log.v(myLOG,e.toString());
		}

	}



//////////////////////////////////////////////////////////////////////////////////////	
	

	private static Runnable mMASSendRunnable = new Runnable() {

		public void run() {
			try {
				mMASSendHandler.removeCallbacks(mMASSendRunnable);

				try {
					if (mMASSend != null) {
						mMASSend.cancel(true);
					}
				} catch (Exception e) {
				}

				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
									mCMD);
				else
					mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
							.execute(mCMD);
				
				
				//specialSend(mCMD);
				
				//mMASSend = (MASAsyncDataSend) new MASAsyncDataSend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,	mCMD);				

			} catch (Exception e) {
			}
		}
	};
		

	public static void sendMASCmd(String cmd, int nowFire) {

		try {
			mCMD = cmd;
			
			//if (MainApplication.getScreenSta() == 1) {

				if (nowFire == 1) {
	
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
						mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
								.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
										mCMD);
					else
						mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
								.execute(mCMD);
					

				
					//specialSend(mCMD);
					
					//mMASSend = (MASAsyncDataSend) new MASAsyncDataSend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,	mCMD);					
	
					
				} else {
	
					mMASSendHandler.removeCallbacks(mMASSendRunnable);
					mMASSendHandler.postDelayed(mMASSendRunnable, mMASSendDelay);
				}
				
			//}

			Log.v(myLOG, cmd);

		} catch (Exception e) {
			
			Log.v(myLOG, e.toString());
		}

	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////

	private static Runnable mMASRunnable = new Runnable() {

		public void run() {
			try {
				mMASHandler.removeCallbacks(mMASRunnable);

				try {
					if (mMASFeed != null) {
						mMASFeed.cancel(true);
					}
				} catch (Exception e) {
				}
				
				
				//mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);				
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed()
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				else
					mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed()
							.execute();

				mMASHandler.removeCallbacks(mMASRunnable);
				mMASHandler.postDelayed(mMASRunnable, mMASDelay);

			} catch (Exception e) {

				mMASHandler.removeCallbacks(mMASRunnable);
				mMASHandler.postDelayed(mMASRunnable, mMASDelay);
			}
		}
	};

	public void getMASFeed() {

		try {
			
			//Log.v(myLOG,"mas feed:" + getMASPath());
			
			// it will continue feed itself
			try {
				if (mMASFeed != null) {
					mMASFeed.cancel(true);
				}
			} catch (Exception e) {
			}

			
			//mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);			
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) 
				mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			else
				mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed().execute();

			mMASHandler.removeCallbacks(mMASRunnable);
			mMASHandler.postDelayed(mMASRunnable, mMASDelay);

		} catch (Exception e) {
		}

	}

	// ////////////////////////////////////////////////////////////////////
	
	// for the room control spinner  
	public void setSelectedArea(int i) {

		myCurSpinnerPos = i;
	}
	
	public int getSelectedArea() {

		return myCurSpinnerPos;
	}	
	
	
	
	///////////////////////////////////////////////////////////////////////

	// for the mas IP and comm 
	public void setCurRoom(int i) {

		myCurRoom = i;

		mMASPath = setupMASPath();

		getMASFeed();
	}

	public int getCurRoom() {

		return myCurRoom;
	}

	public String getMASPath() {

		if ((mMASPath == null) || (mMASPath == "")) {

			mMASPath = setupMASPath();
		}

		//Log.v(myLOG,"MAS:" + mMASPath);
		return (mMASPath);
	}

	public String setupMASPath() {

/*		if (getCurRoom() == 100) {
			mMASBase = MainApplication.getContext()
					.getString(R.string.mas_base);

		} else if (getCurRoom() == 101) {
			mMASBase = MainApplication.getContext().getString(
					R.string.maslr_base);
			
		} else if (getCurRoom() == 102) {
			mMASBase = MainApplication.getContext().getString(
					R.string.massb_base);
			
		} else if (getCurRoom() == 103) {
			mMASBase = MainApplication.getContext().getString(
					R.string.masdr_base);	
			
		} else if (getCurRoom() == 104) {
			mMASBase = MainApplication.getContext().getString(
					R.string.masor_base);	
			
		}*/
		
			
/*		if (getCurRoom() == 1) {
			mMASBase = MainApplication.getContext()
					.getString(R.string.mas_base);

		} else if (getCurRoom() == 0) {
			mMASBase = MainApplication.getContext().getString(
					R.string.maslr_base);
		} else {
			// default
			mMASBase = MainApplication.getContext()
					.getString(R.string.mas_base);
		}*/

		//mMASPath = getIpAddr() + mMASBase;
		
		
		
		//Log.v(myLOG,"MAS:" + mMASPath);
//		
//		if (isFirstTime == 1) {
//			isFirstTime = 0;
//			
//			curMASIPPref = getIpAddr();
//			mMASPath = curMASIPPref + getCurRoom();
//			
//		} else {
//			mMASPath = curMASIPPref + getCurRoom();
//			
//		}
		
		
		curMASIPPref = getIpAddr();
		mMASPath = curMASIPPref + getCurRoom();
		

		return (mMASPath);
	}

	public void setData(String name, String... value) {
		try {

			try {
				if (name.equalsIgnoreCase("myroom")) {
					data_myroom = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("roomtype")) {
					data_roomtype = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("mur")) {
					data_mur = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("dnd")) {
					data_dnd = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("valet")) {
					data_valet = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("temperature")) {
					data_temperature = value[0];
					data_temperature_cf = value[1];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("fan")) {
					data_fan = value[0];
				}

			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("alarm")) {
					data_alarm = value[0];
					data_alarm_hr = value[1];
					data_alarm_min = value[2];
					data_alarm_snooze = value[3];
					data_alarm_time = value[4];
					data_alarm_settime = value[5];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("language")) {
					data_language = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("curtime")) {
					data_curtime = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("curdate")) {
					data_curdate = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("nightlight")) {
					data_nightlight = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("roomlight")) {
					data_roomlight = value[0];
				}
			} catch (Exception e) {
			}

			try {
				if (name.equalsIgnoreCase("roommasterlight")) {
					data_roommasterlight = value[0];
				}
			} catch (Exception e) {
			}
			
			try {
				if (name.equalsIgnoreCase("fax")) {
					data_fax = value[0];
				}
			} catch (Exception e) {
			}		
			
			try {
				if (name.equalsIgnoreCase("msg")) {
					data_msg = value[0];
				}
			} catch (Exception e) {
			}

		} catch (Exception e) {
		}
	}

	public String getData(String name) {
		String r = "";

		try {
			if (name.equalsIgnoreCase("data_myroom")) {
				r = data_myroom;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_roomtype")) {
				r = data_roomtype;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_mur")) {
				r = data_mur;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_dnd")) {
				r = data_dnd;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_valet")) {
				r = data_valet;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_temperature")) {
				r = data_temperature;
			}

			if (name.equalsIgnoreCase("data_temperature_cf")) {
				r = data_temperature_cf;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_fan")) {
				r = data_fan;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_alarm")) {
				r = data_alarm;
			}

			if (name.equalsIgnoreCase("data_alarm_hr")) {
				r = data_alarm_hr;
			}

			if (name.equalsIgnoreCase("data_alarm_min")) {
				r = data_alarm_min;
			}

			if (name.equalsIgnoreCase("data_alarm_snooze")) {
				r = data_alarm_snooze;
			}

			if (name.equalsIgnoreCase("data_alarm_time")) {
				r = data_alarm_time;
			}

			if (name.equalsIgnoreCase("data_alarm_settime")) {
				r = data_alarm_settime;
			}

		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_language")) {
				r = data_language;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_curtime")) {
				r = data_curtime;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_curdate")) {
				r = data_curdate;
			}
		} catch (Exception e) {
		}
		try {
			if (name.equalsIgnoreCase("year")) {
				r = data_curdate.substring(0, 4);
			}
			if (name.equalsIgnoreCase("month")) {
				r = data_curdate.substring(5, 7);
			}
			if (name.equalsIgnoreCase("day")) {
				r = data_curdate.substring(8, 10);
			}
			if (name.equalsIgnoreCase("hour")) {
				r = data_curtime.substring(0, 2);
			}
			if (name.equalsIgnoreCase("minute")) {
				r = data_curtime.substring(3, 5);
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_nightlight")) {
				r = data_nightlight;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_roomlight")) {
				r = data_roomlight;
			}
		} catch (Exception e) {
		}

		try {
			if (name.equalsIgnoreCase("data_roommasterlight")) {
				r = data_roommasterlight;
			}
		} catch (Exception e) {
		}
		
		try {
			if (name.equalsIgnoreCase("data_fax")) {
				r = data_fax;
			}
		} catch (Exception e) {
		}	
		
		try {
			if (name.equalsIgnoreCase("data_msg")) {
				r = data_msg;
			}
		} catch (Exception e) {
		}			

		return r;
	}

	// //////////////////////////////////////////////////////////////////////////////////////

	public String getBSPBase() {
		return mBSPBase;
	}

	private String getIpAddr() {
		WifiManager wifiManager = (WifiManager) MainApplication.getContext()
				.getSystemService(MainApplication.getContext().WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ip = wifiInfo.getIpAddress();

		String ipString = String.format("%d.%d.%d.", (ip & 0xff),
				(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

		mBSPBase = String.format("%d", (ip >> 24 & 0xff));

		return ipString;
	}
}
