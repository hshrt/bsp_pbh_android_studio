package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.comm.*;

import android.app.Application;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

 
public class BatteryChecker {
	
	String myLOG = "BatteryChecker";
	
	private int batteryCharge = -1;
	private int batterySta = -1;
	
	private int proximityFire = 0;
	
	
	public BatteryChecker() {
		loadReceiver();
		
	}
	
	
	public void setProximityFire(int i) {
		proximityFire = i;
	}

	public int getProximityFire() {
		int r = proximityFire;
		proximityFire = 0; 
		
		return r;
	}
	
	public void setBatteryCharge(int i) {
		try {
			if (batteryCharge != i) {
				proximityFire = 1;
				
				Log.v(myLOG,"proximity deteced");
			}
		} catch (Exception e) {}

		batteryCharge = i;
	}
	
	public void setBatterySta(int i) {
		batterySta = i;
	}	
	
	public int getBatteryCharge() {
		return batteryCharge;
	}
	
	public int getBatterySta() {
		return batterySta;
	}		
	
	public int getBatteryLevel() {
		
		int myBatteryDiv = 0;
		
		if (batteryCharge == 1) {
			myBatteryDiv = 100;
		} else {
			if (batterySta >= 90) {
				myBatteryDiv = 90;
			} else if (batterySta >= 75) {
				myBatteryDiv = 75;
			} else if (batterySta >= 50) {
				myBatteryDiv = 50;
			} else if (batterySta >= 25) {
				myBatteryDiv = 25;
			} else {
				myBatteryDiv = 25;
			}
		}
		return myBatteryDiv;
	}
	
	
///////////////////////////////////////////////
	
	private void loadReceiver() {
		try {
			
    		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON); 
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_POWER_CONNECTED);
            filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
            filter.addAction(Intent.ACTION_BATTERY_CHANGED);

            BroadcastReceiver mReceiver = new BatteryReceiver(); 
            MainApplication.getContext().registerReceiver(mReceiver, filter);  			
			
		} catch (Exception e) {}
	}
	
    	
	
	
	

}