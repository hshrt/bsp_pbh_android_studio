package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.Menu;

public class RadioComm {
 
	private String myLOG = "RadioComm";
	
	static String myRadio = "";
	static String myPreset = "";
	static String myCountry = "";
	static String myGenre = "";
	
	static int isRadioOK = 0;
	static int isPresetOK = 0;
	static int isCountryOK = 0;
	static int isGenreOK = 0;
	
	// special for tv channel 
	private static ArrayList data = new ArrayList();
	
	public static void setTVData(ArrayList d) {
		data = d;
	}
	
	public static ArrayList getTVData() {
		return data;
	}
	/////////////////////////////////////////////////////
	

	public static int getPort() {

		int myBase = 0;
		int myPort = 10000;
		
		try {
			myBase = (Integer.parseInt(MainApplication.getMAS().getBSPBase()) 
						- Integer.parseInt(MainApplication.getContext().getString(R.string.bsp_base)));
			
			if ((myBase < 0) || (myBase > 9)) {
				myBase = 1;
			}
		
			myPort = (Integer.parseInt(MainApplication.getMAS().getData("data_myroom")) + 10000 + myBase*10000);
		} catch (Exception e) {}
		
		return myPort;
	}
	
	
	public static void setRadio(String s) {
		
		myRadio = "";
		
		try {
			myRadio = new JSONObject(s).get("list").toString();
			
			isRadioOK = 1;
		} catch (Exception e) {}

	}

	public static void setPreset(String s) {
		
		myPreset = "";
		
		try {
			myPreset = new JSONObject(s).get("list").toString();
			
			isPresetOK = 1;
		} catch (Exception e) {}

	}	
	
	public static void setCountry(String s) {
		
		myCountry = "";
		
		try {
			myCountry = new JSONObject(s).get("list").toString();
			
			isCountryOK = 1;
		} catch (Exception e) {}

	}		
	
	public static void setGenre(String s) {
		
		myGenre = "";
		
		try {
			myGenre = new JSONObject(s).get("list").toString();
			
			isGenreOK = 1;
		} catch (Exception e) {}

	}		
	

	public static String getRadio() {
		String r = myRadio;
		
		isRadioOK = 0;
		myRadio = "";
		
		return r;
	}
	
	public static String getPreset() {
		String r = myPreset;
		
		isPresetOK = 0;
		myPreset = "";
		
		return r;
	}	
	
	
	public static String getCountry() {
		String r = myCountry;
		
		isCountryOK = 0;
		myCountry = "";
		
		
		
		
/*		try {
			
			InputStream is = MainApplication.getAsset().open("radio.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));               
	
			String line = "";
			String inJSON = "";
	    	while ((line = in.readLine()) != null) {  
	    		inJSON = inJSON + line;    
	    	}
	    	
	    	JSONObject jr = new JSONObject(new String(inJSON));
	    	JSONObject jrmain = new JSONObject(jr.get("Country").toString());
	    	
	    	String fullJSON = "{\"list\":\"";
	    	
	    	for(int i=jrmain.length()-1;i>=0;i--) {
				String tmp = jrmain.get("" + i).toString();
				fullJSON = fullJSON + "" + tmp + "#" + checkMyCountryLabel(tmp) + "@";
			}
	    	
	    	fullJSON = fullJSON + "\"}";
	    	
	    	r = new JSONObject(fullJSON).get("list").toString();
    	
		} catch (Exception e) {}*/
		
		return r;
	}	
	
	public static String getGenre() {
		String r = myGenre;
		
		isGenreOK = 0;
		myGenre = "";
		
/*		try {
			
			InputStream is = MainApplication.getAsset().open("radio.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));               
	
			String line = "";
			String inJSON = "";
	    	while ((line = in.readLine()) != null) {  
	    		inJSON = inJSON + line;    
	    	}
	    	
	    	JSONObject jr = new JSONObject(new String(inJSON));
	    	JSONObject jrmain = new JSONObject(jr.get("Genre").toString());
	    	
	    	String fullJSON = "{\"list\":\"";
	    	
	    	for(int i=jrmain.length()-1;i>=0;i--) {
				String tmp = jrmain.get("" + i).toString();
				fullJSON = fullJSON + "" + tmp + "#" + checkMyCountryLabel(tmp) + "@";
			}
	    	
	    	fullJSON = fullJSON + "\"}";
	    	
	    	r = new JSONObject(fullJSON).get("list").toString();
	    	
    	
		} catch (Exception e) {}*/
		
		return r;
	}		
	
	
	
	public static void setRadioOK(int i) {

		isRadioOK = i;
	}
	
	public static void setPresetOK(int i) {

		isPresetOK = i;
	}
	
	public static void setCountryOK(int i) {

		isCountryOK = i;
	}
	
	public static void setGenreOK(int i) {

		isGenreOK = i;
	}
	
	
	
	public static int getRadioOK() {
		
		return isRadioOK;
	}	
	
	public static int getPresetOK() {
		
		return isPresetOK;
	}
	
	public static int getCountryOK() {
		
		return isCountryOK;
	}
	
	public static int getGenreOK() {
		
		return isGenreOK;
	}	

////////////////////////////////////////////////
	
	
public String checkMyCountryLabel(String myCountry) {
		
		String myChk = myCountry;
		
		try {
			
			String[][] lblCountry = new String[][] {
					
					{ "Alternative","Alternatif","另類音樂","另类音乐","オルタナティヴ","Alternative","대안","Альтернатива","Alternativa","بديل","Alternativa"},
					{ "Ambient","Ambiance","環境音樂","环境音乐","アンビエント","Ambient","주위","Окружающий","Ambiente","المحيطة","Ambiente"},
					{ "Big Band","Big Band","大樂隊","大乐队","ビッグバンド","Big Band","빅 밴드","Big band","Big band","بيغ باند","Big Band"},
					{ "Bluegrass","Bluegrass","藍草","蓝草","ブルーグラス","Bluegrass","블루 그래스","Мятлик","Bluegrass","البلو","Bluegrass"},
					{ "Blues","Blues","藍調","蓝调","ブルーズ","Blues","블루스","Блюз","Blues","البلوز","Blues"},
					{ "Business News","Nouvelles Affaires","商業新聞","商业新闻","ビジネスニュース","Business News","비즈니스 뉴스","Бизнес новости","Business news","أخبار الأعمال","Negocios Noticias"},
					{ "Celtic","Celtique","凱爾特音樂","凯尔特音乐","ケルティック","Celtic","켈트족의","Кельтский","Celta","سلتيك","Celta"},
					{ "Christian Contemporary","Christian Contemporain","基督教當代","基督教当代","クリスチャン•コンテンポラリー","Christian Moderne","기독교 현대","Современное христианское","Cristã contemporânea","المسيحية المعاصرة","Cristiana Contemporánea"},
					{ "Christian Rock","Christian Rock","基督教搖滾","基督教摇滚","クリスチャンロック","Christian Fels","기독교 록","Christian rock","Christian rock","كريستيان روك","Christian Rock"},
					{ "Classic Rock","Classic Rock","經典搖滾","经典摇滚","クラシック•ロック","Classic Rock","클래식 락","Classic rock","Classic rock","كلاسيك روك","Classic Rock"},
					{ "Classical","Classique","古典樂","古典乐","クラシック","Klassische","고전적인","Классический","Clássico","كلاسيكي","Clásico"},
					{ "College","College","校園","校园","カレッジ","Hochschule","전문 대학","Колледж","Faculdade","كلية","Colegio"},
					{ "Comedy","Comédie","喜劇","喜剧","コメディ","Comedy","코메디","Комедия","Comédia","كوميديا","Comedia"},
					{ "Country","Country","鄉謠","乡谣","カントリー","Land","국가","Страна","País","بلد","País"},
					{ "Dance","Dance","跳舞","跳舞","ダンス","Tanz","댄스","Танцевать","Dança","الرقص","Danza"},
					{ "Electronica","Electronique","電子音樂","电子音乐","エレクトロニカ","Electronica","일렉트로니카","Electronica","Electronica","الكترونيكا","Electronica"},
					{ "Folk","Fourchette","民歌","民歌","フォーク","Folk","사람들","Народный","Povo","قوم","Gente"},
					{ "Gospel","Gospel","福音","福音","福音","Gospel","복음","Евангелие","Evangelho","الإنجيل","Evangelio"},
					{ "Government","Gouvernement","政府","政府","政府","Regierung","정부","Правительство","Governo","حكومة","Gobierno"},
					{ "Hard Rock","Hard Rock","硬搖滾樂","硬摇滚乐","ハードロック","Hard Rock","하드 록","Hard rock","Hard rock","هارد روك","Hard Rock"},
					{ "Hip Hop","Hip Hop","嘻哈","嘻哈","ヒップホップ","Hip Hop","힙합","Хип-хоп","Hip hop","الهيب هوب","Hip Hop"},
					{ "Holiday","Vacances","節日音樂","节日音乐","シーズン","Urlaub","휴일","Праздник","Férias","عطلة","Fiesta"},
					{ "Jazz","Jazz","爵士樂","爵士乐","ジャズ","Jazz","재즈","Джаз","Jazz","موسيقى الجاز","Jazz"},
					{ "Latin Hits","Latin Hits","拉丁流行歌","拉丁流行歌","最新ラテン","Latin Hits","라틴어 히트","Латинской хиты","Acessos latina","الفعالية اللاتينية","Latin Hits"},
					{ "New Age","New Age","新時代","新时代","ニューエイジ","New Age","뉴 에이지","New age","New age","العصر الجديد","Nueva Era"},
					{ "News","Nouvelles","新聞","新闻","ニュース","Nachrichten","뉴스","Новости","Notícia","أخبار","Noticias"},
					{ "News Talk","Discuter Nouvelles","新聞談話","新闻谈话","ニュース•トーク","News Talk","뉴스 토크","Обсуждение новостей","Notícias discussão","أخبار برامج","News Talk"},
					{ "News Updates","Mises à jour News","最新新聞","最新新闻","最新ニュース","News Updates","뉴스 업데이트","Новости обновления","Notícias atualizações","تحديثات الأخبار","Noticias actualizaciones"},
					{ "Oldies","Oldies","懷舊","怀旧","オールディーズ","Oldies","추억의 음악","Oldies","Oldies","أغاني قديمة","Oldies"},
					{ "Pop","Pop","流行樂","流行乐","ポップ","Pop","팝","Поп","Pop","موسيقى البوب","Pop"},
					{ "Public","Public","公眾","公众","公共","Öffentliche","공공의","Общественность","Público","جمهور","Público"},
					{ "R&B","R&B","R&B","R&B","R&B","R & B","R & B","R & b","R & b","R & B","R & B"},
					{ "Radio Drama","Radio Drama","廣播劇","广播剧","ラジオドラマ","Hörspiel","라디오 드라마","Радио драма","Drama radio","راديو دراما","Radio Teatro"},
					{ "Reggae","Reggae","雷鬼音樂","雷鬼音乐","レゲエ","Reggae","레게","Reggae","Reggae","الريغي","Reggae"},
					{ "Religious","Religieuse","宗教","宗教","宗教的な","Religiöse","종교적인","Религиозный","Religioso","ديني","Religioso"},
					{ "Rock","Rock","搖滾","摇滚","ロック","Fels","록","Рок","Rocha","صخرة","Roca"},
					{ "Scanner","Scanner","掃描器","扫描器","スキャナー","Scanner","스캐너","Сканер","Scanner","الماسح الضوئي","Escáner"},
					{ "Show Tunes","Voir Tunes","舞台音樂","舞台音乐","ステージの音楽","Zeigen Tunes","곡보기","Музыкальные шоу","Mostrar tunes","مشاهدة الألحان","Mostrar Tunes"},
					{ "Smooth Jazz","Smooth Jazz","輕爵士樂","轻爵士乐","スムースジャズ","Smooth Jazz","부드러운 재즈","Smooth jazz","Smooth jazz","الجاز على نحو سلس","Smooth Jazz"},
					{ "Soft Rock","Soft Rock","軟搖滾","软摇滚","ソフトロック","Soft Rock","소프트 록","Мягкая порода","Soft rock","صخرة لينة","Soft Rock"},
					{ "Soundtracks","Musique disponible","電影原聲音樂","电影原声音乐","サントラ","Soundtracks","사운드 트랙","Саундтреки","Soundtracks","الموسيقى التصويرية","Soundtracks"},
					{ "Sports","Sportif","體育","体育","スポーツの","Sport","스포츠의","Спортивный","Esportes","الرياضة","deportes"},
					{ "Talk","Parler","清談","清谈","トーク","Sprechen","이야기","Говорить","Falar","حديث","hablar"},
					{ "Top 40","Top 40","前40名","前40名","トップ40","Top 40","톱 40","Top 40","Top 40","أعلى 40","Top 40"},
					{ "Variety","Variété","多種","多种","多様","Variety","종류","Разнообразие","Variedade","تشكيلة","variedad"},
					{ "Weather","Temps","天氣","天气","天気","Wetter","날씨","Погода","Tempo","طقس","tiempo"},
					{ "World","Monde","世界","世界","世界","Welt","세계","Мир","Mundo","عالم","mundo"},
					{ "World Asia","Monde Asie","亞洲世界","亚洲世界","アジア世界","World Asia","세계 아시아","World asia","Mundo ásia","العالم آسيا","Asia World"},
					{ "World Europe","Monde Europe","歐洲世界","欧洲世界","ヨーロッパ世界","World Europe","세계 유럽","Мир европа","Europa mundial","أوروبا العالم","Europa Mundial"},
					{ "World Hawaiian","Hawaiian Mondiale","夏威夷世界","夏威夷世界","ハワイアン世界","Welt Hawaiian","세계 하와이","Всемирный гавайских","Mundo havaiana","العالم هاواي","Mundial de Hawai"},
					{ "World India","World India","印度世界","印度世界","インド世界","Welt Indien","세계 인도","Всемирный индии","World india","العالم الهند","World India"},
					{ "World Middle East","Mondiale au Moyen-Orient","中東世界","中东世界","中東世界","Welt Naher und Mittlerer Osten","세계 중동","Мир на ближнем востоке","Mundial no oriente médio","العالم الشرق الأوسط","Mundo Oriente Medio"},
					{ "World Native American","Mondiale Native American","美國土著世界","美国土著世界","ネイティブアメリカン世界","Welt Native American","세계 아메리카 인디언","Всемирный индейские","Mundo native american","العالم الأمريكيين","Mundial de nativos americanos"},
					{ "World Tropical","Tropical World","熱帶世界","世界热带","熱帯の世界","Welt Tropical","세계 열대","Тропический мир","Mundo tropical","العالم المدارية","Tropical World"},

					

					{ "Australia","Australie","澳大利亞","澳大利亚","オーストラリア","Australien","호주","Австралия","Austrália","أستراليا","Australia"},
					{ "Brazil","Brésil","巴西","巴西","ブラジル","Brasilien","브라질","Бразилия","Brasil","البرازيل","Brasil"},
					{ "China","Chine","中國","中国","中国","China","중국","Китай","China","الصين","China"},
					{ "France","France","法國","法国","フランス","Frankreich","프랑스","Франция","França","فرنسا","Francia"},
					{ "Germany","Allemagne","德國","德国","ドイツ","Deutschland","독일","Германия","Alemanha","ألمانيا","Alemania"},
					{ "India","Inde","印度","印度","インド","Indien","인도","Индия","Índia","الهند","India"},
					{ "Italy","Italie","意大利","意大利","イタリア","Italien","이탈리아","Италия","Itália","إيطاليا","Italia"},
					{ "Japan","Japon","日本","日本","日本","Japan","일본","Япония","Japão","اليابان","Japón"},
					{ "Mexico","Mexique","墨西哥","墨西哥","メキシコ","Mexiko","멕시코","Мексика","México","المكسيك","México"},
					{ "Qatar","Qatar","卡塔爾","卡塔尔","カタール","Qatar","카타르","Катар","Catar","قطر","Katar"},
					{ "Russia","Russie","俄國","俄国","ロシア","Russland","러시아","Россия","Rússia","روسيا","Rusia"},
					{ "South Africa","Afrique du Sud","南非","南非","南アフリカ","Südafrika","남아프리카 공화국","Юар","África do sul","جنوب أفريقيا","Sudáfrica"},
					{ "South Korea","Corée du Sud","韓國","韩国","韓国","Südkorea","대한민국","Южная корея","Coréia do sul","كوريا الجنوبية","Corea del Sur"},
					{ "Spain","Espagne","西班牙","西班牙","スペイン","Spanien","스페인","Испания","Espanha","إسبانيا","España"},
					{ "Switzerland","Suisse","瑞士","瑞士","スイス","Schweiz","스위스","Швейцария","Suíça","سويسرا","Suiza"},
					{ "Thailand","Thaïlande","泰國","泰国","タイ","Thailand","태국","Таиланд","Tailândia","تايلاند","Tailandia"},
					{ "The Philippines","Les Philippines","菲律賓","菲律宾","フィリピン","Die Philippinen","필리핀","Филиппины","As filipinas","الفلبين","Filipinas"},
					{ "United Arab Emirates","Émirats arabes unis","阿拉伯聯合酋長國","阿拉伯联合酋长国","アラブ首長国連邦","Vereinigte Arabische Emirate","아랍 에미리트","Объединенные арабские эмираты","Emirados árabes unidos","الإمارات العربية المتحدة","Emiratos Árabes Unidos"},
					{ "United Kingdom","Royaume-Uni","聯合王國","联合王国","イギリス","Großbritannien","연합 왕국","Великобритания","Reino unido","المملكة المتحدة","Reino Unido"},
					{ "USA","USA","美國","美国","米国","USA","미국","США","EUA","الولايات المتحدة الأمريكية","EE.UU."}

			};


			for(int i=0;i<lblCountry.length;i++) {
				if (lblCountry[i][0].equalsIgnoreCase(myCountry)) {
					myChk = lblCountry[i][Integer.parseInt(MainApplication.getLabel("LangCode"))];
					break;
				}
			}
			
			
		} catch (Exception e) {}
		
		return myChk;
		
	}
	
	

}