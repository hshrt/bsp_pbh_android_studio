package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
 
 
public class RadioAsyncGenLink extends AsyncTask<String, String, Void> {

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub

        try {          
        	URL ww = null;
        	String myLink = "";
        	
        	if (params[0].equalsIgnoreCase("START")) {
        		myLink = params[1];
        		
        		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
        			ww = new URL("http://" + MainApplication.getContext().getString(R.string.radio_path) + "/gen.php?myPort=" + MainApplication.getRadio().getPort() + "&myLink=\""+myLink+"\"");
        		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
        			ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/gen.php?myPort=" + MainApplication.getRadio().getPort() + "&myLink=\""+myLink+"\""); 
        		}            		
        		
        		
        	} else {
        		
        		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
        			ww = new URL("http://" + MainApplication.getContext().getString(R.string.radio_path) + "/stop.php?myPort=" + MainApplication.getRadio().getPort());
        		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
        			ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/stop.php?myPort=" + MainApplication.getRadio().getPort()); 
        		}         		
        		
        	}
        	
        	
        	    	
        	
        		
        	final URLConnection tc = ww.openConnection();             
        	
        	BufferedReader in = new BufferedReader(new InputStreamReader(                
					tc.getInputStream()));
        	
        	in.close();
        	
        } catch (Exception e) {}		
		
		return null;
	}
	
	
	@Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        
    }
 
 }

