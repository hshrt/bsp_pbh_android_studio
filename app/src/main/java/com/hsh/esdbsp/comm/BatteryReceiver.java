package com.hsh.esdbsp.comm;

import com.hsh.esdbsp.MainApplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.text.Html;
import android.util.Log;
import android.view.WindowManager;
 
 public class BatteryReceiver extends BroadcastReceiver {  

	String myLOG = "BatteryReceiver";
	
	private int batteryCharge = 0;
	private int batterySta = 0;
	
	public void setBatteryCharge(int i) {
		batteryCharge = i;
		
		try {			
			MainApplication.getBatteryChecker().setBatteryCharge(i);
		} catch (Exception e) {}
	}
	
	public void setBatterySta(int i) {
		batterySta = i;
		
		try {
			MainApplication.getBatteryChecker().setBatterySta(i);
		} catch (Exception e) {}
	}
	

    @Override 
    public void onReceive(Context context, Intent intent) {  

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {  

        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {  

        } else if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {  
        	try {
        		setBatteryCharge(1);
        	} catch (Exception e) {}

        } else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {  

        	try {
        		setBatteryCharge(0);
        	} catch (Exception e) {}

        } else if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {  
        	
        	try {
	        	int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
	            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
	            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
	        	
	        	if (usbCharge || acCharge) {
	        		
	        		setBatteryCharge(1);
	        	} else {
	        		
	        		setBatteryCharge(0);
	        	}
	            
	        	int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
	        	int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
	        	
	        	float batteryPct = level / (float)scale;
	        	
	        	setBatterySta(Math.round(batteryPct*100));
	        	
        	} catch (Exception e) {}
        }
    }
 

   
 } 
