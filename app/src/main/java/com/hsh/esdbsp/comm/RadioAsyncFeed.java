package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

 
public class RadioAsyncFeed extends AsyncTask<String, String, Void> {
	
	String myLOG = "RadioAsyncFeed";
	

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		
        try {          
        	URL ww = null;
        	String myFilter = params[0].replace(" ","%20");
        	String myType = params[1].toString();
        	
        	String line = "";
        	String inJSON = ""; 
        	String fullJSON = "";
        	JSONObject jr = null;
        	JSONObject jrmain = null;
        	
        	if (myFilter.equalsIgnoreCase("BASE")) {
        		// country genre
        		
        		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
        			ww = new URL("http://" + MainApplication.getContext().getString(R.string.radio_path) + "/rj.php?mytype=" + myType ); 
        		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
        			ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/rj.php?mytype=" + myType );
        		}        		
        		
        		
        		
        		
        		URLConnection tc = ww.openConnection();
            	BufferedReader in = new BufferedReader(new InputStreamReader(                     
            			tc.getInputStream()));  
            	
            	while ((line = in.readLine()) != null) {  
            		fullJSON = fullJSON + line;    
            	}
            	
            	in.close();
            	
            	if (myType.equalsIgnoreCase("Country")) {
            		MainApplication.getRadio().setCountry(fullJSON);
            	} else {
            		
            		MainApplication.getRadio().setGenre(fullJSON);
            	}
        		
            	
        	} else if (myFilter.equalsIgnoreCase("PRESET")) {
        		
        		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
        			ww = new URL("http://" + MainApplication.getContext().getString(R.string.radio_path) + "/pre.php"); 
        		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
        			ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/pre.php"); 
        		}
        		 
        	
        		URLConnection tc = ww.openConnection();
            	BufferedReader in = new BufferedReader(new InputStreamReader(                     
            			tc.getInputStream()));  
            	
            	while ((line = in.readLine()) != null) {  
            		fullJSON = fullJSON + line;    
            	}
            	
            	in.close();
            	MainApplication.getRadio().setPreset(fullJSON);
            	
        	} else {

        		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
        			ww = new URL("http://" + MainApplication.getContext().getString(R.string.radio_path) + "/rj.php?mytype=" + myType + "&myFilter=" + myFilter + ""); 
        		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
        			ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/rj.php?mytype=" + myType + "&myFilter=" + myFilter + "");
        		}        		
        		
        		
        		
        		
        		URLConnection tc = ww.openConnection();
            	BufferedReader in = new BufferedReader(new InputStreamReader(                     
            			tc.getInputStream(), "ISO-8859-1"));  
            	
            	while ((line = in.readLine()) != null) {     
            		//Log.v("radio",line);
            		fullJSON = fullJSON + line;    
            	}
            	
            	in.close();
            	MainApplication.getRadio().setRadio(fullJSON);
        	}

        	
        	
        } catch (Exception e) {}		
		
		return null;
	}	

	
	
	
//////////////////////////////////////////////////////////////
	
	
	
}



