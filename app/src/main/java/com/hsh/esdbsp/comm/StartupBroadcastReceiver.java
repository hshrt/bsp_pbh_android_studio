package com.hsh.esdbsp.comm;

import com.hsh.esdbsp.MainApplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class StartupBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent startupIntent = new Intent(context, MainApplication.class); // substitute with your launcher class
		startupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(startupIntent);
	}
	
}

