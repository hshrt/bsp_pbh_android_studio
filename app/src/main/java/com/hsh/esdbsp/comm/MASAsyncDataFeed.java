package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.widget.TextView;

 
public class MASAsyncDataFeed extends AsyncTask<Void, String, Void> {
	
	static String myLOG = "MASAsyncDataFeed";
	
	static Handler mMASHandler = new Handler();
	static int mMASDelay = 500; // will read from setting file
	
	
	static String myFeed = "";
	static String lastFeed = "";
	
	static MASAsyncDataFeed mMASFeed;
	
	
	@Override
    protected void onPreExecute() {
		try {

			myFeed = "";

		} catch (Exception e) {}
		
        super.onPreExecute();
        
    }	
	

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub

		try {   

			myFeed = "";

        	final String URL = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=GET%20STATUS&sta=" + lastFeed;
        	//Log.v(myLOG,URL);

        	URL ww = new URL(URL);             
        	URLConnection tc = ww.openConnection();
        	
        	BufferedReader in = new BufferedReader(new InputStreamReader(                     
        			tc.getInputStream()));  
        	
        	
        	
        	String line = "";
        	String fullSTA = "";
        	while ((line = in.readLine()) != null) {  
        		fullSTA = fullSTA + line;    
        	} 

        	myFeed = fullSTA;
        	
        	
        	in.close();
        	
        	//Log.v(myLOG,myFeed);
        	

        } catch (Exception e) {             
        	// TODO Auto-generated catch block  
        	MainApplication.getMAS().setData("die", "die");
        	
        	//MainApplication.getMAS().getMASFeed();
        	
        	mMASHandler.removeCallbacks(mMASRunnable);
        	mMASHandler.postDelayed(mMASRunnable, mMASDelay);
        }	
		
		return null;
	}
	
	
	
	private Runnable mMASRunnable = new Runnable() {

		public void run() {
			try {
				mMASHandler.removeCallbacks(mMASRunnable);
				
				MainApplication.getMAS().setupMASPath();
				MainApplication.getMAS().getMASFeed();
				
				
			} catch (Exception e) {
				
				mMASHandler.removeCallbacks(mMASRunnable);
	        	mMASHandler.postDelayed(mMASRunnable, mMASDelay);
			}
		}
	};	
	
	
	
	
	
	@Override
	protected void onPostExecute(Void data) {
		try {
			
			if (myFeed.length() > 10) {
	
				final String KEY_ITEM = "data"; // parent node 
				
				XMLParser parser = new XMLParser(); 
	        	Document doc = parser.getDomElement(myFeed); // getting DOM element 
	        	
	        	Log.i("MASFEED", doc.toString());
	        	
	        	NodeList nl = doc.getElementsByTagName(KEY_ITEM); 
	        	  
	        	// looping through all item nodes <item>       
	        	for (int i = 0; i < nl.getLength(); i++) { 
	        		
	        		Node nNode = nl.item(i);
	        		Element eElement = (Element) nNode;
	
	        		String name = eElement.getAttribute("name"); 
	        		String value1 = ""; 
	        		String value2 = ""; 
	        		String value3 = ""; 
	        		String value4 = ""; 
	        		String value5 = ""; 
	        		String value6 = ""; 
	        		
	        		try {
		        		if (name.equalsIgnoreCase("org")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			lastFeed = value1;
		        		}
	        		} catch (Exception e) {}
	
	        		try {
		        		if (name.equalsIgnoreCase("myroom")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("roomtype")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("mur")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("dnd")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("valet")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}  
	        		} catch (Exception e) {}
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("temperature")) {
		        			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
		        				value1 = eElement.getAttribute("value");
		        			} else {
		        				value1 = eElement.getAttribute("orgvalue");
		        			}
		        			
		        			value2 = eElement.getAttribute("cf");
		        			
		        			MainApplication.getMAS().setData(name, value1, value2);
		        		} 
	        		} catch (Exception e) {}
		        	
	        		
	        		try {
		        		if (name.equalsIgnoreCase("fan")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}    
		        		
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("alarm")) {
		        			value1 = eElement.getAttribute("value");
		        			value2 = eElement.getAttribute("hour");
		        			value3 = eElement.getAttribute("minute");
		        			value4 = eElement.getAttribute("snooze");
		        			value5 = eElement.getAttribute("time");
		        			value6 = eElement.getAttribute("settime");
		        			
		        			
		        			MainApplication.getMAS().setData(name, value1,value2,value3,value4,value5,value6);
		        		}     
	        		} catch (Exception e) {}
	
	        		try {
		        		if (name.equalsIgnoreCase("language")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}   
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("curtime")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("curdate")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}        		
	        		
	        		
	
	        		try {
		        		if (name.equalsIgnoreCase("nightlight")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}
	        		
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("roomlight")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {Log.i(myLOG, "What is the problem =" + e.toString());}        	
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("roommasterlight")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}
	        		} catch (Exception e) {}      
	        		
	        		
	        		
	        		try {
		        		if (name.equalsIgnoreCase("fax")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}  
	        		} catch (Exception e) {}
	        		
	        		try {
		        		if (name.equalsIgnoreCase("msg")) {
		        			value1 = eElement.getAttribute("value");
		        			
		        			MainApplication.getMAS().setData(name, value1);
		        		}  
	        		} catch (Exception e) {}
	        		
	        		
	        	}
        	
	        	
			} else {
				
				
	        	mMASHandler.removeCallbacks(mMASRunnable);
	        	mMASHandler.postDelayed(mMASRunnable, 50);
			}
        	
        	
		} catch(Exception e) {
			Log.v(myLOG,"Error:" + e.toString());
			
			mMASHandler.removeCallbacks(mMASRunnable);
        	mMASHandler.postDelayed(mMASRunnable, 50);
		}
		
		
		try {
			mMASHandler.removeCallbacks(mMASRunnable);
        	mMASHandler.postDelayed(mMASRunnable, 50);
        	
		} catch (Exception e) {}
	}

 }

