package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.Menu;

public class WeatherComm {

	private String myLOG = "WeatherComm";
	
	private static String myJSON = "";
	
	private static WeatherAsyncFeed mWeatherFeed;
	
	static Handler mWeatherHandler = new Handler();
	static int mWeatherDelay = 15*60*1000;	
	 
//////////////////////////////////////////////////////////////////////////////
	
	private Runnable mWeatherRunnable = new Runnable() {

		public void run() {
			try {
				mWeatherHandler.removeCallbacks(mWeatherRunnable);
				
				try {
					if (mWeatherFeed != null) {
						mWeatherFeed.cancel(true);
					}
				} catch (Exception e) {}
				
				
				if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
					mWeatherFeed = (WeatherAsyncFeed) new WeatherAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				else
					mWeatherFeed = (WeatherAsyncFeed) new WeatherAsyncFeed().execute(); 
				
				
				mWeatherHandler.removeCallbacks(mWeatherRunnable);
				mWeatherHandler.postDelayed(mWeatherRunnable, mWeatherDelay);
				
			} catch (Exception e) {
				mWeatherHandler.removeCallbacks(mWeatherRunnable);
				mWeatherHandler.postDelayed(mWeatherRunnable, 100);
			}
		}
	};		
	
	
	public void getWeatherFeed() {
		
		try {
			
			mWeatherHandler.removeCallbacks(mWeatherRunnable);
			mWeatherHandler.postDelayed(mWeatherRunnable, 100);
			
		} catch (Exception e) {
			
			Log.v(myLOG,e.toString());
		}

	}
	
	
	public static String getJSON() {
		return myJSON;
	}
	
	public static void setJSON(String s) {
		myJSON = s;
	}

}