package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.comm.*;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;


public class AlarmChecker {
	 
	private static String myLOG = "AlarmChecker";
	
	private static Handler alarmHandler = new Handler();
	private static int alarmDelay = 500;
	private static int alarmDelayLong = 5*1000;
	
	private static MediaPlayer myAlarmPlayer;
	
	private static int isRing = 0;
	

/////////////////////////////////////////////////////////
	
	
	
	public AlarmChecker() {
		try {

			initAlarm();
		
		} catch (Exception e) {}
	}
	
	
	private MediaPlayer getPlayer() {
		return myAlarmPlayer;
	}
	
	private static void initAlarm() {
		try {
			alarmHandler.removeCallbacks(alarmRunnable);
			alarmHandler.postDelayed(alarmRunnable, alarmDelayLong);
			
		} catch (Exception e) {}
	}
	
	
	public static int getAlarmStatus() {
		////Log.v(myLOG,"ring:" + isRing);

		return isRing;
		
	}
	
	
	public static void stopAlarm() {

		//Log.v(myLOG,"alarm stop call");

		isRing = 0;
		
		try {
		
			if (myAlarmPlayer != null) {
			    Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			    MainApplication.getCurrentActivity().startActivity(intent); 
			}
		} catch (Exception e) {}
		
		
		try {
			if (myAlarmPlayer != null) {
				myAlarmPlayer.stop();
				myAlarmPlayer = null;
			}
		} catch (Exception e) {}		

		initAlarm();
	}
	

	
	
/////////////////////////////////////////////////////////////////////
	
	
	private void wakeUpScreen() {
		
		WakeLock mWakeLock;
		PowerManager powerManager;		
		
		try {

			powerManager = (PowerManager) MainApplication.getCurrentActivity().getSystemService(MainApplication.getContext().POWER_SERVICE);
			mWakeLock = powerManager.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, MainApplication.getCurrentActivity().getClass().getName());
			mWakeLock.acquire();			

			
		} catch (Exception e) {
			Log.v("ALARM","cannot wake screen ... " + e.toString());
		}
		
	}
	
	
	private static Runnable alarmRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			try {
				alarmHandler.removeCallbacks(alarmRunnable);
				
				//Log.v("ALARM","alarm running ... " + MainApplication.getMAS().getData("data_alarm").toString() + ":" + MainApplication.getMAS().getData("data_curtime").toString() );
				
				
				////Log.v(myLOG,"checking alarm:" + MainApplication.getMAS().getData("data_alarm_settime"));
				
				if (MainApplication.getMAS().getData("data_alarm").equalsIgnoreCase("ON")) {
					
					if (MainApplication.getMAS().getData("data_curtime").equalsIgnoreCase(MainApplication.getMAS().getData("data_alarm_settime"))) {
						
						
						MainApplication.getAlarmChecker().wakeUpScreen();
						

						
						Window mWindow = MainApplication.getCurrentActivity().getWindow();
						WindowManager.LayoutParams lp = mWindow.getAttributes();
						lp.screenBrightness = (float) 1;
						mWindow.setAttributes(lp);

						if (isRing == 0) {
							
							isRing = 1;
							
							Log.v("ALARM","ALARM !!!");

							try {
							    Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
							    MainApplication.getCurrentActivity().startActivity(intent);
								
								AudioManager audioManager = (AudioManager) MainApplication.getCurrentActivity().getSystemService(MainApplication.getContext().AUDIO_SERVICE); 
				    			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0); 
								
								try {
									myAlarmPlayer.stop();
								} catch (Exception e) {}
	
							    myAlarmPlayer = MediaPlayer.create(MainApplication.getContext(),MainApplication.getCurrentActivity().getResources().getIdentifier("raw/alarm_sound","raw", MainApplication.getCurrentActivity().getPackageName()));
							    myAlarmPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
	
									@Override
									public void onCompletion(MediaPlayer arg0) {
										// TODO Auto-generated method stub
										try {
											myAlarmPlayer.start();
										} catch (Exception e) {}
									}
							    	
							    });
							    
							    myAlarmPlayer.start();
							    

							} catch (Exception e) {}

						} else {
							
							// is it correct ?
							if (MainApplication.getMAS().getData("data_curtime").equalsIgnoreCase(MainApplication.getMAS().getData("data_alarm_settime"))) {
								// ok then
								
							} else {
								
								stopAlarm();
								
							}
							
						}
						
						
					} else {
						// snooze stop
						stopAlarm();						
					}
					
					
				} else {

					// just stop it 
					stopAlarm();
					
				}
				
				alarmHandler.removeCallbacks(alarmRunnable);
				alarmHandler.postDelayed(alarmRunnable, alarmDelay);
				
			} catch (Exception e) {
				
				Log.v("ALARM",e.toString());
				
				alarmHandler.removeCallbacks(alarmRunnable);
				alarmHandler.postDelayed(alarmRunnable, alarmDelay);				
			}
			
		}
		
	};
	
	
}