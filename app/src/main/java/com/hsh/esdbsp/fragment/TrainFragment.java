package com.hsh.esdbsp.fragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baidu.apistore.sdk.ApiCallBack;
import com.baidu.apistore.sdk.ApiStoreSDK;
import com.baidu.apistore.sdk.network.Parameters;
import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.AirportAdapter;
import com.hsh.hshservice.adapter.FlightAdapter;
import com.hsh.hshservice.adapter.TimeAdapter;
import com.hsh.hshservice.adapter.TrainAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airline;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.Train;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetFlightStatusParser;
import com.hsh.hshservice.parser.GetFlightStatusParser.GetFlightStatusParserInterface;
import com.loopj.android.http.AsyncHttpClient;
import com.hsh.hshservice.ServiceAirportActivity;

public class TrainFragment extends BaseFragment implements
		GetFlightStatusParserInterface {

	private String TAG = "TrainFragment";
	private static int NUM_OF_HOUR_RANGE = 4;
	private static int CURRENT_TIME_CODE = 7;

	public static int ARRIVAL = 1;
	public static int DEPARTURE = 2;

	private int current_choice;// choose arrvial or departure
	private int current_time_choice;

	private int current_airport_choice = 0;

	private String current_time;

	private View baseView;
	private ProgressBar mProgressBar;

	private LinearLayout container;
	private LinearLayout flightTrackContainer;

	private ListView trainList;
	private ListView timeList;
	private ListView airportList;

	private TrainAdapter trainAdapter;
	private FlightAdapter flightAdapter;
	private TimeAdapter timeAdapter;
	private AirportAdapter airportAdapter;

	private Button arrialButton;
	private Button departButton;
	private Button stationButton;
	private Button timeButton;

	private MyTextView departHead;
	private MyTextView arriveHead;
	private MyTextView trainHead;
	private MyTextView depTimeHead;
	private MyTextView arrTimeHead;
	private MyTextView intervalHead;
	private MyTextView typeHead;

	private String parentId;

	private ArrayList<FlightInfo> flightInfoArray;
	
	private ArrayList<String> timeRangeArray;
	private ArrayList<Airport> airportArray;

	private ArrayList<Train> trainInfoArray;
	private ArrayList<Train> filterTrainInfoArray;

	private FlightTrackFragment flightTrackFragment;

	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;

	// a key for what is the current topic of this page and what to get from DB
	private String objectString;

	public enum ApiCallType {
		GET_FLIGHT_INFO
	}

	ApiCallType apiCallType;

	private AsyncHttpClient client;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		Log.i(TAG, TAG + "onCreate");
		super.onCreate(savedInstanceState);

		Log.i(TAG, "date = " + MainApplication.getMAS().getData("data_curdate"));
		Log.i(TAG, "time = " + MainApplication.getMAS().getData("data_curtime"));

		Log.i(TAG, "year = " + MainApplication.getMAS().getData("year"));
		Log.i(TAG, "month = " + MainApplication.getMAS().getData("month"));
		Log.i(TAG, "day = " + MainApplication.getMAS().getData("day"));
		Log.i(TAG, "hour = " + MainApplication.getMAS().getData("hour"));
		Log.i(TAG, "minute = " + MainApplication.getMAS().getData("minute"));

		baseView = inflater.inflate(R.layout.train_info, container, false);
		
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		Helper.showInternetWarningToast();

		initUI();

		/*
		 * if(objectString.equalsIgnoreCase("hotel_restaurants")){
		 * bgPhoto.setImageResource(R.drawable.restaurant); }
		 */

		if (flightInfoArray == null) {
			flightInfoArray = new ArrayList<FlightInfo>();
		}
		
		

		if (timeRangeArray == null) {
			timeRangeArray = new ArrayList<String>();
		}
		if (airportArray == null) {
			airportArray = new ArrayList<Airport>();
		}

		if (trainInfoArray == null) {
			trainInfoArray = new ArrayList<Train>();
		}
		if (filterTrainInfoArray == null) {
			filterTrainInfoArray = new ArrayList<Train>();
		}

		if (trainAdapter == null) {
			trainAdapter = new TrainAdapter(this.getActivity(), filterTrainInfoArray);
			// trainAdapter.setSetOnClickInAdapter(false);
			trainList.setAdapter(trainAdapter);
		}

		Log.i(TAG, "length = " + flightInfoArray.size());

		airportList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> myAdapter, View myView,
					int pos, long mylng) {

				current_airport_choice = pos;

				if (current_choice == ARRIVAL) {
					arrialButton.performClick();
				} else {
					departButton.performClick();
				}

				stationButton.setText(airportArray.get(pos).getName());
			}

		});

		current_time_choice = CURRENT_TIME_CODE;
		arrialButton.performClick();

		if (timeRangeArray.size() > 0)
			timeButton.setText(timeRangeArray.get(0));

		setupTimeRange();

		//requestService();

		return baseView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	private void initUI() {

		arrialButton = (Button) baseView.findViewById(R.id.arrialButton);
		departButton = (Button) baseView.findViewById(R.id.departButton);
		stationButton = (Button) baseView.findViewById(R.id.stationButton);
		timeButton = (Button) baseView.findViewById(R.id.timeButton);

		departHead = (MyTextView) baseView.findViewById(R.id.departHead);
		arriveHead = (MyTextView) baseView.findViewById(R.id.arriveHead);
		trainHead = (MyTextView) baseView.findViewById(R.id.trainHead);
		depTimeHead = (MyTextView) baseView.findViewById(R.id.depTimeHead);
		arrTimeHead = (MyTextView) baseView.findViewById(R.id.arrTimeHead);
		intervalHead = (MyTextView) baseView.findViewById(R.id.intervalHead);
		typeHead = (MyTextView) baseView.findViewById(R.id.typeHead);

		progressBar = (ProgressBar) baseView
				.findViewById(R.id.progressIndicator);

		trainList = (ListView) baseView.findViewById(R.id.trainList);

		// LayoutInflater inflater = baseView.getLayoutInflater();

		timeList = (ListView) baseView.findViewById(R.id.timeList);
		airportList = (ListView) baseView.findViewById(R.id.airportList);

		errorMessageDialogLayout = (LinearLayout) baseView
				.findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) baseView
				.findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogLabel = (MyTextView) baseView
				.findViewById(R.id.errorMessageDialogLabel);

		Helper.setFonts(arrialButton, getString(R.string.app_font));
		Helper.setFonts(departButton, getString(R.string.app_font));
		Helper.setFonts(stationButton, getString(R.string.app_font));
		Helper.setFonts(timeButton, getString(R.string.app_font));

		arrialButton.setText(MainApplication
				.getLabel("flightstats.arrivals.label"));
		departButton.setText(MainApplication
				.getLabel("flightstats.departures.label"));

		departHead
				.setText(MainApplication.getLabel("flightstats.origin.label"));

		arriveHead.setText(MainApplication
				.getLabel("flightstats.destination.label"));
		// trainHead.setText(MainApplication.getLabel("flightstats.arrivals.label"));
		//"flightstats.time.label"
		depTimeHead.setText("Depart time");
		arrTimeHead.setText("Arrival time");
		intervalHead.setText("Interval");
		typeHead.setText("Type");

		container = (LinearLayout) baseView.findViewById(R.id.container);
		// container.setVisibility(View.GONE);

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.arrialButton:

					// for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("Arrivals", map);

					current_choice = ARRIVAL;
					//flightAdapter.setType(ARRIVAL);

					//setupTimeRange();
					
					filterTrainInfoArray.clear();
					trainAdapter.notifyDataSetChanged();

					trainList.setVisibility(View.VISIBLE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.GONE);

					departButton
							.setBackgroundResource(R.drawable.base_btn_selector);
					arrialButton.setBackgroundResource(R.drawable.button_on);
					
					if(trainInfoArray.size()>0){
						filterTrain();
					}
					else{
						requestService();
					}
					
					//getFlightInfo("arrival", current_time_choice);
					break;
				case R.id.departButton:

					// for Flurry log
					final Map<String, String> map2 = new HashMap<String, String>();
					map2.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("Departures", map2);

					//setupTimeRange();
					current_choice = DEPARTURE;
					//flightAdapter.setType(DEPARTURE);

					filterTrainInfoArray.clear();
					trainAdapter.notifyDataSetChanged();

					trainList.setVisibility(View.VISIBLE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.GONE);

					departButton.setBackgroundResource(R.drawable.button_on);
					arrialButton
							.setBackgroundResource(R.drawable.base_btn_selector);

					if(trainInfoArray.size()>0){
						filterTrain();
					}
					else{
						requestService();
					}
					break;
				case R.id.airportButton:
					// for Flurry log
					final Map<String, String> airportRange = new HashMap<String, String>();
					airportRange.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("TimeRange", airportRange);

					/* setupTimeRange(); */
					trainList.setVisibility(View.GONE);
					timeList.setVisibility(View.GONE);
					airportList.setVisibility(View.VISIBLE);
					break;
				case R.id.timeButton:

					// for Flurry log
					final Map<String, String> mapTimeRange = new HashMap<String, String>();
					mapTimeRange.put("Room",
							MainApplication.getMAS().getData("data_myroom"));
					FlurryAgent.logEvent("TimeRange", mapTimeRange);

					setupTimeRange();
					trainList.setVisibility(View.GONE);
					timeList.setVisibility(View.VISIBLE);
					airportList.setVisibility(View.GONE);
					break;
				case R.id.errorMessageDialogCloseImage:
					errorMessageDialogLayout.setVisibility(View.GONE);
					break;
				default:
					break;
				}
			}
		};

		arrialButton.setOnClickListener(onClickListener);
		departButton.setOnClickListener(onClickListener);
		stationButton.setOnClickListener(onClickListener);
		timeButton.setOnClickListener(onClickListener);
		errorMessageDialogCloseImage.setOnClickListener(onClickListener);

	}

	@SuppressWarnings("unchecked")
	private void requestService() {

		Parameters para = new Parameters();
		para.put("version", "1.0");
		para.put("station", "北京");
		
		ServiceAirportActivity s = (ServiceAirportActivity)this.getActivity();
		showLoading();

		ApiStoreSDK.execute(
				this.getActivity().getString(R.string.baidu_train_api),
				ApiStoreSDK.GET, para, new ApiCallBack() {
					@Override
					public void onSuccess(int status, String responseString) {
						Log.i("sdkdemo", "onSuccess");
						// mTextView.setText(responseString);

						Log.i("sdkdemo responseString:", responseString);
						try {
							JSONObject object = new JSONObject(responseString);
							JSONObject trainInfo = object.getJSONObject("data")
									.getJSONObject("trainInfo");

							Iterator x = trainInfo.keys();
							JSONArray trainArray = new JSONArray();

							while (x.hasNext()) {
								String key = (String) x.next();
								trainArray.put(trainInfo.get(key));
							}

							Log.i(TAG,
									"trainArray array = " + trainArray.length());

							for (int y = 0; y < trainArray.length(); y++) {
								JSONObject trainObj = (JSONObject) trainArray
										.get(y);
								Log.i(TAG, "trainObj = " + trainObj.toString());

								Train train = new Train();
								if (trainObj.has("code"))
									train.setCode(trainObj.getString("code"));
								
								if (trainObj.has("arriTime"))
									train.setArriTime(trainObj.getString("arriTime"));
								if (trainObj.has("sort"))
									train.setSort(trainObj.getInt("sort"));
								if (trainObj.has("interval"))
								train.setInterval(trainObj.getString("interval"));
								if (trainObj.has("station"))
									train.setStation(trainObj.getString("station"));
								if (trainObj.has("arriCity"))
									train.setArriCity(trainObj.getString("arriCity"));
								if (trainObj.has("deptTimeRange"))
								train.setDeptTimeRange(trainObj.getString("deptTimeRange"));
								if (trainObj.has("station"))
								train.setStation(trainObj.getString("station"));
								if (trainObj.has("arriStation"))
								train.setArriStation(trainObj.getString("arriStation"));
								
								if (trainObj.has("dayAfter"))
								train.setDayAfter(trainObj.getString("dayAfter"));
								if (trainObj.has("intervalSort"))
								train.setIntervalSort(trainObj.getInt("intervalSort"));
								if (trainObj.has("deptTime"))
								train.setDeptTime(trainObj.getString("deptTime"));
								
								if (trainObj.has("deptStation"))
								train.setDeptStation(trainObj.getString("deptStation"));
								
								if (trainObj.has("arriTimeRange"))
								train.setArriTimeRange(trainObj.getString("arriTimeRange"));
								
								if (trainObj.has("deptCity"))
								train.setDeptCity(trainObj.getString("deptCity"));
								
								if (trainObj.has("trainType"))
								train.setTrainType(trainObj.getString("trainType"));
								
								if (trainObj.has("arriCity_py"))
								train.setArriCity_py(trainObj.getString("arriCity_py"));
								
								if (trainObj.has("deptCity_py"))
								train.setDeptCity_py(trainObj.getString("deptCity_py"));
								
								if (trainObj.has("stationType"))
								train.setStationType(trainObj.getString("stationType"));
								
								if (trainObj.has("tType"))
								train.settType(trainObj.getString("tType"));

								trainInfoArray.add(train);

							}
							
							hideLoading();

							filterTrain();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onComplete() {
						Log.i("sdkdemo", "onComplete");
					}

					@Override
					public void onError(int status, String responseString,
							Exception e) {
						Log.i("sdkdemo", "onError, status: " + status);
						Log.i("sdkdemo",
								"errMsg: " + (e == null ? "" : e.getMessage()));
						// mTextView.setText(getStackTrace(e));
					}

				});

	}
	
	private void filterTrain(){
		filterTrainInfoArray.clear();
		
		for(int x=0; x<trainInfoArray.size();x++){
			Train train = trainInfoArray.get(x);
			if(current_choice == ARRIVAL){
				if(train.getArriCity_py().equalsIgnoreCase("beijing")){
					filterTrainInfoArray.add(train);
				}
			}
			else{
				if(train.getDeptCity_py().equalsIgnoreCase("beijing")){
					filterTrainInfoArray.add(train);
				}
			}
		}
		
		trainAdapter.notifyDataSetChanged();
	}

	public void onFlightInfoPressed() {

		arrialButton.performClick();
	};

	public void onFlightTrackPressed() {
		container.setVisibility(View.GONE);
		flightTrackContainer.setVisibility(View.VISIBLE);
	}

	public void onAirlinePressed(int position, Airline _airline) {
		if (flightTrackFragment != null) {
			flightTrackFragment.onAirlinePressed(position, _airline);
		}
	}

	public void onAirportPressed(int position, Airport _airport) {
		if (flightTrackFragment != null) {
			flightTrackFragment.onAirportPressed(position, _airport);
		}
	}

	private void setupTimeRange() {
		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();

		try {
			// String additionString =
			// MainApplication.getMAS().getData("minute");

			String supply0 = "";
			String time0 = "";

			Log.i(TAG,
					"the hour length = "
							+ MainApplication.getMAS().getData("hour").length());

			if (MainApplication.getMAS().getData("hour").length() > 0) {

				if ((Integer.parseInt(MainApplication.getMAS().getData("hour")) + 5) % 24 < 10) {
					supply0 = "0";
				}
				time0 = MainApplication.getMAS().getData("hour")
						+ ":"
						+ "00"
						+ " - "
						+ supply0
						+ (Integer.parseInt(MainApplication.getMAS().getData(
								"hour")) + 5) % 24 + ":" + "00";
			} else {
				if ((today.hour + 5) % 24 < 10) {
					supply0 = "0";
				}
				time0 = today.hour + ":" + "00" + " - " + supply0
						+ (today.hour + 5) % 24 + ":" + "00";
			}

			// timeButton.setText(time0);

			timeRangeArray.clear();
			timeRangeArray.add(time0);
			timeRangeArray.add("00:00 - 04:00");
			timeRangeArray.add("04:00 - 08:00");
			timeRangeArray.add("08:00 - 12:00");
			timeRangeArray.add("12:00 - 16:00");
			timeRangeArray.add("16:00 - 20:00");
			timeRangeArray.add("20:00 - 24:00");

			timeAdapter.notifyDataSetChanged();
		} catch (Exception e) {

		}
	}

	private void showError(String msg) {
		errorMessageDialogLayout.setVisibility(View.VISIBLE);
		errorMessageDialogLabel.setText(msg);
	}

	private void getFlightInfo(String type, int _startHour) {
		/*
		 * mDialog.setCancelable(false);
		 * mDialog.setMessage(MainApplication.getLabel("loading"));
		 * 
		 * showDialog();
		 */
		showProgress();

		Log.i(TAG, "getFlightInfo start");
		apiCallType = ApiCallType.GET_FLIGHT_INFO;

		try {

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();

			String apiType = "";
			int startHour = 0;

			if (type.equalsIgnoreCase("arrival")) {
				apiType = this.getString(R.string.arrival);
			} else {
				apiType = this.getString(R.string.departure);
			}

			startHour = _startHour;

			if (_startHour == CURRENT_TIME_CODE
					&& MainApplication.getMAS().getData("hour").length() > 0) {
				startHour = Integer.parseInt(MainApplication.getMAS().getData(
						"hour"));
			} else {
				startHour = today.hour;
			}

			URL url = null;
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/HKG/arr/2014/10/11/23?appId=1b5bbebb&appKey=0bd343eb43861dbd7c82996a3f8d07ac&utc=false&numHours=1&maxFlights=5

			String airportCode = this.getString(R.string.pbh_local_airport);

			if (this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")) {
				airportCode = this.getString(R.string.phk_local_airport);
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
				airportCode = this.getString(R.string.pbj_local_airport);
				// airportCode =
				// airportArray.get(current_airport_choice).getIATA();
			}
			Log.i("PLANE", MainApplication.getMAS().getData("year"));
			Log.i("PLANE", MainApplication.getMAS().getData("month"));
			Log.i("PLANE", MainApplication.getMAS().getData("day"));
			Log.i("PLANE", MainApplication.getMAS().getData("hour"));

			url = new URL(
					this.getString(R.string.flightstat_api_base)
							+ "airport/"
							+ "status/"
							+ airportCode
							+ "/"
							+ apiType
							+ "/"
							+ MainApplication.getMAS().getData("year")
							+ "/"
							+ MainApplication.getMAS().getData("month")
							+ "/"
							+ MainApplication.getMAS().getData("day")
							+ "/"
							+ startHour
							+ "/"
							+ "?appId="
							+ this.getString(R.string.flightstat_app_id)
							+ "&appKey="
							+ this.getString(R.string.flightstat_app_token)
							+ "&utc=false&numHours="
							+ (int) (current_time_choice == CURRENT_TIME_CODE ? NUM_OF_HOUR_RANGE + 1
									: NUM_OF_HOUR_RANGE)
							+ "&maxFlights=150"
							+ "&extendedOptions=languageCode:"
							+ Helper.changeLangCode(
									MainApplication.getMAS().getData(
											"data_language") == "" ? "E"
											: MainApplication.getMAS().getData(
													"data_language"))
									.toString());

			if (MainApplication.getMAS().getData("year").equalsIgnoreCase("")) {
				Log.i("PLANE", "use local");

				url = new URL(
						this.getString(R.string.flightstat_api_base)
								+ "airport/"
								+ "status/"
								+ airportCode
								+ "/"
								+ apiType
								+ "/"
								+ today.year
								+ "/"
								+ (today.month + 1)
								+ "/"
								+ today.monthDay
								+ "/"
								+ startHour
								+ "/"
								+ "?appId="
								+ this.getString(R.string.flightstat_app_id)
								+ "&appKey="
								+ this.getString(R.string.flightstat_app_token)
								+ "&utc=false&numHours="
								+ (int) (current_time_choice == CURRENT_TIME_CODE ? NUM_OF_HOUR_RANGE + 1
										: NUM_OF_HOUR_RANGE)
								+ "&maxFlights=150"
								+ "&extendedOptions=languageCode:"
								+ Helper.changeLangCode(
										MainApplication.getMAS().getData(
												"data_language") == "" ? "E"
												: MainApplication
														.getMAS()
														.getData(
																"data_language"))
										.toString());
			}

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		hideProgress();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideProgress();
		if (apiCallType == ApiCallType.GET_FLIGHT_INFO) {
			GetFlightStatusParser parser = new GetFlightStatusParser(json, this);
			parser.setType(current_choice);
			parser.startParsing();
		}
	}

	@Override
	public void onGetFlightStatusParsingError(int failMode,
			boolean isPostExecute) {

		showError(MainApplication.getLabel("flightstats.empty.label"));
	}

	@Override
	public void onGetFlightStatusFinishParsing(
			ArrayList<FlightInfo> _flightArrivalArray) {
		// TODO Auto-generated method stub
		hideProgress();

		flightInfoArray.clear();
		flightInfoArray.addAll(_flightArrivalArray);
		Log.i(TAG, flightInfoArray.toString());

		if (current_choice == ARRIVAL) {
			// Now sort by the flight info base on arrival date.
			Collections.sort(flightInfoArray, new Comparator<FlightInfo>() {
				public int compare(FlightInfo one, FlightInfo other) {
					return one.getArrivalDate().compareTo(
							other.getArrivalDate());
				}
			});
		} else if (current_choice == DEPARTURE) {
			// Now sort by the flight info base on departure date.
			Collections.sort(flightInfoArray, new Comparator<FlightInfo>() {
				public int compare(FlightInfo one, FlightInfo other) {
					return one.getDepartureDate().compareTo(
							other.getDepartureDate());
				}
			});
		}

		flightAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetFlightStatusError() {
		// TODO Auto-generated method stub

	}
	private void showLoading(){
		mProgressBar.setVisibility(View.VISIBLE);
	}
	private void hideLoading(){
		mProgressBar.setVisibility(View.GONE);
	}

}