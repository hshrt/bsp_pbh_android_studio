package com.hsh.esdbsp.fragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceGeneralItemActivity;
import com.hsh.hshservice.adapter.AirlineAdapter;
import com.hsh.hshservice.adapter.AirportAdapter;
import com.hsh.hshservice.adapter.FlightRouteAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.dinning.adapter.FoodSubCatAdapter;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airline;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllAirlineParser;
import com.hsh.hshservice.parser.GetAllAirlineParser.GetAllAirlineParserInterface;
import com.hsh.hshservice.parser.GetAllAirportParser;
import com.hsh.hshservice.parser.GetAllAirportParser.GetAllAirportParserInterface;
import com.hsh.hshservice.parser.GetFlightStatusParser;
import com.hsh.hshservice.parser.GetFlightStatusParser.GetFlightStatusParserInterface;
import com.hsh.hshservice.parser.GetFlightTrackParser;

public class InRoomDinItemFragment extends BaseFragment{

	private final String TAG = "InRoomDinItemFragment";

	private View baseView;
	private ProgressBar mProgressBar;

	private ListView foodList;
	private Button backBtn;
	
	private ArrayList<GeneralItem> foodItemArray;
	
	private FoodSubCatAdapter foodItemAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.cms_in_room_sub_category_pch, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();
		
		if(foodItemArray == null){
			foodItemArray = new ArrayList<GeneralItem>();
		}
		
		
		foodItemArray = this.getArguments().getParcelableArrayList("foodItemArray");
		
		foodItemAdapter = new FoodSubCatAdapter(this.getActivity(), foodItemArray);
		foodItemAdapter.setParentName(TAG);
		foodList.setAdapter(foodItemAdapter);
		
		return baseView;
	}
	
	public void setupList(ArrayList<GeneralItem> _foodSubCatArray){
		foodItemArray.clear();;
		
		for(GeneralItem item : _foodSubCatArray){
			foodItemArray.add(item);
    	}
		
		Log.i(TAG,"fooSubCatArray length = " + foodItemArray.size());
		foodItemAdapter.setCurrentPosition(0);
		foodItemAdapter.notifyDataSetChanged();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	private void initUI() {
		foodList = (ListView) baseView.findViewById(R.id.list);
		backBtn = (Button) baseView.findViewById(R.id.backBtn);
		
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InRoomDinItemFragment.this.getActivity().getSupportFragmentManager().popBackStack();
			}
		});
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

}
