package com.hsh.esdbsp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.LangActivity;
import com.hsh.esdbsp.activity.RadioActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.activity.TVActivity;
import com.hsh.esdbsp.activity.WeatherActivity;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceAirportActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.global.Log;

public class TopBarFragment extends BaseFragment {

	private final String TAG = "TopBarFragment";

	private View baseView;

	private String titleId;

	private Boolean hideBackBtn; //should hide backBtn or not
	private String hightLightChoice; // a string to store when button should be hightlighted

	private MyTextView rcHeadClock;

	private Button backBtn;

	private MyTextView title;
	private MyTextView headRC;
	private MyTextView headService;
	private MyTextView headTV;
	private MyTextView headRadio;
	private MyTextView headLang;
	private MyTextView headWeather;
	
	//for flight stat
	private MyTextView flightInformation;
	private MyTextView flightTracking;

	// ////////////////////////////////////////////////////
	
	// for bsp sleep
	private WakeLock mWakeLock;
	private PowerManager powerManager;
	

	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5 * 1000;

	// ////////////////////////////////////////////////////

	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 300 * 1000;

	// ////////////////////////////////////////////////////

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		Log.d(TAG, "test commit");
		if(RoomControlActivity.timeoutHandler != null){
			Log.i(TAG, "remove timeoutHandler");
			RoomControlActivity.timeoutHandler.removeCallbacksAndMessages(null);
		}

		
		baseView = inflater.inflate(R.layout.top_bar, container, false);

		titleId = this.getArguments().getString("titleId");
		hideBackBtn = this.getArguments().getBoolean("hideBackBtn");
		hightLightChoice = this.getArguments().getString("hightLightChoice");

		Log.i(TAG, "titleId = " + titleId);
		
		//this is a handle to make sure the screen is always on for activity which are not room control
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		powerManager = (PowerManager) getActivity().getSystemService(MainApplication.getContext().POWER_SERVICE);
		mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getActivity().getClass().getName());
		mWakeLock.acquire();

		loadPageItem();

		// addRunnableCall();

		loadPageLabel();
		
		//in PHK RollRoyce App, no TV, radio and room control
		if(this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
			headRC.setVisibility(View.GONE);
			headTV.setVisibility(View.GONE);
			headRadio.setVisibility(View.GONE);
		}

		return baseView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		addRunnableCall();

		super.onResume();
	}

	@Override
	public void onPause() {

		//clearReferences();

		removeRunnableCall();

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		//clearReferences();

		removeRunnableCall();

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		mWakeLock.release();
		super.onDestroy();
	}

	private void clearReferences() {
		try {
			Activity currActivity = MainApplication.getCurrentActivity();

			if (currActivity != null && currActivity.equals(this.getActivity())) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {
		}
	}

	// ////////////////////////////////////////////////////////

	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext()
					.getString(R.string.timeoutvalue)) * 1000;
			
			if(MainApplication.getCurrentActivity() instanceof ServiceMainActivity 
					&& this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				timeoutDelay = 40*1000;
			}

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);

		} catch (Exception e) {
		}
	}

	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);

		} catch (Exception e) {
		}
	}

	// ////////////////////////////////////////////////////////

	private void handleSleep() {
		try {

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

		} catch (Exception e) {
		}

	}

	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);
				Log.i(TAG,"highLightChoice = " + hightLightChoice);
				if (hightLightChoice == null || (hightLightChoice.compareToIgnoreCase("radio") != 0 && hightLightChoice.compareToIgnoreCase("setting") != 0)) {
					goMain();
				}

			} catch (Exception e) {
			}
			

		}
	};

	// ////////////////////////////////////////////////////////

	public void goMain() {
		
		Log.i(TAG, "TopBar go main fire");
		try {
			Log.i(TAG, "TopBar go main fire2");
			Intent intent;
			
			if(this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				intent = new Intent(MainApplication.getContext(),
						ServiceMainActivity.class);
				if(MainApplication.getCurrentActivity() instanceof ServiceMainActivity){
					return;
				}
			}
			else{
				intent = new Intent(MainApplication.getContext(),
					RoomControlActivity.class);
			}
			//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			MainApplication.getCurrentActivity().startActivity(intent);
			Log.i(TAG, "goBackToRoom");
			MainApplication.getCurrentActivity().finish();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadPageLabel() {
		try {
			
			// change all label
			
			//for case of Airport
			if(titleId != null && titleId.compareToIgnoreCase("Flight") == 0){
				
				flightInformation.setVisibility(View.VISIBLE);
				flightTracking.setVisibility(View.VISIBLE);
				
				title.setVisibility(View.GONE);
				headRC.setVisibility(View.GONE);
				headService.setVisibility(View.GONE);
				headTV.setVisibility(View.GONE);
				headRadio.setVisibility(View.GONE);
				headLang.setVisibility(View.GONE);
				headWeather.setVisibility(View.GONE);
				
				flightInformation.setText(MainApplication.getLabel("flightstats.name.label"));
				flightTracking.setText(MainApplication.getLabel("flighttrack.name.label"));
				
			}
			else if (titleId != null) {
				title.setText(MainApplication.getLabel(titleId).toUpperCase());
				//title.setText("                ");
				title.setBackgroundResource(R.drawable.actionbar_selected);
				
				headRC.setVisibility(View.GONE);
				headService.setVisibility(View.GONE);
				headTV.setVisibility(View.GONE);
				headRadio.setVisibility(View.GONE);
				headLang.setVisibility(View.GONE);
				headWeather.setVisibility(View.GONE);
				
			}
			else {
				title.setVisibility(View.GONE);
				
				headRC.setText(MainApplication.getLabel("RC").toUpperCase());
				headService.setText(MainApplication.getLabel("Services")
						.toUpperCase());
				headTV.setText(MainApplication.getLabel("TV").toUpperCase());
				headRadio.setText(MainApplication.getLabel("Radio")
						.toUpperCase());
				headLang.setText(MainApplication.getLabel("Lang").toUpperCase());
				headWeather.setText(MainApplication.getLabel("Weather")
						.toUpperCase());
				
				//for PBH, there is TV function
				if(this.getString(R.string.hotel).equalsIgnoreCase("pbh")){
					headTV.setVisibility(View.GONE);					
				}
				
				if(this.getString(R.string.hastvpage).equalsIgnoreCase("notv")){
					headTV.setVisibility(View.GONE);					
				}
				//Log.i(TAG, "JAMJAMJAM");
			}
			

		} catch (Exception e) {
			//Log.i(TAG, "set label error = " + );
			e.printStackTrace();
		}
		;
	}

	private void loadPageItem() {
		
		try {
			
			
			
			flightInformation = (MyTextView) baseView.findViewById(R.id.flightInformation);
			flightTracking = (MyTextView) baseView.findViewById(R.id.flightTracking);
			
			flightInformation.setBackgroundResource(R.drawable.actionbar_selected);
			flightTracking.setBackgroundResource(0);
			
			flightInformation.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					ServiceAirportActivity s = (ServiceAirportActivity)TopBarFragment.this.getActivity();
					s.onFlightInfoPressed();
					
					flightInformation.setBackgroundResource(R.drawable.actionbar_selected);
					flightTracking.setBackgroundResource(0);
					
					return false;
				}
			});
			
			flightTracking.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					ServiceAirportActivity s = (ServiceAirportActivity)TopBarFragment.this.getActivity();
					s.onFlightTrackPressed();
					// TopBarFragment.this.getActivity().onBackPressed();
					
					flightTracking.setBackgroundResource(R.drawable.actionbar_selected);
					flightInformation.setBackgroundResource(0);
					
					return false;
				}
			});
			
			backBtn = (Button) baseView.findViewById(R.id.backBtn);
			
			if (hideBackBtn) {
				backBtn.setVisibility(View.GONE);
				((ImageView)baseView.findViewById(R.id.backBtnImage)).setVisibility(View.GONE);
			}
			;
			
			backBtn.setOnClickListener(new OnClickListener() {
				
				

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					TopBarFragment.this.getActivity().onBackPressed();
					// TopBarFragment.this.getActivity().onBackPressed();
					Log.i(TAG, "Class = " + TopBarFragment.this.getActivity().getClass().toString());
				}
			});

			/*backBtn.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					
				}
			});*/

			title = (MyTextView) baseView.findViewById(R.id.title);
			headService = (MyTextView) baseView.findViewById(R.id.headService);
			// ////////////////////////////////////////////////////////

			headRC = (MyTextView) baseView.findViewById(R.id.headRoomControl);
			headRC.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						
						
						
						
						
						MainApplication.playClickSound(headRC);
						Log.i(TAG, "goMain xxx");
						Intent intent = new Intent(TopBarFragment.this
								.getActivity().getApplicationContext(),
								RoomControlActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
						Log.i(TAG, e.toString());
					}

					return false;
				}

			});
			
			headService.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						
						if(MainApplication.getCurrentActivity() instanceof ServiceMainActivity){							
							return false;
						}
						
						MainApplication.playClickSound(headTV);
						
						Intent intent = new Intent(TopBarFragment.this
								.getActivity().getApplicationContext(),
								ServiceMainActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();
					

					} catch (Exception e) {
					}

					return false;
				}

			});

			headTV = (MyTextView) baseView.findViewById(R.id.headTV);
			//headTV.setVisibility(View.GONE);

			headTV.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						if(MainApplication.getCurrentActivity() instanceof TVActivity){							
							return false;
						}
						
						MainApplication.playClickSound(headTV);
						Intent intent = new Intent(TopBarFragment.this
								.getActivity().getApplicationContext(),
								TVActivity.class);
						
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			headRadio = (MyTextView) baseView.findViewById(R.id.headRadio);
			headRadio.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						if(MainApplication.getCurrentActivity() instanceof RadioActivity){							
							return false;
						}
						
						MainApplication.playClickSound(headRadio);

						Intent intent = new Intent(TopBarFragment.this
								.getActivity().getApplicationContext(),
								RadioActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			headLang = (MyTextView) baseView.findViewById(R.id.headLang);
			headLang.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						
						if(MainApplication.getCurrentActivity() instanceof LangActivity){							
							return false;
						}
						
						MainApplication.playClickSound(headLang);

						Intent intent = new Intent(
								MainApplication.getContext(),
								LangActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			// ///////////////

			headWeather = (MyTextView) baseView.findViewById(R.id.headWeather);
			headWeather.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						if(MainApplication.getCurrentActivity() instanceof WeatherActivity){							
							return false;
						}
						
						MainApplication.playClickSound(headWeather);
						
						Intent intent = new Intent(
								MainApplication.getContext(),
								WeatherActivity.class);
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			// /////////////////////////////////////////////////////////

			rcHeadClock = (MyTextView) baseView.findViewById(R.id.headClock);
			rcHeadClock.setText("");

			try {
				String tmpHeadClock[] = MainApplication.getMAS()
						.getData("data_curtime").split(":");
				int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
				int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
				String myHeadClockSign = "am";

				if (myHeadClockHour == 0) {
					myHeadClockHour = 12;
					myHeadClockSign = "am";
				} else if (myHeadClockHour == 12) {
					myHeadClockHour = myHeadClockHour;
					myHeadClockSign = "pm";
				} else if (myHeadClockHour > 12) {
					myHeadClockHour = myHeadClockHour - 12;
					myHeadClockSign = "pm";
				} else {
					myHeadClockSign = "am";
				}

				rcHeadClock.setText(Html.fromHtml("<font>"
						+ MainApplication.twoDigitMe(myHeadClockHour) + ":"
						+ MainApplication.twoDigitMe(myHeadClockMin)
						+ "</font><small>" + myHeadClockSign + "</small>"));

			} catch (Exception e) {
			}

			if (hightLightChoice.compareToIgnoreCase("service") == 0) {
				headService.setBackgroundResource(R.drawable.actionbar_selected);
			}
			else if (hightLightChoice.compareToIgnoreCase("tv") == 0) {
				headTV.setBackgroundResource(R.drawable.actionbar_selected);
			}
			else if (hightLightChoice.compareToIgnoreCase("radio") == 0) {
				headRadio.setBackgroundResource(R.drawable.actionbar_selected);
			}
			else if (hightLightChoice.compareToIgnoreCase("weather") == 0) {
				headWeather.setBackgroundResource(R.drawable.actionbar_selected);
			}
			else if (hightLightChoice.compareToIgnoreCase("lang") == 0) {
				headLang.setBackgroundResource(R.drawable.actionbar_selected);
			}
			// /////////////////////////////////////////////////////////

		} catch (Exception e) {
		}

	}

	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {

				guiHandler.removeCallbacks(guiRunnable);

				try {
					// battery
					ImageView imgBB = (ImageView) baseView
							.findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources()
							.getDrawable(getResources().getIdentifier(
									"drawable/battery_"
											+ MainApplication
													.getBatteryChecker()
													.getBatteryLevel(),
									"drawable",
									TopBarFragment.this.getActivity()
											.getPackageName()))));

				} catch (Exception e) {
				}

				try {

					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();

						Log.v(TAG, "alarm wake");
					}

				} catch (Exception e) {
				}

				try {
					try {
						String tmpHeadClock[] = MainApplication.getMAS()
								.getData("data_curtime").split(":");
						int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
						int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
						String myHeadClockSign = "am";

						if (myHeadClockHour == 0) {
							myHeadClockHour = 12;
							myHeadClockSign = "am";
						} else if (myHeadClockHour == 12) {
							myHeadClockHour = myHeadClockHour;
							myHeadClockSign = "pm";
						} else if (myHeadClockHour > 12) {
							myHeadClockHour = myHeadClockHour - 12;
							myHeadClockSign = "pm";
						} else {
							myHeadClockSign = "am";
						}

						rcHeadClock.setText(Html.fromHtml("<font>"
								+ MainApplication.twoDigitMe(myHeadClockHour)
								+ ":"
								+ MainApplication.twoDigitMe(myHeadClockMin)
								+ "</font><small>" + myHeadClockSign
								+ "</small>"));

					} catch (Exception e) {
					}

				} catch (Exception e) {
				}

				// ////////////////////////////////////////////////////////

				// ////////////////////////////////////////////////////////

				loadPageLabel();

				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);

			} catch (Exception e) {
			}
		}
	};
	public void resetBackMainTimer(){
		Log.i(TAG, "resetBackMainTimer fire"); 
		timeoutHandler.removeCallbacks(timeoutRunnable);
		timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
		//this is a handle to make sure the screen is always on for activity which are not room control
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

}
