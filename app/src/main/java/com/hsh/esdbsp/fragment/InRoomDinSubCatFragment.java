package com.hsh.esdbsp.fragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.adapter.AirlineAdapter;
import com.hsh.hshservice.adapter.AirportAdapter;
import com.hsh.hshservice.adapter.FlightRouteAdapter;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.dinning.adapter.FoodSubCatAdapter;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.hsh.hshservice.model.Airline;
import com.hsh.hshservice.model.Airport;
import com.hsh.hshservice.model.FlightInfo;
import com.hsh.hshservice.model.GeneralItem;
import com.hsh.hshservice.network.ApiRequest;
import com.hsh.hshservice.network.XMLCaller;
import com.hsh.hshservice.parser.GetAllAirlineParser;
import com.hsh.hshservice.parser.GetAllAirlineParser.GetAllAirlineParserInterface;
import com.hsh.hshservice.parser.GetAllAirportParser;
import com.hsh.hshservice.parser.GetAllAirportParser.GetAllAirportParserInterface;
import com.hsh.hshservice.parser.GetFlightStatusParser;
import com.hsh.hshservice.parser.GetFlightStatusParser.GetFlightStatusParserInterface;
import com.hsh.hshservice.parser.GetFlightTrackParser;

public class InRoomDinSubCatFragment extends BaseFragment{

	private final String TAG = "InRoomDinSubCatFragment";

	private View baseView;
	private ProgressBar mProgressBar;

	private ListView subCatList;
	private Button backBtn;
	
	private ArrayList<GeneralItem> foodSubCatArray;
	
	private FoodSubCatAdapter foodSubCatAdapter;

	public enum ApiCallType {
		GET_FLIGHT_STATUS, GET_FLIGHT_TRACK_BY_ID, GET_FLIGHT_TRACK_BY_DESTINATION, GET_ALL_AIRLINE, GET_ALL_AIRPORT
	}

	ApiCallType apiCallType;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.cms_in_room_sub_category_pch, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();
		
		if(foodSubCatArray == null){
			foodSubCatArray = new ArrayList<GeneralItem>();
		}
		
		foodSubCatAdapter = new FoodSubCatAdapter(this.getActivity(), foodSubCatArray);
		subCatList.setAdapter(foodSubCatAdapter);
		
		return baseView;
	}
	
	public void setupList(ArrayList<GeneralItem> _foodSubCatArray){
		foodSubCatArray.clear();;
		
		for(GeneralItem item : _foodSubCatArray){
    		foodSubCatArray.add(item);
    	}
		
		Log.i(TAG,"fooSubCatArray length = " + foodSubCatArray.size());
		foodSubCatAdapter.setCurrentPosition(0);
    	foodSubCatAdapter.notifyDataSetChanged();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
	
	private void setupLabel(){
		
	}

	private void initUI() {
		subCatList = (ListView) baseView.findViewById(R.id.list);
		backBtn = (Button) baseView.findViewById(R.id.backBtn);		
		backBtn.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

}
