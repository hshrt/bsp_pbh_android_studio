package com.hsh.esdbsp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.LangActivity;
import com.hsh.esdbsp.activity.RadioActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.customclass.BaseFragment;
import com.hsh.hshservice.global.Log;

public class BottomBarFragment extends BaseFragment {

	private final String TAG = "BottomBarFragment";

	private View baseView;

	private MyTextView footRoomNum;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.bottom_bar, container, false);

		footRoomNum = (MyTextView) baseView.findViewById(R.id.footRoomNum);
		
		footRoomNum.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));
		
		try{
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				footRoomNum.setVisibility(View.GONE);
			}
		}
		catch (Exception e) {
		}
			
		return baseView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

	public void goMain() {
		try {

			Intent intent = new Intent(MainApplication.getContext(),
					RoomControlActivity.class);
			MainApplication.getCurrentActivity().startActivity(intent);

		} 
		catch (Exception e) {
		}
	}

}
