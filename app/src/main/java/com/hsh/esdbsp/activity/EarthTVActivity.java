package com.hsh.esdbsp.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.earthtv.publicapihelper.Channel;
import com.earthtv.publicapihelper.EarthTvPublicApiClient_;
import com.earthtv.publicapihelper.Location;
import com.earthtv.publicapihelper.PlayListItem;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.customclass.BaseActivity;

import java.util.Iterator;
import java.util.List;

public class EarthTVActivity extends BaseActivity {

    EarthTvPublicApiClient_ apiClient;
    Button reloadButton;
    List<PlayListItem> playList;
    PlayListItem currentClip;
    VideoView videoPlayer;
    MediaController mediaController;
    Iterator<PlayListItem> playListIterator;
    TextView textView;


    protected void startPlayList(){

        Log.i("Main", "startPlayList fire");

        // 1080 HD example, EarthTV selected clips only
        // playList = apiClient.buildPlaylist("limit=20&channel=BestOf&location_id=BKK&location_id=HKG&location_id=HND", 1080, 5000000L, 360);
        //
        // 720 HD example, last 20 clips recorded
        // playList = apiClient.buildPlaylist("limit=20&channel=Latest", 720, 2500000L, 360);
        //
        // How to use tags, sunset hour clips only
        // playList = apiClient.buildPlaylist("limit=20&tags=sunset", 360, 1800000L, 360);
        //
        // Mobile Optimized Bitrate Example
        playList = apiClient.buildPlaylist("limit=20&channel=Latest", 360, 800000L, 360);


        playListIterator = playList.iterator();
        playNextVideo();
    }

    protected void playNextVideo(){
        if (playListIterator.hasNext()) {
            // Get next clip from playlist
            currentClip = playListIterator.next();
            // Send Statistic Event to EarthTV
            apiClient.sendStatisticVideoPlayEvent(currentClip);
            // Load video
            videoPlayer.setVideoURI(Uri.parse(currentClip.videoURI));
            videoPlayer.requestFocus();

        } else {
            Log.d("Player", "Nothing to play");
            textView.setText("");
        }
    }


    @Override
	public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthtv);

        Log.i("Main", "onCreate fire");

        this.reloadButton = (Button)this.findViewById(R.id.button);
        this.videoPlayer = (VideoView)this.findViewById(R.id.videoView);
        this.textView = (TextView)this.findViewById(R.id.textView);


        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoPlayer);
        videoPlayer.setMediaController(mediaController);


        // Resize Player
        Point screenSize = new Point();
        //getWindowManager().getDefaultDisplay().getSize(screenSize);
        float screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ViewGroup.LayoutParams playerSize = videoPlayer.getLayoutParams();
        playerSize.width = Math.round(screenWidth - (screenWidth *0.1f));
        playerSize.height = Math.round((screenWidth- (screenWidth *0.1f)) * 9f / 16f);
        videoPlayer.setLayoutParams(playerSize);


        // Init Library
        apiClient = EarthTvPublicApiClient_.getInstance_(this);
        // Set token, origin identifier and language.
        apiClient.initEarthTVPublicAPIClient("55b1e8d7bbf04785a35f6af7", "http://www.hshgroup.com/", "en_US");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        this.reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoPlayer.isPlaying()) videoPlayer.stopPlayback();
                startPlayList();
            }
        });


        videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // show some brief clip info
                textView.setText(currentClip.country + "/" + currentClip.city);
                // start playback
                videoPlayer.start();

            }
        });


        videoPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // prepare next clip from playlist
                playNextVideo();
            }
        });

        startPlayList();
    }

}
