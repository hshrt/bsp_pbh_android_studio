package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.Helper;

//william test 2
public class LangActivity extends BaseActivity {
	
	String myLOG = "LangActivity";
	 
	LinearLayout langE;
	LinearLayout langCT;
	LinearLayout langCS;
	LinearLayout langJ;
	LinearLayout langF;
	LinearLayout langK;
	LinearLayout langP;
	LinearLayout langS;
	LinearLayout langG;
	LinearLayout langR;
	LinearLayout langA;

	//-----------
	
	MyTextView rcHeadClock;

	MyTextView headRC;
	MyTextView headService;
	MyTextView headTV;
	MyTextView headRadio;
	MyTextView headLang;
	
	MyTextView headWeather;
	
	MyTextView footRoom;	
	
	//-----------
	
	LinearLayout layoutPage;
	
	String myCMD;
	
	//////////////////////////////////////////////////////
	
	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5*1000;
	
	//////////////////////////////////////////////////////
	
	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30*1000;	
	
	//////////////////////////////////////////////////////
	
	private TopBarFragment topBarfragment;
	
	private LinearLayout pinDialogLayout;
	private RelativeLayout pinDialogInnerLayout;
	private Button okButton;
	private ImageView pinCloseImage;
	private EditText pinInput;
	private Button goPinButton;
	
/*	@Override
    public void onUserInteraction(){
        //renew your timer here
		
		try {
			handleSleep();
			
		} catch (Exception e) {}
    }	*/

	
	@Override
	protected void onResume() {
		
		try {
		    MainApplication.setCurrentActivity(this);
		    
		    addRunnableCall();
	    
		} catch (Exception e) {}
	    
        super.onResume();
    }
	
	@Override
	protected void onPause() {
		try {
	        clearReferences();
	        
	        removeRunnableCall();
		} catch (Exception e) {}

        super.onPause();
    }
	
	
	@Override
    protected void onDestroy() {     
		try {
	        clearReferences();
	        
	        removeRunnableCall();
		} catch (Exception e) {}

        super.onDestroy();
    }	
	
//////////////////////////////////////////////////////////
    
	private void clearReferences(){
		try {
			Activity currActivity = MainApplication.getCurrentActivity();
			
			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {}
	}
	
//////////////////////////////////////////////////////////	
	
	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext().getString(R.string.timeoutvalue))*1000;
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);
			
		} catch (Exception e) {}
	}
	
	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);
			
		} catch (Exception e) {}
	}	
	
	
//////////////////////////////////////////////////////////
	
	
	private void handleSleep() {
		try {
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
		} catch (Exception e) {}
		
	}
	
	
	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				goMain();
				
			} catch (Exception e) {}
			
		}
	};	
	
	
//////////////////////////////////////////////////////////
	
	
	public void goMain() {
		try {
			
			
			
			Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				intent = new Intent(MainApplication.getContext(), ServiceMainActivity.class);
			}
			
			MainApplication.getCurrentActivity().startActivity(intent);				
			
		} catch (Exception e) {}
	}	
	
	

//////////////////////////////////////////////////////////
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			setContentView(R.layout.language_all);
			
			MainApplication.setCurrentActivity(this);
			
			//for Flurry log
			final Map<String, String> map = new HashMap<String, String>();
			map.put("Room", MainApplication.getMAS().getData("data_myroom"));
			
			FlurryAgent.logEvent("Languages", map);
			
			FragmentManager fragmentManager = getSupportFragmentManager();

			Bundle bundle = new Bundle();
			// bundle.putString("titleId", "");
			bundle.putBoolean("hideBackBtn", true);
			bundle.putString("hightLightChoice", "lang");

			// Create new fragment and transaction
			topBarFragment = new TopBarFragment();
			topBarFragment.setArguments(bundle);

			FragmentTransaction transaction = fragmentManager.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.topBarFragmentContainer, topBarFragment);

			// Commit the transaction
			transaction.commit();
			
			Helper.showInternetWarningToast();
			
			loadPageItem();
			
			addRunnableCall();
			
			loadPageLabel();
			
			
			
		} catch (Exception e) {}
	}
	
	
	private void loadPageLabel() {
		try {

			
			if(this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				footRoom.setVisibility(View.GONE);
			}
			
			footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));

		} catch (Exception e) {}
	}	
	
	
	private void loadPageItem() {
    	
    	try {
    		
    		MainApplication.brightUp();
    		
    		layoutPage = (LinearLayout)findViewById(R.id.layoutPage);
    		layoutPage.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						
						//handleSleep();
						
					} catch (Exception e) {}
					
					return false;
				}
    		});
    		
    		
			
    		
			footRoom = (MyTextView) findViewById(R.id.footRoomNum);
    		footRoom.setText("");
    		
    		///////////////////////////////////////////////////////////

	        
	        langE =  (LinearLayout)findViewById(R.id.langE);
	        langE.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("English", map);
					
					try {
						myCMD = "SET%20LANG%20E";
						
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("E");
	
						goMain();
						
					} catch (Exception e) {
						Log.v(myLOG,e.toString());
					}
				}
	        	
	        });
	        
	        
	        
	        langCT =  (LinearLayout)findViewById(R.id.langCT);
	        langCT.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("TraditionalChinese", map);
					
					try {
						myCMD = "SET%20LANG%20CT";
						
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("CT");
	
						goMain();
						
					} catch (Exception e) {}
				}
	        	
	        });	  
	        
	        
	        
	        langCS =  (LinearLayout)findViewById(R.id.langCS);
	        langCS.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("SimpleChinese", map);
					
					try {
						myCMD = "SET%20LANG%20CS";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("CS");
	
						goMain();
						
					} catch (Exception e) {}
				}
	        	
	        });	        
	        
	        
	        
	        langJ =  (LinearLayout)findViewById(R.id.langJ);
	        langJ.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Japanese", map);
					
					try {
						myCMD = "SET%20LANG%20J";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("J");
	
						goMain();
						
					} catch (Exception e) {}
				}
	        	
	        });	 	        
	        
	        
	        
	        langF =  (LinearLayout)findViewById(R.id.langF);
	        langF.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("French", map);
					
					try {
						myCMD = "SET%20LANG%20F";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("F");
	
						goMain();
					} catch (Exception e) {}
				}
	        	
	        });	 	
	        
	        
	        
	        langK =  (LinearLayout)findViewById(R.id.langK);
	        langK.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Korean", map);
					
					try {
						myCMD = "SET%20LANG%20K";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("K");
	
						goMain();
						
					} catch (Exception e) {}
				}
	        	
	        });	 
	        
	        
	        
	        langP =  (LinearLayout)findViewById(R.id.langP);
	        langP.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Portuguese", map);
					
					try {
						myCMD = "SET%20LANG%20P";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("P");
	
						goMain();
					} catch (Exception e) {}
				}
	        	
	        });	 
	        
	        
	        
	        langS =  (LinearLayout)findViewById(R.id.langS);
	        langS.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Spanish", map);
					
					try {
	
						myCMD = "SET%20LANG%20S";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("S");
	
						goMain();
					} catch (Exception e) {}
				}
	        	
	        });	 	        
	        
	        
	        
	        
	        langG =  (LinearLayout)findViewById(R.id.langG);
	        langG.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("German", map);
					
					try {
						
						myCMD = "SET%20LANG%20G";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("G");
	
						goMain();
						
					} catch (Exception e) {}
				}
	        	
	        });	 	        
	        
	        
	        
	        langR =  (LinearLayout)findViewById(R.id.langR);
	        langR.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Russian", map);
					
					try {
	
						myCMD = "SET%20LANG%20R";
	
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("R");
	
						goMain();
			            
	
					} catch (Exception e) {}
				}
	        	
	        });	 	       
	        
	        
	        
	        langA =  (LinearLayout)findViewById(R.id.langA);
	        langA.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					//for Flurry log
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));					
					FlurryAgent.logEvent("Arabic", map);
					
					try {
	
						myCMD = "SET%20LANG%20A";
						
						MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						DataCacheManager.getInstance().setLang("A");
	
						goMain();
			            
					} catch (Exception e) {}
				}
	        	
	        });
	        
	    	pinDialogLayout = (LinearLayout)findViewById(R.id.pinDialogLayout);
	    	pinDialogInnerLayout = (RelativeLayout)findViewById(R.id.pinDialogInnerLayout);
	    	pinInput = (EditText)findViewById(R.id.pinInput);
	    	
	    	goPinButton = (Button)findViewById(R.id.goPinButton);

	    	goPinButton.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					Log.i(myLOG, "gonPinButton onclicked");
					pinDialogLayout.setVisibility(View.VISIBLE);
					pinDialogInnerLayout.setVisibility(View.VISIBLE);
					//pinDialogLayout.bringToFront();
					
					//go it more 30 sec to go before go back to RoomControl
					topBarFragment.resetBackMainTimer();
					return false;
				}	        	
	        });
	    	
	    	okButton = (Button)findViewById(R.id.okButton);
	    	okButton.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					
					if(pinInput.getText().toString().equalsIgnoreCase(getString(R.string.pin))){
						//start intent to SettingActivity
						
						Log.i(myLOG, "okButton clicked");
						
						Intent intent = new Intent(MainApplication.getContext(),
								SettingActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						MainApplication.getCurrentActivity().startActivity(intent);
						
						LangActivity.this.finish();		
					}
				}	        	
	        });
	    	
	    	pinCloseImage = (ImageView)findViewById(R.id.pinCloseImage);
	    	pinCloseImage.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					pinInput.setText("");
					pinDialogLayout.setVisibility(View.GONE);	
					
					InputMethodManager imm = (InputMethodManager)getSystemService(
						      Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(pinInput.getWindowToken(), 0);
					
				}	        	
	        });

    	} catch (Exception e) {}
    	
	}
	
	
	
	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {
				
				guiHandler.removeCallbacks(guiRunnable);

				try { 
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources().getDrawable(getResources().getIdentifier("drawable/battery_" + MainApplication.getBatteryChecker().getBatteryLevel(), "drawable", getPackageName()))));

				} catch (Exception e) {}
				
				
				try {
					
					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();
						
						Log.v(myLOG,"alarm wake");
					}						
					
				} catch (Exception e) {}
				
				
				try {
					try {
		    			String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		    			int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
		    			int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
		    			String myHeadClockSign = "am";
		    			
		    			if (myHeadClockHour == 0) {
		    				myHeadClockHour = 12;
		    				myHeadClockSign = "am";
		    			} else if (myHeadClockHour == 12) {
		    				myHeadClockHour = myHeadClockHour;
		    				myHeadClockSign = "pm";
		    			} else if (myHeadClockHour > 12) {
		    				myHeadClockHour = myHeadClockHour - 12;
		    				myHeadClockSign = "pm";
		    			} else {
		    				myHeadClockSign = "am";
		    			}
		    			
						
		    			rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign + "</small>"));
		    			
		    			
					} catch (Exception e) {}   					
	
				} catch (Exception e) {}
				
				
				
				
				loadPageLabel();
				
				
				
				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);
				
			} catch (Exception e) {}
		}
	};

	
	
	
}
