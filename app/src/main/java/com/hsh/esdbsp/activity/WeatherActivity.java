
package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.Helper;


public class WeatherActivity extends BaseActivity {
		
	String myLOG = "WeatherActivity";
	
	//----------- 
 
	MyTextView rcHeadClock;

	MyTextView headRC;
	MyTextView headService;
	MyTextView headTV;
	MyTextView headRadio;
	MyTextView headLang;
	
	MyTextView headWeather;
	
	MyTextView footRoom;
	
	//-----------
	
	static String day_0_wdate = "";
	static String day_0_low = "";
	static String day_0_high = "";
	static String day_0_cond = "";
	static String day_0_temp = "";
	static String day_0_hum = "";
	static String day_0_winsp = "";
	static String day_0_windir = "";
	static String day_0_uv = "";
	static String day_0_windchill = "";
	
	static String day_1_wdate = "";
	static String day_1_low = "";
	static String day_1_high = "";
	static String day_1_cond = "";

	static String day_2_wdate = "";
	static String day_2_low = "";
	static String day_2_high = "";
	static String day_2_cond = "";	
	
	static String day_3_wdate = "";
	static String day_3_low = "";
	static String day_3_high = "";
	static String day_3_cond = "";
	
	//-----------
	
	TextView wBigTemp;
	ImageView wBigIcon;
	
	TextView wTempLabel;
	TextView wTemp;
	
	TextView wHumLabel;
	TextView wHum;
	
	TextView wWindDirLabel;
	TextView wWindDir;
	
	TextView wUVLabel;
	TextView wUV;
	
	
	TextView wF1Temp;
	ImageView wF1Icon;
	TextView wF1Day;
	
	TextView wF2Temp;
	ImageView wF2Icon;
	TextView wF2Day;
	
	TextView wF3Temp;
	ImageView wF3Icon;
	TextView wF3Day;
	
	//-----------
	
	LinearLayout layoutPage;
	
	private TopBarFragment topBarFragment;
	
	
	//////////////////////////////////////////////////////
	
	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5*1000;
	
	//////////////////////////////////////////////////////
	
	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30*1000;	
	
	//////////////////////////////////////////////////////
/*
	@Override
    public void onUserInteraction(){
        //renew your timer here
		
		try {
			handleSleep();
			
		} catch (Exception e) {}
    }	
*/	
	
	@Override
	protected void onResume() {
		
		try {
		    MainApplication.setCurrentActivity(this);
		    
		    addRunnableCall();
	    
		} catch (Exception e) {}
	    
        super.onResume();
    }
	
	@Override
	protected void onPause() {
		
		try {
	        clearReferences();
	        
	        removeRunnableCall();
        
		} catch (Exception e) {}

        super.onPause();
    }
	
	
	@Override
    protected void onDestroy() {      
		
		try {
	        clearReferences();
	        
	        removeRunnableCall();
        
		} catch (Exception e) {}

        super.onDestroy();
    }	
	
//////////////////////////////////////////////////////////
    
	private void clearReferences(){
		try {
			Activity currActivity = MainApplication.getCurrentActivity();
			
			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {}
	}
	
//////////////////////////////////////////////////////////	
	
	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext().getString(R.string.timeoutvalue))*1000;
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);
			
		} catch (Exception e) {}
	}
	
	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);
			
		} catch (Exception e) {}
	}	
	
	
//////////////////////////////////////////////////////////
	
	
	private void handleSleep() {
		try {
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
		} catch (Exception e) {}
		
	}
	
	
	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				goMain();
				
			} catch (Exception e) {}
			
		}
	};	
	
	
//////////////////////////////////////////////////////////
	
	
	public void goMain() {
		try {
			
			Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			MainApplication.getCurrentActivity().startActivity(intent);				
			
		} catch (Exception e) {}
	}	
	
	

//////////////////////////////////////////////////////////
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			setContentView(R.layout.weather_all);
			
			//for Flurry log
			final Map<String, String> map = new HashMap<String, String>();
			map.put("Room", MainApplication.getMAS().getData("data_myroom"));
			
			FlurryAgent.logEvent("Weather", map);

			MainApplication.setCurrentActivity(this);
			
			FragmentManager fragmentManager = getSupportFragmentManager();

			Bundle bundle = new Bundle();
			// bundle.putString("titleId", "");
			bundle.putBoolean("hideBackBtn", true);
			bundle.putString("hightLightChoice", "weather");

			// Create new fragment and transaction
			topBarFragment = new TopBarFragment();
			topBarFragment.setArguments(bundle);

			FragmentTransaction transaction = fragmentManager.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.topBarFragmentContainer, topBarFragment);

			// Commit the transaction
			transaction.commit();
			
			
			Helper.showInternetWarningToast();
			
			loadPageItem();
			
			addRunnableCall();
			
			loadPageLabel();
			
			
			
		} catch (Exception e) {}
	}
	
	
	private void loadPageLabel() {
		try {

			/*// change all label 
			headRC.setText(MainApplication.getLabel("RC").toUpperCase());
			headService.setText(MainApplication.getLabel("Services").toUpperCase());
			
			headTV.setText(MainApplication.getLabel("TV").toUpperCase());
			headRadio.setText(MainApplication.getLabel("Radio").toUpperCase());
			headLang.setText(MainApplication.getLabel("Lang").toUpperCase());	
			headWeather.setText(MainApplication.getLabel("Weather").toUpperCase());*/
			
			footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));
			if(this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				footRoom.setVisibility(View.GONE);
			}
		} catch (Exception e) {}
	}	
	
	
	private void loadPageItem() {
    	
    	try {
    		
    		MainApplication.brightUp();
    		
    		layoutPage = (LinearLayout)findViewById(R.id.layoutPage);
    		layoutPage.setVisibility(View.INVISIBLE); // hide first
    		
    		layoutPage.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						
						//handleSleep();
						
					} catch (Exception e) {}
					
					return false;
				}
    		});
    		
    		//////////////////////////////////////////////////////////
    		
    		
    		
    		
    		///////////////////////////////////////////////////////////

    		
			footRoom = (MyTextView) findViewById(R.id.footRoomNum);
    		footRoom.setText("");
    		
    		///////////////////////////////////////////////////////////	
    		
    		
    		wBigTemp = (MyTextView)findViewById(R.id.wBigTemp);
        	wBigIcon = (ImageView)findViewById(R.id.wBigIcon);
        	
        	wTempLabel = (MyTextView)findViewById(R.id.wTempLabel);
        	wTempLabel.setText(MainApplication.getLabel("wwTemperature"));
        	
        	wTemp = (MyTextView)findViewById(R.id.wTemp);
        	
        	wHumLabel = (MyTextView)findViewById(R.id.wHumLabel);
        	wHumLabel.setText(MainApplication.getLabel("wwHumidity"));
        	
        	wHum = (MyTextView)findViewById(R.id.wHum);
        	
        	wWindDirLabel = (MyTextView)findViewById(R.id.wWindDirLabel);
        	wWindDirLabel.setText(MainApplication.getLabel("wwWind"));
        	
        	wWindDir = (MyTextView)findViewById(R.id.wWindDir);
        	
        	wUVLabel = (MyTextView)findViewById(R.id.wUVLabel);
        	wUVLabel.setText(MainApplication.getLabel("wwUV"));
        	
        	wUV = (MyTextView)findViewById(R.id.wUV);
        	
        	
        	wF1Temp = (MyTextView)findViewById(R.id.wF1Temp);
        	wF1Icon = (ImageView)findViewById(R.id.wF1Icon);
        	wF1Day = (MyTextView)findViewById(R.id.wF1Day);
        	
        	wF2Temp = (MyTextView)findViewById(R.id.wF2Temp);
        	wF2Icon = (ImageView)findViewById(R.id.wF2Icon);
        	wF2Day = (MyTextView)findViewById(R.id.wF2Day);
        	
        	wF3Temp = (MyTextView)findViewById(R.id.wF3Temp);
        	wF3Icon = (ImageView)findViewById(R.id.wF3Icon);
        	wF3Day = (MyTextView)findViewById(R.id.wF3Day);    		
    		
	
	
    	} catch (Exception e) {
    		Log.i(myLOG, "error = " + e);
    	}
    	
	}
	
	
	
	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {
				
				guiHandler.removeCallbacks(guiRunnable);

				try { 
					
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources().getDrawable(getResources().getIdentifier("drawable/battery_" + MainApplication.getBatteryChecker().getBatteryLevel(), "drawable", getPackageName()))));

				} catch (Exception e) {}
				
				
				try {
					
					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();
						
						Log.v(myLOG,"alarm wake");
					}						
					
				} catch (Exception e) {}
				
				
				try {
					try {
		    			String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		    			int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
		    			int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
		    			String myHeadClockSign = "am";
		    			
		    			if (myHeadClockHour == 0) {
		    				myHeadClockHour = 12;
		    				myHeadClockSign = "am";
		    			} else if (myHeadClockHour == 12) {
		    				myHeadClockHour = myHeadClockHour;
		    				myHeadClockSign = "pm";
		    			} else if (myHeadClockHour > 12) {
		    				myHeadClockHour = myHeadClockHour - 12;
		    				myHeadClockSign = "pm";
		    			} else {
		    				myHeadClockSign = "am";
		    			}
		    			
						
		    			rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign + "</small>"));
		    			
		    			
					} catch (Exception e) {}   					
	
				} catch (Exception e) {}
				
				//////////////////////////////////////////////////////////
				
				String fullJSON = MainApplication.getWeather().getJSON();
				JSONObject jsonResponse = new JSONObject(new String(fullJSON));
				
				day_0_wdate = (jsonResponse.get("day_0_wdate").toString());
	        	
	        	day_0_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_0_low").toString()).trim()));
	        	day_0_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_0_high").toString()).trim()));
	        	//day_0_cond = feedCond( (jsonResponse.get("day_0_cond").toString()).trim().toLowerCase());
	        	
	        	day_0_cond = "w" + (jsonResponse.get("day_0_pic").toString()).trim().toLowerCase();

	        	day_0_temp = (jsonResponse.get("day_0_temp").toString());
	        	day_0_hum = (jsonResponse.get("day_0_hum").toString());
	        	day_0_winsp = (jsonResponse.get("day_0_winsp").toString());
	        	
	        	int myWindDir = Integer.parseInt(jsonResponse.get("day_0_windir").toString());
	        	
	        	if (myWindDir < 45) {
	        		day_0_windir = "N";
	        	
	        	} else if  (myWindDir < 90) {
	        		day_0_windir = "NE";
	        		
	        	} else if  (myWindDir < 135) {
	        		day_0_windir = "E";
	        		
	        	} else if  (myWindDir < 180) {
	        		day_0_windir = "SE";
	        		
	        	} else if  (myWindDir < 225) {
	        		day_0_windir = "S";
	        		
	        	} else if  (myWindDir < 270) {
	        		day_0_windir = "SW";
	        		
	        	} else if  (myWindDir < 315) {
	        		day_0_windir = "W";
	        		
	        	} else if  (myWindDir < 360) {
	        		day_0_windir = "NW";
	        		
	        	}
	        	
	        	day_0_uv = (jsonResponse.get("day_0_uv").toString());
	        	day_0_windchill = (jsonResponse.get("day_0_windchill").toString());
	        	
	        	day_1_wdate = (jsonResponse.get("day_1_wdate").toString());
	        	day_1_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_1_low").toString()).trim()));
	        	day_1_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_1_high").toString()).trim()));
	        	//day_1_cond = feedCond( (jsonResponse.get("day_1_cond").toString()).trim().toLowerCase());
	        	day_1_cond = "w" + (jsonResponse.get("day_1_pic").toString()).trim().toLowerCase();	        	

	        	day_2_wdate = (jsonResponse.get("day_2_wdate").toString());
	        	day_2_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_2_low").toString()).trim()));
	        	day_2_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_2_high").toString()).trim()));
	        	//day_2_cond = feedCond( (jsonResponse.get("day_2_cond").toString()).trim().toLowerCase());
	        	day_2_cond = "w" + (jsonResponse.get("day_2_pic").toString()).trim().toLowerCase();	

	        	day_3_wdate = (jsonResponse.get("day_3_wdate").toString());
	        	day_3_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_3_low").toString()).trim()));
	        	day_3_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_3_high").toString()).trim()));
	        	//day_3_cond = feedCond( (jsonResponse.get("day_3_cond").toString()).trim().toLowerCase());
	        	day_3_cond = "w" + (jsonResponse.get("day_3_pic").toString()).trim().toLowerCase();	

	        	
	        	/// set it
	        	
	        	wBigIcon.setImageResource(getResources().getIdentifier(day_0_cond, "drawable", getPackageName()));
	        	
				if (MainApplication.getMAS().getData("data_temperature_cf").equalsIgnoreCase("C")) {
					
					wTemp.setText(Html.fromHtml(day_0_low + "&deg;C - " + day_0_high + "&deg;C"));
					wBigTemp.setText(Html.fromHtml(day_0_temp + "&deg;C"));
					
					
		        	wF1Temp.setText(Html.fromHtml(day_1_low + "&deg;C - " + day_1_high + "&deg;C"));
		        	wF1Icon.setImageResource(getResources().getIdentifier(day_1_cond, "drawable", getPackageName()));
		        	wF1Day.setText(Html.fromHtml(checkMyLabel(day_1_wdate)));

		        	wF2Temp.setText(Html.fromHtml(day_2_low + "&deg;C - " + day_2_high + "&deg;C"));
		        	wF2Icon.setImageResource(getResources().getIdentifier(day_2_cond, "drawable", getPackageName()));
		        	wF2Day.setText(Html.fromHtml(checkMyLabel(day_2_wdate)));
		        	
		        	wF3Temp.setText(Html.fromHtml(day_3_low + "&deg;C - " + day_3_high + "&deg;C"));
		        	wF3Icon.setImageResource(getResources().getIdentifier(day_3_cond, "drawable", getPackageName()));
		        	wF3Day.setText(Html.fromHtml(checkMyLabel(day_3_wdate)));					

				} else {
					
					wTemp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_0_low) * 9/5 + 32f) + "&deg;F - " + Math.round(Float.parseFloat(day_0_high) * 9/5 + 32f) + "&deg;F"));
		        	wBigTemp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_0_temp) * 9/5 + 32f) + "&deg;F"));
		        	
					
		        	wF1Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_1_low) * 9/5 + 32f) + "&deg;F - " + Math.round(Float.parseFloat(day_1_high) * 9/5 + 32f) + "&deg;F"));
		        	wF1Icon.setImageResource(getResources().getIdentifier(day_1_cond, "drawable", getPackageName()));
		        	wF1Day.setText(Html.fromHtml(checkMyLabel(day_1_wdate)));

		        	wF2Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_2_low) * 9/5 + 32f) + "&deg;F - " + Math.round(Float.parseFloat(day_2_high) * 9/5 + 32f) + "&deg;F"));
		        	wF2Icon.setImageResource(getResources().getIdentifier(day_2_cond, "drawable", getPackageName()));
		        	wF2Day.setText(Html.fromHtml(checkMyLabel(day_2_wdate)));
		        	
		        	wF3Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_3_low) * 9/5 + 32f) + "&deg;F - " + Math.round(Float.parseFloat(day_3_high) * 9/5 + 32f) + "&deg;F"));
		        	wF3Icon.setImageResource(getResources().getIdentifier(day_3_cond, "drawable", getPackageName()));
		        	wF3Day.setText(Html.fromHtml(checkMyLabel(day_3_wdate)));
		        	
				}
	        	
	        	
	        	
	        	wHum.setText(Html.fromHtml(day_0_hum + "%"));
	        	wWindDir.setText(Html.fromHtml(day_0_windir + "@" + day_0_winsp + "mph"));
	        	wUV.setText(Html.fromHtml(day_0_uv));
	        	
	        		        	
	        	

				
				//////////////////////////////////////////////////////////
				
				loadPageLabel();
				
				layoutPage.setVisibility(View.VISIBLE); // 
				
				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);
				
			} catch (Exception e) {}
		}
	};
	        
	
	
	
///////////////////////////////////////
	
	
private int f2c(int myF) {
		
		int myC = 0;
		
		myC = Math.round((myF - 32) * 5/9);
		
		
		return myC;
	}
	
	private String feedCond(String myCond) {
		
		String myCondition = (myCond.toString()).trim().toLowerCase();
    	
    	if (myCondition.contains("sunny")) {
    		myCondition = "dayclear";
    		
    	} else if (myCondition.contains("night")) {
    		myCondition = "nightclear";

    	} else if (myCondition.contains("clear")) {
    		myCondition = "dayclear";
    		
    	} else if (myCondition.contains("rain")) {
    		myCondition = "rainy";
    		
    	} else if (myCondition.contains("shower")) {
    		myCondition = "rainy";
    		
    	} else if (myCondition.contains("storm")) {
    		myCondition = "storm";
    		
    	} else if (myCondition.contains("cloud")) {
    		myCondition = "cloudy";
    		
    	} else if (myCondition.contains("mist")) {
    		myCondition = "cloudy";

    	} else if (myCondition.contains("snow")) {
    		myCondition = "snow";
    		
    	} else {
    		myCondition = "cloudy";
    		
    	}
    	
    	
    	return myCondition;
		
		
	}	
	
	
///////////////////////////////////////////////////
	
	
	private String checkMyLabel(String myWord) {

		String myChk = MainApplication.getLabel(myWord.trim());

		//Log.v(myLOG,""+ myWord + ":" + myChk) ;
		
		return myChk;
		
		
	}
	
	
	
	
		
	
}
