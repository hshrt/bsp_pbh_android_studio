
package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.Helper;
import com.hsh.hshservice.global.Log;
import com.nostra13.universalimageloader.core.ImageLoader;


public class SettingActivity extends BaseActivity {
		
	String TAG = "SettingActivity";
	
	private TextView deviceIdText;
	private TextView versionText;
	private TextView macAddressText;
	private TextView mainTenText;
	private Button clearAllButton;
	private Button clearImageMemoryButton;
	private Button clearImageDiskButton;
	private Button backBtn;
	private CheckBox checkbox_enable_log;
	private CheckBox checkbox_enable_curtain;
	
	private SharedPreferences settings;
	public static final String PREFS_NAME = "MyPrefsFile";
	
	
	//-----------
	
	LinearLayout layoutPage;
	
	private TopBarFragment topBarFragment;
	
	
	//////////////////////////////////////////////////////
	
	// tester part 
	
	TextView deviceWiFiSSID;
	TextView deviceWiFiSignalStrenght;
	TextView deviceIP;
	
	
	TextView mas100;
	TextView mas101;
	TextView mas102;
	TextView mas103;
	TextView mas104;
	
	TextView cms;
	
	
	//////////////////////////////////////////////////////
	
	static Handler guiHandler = new Handler();

	static int guiDelay = 500;
	static int guiDelayLong = 5*1000;
	
	//////////////////////////////////////////////////////
	
	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30*1000;	
	
	//////////////////////////////////////////////////////
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			
			Log.i(TAG, "Setting onCreate");
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			setContentView(R.layout.setting);
			
			settings = MainApplication.getContext().getSharedPreferences(PREFS_NAME, 0);
						
			MainApplication.setCurrentActivity(this);
			
			
			try {
				// by pass the async send cmd 
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
			
			} catch (Exception e) {
				Log.v(TAG, e.toString());
			}			
			
			
			
			
			FragmentManager fragmentManager = getSupportFragmentManager();

			Bundle bundle = new Bundle();
			// bundle.putString("titleId", "");
			bundle.putBoolean("hideBackBtn", false);
			bundle.putBoolean("goMain", false);
			bundle.putString("hightLightChoice", "setting");

			// Create new fragment and transaction
			topBarFragment = new TopBarFragment();
			topBarFragment.setArguments(bundle);

			FragmentTransaction transaction = fragmentManager.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack
			//transaction.replace(R.id.topBarFragmentContainer, topBarFragment);

			// Commit the transaction
			//transaction.commit();
			
			
			Helper.showInternetWarningToast();
			
			initUI();
			
			
		} catch (Exception e) {}
	}
	
	private void initUI(){
		
		Log.i(TAG, "initUI fire");
		
		checkbox_enable_log = (CheckBox) findViewById(R.id.checkbox_enable_log);
		
		boolean enableLog = settings.getBoolean("enableLog", false);
		checkbox_enable_log.setChecked(enableLog);
		
		checkbox_enable_curtain = (CheckBox) findViewById(R.id.checkbox_enable_curtain);
		
		boolean hasCurtain = settings.getBoolean("hasCurtain", false);
		checkbox_enable_curtain.setChecked(hasCurtain);
				
		deviceIdText = (TextView) findViewById(R.id.deviceIdText);
		
		TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE); 
		  
		String deviceId =  mngr.getDeviceId();
		String device_unique_id = Secure.getString(this.getContentResolver(),
	            Secure.ANDROID_ID);
		
		deviceIdText.setText("DeviceId: " +  device_unique_id.toUpperCase());
		
		versionText = (TextView) findViewById(R.id.versionText);
		
		String versionName = "";
		try {
			versionName = getPackageManager()
				    .getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		versionText.setText("App Version: " + versionName);
		
		macAddressText = (TextView) findViewById(R.id.macAddressText);
		macAddressText.setText("MAC Address: " + Helper.getMacAddress(this));
		
		mainTenText = (TextView) findViewById(R.id.maintenText);
		clearAllButton = (Button) findViewById(R.id.clearAllButton);
		clearImageMemoryButton = (Button) findViewById(R.id.clearImageMemoryButton);
		clearImageDiskButton = (Button) findViewById(R.id.clearImageDiskButton);
		
		backBtn = (Button) findViewById(R.id.backBtn);
		
		clearAllButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG, "clearAllButton clicked");
				ImageLoader.getInstance().clearMemoryCache();
				ImageLoader.getInstance().clearDiskCache();	
				settings.edit().clear().commit();				
			}
		});
		
		clearImageMemoryButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG, "clearAllButton clicked");
				ImageLoader.getInstance().clearMemoryCache();	
			}
		});
		
		clearImageDiskButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG, "clearAllButton clicked");
				ImageLoader.getInstance().clearDiskCache();	
				settings.edit().remove("lastMediaUpdateTime").commit();
			}
		});
		
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG, "clearAllButton clicked");
				settings.edit().putBoolean("enableLog", checkbox_enable_log.isChecked()).commit();
				settings.edit().putBoolean("hasCurtain", checkbox_enable_curtain.isChecked()).commit();
				
				DataCacheManager.getInstance().setStartLoop(false);
				DataCacheManager.getInstance().startLoadData();
				
				Intent intent = new Intent(MainApplication.getContext(),
						RoomControlActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				MainApplication.getCurrentActivity().startActivity(intent);
				
				SettingActivity.this.finish();				
			}
		});
		

		Helper.setFonts(versionText, getString(R.string.app_font));
		Helper.setFonts(macAddressText, getString(R.string.app_font));
		Helper.setFonts(mainTenText, getString(R.string.app_font));
		Helper.setFonts(clearAllButton, getString(R.string.app_font));
		Helper.setFonts(clearImageMemoryButton, getString(R.string.app_font));
		Helper.setFonts(clearImageDiskButton, getString(R.string.app_font));
		Helper.setFonts(deviceIdText, getString(R.string.app_font));
		
		
		
		
		
		///////////////////////
		
		
		if(this.getString(R.string.hotel).equalsIgnoreCase("pch")){
		
			try {
				
				deviceWiFiSSID = (TextView) findViewById(R.id.deviceWiFiSSID);
				deviceWiFiSSID.setText("SSID: " + getCurrentSsid() + "[" + getCurrentSsidID() + "]");
				
				deviceWiFiSignalStrenght = (TextView) findViewById(R.id.deviceWiFiSignalStrenght);
				deviceWiFiSignalStrenght.setText("Signal Strenght: " + getSignalStrenght());
				
				deviceIP = (TextView) findViewById(R.id.deviceIP);
				deviceIP.setText("IP: " + getIpAddrFull());
				
				
				///////////////
				
				
				mas100 = (TextView) findViewById(R.id.mas100);
				mas100.setText("mas 100:" + testMAS(100));
				
				mas101 = (TextView) findViewById(R.id.mas101);
				mas101.setText("mas 101:" + testMAS(101));
				
				mas102 = (TextView) findViewById(R.id.mas102);
				mas102.setText("mas 102:" + testMAS(102));
				
				mas103 = (TextView) findViewById(R.id.mas103);
				mas103.setText("mas 103:" + testMAS(103));
				
				mas104 = (TextView) findViewById(R.id.mas104);
				mas104.setText("mas 104:" + testMAS(104));
				
				cms = (TextView) findViewById(R.id.cmslink);
				cms.setText("cms:" + testCMS());
				
				
				
				
				
			} catch (Exception e) {}
		}
		

	}
	
	public void onCheckboxClicked(View view) {
	    // Is the view now checked?
	    boolean checked = ((CheckBox) view).isChecked();
	    settings.edit().putBoolean("enableLog", checked).commit();

	}
	
	@Override
    public void onUserInteraction(){
        //renew your timer here
		
		try {
			//handleSleep();
			
		} catch (Exception e) {}
    }	
	
	
	@Override
	protected void onResume() {
	    MainApplication.setCurrentActivity(this);	   
	    
        super.onResume();
    }
	
	@Override
	protected void onPause() {
        clearReferences();

        super.onPause();
    }
	
	
	@Override
    protected void onDestroy() {        
        clearReferences();

        super.onDestroy();
    }	
	
//////////////////////////////////////////////////////////
    
	private void clearReferences(){
		try {
			Activity currActivity = MainApplication.getCurrentActivity();
			
			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {}
	}
	
//////////////////////////////////////////////////////////
	
	
	private void handleSleep() {
		try {
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
		} catch (Exception e) {}
		
	}
	
	
	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				goMain();
				
			} catch (Exception e) {}
			
		}
	};	
	
	
//////////////////////////////////////////////////////////
	
	
	public void goMain() {
		try {
			
			Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			
			if(this.getString(R.string.hotel).equalsIgnoreCase("PHK_R")){
				intent = new Intent(MainApplication.getContext(), ServiceMainActivity.class);
			}
			MainApplication.getCurrentActivity().startActivity(intent);				
			
		} catch (Exception e) {}
	}	
	
	public String getMacAddress(Context context) {
	    WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    String macAddress = wimanager.getConnectionInfo().getMacAddress();
	    if (macAddress == null) {
	        macAddress = "Device don't have mac address or wi-fi is disabled";
	    }
	    return macAddress;
	}

//////////////////////////////////////////////////////////

	// added for wifi and connectivity 
	
	
	private int getCurrentSsidID() {
		  String ssid = null;
		  
		  WifiManager wifiManager = (WifiManager) MainApplication.getContext().getSystemService(Context.WIFI_SERVICE); 
		  
		  return wifiManager.getConnectionInfo().getNetworkId();
	}	
	
	private String getCurrentSsid() {
		  String ssid = null;
		  
		  ConnectivityManager connManager = (ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		  if (networkInfo.isConnected()) {
		    final WifiManager wifiManager = (WifiManager) MainApplication.getContext().getSystemService(Context.WIFI_SERVICE);
		    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
		    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
		      ssid = connectionInfo.getSSID();

		    }
		  }
		  return ssid;
	}	
	
	
	private int getSignalStrenght() {

		  int signalStrenght = 0;
		  
		  ConnectivityManager connManager = (ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		  if (networkInfo.isConnected()) {
		    final WifiManager wifiManager = (WifiManager) MainApplication.getContext().getSystemService(Context.WIFI_SERVICE);
		    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
		    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
		    	
		      signalStrenght = connectionInfo.getRssi();
		    }
		  }
		  return signalStrenght;
	}	
	
	
	private String getIpAddrFull() {
		WifiManager wifiManager = (WifiManager) MainApplication.getContext()
				.getSystemService(MainApplication.getContext().WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ip = wifiInfo.getIpAddress();

		String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
				(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

		return ipString;
	}	
	
	
	private String getIpAddr() {
		WifiManager wifiManager = (WifiManager) MainApplication.getContext()
				.getSystemService(MainApplication.getContext().WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ip = wifiInfo.getIpAddress();

		String ipString = String.format("%d.%d.%d.", (ip & 0xff),
				(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

		return ipString;
	}
	
	
	
	//////////////////////////
	
	private String specialSend(String url){
		InputStream inputStream = null;
		String result = "";

		try {

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();
	
			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
	
			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();
			
			
			BufferedReader in = new BufferedReader(new InputStreamReader(                     
					inputStream));  
			
			
			String line = "";
        	String fullSTA = "";
        	while ((line = in.readLine()) != null) {  
        		fullSTA = fullSTA + line;    
        	} 

        	
        	result = fullSTA;


		} catch (Exception e) {}
		
		
		return result;

	}	
	
	
	
	private String testMAS(int ip) {
		
		String url1 = "http://" + getIpAddr() + "" + ip + "/mas.php?cmd=GET%20STATUS";
		String url2 = "http://" + getIpAddr() + "" + ip + ":9111/rcpu.cgi?cmd=H0";
		
		String chkMAS = (specialSend(url1).length() > 3 ? "OK" : "FAIL");
		String chkCPU = (specialSend(url2).length() > 3 ? "OK" : "FAIL");
		
		//String chkCPU = specialSend(url2);
		
		return "[uplink:" + chkMAS + "]," + "[inroom-cpu:" +  chkCPU + "]" ;
		
	}
	
	
	private String testCMS() {
		
		String url = MainApplication.getContext().getString(R.string.api_base);
		
		String chkCMS = (specialSend(url).length() > 3 ? "OK" : "FAIL");
		
		return chkCMS ;
		
	}
	
	
	

	
}
