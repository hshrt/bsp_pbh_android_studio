package com.hsh.esdbsp.activity;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.R.id;
import com.hsh.esdbsp.R.layout;
import com.hsh.esdbsp.R.string;
import com.hsh.esdbsp.comm.*;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.*;
import com.hsh.hshservice.customclass.BaseActivity;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONObject;

import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.FloatMath;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class TVActivity extends BaseActivity {
	
	String myLOG = "TVActivity";

	//-----------
	 
	MyTextView rcHeadClock;
	
	MyTextView footRoom;	
	
	MyGridView gridView;
	GridViewAdapter customGridAdapter;
	
	int isMute = 0;
	ImageView tvMute;
	ImageView tvMinus;
	ImageView tvPlus; 	
	
	TextView tvONLabel;
	LinearLayout tvON;
	
	TextView tvOFFLabel;
	LinearLayout tvOFF;
	
	//-----------
	
	ScrollView layoutPage;
	
	String myCMD;
	
	//////////////////////////////////////////////////////
	
	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5*1000;
	
	//////////////////////////////////////////////////////
	
	static Handler tvChannelHandler = new Handler();
	static int tvChannelDelay = 500;
	
	//////////////////////////////////////////////////////
	
	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30*1000;	
	
	

	
	
	//////////////////////////////////////////////////////

	/*@Override
    public void onUserInteraction(){
        //renew your timer here
		
		try {
			handleSleep();
			
		} catch (Exception e) {}
    }		*/
	
	
	@Override
	protected void onResume() {
		try {
		    MainApplication.setCurrentActivity(this);
		    
		    addRunnableCall();
		} catch (Exception e) {}
	    
        super.onResume();
    }
	
	@Override
	protected void onPause() {
		try {
	        clearReferences();
	        
	        removeRunnableCall();
        
		} catch (Exception e) {}

        super.onPause();
    }
	
	
	@Override
    protected void onDestroy() {        
		try {
	        clearReferences();
	        
	        removeRunnableCall();
		} catch (Exception e) {}

        super.onDestroy();
    }	
	
//////////////////////////////////////////////////////////
    
	private void clearReferences(){
		try {
			Activity currActivity = MainApplication.getCurrentActivity();
			
			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {}
	}
	
//////////////////////////////////////////////////////////	
	
	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext().getString(R.string.timeoutvalue))*1000;
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			//timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);
			
		} catch (Exception e) {}
	}
	
	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);
			
		} catch (Exception e) {}
	}	
	
	
//////////////////////////////////////////////////////////
	
	
	private void handleSleep() {
		try {
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			//timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
		} catch (Exception e) {}
		
	}
	
	
	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				goMain();
				
			} catch (Exception e) {}
			
		}
	};	
	
	
//////////////////////////////////////////////////////////
	
	
	public void goMain() {
		try {
			
			Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			MainApplication.getCurrentActivity().startActivity(intent);				
			
		} catch (Exception e) {}
	}	
	
	

//////////////////////////////////////////////////////////
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.tv_all);
			
			MainApplication.setCurrentActivity(this);
			
			FragmentManager fragmentManager = getSupportFragmentManager();

			Bundle bundle = new Bundle();
			// bundle.putString("titleId", "");
			bundle.putBoolean("hideBackBtn", true);
			bundle.putString("hightLightChoice", "tv");
			
			// Create new fragment and transaction
			topBarFragment = new TopBarFragment();
			topBarFragment.setArguments(bundle);


			FragmentTransaction transaction = fragmentManager.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
			

			// Commit the transaction
			transaction.commit();
			
			if (MainApplication.getTVGrid() == null) {
				new Thread(new Runnable() {
				      public void run() {
				
				    	  loadPageItem();
						
				      }
				}).start();
			} else {
				loadPageItem();
			}

			
			
			
			
			addRunnableCall();
			
			loadPageLabel();
			
			
			
		} catch (Exception e) {}
	}
	
	
	private void loadPageLabel() {
		try {
			
			footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));

		} catch (Exception e) {}
	}	
	
	
	private void loadPageItem() {
    	
    	try {
    		
    		MainApplication.brightUp();
    		
    		layoutPage = (ScrollView)findViewById(R.id.layoutPage);
    		layoutPage.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						
						//handleSleep();
						
					} catch (Exception e) {}
					
					return false;
				}
    		});
    		
    		
			
			
    		
			footRoom = (MyTextView) findViewById(R.id.footRoomNum);
    		footRoom.setText("");
    		
    		///////////////////////////////////////////////////////////

    		loadTVFixItem();
    		
    		tvChannelHandler.removeCallbacks(tvChannelRunnable);
    		tvChannelHandler.postDelayed(tvChannelRunnable, tvChannelDelay);
    		

    	} catch (Exception e) {}
    	
	}
	
	
	private ArrayList getChannelData() {
		
		@SuppressWarnings("rawtypes")
		final ArrayList <ImageItem> imageItems = new ArrayList<ImageItem>();		
		
		try {
		

			// retrieve String drawable array
			
	//		new Thread(new Runnable() {
	//		      public void run() {
	//		  		for(int i=2; i<=90; i++) {
	//					
	//					if (i>73 && i<79) {
	//						//skip
	//					} else {
	//						
	//			    		String imageID;
	//			    		String imageID2;
	//			    		
	//			    		String j = "00";
	//			        	
	//			        	if (i<10) { 
	//			        		j = "0" + i;
	//			        	} else {
	//			        		j = "" + i;
	//			        	}
	//			        	
	//			        	imageID = "tv" + j + "_selector";
	//			        	
	//			        	int resID = getResources().getIdentifier(imageID, "drawable", "com.hsh.esdbsp");
	//			        	//int resID2 = getResources().getIdentifier(imageID2, "drawable", "com.hsh.esdbsp");
	//			        	
	//			        	//Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resID);
	//			        	//Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), resID2);
	//			        	
	//						//imageItems.add(new ImageItem(bitmap, bitmap2, "Channel " + i));
	//						
	//						imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));
	//						//imageItems.add(new ImageItem(imageID, "Channel " + j, "SET%20TV%20" + i));
	//						
	//						
	//			        	
	//						
	//					}
	//					
	//				}
	//		      }
	//		  }).start();
			
			
			// 86 - pen history 
			imageItems.add(getChannelDataSub(86));
			
			// 85 - pen moment 
			imageItems.add(getChannelDataSub(85));
			
			// 84 - spa 
			imageItems.add(getChannelDataSub(84));			
			
			
			
	  		for(int i=2; i<=90; i++) {
				
				if (i>73 && i<79) {
					// skip
				} else if (i == 82) {
					// skip direct tv
				} else if ( (i >= 84) && (i <= 86) ) {
					// first three -- requested by Chris , 20151106
				} else {
					
					
					imageItems.add(getChannelDataSub(i)); 

				}
				
			}
			
	
			
		
		} catch (Exception e) {}
		
		return imageItems;
	}
	
	
	private ImageItem getChannelDataSub(int i) {
		ImageItem myIMG = null;
		
		try {
			
    		String imageID;
    		String imageID2;
    		
    		String j = "00";
        	
        	if (i<10) { 
        		j = "0" + i;
        	} else {
        		j = "" + i;
        	}
        	
        	imageID = "tv" + j + "_selector";
        	
        	int resID = getResources().getIdentifier(imageID, "drawable", "com.hsh.esdbsp");
        	myIMG = new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i);

		} catch (Exception e) {}
		
		return myIMG;
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////	
	
	
	private ArrayList getChannelDataPBJ() {
		
		@SuppressWarnings("rawtypes")
		final ArrayList <ImageItem> imageItems = new ArrayList<ImageItem>();		
		
		try {
		

	  		for(int i=1; i<=39; i++) {
				
	  			String imageID;
	    		String imageID2;
	    		
	    		String j = "00";
	        	
	        	if (i<10) { 
	        		j = "0" + i;
	        	} else {
	        		j = "" + i;
	        	}
	        	
	        	imageID = "pbjtv" + j + "_selector";
	        	
	        	int resID = getResources().getIdentifier(imageID, "drawable", "com.hsh.esdbsp");
				imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));
				
			}
			
	
			
		
		} catch (Exception e) {}
		
		return imageItems;
	}	
	
	
	
	private void loadTVFixItem() {
		
		try {
			
			tvON = (LinearLayout) findViewById(R.id.tvON);
			tvON.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(tvON);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
						
							myCMD = "SET%20TVMODE";
							MainApplication.getMAS().sendMASCmd(myCMD,1);
						
						}
					} catch (Exception e) {}
					
					return false;
				}
				
			});	  
			
			
			tvOFF = (LinearLayout) findViewById(R.id.tvOFF);
			tvOFF.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(tvOFF);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							myCMD = "SET%20AVOFF";
							MainApplication.getMAS().sendMASCmd(myCMD,1);
							
						}
						
					} catch (Exception e) {}
					
					return false;
				}
				
			});	
			
			
			//-------------------------------
			
			
			tvMute = (ImageView) findViewById(R.id.tvMute);
			tvMute.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(tvMute);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
						
							isMute = (isMute + 1) % 2;
							
							if (isMute == 1) {
							
								myCMD = "SET%20RADIOVOL%20MUTE";
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							} else {
								
								myCMD = "SET%20RADIOVOL%20UNMUTE";
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							}
						}
						
					} catch (Exception e) {}
					
					return false;
				}
				
			});	
			
			
			tvPlus = (ImageView) findViewById(R.id.tvPlus);
			tvPlus.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(tvPlus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							myCMD = "SET%20RADIOVOL%20UP";
							MainApplication.getMAS().sendMASCmd(myCMD,1);
						}
					} catch (Exception e) {}
					
					return false;
				}
				
			});		
			
			
			tvMinus = (ImageView) findViewById(R.id.tvMinus);
			tvMinus.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(tvMinus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							myCMD = "SET%20RADIOVOL%20DOWN";
							MainApplication.getMAS().sendMASCmd(myCMD,1);
						}
						
					} catch (Exception e) {}
					
					return false;
				}
				
			});					
			
			
			
		} catch (Exception e) {}
		
	}
	
	
	private void loadTVItem() {
		
		try {
			
			gridView = (MyGridView) findViewById(R.id.gridView);
			
			//customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelData());
			//gridView.setAdapter(customGridAdapter);
			
			if (MainApplication.getTVGrid() == null) {
				
				if(this.getString(R.string.tv).equalsIgnoreCase("pch")){
					customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelData());
					MainApplication.setTVGrid(customGridAdapter);
				} else if(this.getString(R.string.tv).equalsIgnoreCase("pbj")){
					customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelDataPBJ());
					MainApplication.setTVGrid(customGridAdapter);			
				}
				
				
			} else {
				customGridAdapter = MainApplication.getTVGrid();
			}
			
			gridView.setAdapter(customGridAdapter);
			
			
			gridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					
					ImageItem item = (ImageItem) MainApplication.getRadio().getTVData().get(position);
					MainApplication.getMAS().sendMASCmd(item.getCmd(), 0);
					
					Log.v(myLOG,"testing:" + item.getCmd());
					
					
				}
				
			});

			
			/*
			for(int i=2; i<=10; i++) {
				
				if (i>73 && i<79) {
					//skip
				} else {
					
					ImageButton image = new ImageButton(MainApplication.getContext());
		    		String imageID;
		    		
		        	if (i<10) { 
		        		imageID = "tv0" + i + "_selector";
		        	} else {
		        		imageID = "tv" + i + "_selector";
		        	}
		        	
		        	int resID = getResources().getIdentifier(imageID, "drawable", "com.hsh.esdbsp");
		        	
		        	image.setBackgroundColor(Color.argb(0,0,0,0));
		        	image.setLayoutParams(new LinearLayout.LayoutParams(
		        			200,
		        			200
		        			));
		        	
		        	image.setPadding(4, 6, 0, 0); // left, top, right, bottom
		        	image.setClickable(true);
		        	
		        	image.setImageResource(resID);
		        	
		        	image.setId(i);
					
		        	gridView.addView(image);
		        	
				}
			}
			*/

		} catch (Exception e) {
			Log.v(myLOG,e.toString());
		}
		
	}
	
	
	private Runnable tvChannelRunnable = new Runnable() {

		public void run() {
			try {
				
				tvChannelHandler.removeCallbacks(tvChannelRunnable);
				loadTVItem();

				
			} catch (Exception e) {}
		}
	};
	
	
	
	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {
				
				guiHandler.removeCallbacks(guiRunnable);

				try { 
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources().getDrawable(getResources().getIdentifier("drawable/battery_" + MainApplication.getBatteryChecker().getBatteryLevel(), "drawable", getPackageName()))));

				} catch (Exception e) {}
				
				
				try {
					
					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();
						
						Log.v(myLOG,"alarm wake");
					}						
					
				} catch (Exception e) {}
				
				
				try {
					try {
		    			String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		    			int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
		    			int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
		    			String myHeadClockSign = "am";
		    			
		    			if (myHeadClockHour == 0) {
		    				myHeadClockHour = 12;
		    				myHeadClockSign = "am";
		    			} else if (myHeadClockHour == 12) {
		    				myHeadClockHour = myHeadClockHour;
		    				myHeadClockSign = "pm";
		    			} else if (myHeadClockHour > 12) {
		    				myHeadClockHour = myHeadClockHour - 12;
		    				myHeadClockSign = "pm";
		    			} else {
		    				myHeadClockSign = "am";
		    			}
		    			
						
		    			rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign + "</small>"));
		    			
		    			
					} catch (Exception e) {}   					
	
				} catch (Exception e) {}
				
				
				
				
				loadPageLabel();
				
				
				
				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);
				
			} catch (Exception e) {}
		}
	};

	
	
	
}
