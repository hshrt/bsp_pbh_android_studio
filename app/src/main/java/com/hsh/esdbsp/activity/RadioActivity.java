
package com.hsh.esdbsp.activity;

import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.RadioAsyncFeed;
import com.hsh.esdbsp.comm.RadioAsyncGenLink;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.customclass.BaseActivity;
import com.hsh.hshservice.global.Helper;

public class RadioActivity extends BaseActivity {
		
	String myLOG = "RadioActivity";
	
	//-----------
	 
	static int toRoom = 0;
	
	static int myVol = 20; // tablet sound level
	
	static int restartCount = 0;
	static int restartMax = 10;
	
	static String radioUrl = "";
	static String radioName = "";
	
	static MediaPlayer aPlayer; // default player
	
	static int initLinkWait = 500;
	static Handler initLinkLooper = new Handler();	

	private RadioAsyncFeed aRadioFeed;
	private RadioAsyncGenLink aRadioGenLink;

	static Handler realPlayHandler = new Handler();
	static int realPlayDelay = 1000;	
	
	
	static String myCurType = "Country";
	
	private TopBarFragment topBarFragment;
	
	//-----------
	
	ListView lvItem1; 
    ListView lvItem2; 
    ListView lvItem3; 
    
    ArrayList<String> itemArray1; 
    ArrayAdapter<String> itemAdapter1;     
    
    ArrayList<String> itemArray2; 
    ArrayAdapter<String> itemAdapter2; 		
    
    ArrayList<String> itemArray3; 
    ArrayAdapter<String> itemAdapter3;     
	
    ImageView radioBBC;
    ImageView radioRFI;
    ImageView radioVOA;
    ImageView radioRTHK;
    ImageView radioDLF;	
    
    ImageView radioLoad;
	ImageView radioStop;
	ImageView radioMinus;
	ImageView radioPlus;    
	
	//-----------
	
	
	MyTextView rcHeadClock;

	MyTextView headRC;
	MyTextView headService;
	MyTextView headTV;
	MyTextView headRadio;
	MyTextView headLang;
	
	MyTextView headWeather;
	
	MyTextView footRoom;	
	
	MyTextView footOtherWord;
	
	
	LinearLayout streamToRoom;
	MyTextView streamToRoomLabel;
	
	//-----------
	
	LinearLayout layoutPage;
	
	String myCMD;
	
	//////////////////////////////////////////////////////
	
	static String myPresetList = "";
	static String myP1List = "";
	static String myP2List = "";
	static String myP3List = "";
	
	//////////////////////////////////////////////////////
	
	static Handler guiHandler = new Handler();
	static int guiDelay = 500;
	static int guiDelayLong = 5*1000;
	
	static int feedDelay = 100;
	static Handler feedPresetHandler = new Handler();
	static Handler feedP1Handler = new Handler();
	static Handler feedP2Handler = new Handler();
	static Handler feedP3Handler = new Handler();

	
	//////////////////////////////////////////////////////
	
	static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30*1000;	
	
	//////////////////////////////////////////////////////
	
	@Override
    public void onUserInteraction(){
        //renew your timer here
		
		try {
			handleSleep();
			
		} catch (Exception e) {}
    }	

	
	@Override
	protected void onResume() {
		
		Log.v(myLOG, "radio on resume");
		radioUrl = "";
		radioName = "";
		
		//stop();
		
		try {
		
			MainApplication.setCurrentActivity(this);
			
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
				toRoom = 1;
			} else {
				toRoom = 0;
			}
			

		    addRunnableCall();
	    
		} catch (Exception e) {
			Log.v(myLOG,e.toString());
		}
	    
        super.onResume();
    }
	
	@Override
	protected void onPause() {
		Log.v(myLOG, "radio on pause");
		
		try {
		
			radioUrl = "";
			radioName = "";
			
			stop();
			
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
				toRoom = 1;
			} else {
				toRoom = 0;
			}
			
			removeRunnableCall();
			
			clearReferences();
	        
	        
        
		} catch (Exception e) {}

        super.onPause();
    }
	
	
	@Override
    protected void onDestroy() {  
		Log.v(myLOG, "radio on destroy");
		
		try {
		
			radioUrl = "";
			radioName = "";
			
			stop();
			
			
			if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
				toRoom = 1;
			} else {
				toRoom = 0;
			}
			
			removeRunnableCall();
			
	        clearReferences();
	        
	        
        
		} catch (Exception e) {
			
			Log.v(myLOG,e.toString());
		}

        super.onDestroy();
    }	
	
//////////////////////////////////////////////////////////
    
	private void clearReferences(){
		try {
			Activity currActivity = MainApplication.getCurrentActivity();
			
			if (currActivity != null && currActivity.equals(this)) {
				MainApplication.setCurrentActivity(null);
			}
		} catch (Exception e) {}
	}
	
//////////////////////////////////////////////////////////	
	
	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext().getString(R.string.timeoutvalue))*1000;
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);
			
			hideLoad();
			
		} catch (Exception e) {}
	}
	
	private void removeRunnableCall() {
		try {
			try {
				feedP1Handler.removeCallbacks(feedP1Runnable);
				feedP2Handler.removeCallbacks(feedP2Runnable);
			} catch (Exception e) {}
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			guiHandler.removeCallbacks(guiRunnable);
			
		} catch (Exception e) {}
	}	
	
	
//////////////////////////////////////////////////////////
	
	
	private void handleSleep() {
		try {
			
			timeoutHandler.removeCallbacks(timeoutRunnable);
			//timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);
			
		} catch (Exception e) {}
		
	}
	
	
	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				//goMain();
				
			} catch (Exception e) {}
			
		}
	};	
	
	
//////////////////////////////////////////////////////////
	
	
	public void goMain() {
		try {
			
			Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
			MainApplication.getCurrentActivity().startActivity(intent);				
			
		} catch (Exception e) {}
	}
	
	

//////////////////////////////////////////////////////////
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			setContentView(R.layout.radio_all);
			
			//for Flurry log
			final Map<String, String> map = new HashMap<String, String>();
			map.put("Room", MainApplication.getMAS().getData("data_myroom"));
			
			FlurryAgent.logEvent("Radio", map);
			
			
			MainApplication.setCurrentActivity(this);
			
			
			try {
				if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
					toRoom = 1;
				}			
				
				
	
				loadPageItem();
				addRunnableCall();
				loadPageLabel();			
			} catch (Exception e) {}
			
			
			
			
			FragmentManager fragmentManager = getSupportFragmentManager();

			Bundle bundle = new Bundle();
			// bundle.putString("titleId", "");
			bundle.putBoolean("hideBackBtn", true);
			bundle.putString("hightLightChoice", "radio");

			// Create new fragment and transaction
			topBarFragment = new TopBarFragment();
			topBarFragment.setArguments(bundle);

			FragmentTransaction transaction = fragmentManager.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.topBarFragmentContainer, topBarFragment);

			// Commit the transaction
			transaction.commit();
			
			AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0); 
			
			Helper.showInternetWarningToast();
			
			

			
			
		} catch (Exception e) {
			//Log.i(myLOG, "Error = " + e.toString());
			}
	}
	
	
	private void loadPageLabel() {
		try {

			// change all label 
			/*headRC.setText(MainApplication.getLabel("RC").toUpperCase());
			headService.setText(MainApplication.getLabel("Services").toUpperCase());
			
			headTV.setText(MainApplication.getLabel("TV").toUpperCase());
			headRadio.setText(MainApplication.getLabel("Radio").toUpperCase());
			headLang.setText(MainApplication.getLabel("Lang").toUpperCase());	
			headWeather.setText(MainApplication.getLabel("Weather").toUpperCase());*/
			
			try {
				footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));
			} catch (Exception e) {}
			
			try {
				if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
					footRoom.setVisibility(View.INVISIBLE);
				}
				
			} catch (Exception e) {}
			
			try {
				streamToRoomLabel.setText(MainApplication.getLabel("RadioStreamToRoom"));
				
			} catch (Exception e) {} 
			
		} catch (Exception e) {}
	}	
	
	
	private void loadPageItem() {
    	
    	try {
    		
    		MainApplication.brightUp();
    		
    		layoutPage = (LinearLayout)findViewById(R.id.layoutPage);
    		
    		
    		layoutPage.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						
						//handleSleep();
						
					} catch (Exception e) {}
					
					return false;
				}
    		});
    		
    		
    		//////////////////////////////////////////////////////////
    		
    		
    		/*headRC = (MyTextView)findViewById(R.id.headRoomControl);
			headRC.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headRC);
						
						Intent intent = new Intent(getApplicationContext(), RoomControlActivity.class);
						MainApplication.getCurrentActivity().startActivity(intent);

					} catch (Exception e) {}
					
					return false;
				}
				
			});
			
			
			
			headTV = (MyTextView)findViewById(R.id.headTV);
			headTV.setVisibility(View.GONE);
			
			headTV.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headTV);

			        } catch (Exception e) {}
					
					return false;
				}
				
			});	
			
			
			headService = (MyTextView)findViewById(R.id.headService);
			//headService.setVisibility(View.GONE); // tmp add by william ( confirm with andrew at 2014-11-29 )
			
			headService.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headService);
						
						Intent intent = new Intent(getApplicationContext(), ServiceMainActivity.class);
			            startActivity(intent);
						
					} catch (Exception e) {}
					
					return false;
				}
				
			});			
			
			
			
			headRadio = (MyTextView)findViewById(R.id.headRadio);
			headRadio.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headRadio);
						
			        } catch (Exception e) {}
					
					return false;
				}
				
			});	
			
			headLang = (MyTextView)findViewById(R.id.headLang);
			headLang.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headLang);
						
						Intent intent = new Intent(MainApplication.getContext(), LangActivity.class);
						MainApplication.getCurrentActivity().startActivity(intent);	

			        } catch (Exception e) {}
					
					return false;
				}
				
			});				
			
			
			/////////////////
			
			
			headWeather = (MyTextView)findViewById(R.id.headWeather);
			headWeather.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {
						MainApplication.playClickSound(headWeather);
						
						Intent intent = new Intent(MainApplication.getContext(), WeatherActivity.class);
						MainApplication.getCurrentActivity().startActivity(intent);	

					} catch (Exception e) {}
					
					return false;
				}
				
			});	    		
    		
    		
    		///////////////////////////////////////////////////////////
			
			rcHeadClock = (MyTextView) findViewById(R.id.headClock);
    		rcHeadClock.setText("");
			
			try {
    			String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
    			int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
    			int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
    			String myHeadClockSign = "am";
    			
    			if (myHeadClockHour == 0) {
    				myHeadClockHour = 12;
    				myHeadClockSign = "am";
    			} else if (myHeadClockHour == 12) {
    				myHeadClockHour = myHeadClockHour;
    				myHeadClockSign = "pm";
    			} else if (myHeadClockHour > 12) {
    				myHeadClockHour = myHeadClockHour - 12;
    				myHeadClockSign = "pm";
    			} else {
    				myHeadClockSign = "am";
    			}
    			
				
				rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign + "</small>"));

			} catch (Exception e) {} */
			
			
    		try {
				footRoom = (MyTextView) findViewById(R.id.footRoomNum);
	    		footRoom.setText("");
    		} catch (Exception e) {}
    		
    		try {
    			footOtherWord = (MyTextView) findViewById(R.id.footOtherWord);
    			footOtherWord.setText("");
    		} catch (Exception e) {}
    		
    		
    		try {
	    		streamToRoomLabel = (MyTextView) findViewById(R.id.streamToRoomLabel);
	    		streamToRoom = (LinearLayout)findViewById(R.id.streamToRoom);
				streamToRoom_logic();
    		} catch (Exception e) {}
    		
			
			//for PBH, there is no stream to Room function
			if(this.getString(R.string.hotel).equalsIgnoreCase("pbh")){
				try {
				
					streamToRoomLabel.setVisibility(View.GONE);
					streamToRoom.setVisibility(View.GONE);
					
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
					params.addRule(RelativeLayout.RIGHT_OF, R.id.footRoomNum);
					footOtherWord.setLayoutParams(params);
				
				} catch (Exception e) {}

			} else {
				
				try {
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
					params.addRule(RelativeLayout.RIGHT_OF, R.id.streamToRoom);
					footOtherWord.setLayoutParams(params);
				
				} catch (Exception e) {}
			}
			
			try {
				streamToRoom.setOnTouchListener(new OnTouchListener(){
	
					@Override
					public boolean onTouch(View arg0, MotionEvent arg1) {
						// TODO Auto-generated method stub
						
						try {
							MainApplication.playClickSound(streamToRoom);
							
							stop();
							
							
							// send off to room when switching 
							if (toRoom == 1) {
								
								myCMD = "SET%20AVOFF";
								
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							}
							
							
							
							toRoom = (toRoom + 1)%2;  
							
							streamToRoom_logic();
				        	
				        } catch (Exception e) {
				        	//Log.v("kkkkk",e.toString());
				        }
						
						return false;
					}
					
				});	    
				
			} catch (Exception e) {}
    		
    		
    		////////////////////////////////////////////////
    		
    		
    		loadRadioItem();

    	} catch (Exception e) {
    		Log.i(myLOG, "loadPageItem error = " + e.toString());
    	}
    	
	}
	
//////////////////////////////////////////////////////////////////
	
	public void streamToRoom_logic() {
    	
    	try {
    		
    		if (toRoom == 0) {    			
    			Log.i(myLOG, "toRoom = 0");
    			streamToRoom.setActivated(false);
    			
    			if (radioUrl != "") {
    				start( radioUrl, radioName, 0 );
    			}
    			
    			
			} else {
				Log.i(myLOG, "toRoom = 1");
				streamToRoom.setActivated(true);
				
    			if (radioUrl != "") {
    				start( radioUrl, radioName, 0 );
    			}
			} 
    		
    	} catch (Exception e) {Log.i(myLOG, e.toString());}
    	
    }   	
	
	
//////////////////////////////////////////////////////////////////

	private void showLoad() {
		try {
			radioLoad.setVisibility(View.VISIBLE);
			radioLoad.startAnimation(MainApplication.getAnim());
			
		} catch (Exception e) {}
	}
	
	private void hideLoad() {
		try {
			radioLoad.setVisibility(View.INVISIBLE);
			radioLoad.clearAnimation();
			
		} catch (Exception e) {}
	}
	
	
	private void loadRadioItem() {
		
		try {
			
			radioLoad = (ImageView)findViewById(R.id.radioLoad); 
			radioLoad.setAnimation(MainApplication.getAnim());
			radioLoad.setVisibility(View.INVISIBLE);
			
			
			
			footOtherWord = (MyTextView)findViewById(R.id.footOtherWord);
			
			
			
			radioStop = (ImageView)findViewById(R.id.radioStop);
			radioStop.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						
			    		radioUrl = "";
			    		radioName = "";
			    		
						
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(radioStop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							stop();
							
							if (toRoom == 1) {
								
								myCMD = "SET%20AVOFF";
								
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							}
						}
						
			        } catch (Exception e) {}
					
					return false;
				}
				
			});	
			
			radioMinus = (ImageView)findViewById(R.id.radioMinus);
			radioMinus.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(radioMinus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
							Log.v(myLOG,"toRoom:" + toRoom + "");
						
							if (toRoom == 0) {
								myVol = myVol - 1;
					        	if (myVol < 0) {
					        		myVol = 0;
					        	}		        	
					        	
					        	AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
				    			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0); 
								
							} else {
								
								myCMD = "SET%20RADIOVOL%20DOWN";
								
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							}
						
						}
		    			
			        } catch (Exception e) {}
					
					return false;
				}
				
			});					
			
			radioPlus = (ImageView)findViewById(R.id.radioPlus);
			radioPlus.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View arg0, MotionEvent event) {
					// TODO Auto-generated method stub
					
					try {
						
						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
							
							MainApplication.playClickSound(radioPlus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
						
							Log.v(myLOG,"toRoom:" + toRoom + "");
							
							if (toRoom == 0) {
							
								myVol = myVol + 1;
					        	if (myVol >= 20) {
					        		myVol = 20;
					        	}		        	
					        	
					        	AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
				    			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0); 
								
							} else {
								
								myCMD = "SET%20RADIOVOL%20UP";
								
								MainApplication.getMAS().sendMASCmd(myCMD,1);
							}
						}
			    			
			        } catch (Exception e) {}
					
					return false;
				}
				
			});				
			
			
			
			

			/* preset */
			
			radioBBC = (ImageView)findViewById(R.id.radioBBC);  
    		radioRFI = (ImageView)findViewById(R.id.radioRFI);  
    		radioVOA = (ImageView)findViewById(R.id.radioVOA);  
    		radioRTHK = (ImageView)findViewById(R.id.radioRTHK);  
    		radioDLF = (ImageView)findViewById(R.id.radioDLF); 
    		
    		radioBBC.setOnClickListener(new OnClickListener() {


				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try { 
						String[] myPlayURL = myPresetList.split("@"); 
						start(myPlayURL[0],"PRESET BBC",0);
					} catch (Exception e) {}
					
				} 
    			
    		});
    		
    		radioRFI.setOnClickListener(new OnClickListener() {


				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try { 
						String[] myPlayURL = myPresetList.split("@"); 
						start(myPlayURL[1],"PRESET RFI",0);
					} catch (Exception e) {}
					
				} 
    			
    		});    	
    		
    		radioVOA.setOnClickListener(new OnClickListener() {


				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try { 
						String[] myPlayURL = myPresetList.split("@"); 
						start(myPlayURL[2],"PRESET VOA",0);
					} catch (Exception e) {}
					
				} 
    			
    		}); 
    		
    		radioRTHK.setOnClickListener(new OnClickListener() {


				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try { 
						String[] myPlayURL = myPresetList.split("@"); 
						start(myPlayURL[3],"PRESET RTHK",0);
					} catch (Exception e) {}
					
				} 
    			
    		});
    		
    		radioDLF.setOnClickListener(new OnClickListener() {


				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try { 
						String[] myPlayURL = myPresetList.split("@"); 
						start(myPlayURL[4],"PRESET Deutschlandfunk",0);
					} catch (Exception e) {}
					
				} 
    			
    		});      	    		
    		    		
    		
    		
    		
			
			MainApplication.getRadio().setPresetOK(0);
			
    		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)				    
    			aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"PRESET",myCurType);
			else
				aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("PRESET",myCurType);
			            			

			feedPresetHandler.removeCallbacks(feedPresetRunnable);
			feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);

			/* end preset */
			
			
    		loadRadioThreeList();
    		
    		loadDefaultList();
    		
			
		} catch (Exception e) {
			Log.v(myLOG,e.toString());
		}	
		
	}
	
	private void loadDefaultList() {
		try {
			/* load the first item */
			myCurType = "Country";
			
			itemAdapter2.clear();
            itemAdapter3.clear();
    		
            
    		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)				    
    			aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"BASE",myCurType);
			else
				aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("BASE",myCurType);
			            
    		
    		
    		MainApplication.getRadio().setCountryOK(0);
    		
			feedP1Handler.removeCallbacks(feedP1Runnable);
			feedP1Handler.postDelayed(feedP1Runnable, feedDelay);			
			
		} catch (Exception e) {}
	}
	
	private void loadRadioThreeList() {
		
		try {
			
			lvItem1 = (ListView)this.findViewById(R.id.typeList1);  
    		lvItem1.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
	        
	        String[] lvItem1values = new String[] { MainApplication.getLabel("RadioCountry"), MainApplication.getLabel("RadioGenre") };
	        
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	                R.layout.radio_scrollview_row, R.id.label, lvItem1values);
	            lvItem1.setAdapter(adapter);  
	            
	            lvItem1.setOnItemClickListener(new OnItemClickListener() { 

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						
						try {
							
							if (position == 0) {
								myCurType = "Country";
								//for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS().getData("data_myroom"));
								FlurryAgent.logEvent("RadioCountry", map);
							} else {
								myCurType = "Genre";
								//for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS().getData("data_myroom"));
								FlurryAgent.logEvent("RadioGenres", map);
							}

							
				            itemAdapter2.clear();
				            itemAdapter3.clear();
				            
							
				            try {
				            	if (aRadioFeed != null) {
				            		aRadioFeed.cancel(true);
				            	}
				            } catch (Exception e) {}
				            
				            
				    		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)				    
				    			aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"BASE",myCurType);
							else
								aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("BASE",myCurType);
							            				            
				            
				            if (position == 0) {
				            	MainApplication.getRadio().setCountryOK(0);
				            } else {
				            	MainApplication.getRadio().setGenreOK(0);
				            }
							feedP1Handler.removeCallbacks(feedP1Runnable);
							feedP1Handler.postDelayed(feedP1Runnable, feedDelay); 	

						} catch (Exception e) {}
						
					} 
	            }); 
	            
	            //////////////////////
	            
	            lvItem2 = (ListView)this.findViewById(R.id.typeList2); 
	            lvItem2.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
	            
	            itemArray2 = new ArrayList<String>(); 
	            itemArray2.clear(); 
	            
	            itemAdapter2 = new ArrayAdapter<String>(this, 
	            		R.layout.radio_scrollview_row,R.id.label,itemArray2); 
	            lvItem2.setAdapter(itemAdapter2); 
	            
	            
	            lvItem2.setOnItemClickListener(new OnItemClickListener() { 

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub

						try {
							String[] myFeed = myP1List.split("@");
							
							String strList2Type = myFeed[(myFeed.length-1) - position];
							String[] strList2TypeDetail = strList2Type.split("#");

							String myFilter = strList2TypeDetail[0].toString();
							
							myFilter = myFilter.replaceAll("&amp;", "%26");
							myFilter = myFilter.replaceAll("&", "%26");
							
							Log.i("Radio", "myFilter = " + myFilter);
							
							//for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("Type", myFilter);
							FlurryAgent.logEvent("RadioType", map);
							
				            itemAdapter3.clear();					
							
				            try {
				            	if (aRadioFeed != null) {
				            		aRadioFeed.cancel(true);
				            	}
				            } catch (Exception e) {}
				            
				            
				            
				            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)				    
				            	aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,myFilter,myCurType);
							else
								aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute(myFilter,myCurType);
				            
							  
				            
				            MainApplication.getRadio().setRadioOK(0);
				            feedP2Handler.removeCallbacks(feedP2Runnable);
							feedP2Handler.postDelayed(feedP2Runnable, feedDelay); 	

						} catch (Exception e) {}
						
					} 
	            });	            
	            
	            
	            //////////////////////
	            
	            lvItem3 = (ListView)this.findViewById(R.id.typeList3); 
	            lvItem3.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
	            
	            itemArray3 = new ArrayList<String>(); 
	            itemArray3.clear(); 
	            
	            itemAdapter3 = new ArrayAdapter<String>(this, 
	            		R.layout.radio_scrollview_row,R.id.label,itemArray3);  
	            lvItem3.setAdapter(itemAdapter3); 
	            
	            lvItem3.setOnItemClickListener(new OnItemClickListener() { 

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						
	                    //itemArray.clear(); 
						try {
							
							String[] myFeed = myP2List.split("@"); 
							String strStation = myFeed[(myFeed.length-1) - position];
							String[] strStationDetail = strStation.split("#");
							start( strStationDetail[1], strStationDetail[0],0 );
							
							//for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("Name", strStationDetail[0]);
							FlurryAgent.logEvent("RadioStation", map);	

						} catch (Exception e) {}
						
					} 
	            });
	            
			
		} catch (Exception e) {
			Log.v(myLOG,e.toString());
		}
		
	}
	
	Runnable feedPresetRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			try {
				feedPresetHandler.removeCallbacks(feedPresetRunnable);
				
				if (MainApplication.getRadio().getPresetOK() == 0) {
					feedPresetHandler.removeCallbacks(feedPresetRunnable);
					feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
					
				} else {
					myPresetList = MainApplication.getRadio().getPreset();

				}
				
			} catch (Exception e) {}
			
		}
		
		
	};
	
	
	Runnable feedP1Runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			String[] strFEED;
			String[] strFEEDDetail;
			
			int myOK = 0;
			
			try {
				feedP1Handler.removeCallbacks(feedP1Runnable);
				
				if (myCurType.equalsIgnoreCase("Country")) {
					myOK = MainApplication.getRadio().getCountryOK();
				} else {
					myOK = MainApplication.getRadio().getGenreOK();
				}

				//myOK = 1; // changed no async
				if (myOK == 0) {
					feedP1Handler.removeCallbacks(feedP1Runnable);
					feedP1Handler.postDelayed(feedP1Runnable, feedDelay);
					
					Log.v(myLOG,"loop die p1"); 
					
				} else {
					
					if (myCurType.equalsIgnoreCase("Country")) {
						myP1List = MainApplication.getRadio().getCountry();
					} else {
						myP1List = MainApplication.getRadio().getGenre();
					}					
					
					//

					strFEED = myP1List.split("@");

					for(int i=0;i<strFEED.length;i++) {
						
						try {
							strFEEDDetail = strFEED[i].split("#");

							if (strFEEDDetail.length == 2) {
								addList1("" + strFEEDDetail[1]);
							} else {
								addList1("" + MainApplication.getRadio().checkMyCountryLabel(strFEEDDetail[0].trim() + ""));
							}

						} catch (Exception e) {}
					}					

				}
				
			} catch (Exception e) {}
			
		}
		
		
	};
	
	
	Runnable feedP2Runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			String[] strFEED;
			String[] strFEEDDetail;
			
			try {
				feedP2Handler.removeCallbacks(feedP2Runnable);
				
				if (MainApplication.getRadio().getRadioOK() == 0) {
					feedP2Handler.removeCallbacks(feedP2Runnable);
					feedP2Handler.postDelayed(feedP2Runnable, feedDelay);
					
					Log.v(myLOG,"cannot feed the data from server"); 
					
				} else {
					myP2List = MainApplication.getRadio().getRadio();
					
					//

					strFEED = myP2List.split("@");

					for(int i=0;i<strFEED.length;i++) {
						
						try {
							strFEEDDetail = strFEED[i].split("#");
							String tmp;

							if (strFEEDDetail.length == 2) {
								tmp = URLDecoder.decode(strFEEDDetail[1], "UTF-8");
								addList2("" + tmp);
							} else {
								tmp = URLDecoder.decode(strFEEDDetail[0], "UTF-8");
								addList2("" + tmp);
							}

						} catch (Exception e) {}
					}					

				}
				
			} catch (Exception e) {}
			
		}
		
		
	};	
	
	
///////////////////////////////////////////////////////////////////
	
	
	protected void addList1(String name) { 
        // TODO Auto-generated method stub 
 
        try {
        	//String traName = name.replaceAll("&amp;", "&");
        	
        	itemArray2.add(0,Html.fromHtml(name) + ""); 
        	itemAdapter2.notifyDataSetChanged();

        } catch (Exception e) {  }
 
    }      
    
    protected void addList2(String name) { 
        // TODO Auto-generated method stub 
 
        try {
    	
        	itemArray3.add(0,Html.fromHtml(name) + ""); 
        	itemAdapter3.notifyDataSetChanged();
        
        } catch (Exception e) {  }
 
    }	
	
	
///////////////////////////////////////////////////////////////////
	
	private void restart() {
    	
    	try {
    		Log.v(myLOG,"restarted:"+restartCount);
    		
    		start(radioUrl,radioName, restartCount);
    		
    	} catch (Exception e) {}
    	
    }
	
	
	private void start( final String myurl, String name, int isRestart ) {
    	
    	String myRadioName = name;
    	
    	//stop();
    	
    	radioUrl = myurl;
    	radioName = name;

		if (isRestart == 0) {
			restartCount = 1;
		} else {
			restartCount = restartCount + 1;
		}

    	try {   		

			try {
				
				if (toRoom == 0) {

					footOtherWord.setText(Html.fromHtml("STREAMING"));
					showLoad();	
					
					myRadioName = name;
					myRadioName = myRadioName.replace(" ","%20");
					myRadioName = myRadioName.replace("&amp;","%26");
					
					
					initLinkLooper.removeCallbacks(initLinkLoop);
					initLinkLooper.postDelayed(initLinkLoop, initLinkWait);  

				} else {
					try {
						hideLoad();	
					} catch (Exception e) {}
					
					myRadioName = name;
					myRadioName = myRadioName.replace(" ","%20");
					myRadioName = myRadioName.replace("&amp;","%26");
					
					
					myCMD = "SET%20RADIOMODE";
					MainApplication.getMAS().sendMASCmd(myCMD,1);
					
					myCMD = "SET%20RADIO%20"+myRadioName;
					MainApplication.getMAS().sendMASCmd(myCMD,0);
					
					
					footOtherWord.setText(Html.fromHtml("") + radioName);
				}
				
				
			} catch (Exception e) {}    		    		

    	} catch (Exception e) {}
    	
    }    
    
    
    private void stop() {
    	try {
    		initLinkLooper.removeCallbacks(initLinkLoop);
    		realPlayHandler.removeCallbacks(realPlayRunnable);
    		
    		
    		if (aPlayer != null) { aPlayer.stop(); aPlayer = null; }
    		footOtherWord.setText("");

    		try {
    			hideLoad();				
				
			} catch (Exception e) {}
    		
    		
    		
    		if (toRoom == 1) {
				
				//myCMD = "SET%20AVOFF";
				//MainApplication.getMAS().sendMASCmd(myCMD, 1);
			}
			
    		
			//myCMD = "SET%20AVOFF";
			//MainApplication.getMAS().sendMASCmd(myCMD, 1);    		
    		
    		
    		
    		try {
				if (aRadioGenLink != null) {
					aRadioGenLink.cancel(true);
				}
			} catch (Exception e) {}    		


    		try {
    			if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
    				aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"STOP"); 
    			} else {
    				aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().execute("STOP"); 
    			}
    			                			
    			
    		} catch (Exception e) {}

    	} catch (Exception e) {}
    }
    
    
    private void realStart() {
    	
    	final String url;
    	
    	try {   		
    		
    		realPlayHandler.removeCallbacks(realPlayRunnable);
    		
    		
    		if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pbh")){
    			url = "http://" + MainApplication.getContext().getString(R.string.radio_path) + ":" + MainApplication.getRadio().getPort();
    		} else if(MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")){
    			url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
    			
    		}  else {
    			url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
    		}
    		
    		
    		
    		aPlayer = null;
    		aPlayer = new MediaPlayer();
    		aPlayer.setDisplay(null);
    		aPlayer.reset();

    		aPlayer.setOnCompletionListener(         
					new MediaPlayer.OnCompletionListener() {         
						public void onCompletion(MediaPlayer mp) {        
							try {
								//Toast.makeText(getApplicationContext(), "Link completed ...", Toast.LENGTH_LONG).show();
								
								// should not complete
								restartCount = 0;
								restart();
								
							} catch (Exception e) {
								
								restartCount = 0;
								restart();

							}
							
						}
					}); 
    		
    		aPlayer.setOnPreparedListener(
	    			new MediaPlayer.OnPreparedListener() {
						
						@Override
						public void onPrepared(MediaPlayer mp) {
							// TODO Auto-generated method stub
							
							try {
								
								try {
									hideLoad();
									
								} catch (Exception e) {}
								
								aPlayer.start();
								footOtherWord.setText(Html.fromHtml("") + radioName);
							} catch (Exception e) {  
								
								restartCount = 0;
								restart();
							}
						}
					});
    		
    		aPlayer.setOnErrorListener(
					
					new MediaPlayer.OnErrorListener() {
						
						@Override
						public boolean onError(MediaPlayer mp, int what, int extra) {
							// TODO Auto-generated method stub
							
							try {
								
								if (restartCount < restartMax) {
									stop();
									restart();
									
								} else {
					    			
					    			try {

										footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
										hideLoad();
										
									} catch (Exception e) {}	
									
								}
								
							} catch (Exception e) {
								footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
								hideLoad();
							}
							
							return true;
						}
						
					});
    		
    		
    		// first start
			try {
				Log.v(myLOG,"here start");
				
				aPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				aPlayer.setDataSource(url); 
				aPlayer.prepareAsync();

			} catch (Exception e) {
				Log.v(myLOG,"here die" + e.toString());
			}

    	} catch (Exception e) {}
    	
    }    
    

    
//////////////////////////////////////////////////////////////////    
    
    private Runnable realPlayRunnable = new Runnable() {

		public void run() {
			try {
				realPlayHandler.removeCallbacks(realPlayRunnable);
				realStart();
				
			} catch (Exception e) {}
		}
    };
    
    
    private Runnable initLinkLoop = new Runnable() {

		public void run() {
			
			String[] strFEED;
			
			try {
				initLinkLooper.removeCallbacks(initLinkLoop);
				
				try {
					
					stop();
					
					if (toRoom == 1) {
						
						myCMD = "SET%20AVOFF";
						
						//MainApplication.getMAS().sendMASCmd(myCMD, 1);
					}
					
				} catch (Exception e) {}
				
				
				try {
					footOtherWord.setText(Html.fromHtml("STREAMING"));
					
					showLoad();
					
				} catch (Exception e) {}
				
				
				try {
					if (aRadioGenLink != null) {
						aRadioGenLink.cancel(true);
					}
				} catch (Exception e) {}

				
				if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
					aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"START",radioUrl);
    			else
    				aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().execute("START",radioUrl); 

				
				realPlayHandler.removeCallbacks(realPlayRunnable);
				realPlayHandler.postDelayed(realPlayRunnable, realPlayDelay);
				
			} catch (Exception e) {}
		}
    };
    
    
 ////////////////////////////////////////////////////
    
    
    private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {
				
				guiHandler.removeCallbacks(guiRunnable);

				try { 
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources().getDrawable(getResources().getIdentifier("drawable/battery_" + MainApplication.getBatteryChecker().getBatteryLevel(), "drawable", getPackageName()))));

				} catch (Exception e) {}
				
				
				try {
					
					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {
						goMain();
						
						Log.v(myLOG,"alarm wake");
					}						
					
				} catch (Exception e) {}
				
				
				try {
					try {
		    			String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		    			int myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
		    			int myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
		    			String myHeadClockSign = "am";
		    			
		    			if (myHeadClockHour == 0) {
		    				myHeadClockHour = 12;
		    				myHeadClockSign = "am";
		    			} else if (myHeadClockHour == 12) {
		    				myHeadClockHour = myHeadClockHour;
		    				myHeadClockSign = "pm";
		    			} else if (myHeadClockHour > 12) {
		    				myHeadClockHour = myHeadClockHour - 12;
		    				myHeadClockSign = "pm";
		    			} else {
		    				myHeadClockSign = "am";
		    			}
		    			
						
		    			rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign + "</small>"));
		    			
		    			
					} catch (Exception e) {}   					
	
				} catch (Exception e) {}
				

				loadPageLabel();
				

				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);
				
			} catch (Exception e) {}
		}
	};
	
	
/////////////////////////////////////////////////////
	
	@Override 
    public boolean onKeyDown(int keyCode, KeyEvent event) {    
    	//Log.v("feed",""+event.getKeyCode());
    	
    	
    	try {
    		//AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
			//audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 30, 0);	
			
			
	    	if (keyCode == KeyEvent.KEYCODE_BACK) {
	    		
	            // 		    		
	    		
	    		return true; 
	    	}
    		
	    	if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
	        	try {

		        	myVol = myVol - 1;
		        	if (myVol < 0) {
		        		myVol = 0;
		        	}		        	
		        	
		        	AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
	    			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0); 

	        	} catch (Exception e) {} 
	    		
	    		return true; 
	    	}
	    	
	    	if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
	        	try {
	            	// no system bar
	            	//Process proc = Runtime.getRuntime().exec(new String[]{"su","-c","service call activity 79 s16 com.android.systemui"});
	            	//proc.waitFor();
	            	
		        	myVol = myVol + 1;
		        	if (myVol >= 20) {
		        		myVol = 20;
		        	}
		        	
		        	AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
	    			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0); 
	    			
	    			
	        	} catch (Exception e) {} 
	    		
	    		return true;
	    	}	       	
	    	
	        if (keyCode == KeyEvent.KEYCODE_POWER) {         
	        	
	        	return true;
	        }
	        
    	} catch (Exception e) {  }
    	
    	return super.onKeyDown(keyCode, event); 
    	
    }  	
	
	
	

	
}