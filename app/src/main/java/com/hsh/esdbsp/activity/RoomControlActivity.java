package com.hsh.esdbsp.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.app.enterprise.knoxcustom.KnoxCustomManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ClipDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.ESDDeviceAdminReceiver;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.ui.MyNumberPicker;
import com.hsh.esdbsp.ui.MySpinnerAdapter;
import com.hsh.esdbsp.ui.MyTextView;
import com.hsh.esdbsp.ui.SlidingPanel;
import com.hsh.esdbsp.ui.TwoDigitFormatter;
import com.hsh.hshservice.MessageActivity;
import com.hsh.hshservice.ServiceMainActivity;
import com.hsh.hshservice.global.CheckMessageUpdateManager;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.global.Helper;
import com.splunk.mint.Mint;

public class RoomControlActivity extends Activity implements SensorEventListener {

	String myLOG = "RoomControlActivity";

	// SAMSUNG
	private KnoxCustomManager kcm;

	////////////////
	// system admin

	static final int ACTIVATION_REQUEST = 47; // identifies our request id
	DevicePolicyManager devicePolicyManager;
	ComponentName myDeviceAdmin;

	// for bsp sleep
/*	private WakeLock mWakeLock;
	private PowerManager powerManager;*/
	
	///////////////////////////////// loadRCLabel/////////////////////

	// static int isBR = 0; // current page , i.e.: br or lr
	static int isSuite = 0; // for display the selection or not

	static float myBrightness = 0.01f; // for testing, normal is 0.01 full dark
	// static float myBrightness = -1f;

	String myCMD = "";

	//////////////////////////////////////////////////////

	private static String data_myroom = "";
	private static String data_curtime = "";
	private static String data_language = "";
	private static String data_mur = "";
	private static String data_dnd = "";
	private static String data_valet = "";
	private static String data_valetbox = "";
	private static String data_tdnd = "";
	private static String data_roommasterlight = "";
	private static String data_roomlight = "";
	private static String data_nightlight = "";
	private static String data_temperature = "";
	private static String data_temperature_cf = "";
	private static String data_fan = "";
	private static String data_alarm = "";
	private static String data_alarm_hr = "";
	private static String data_alarm_snooze = "";
	private static String data_alarm_time = "";
	private static String data_alarm_settime = "";

	private static String data_havemsg = "";
	private static String data_soundbarmute = "";

	//////////////////////////////////////////////////////

	static int myHeadClockHour;
	static int myHeadClockMin;
	static String myHeadClockSign; // am pm

	static int myHeadAlarmHour;
	static int myHeadAlarmMin;
	static String myHeadAlarmSign; // am pm

	//////////////////////////////////////////////////////

	MyTextView headRC;
	MyTextView headTV;
	MyTextView headRadio;
	MyTextView headLang;
	MyTextView headWeather;

	MyTextView footRoom; // room number
	// MyTextView footRoomArea; // zone
	Spinner footRoomArea; // zone
	MySpinnerAdapter dataAdapter; // for the spinner

	ImageView rcMasterSwitch;
	int int_rcMasterSwitch = 0;
	TextView rcMasterSwitchLabel;

	ImageView rcBedroomLightsTop;
	ImageView rcBedroomLightsBase;
	ImageView rcBedroomLightsDisplay;
	ClipDrawable rcBedroomLightsDisplayDrawable;
	ImageView rcBedroomLightsMinus;

	ImageView rcBedroomLightsPlus;
	int int_rcBedroomLights = 0;
	TextView rcBedroomLightsLabel;

	TextView rcBedroomTempLabel;
	TextView rcBedroomTempDisplay;
	ImageView rcBedroomTempButton;
	ImageView rcBedroomTempType;
	ImageView rcBedroomTempMinus;
	ImageView rcBedroomTempPlus;
	int int_rcBedroomTempCF = 0; // 0 is c
	float ff_rcBedroomTemp = 22.0f;
	float ff_rcBedroomTempOrg = 22.0f;
	float ff_rcBedroomTempValue = ff_rcBedroomTemp;

	TextView rcNightLightLabel;
	ImageView rcNightLight;
	LinearLayout rcNightLightALL; // special for suite room only
	int int_rcNightLight = 0;

	TextView rcCurtainsLabel;
	ImageView rcCurtainsTop;
	ImageView rcCurtainsBase;
	ImageView rcCurtainsClose;
	ImageView rcCurtainsOpen;
	int int_rcCurtains = 0;

	TextView rcFanLabel;
	ImageView rcFanTop;
	ImageView rcFanBase;
	ImageView rcFanDisplay;
	ClipDrawable rcFanDisplayDrawable;
	ImageView rcFanMinus;
	ImageView rcFanPlus;
	int int_rcFan = 0;

	TextView rcHeadClock;
	TextView rcHeadClockSign;
	TextView rcHeadBigClock;
	TextView rcHeadBigClockSign;

	TextView rcVCLabel;
	LinearLayout rcVC;
	int int_rcVC = 0;

	TextView rcMURLabel;
	LinearLayout rcMUR;
	int int_rcMUR = 0;

	TextView rcDNDLabel;
	LinearLayout rcDND;
	int int_rcDND = 0;

	// new add alarm button for pch
	TextView rcAlarmLabel;
	LinearLayout rcAlarm;

	TextView headAlarmClock;
	ImageView headAlarm;

	TextView rcSetAlarmLabel;
	LinearLayout rcSetAlarm;
	int int_rcSetAlarm = 0;

	TextView rcSnoozeLabel;
	LinearLayout rcSnooze;
	int int_rcSnooze = 0;

	TextView rcSetHourLabel;
	LinearLayout rcSetHour;
	int int_rcSetHour = 12; // will read from setting file
	int int_rcSetHourMAS = 12; // mas value

	TextView rcSetMinuteLabel;
	LinearLayout rcSetMinute;
	int int_rcSetMinute = 0;
	int int_rcSetMinuteMAS = 0; // mas value

	// added for the touch wake
	Button btnTouchWake;

	LinearLayout layoutRC;

	LinearLayout pch_rc_top; // default is 5 block
	LinearLayout pch_rc_bottom;

	LinearLayout pbh_rc_top;
	LinearLayout pbh_rc_bottom;

	// ViewFlipper vf; // switch between the layout
	final int bedroompage = 0;
	final int otherroompage = 1;

	int curroompage = 0;

	SlidingPanel pchpage_3;
	SlidingPanel pchpage_4;
	SlidingPanel pchpage_5;
	SlidingPanel pchpage_6;

	//////////////////////////////////

	int checkFootAreaInit = 0;

	String checkCurLang = "E";

	///////////////////////////////////
	// for the light bar len

	int levelStep = 1100;

	int bedroomlightBase = 2100;
	int fanspeedBase = 3200;

	int ACMin = 18; // for PBHsp is 15 , PBH is 18 , PCH is 18
	int ACMax = 28;

	//////////////////////////////////////////////////////

	static int autoSetAlarmDoMe = 0;

	static Handler autoHHHandler = new Handler();
	static int autoHHDelay = 100;

	static Handler autoMMHandler = new Handler();
	static int autoMMDelay = 100;

	/////////////////////////////////////////

	// for pch alarm
	MyNumberPicker hrList;
	MyNumberPicker minList;
	MyNumberPicker apmList;

	// for the pop
	RelativeLayout rcBigSnooze;
	RelativeLayout rcBigOff;

	MyTextView rcBigSnoozeLabel;
	MyTextView rcBigOffLabel;

	// pch version
	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;

	// for pch alarm pop-up
	private Animation alarmPopAnimShow, alarmPopAnimHide;
	SlidingPanel alarmPopup;
	SlidingPanel alarmPopupAlert;

	int int_AlarmPop = 0; // 1 is set , 2 is alert

	/////////////////////////////////////////
	// g sensor

	private SensorManager sensorMan;
	private Sensor accelerometer;

	private float[] mGravity;
	private float mAccel;
	private float mAccelCurrent;
	private float mAccelLast;

	//////////////////////////////////////////////////////

	static Handler guiHandler = new Handler();
	static int guiDelay = 300;
	static int guiDelayLong = 5 * 1000;

	//////////////////////////////////////////////////////

	static Handler loadLabelHandler = new Handler();
	static int loadLabelDelay = 300;

	//////////////////////////////////////////////////////

	public static Handler timeoutHandler = new Handler();
	static int timeoutDelay = 30 * 1000;

	//////////////////////////////////////////////////////

	// add by Marco
	private MyTextView headService;
	private ImageView msgImage;
	private MyTextView msgText;
	private RelativeLayout loadingProgressBar;

	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;

	private Handler msgLogoHandler = new Handler();
	private int blindDelay = 1 * 1000;
	private Boolean messageIsRed = false;

	private Runnable loadDataRunnable = new Runnable() {

		public void run() {
			try {

				if (!messageIsRed) {
					msgImage.setImageResource(R.drawable.red_mail);
					messageIsRed = true;
				} else {
					msgImage.setImageResource(R.drawable.normal_mail);
					messageIsRed = false;
				}

				msgLogoHandler.removeCallbacks(loadDataRunnable);
				msgLogoHandler.postDelayed(loadDataRunnable, blindDelay);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	//////////////////////////////////////////////////////

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			try {
				if (intent.getAction().equalsIgnoreCase("change_gold_msg")) {
					Log.i(myLOG, "yeah have broadcast, change to gold color");
					CheckMessageUpdateManager.getInstance().setIsGold(true);
					msgLogoHandler.postDelayed(loadDataRunnable, 500);
				} else if (intent.getAction().equalsIgnoreCase("change_white_msg")) {
					Log.i(myLOG, "yeah have broadcast, change to white color");
					CheckMessageUpdateManager.getInstance().setIsGold(false);
					msgLogoHandler.removeCallbacks(loadDataRunnable);
					msgImage.setImageResource(R.drawable.normal_mail);
				}

			} catch (Exception e) {
			}
		}
	};
	
	
	
	
	

	@Override
	public void onUserInteraction() {
		// renew your timer here

		try {
			//wakeBSPLightOnly();
			wakeBSP();

			// handleSleep();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		
		Log.v(myLOG, "onResume fire");
		try {

			if (MainApplication.mWakeLock != null) {
				MainApplication.mWakeLock.release();
			}

			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			MainApplication.mWakeLock.acquire();

			setGSensor();
			//sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

		} catch (Exception e) {
		}

		try {
			MainApplication.setCurrentActivity(this);

			try {
				checkFootAreaInit = 0;

				// MainApplication.getMAS().setCurRoom(isBR);
				// switchLayout(MainApplication.getMAS().getCurRoom());

			} catch (Exception e) {
			}

			wakeBSP();

			addRunnableCall();

			switchLayout(MainApplication.getMAS().getCurRoom());

		} catch (Exception e) {
		}

		try {
			DataCacheManager.getInstance().startLoadData();

			Helper.showInternetWarningToast();

			Log.v(myLOG, "onResume");

			IntentFilter filterRefreshUpdate = new IntentFilter();
			filterRefreshUpdate.addAction("change_gold_msg");
			filterRefreshUpdate.addAction("change_white_msg");

			// broadcast
			LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filterRefreshUpdate);

		} catch (Exception e) {
		}

		super.onResume();
	}

	@Override
	protected void onPause() {
		
		Log.v(myLOG, "onPause fire");
		
		try {

			if (MainApplication.mWakeLock != null) {
				MainApplication.mWakeLock.release();
			}

			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			MainApplication.mWakeLock.acquire();

			setGSensor();
			//sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
			

		} catch (Exception e) {
			Log.v(myLOG, e.toString());
		}

		try {

			//clearReferences();
			removeRunnableCall();

			LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
			
			Log.v(myLOG, "onPause");

		} catch (Exception e) {
			
			e.printStackTrace();
		}

		super.onPause();

	}

	@Override
	protected void onStop() {

		try {

			if (MainApplication.mWakeLock != null) {
				MainApplication.mWakeLock.release();
			}

			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			MainApplication.mWakeLock.acquire();

			setGSensor();
			//sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

		} catch (Exception e) {
		}

		try {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
		} catch (Exception e) {
		}

		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.v(myLOG, "onDestroy fire");
		try {
			//clearReferences();

			removeRunnableCall();
			
			RoomControlActivity.timeoutHandler.removeCallbacksAndMessages(null);

			LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
			
			sensorMan.unregisterListener(this, accelerometer);
			
			Log.v(myLOG, "onDestroy");

		} catch (Exception e) {
		}

		

	}

	///////////////////////////////////////////////////////////////////

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			
			
			super.onCreate(savedInstanceState);

			Mint.initAndStartSession(RoomControlActivity.this, this.getString(R.string.splunk_key));

			settings = MainApplication.getContext().getSharedPreferences(PREFS_NAME, 0);

			requestWindowFeature(Window.FEATURE_NO_TITLE);
			

			Log.v(myLOG, "onCreate fire");  
			
			///
			
			//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			//powerManager = (PowerManager) getSystemService(MainApplication.getContext().POWER_SERVICE);
/*			
			mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			mWakeLock.acquire();
			
			*/
			
			
			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			MainApplication.mWakeLock.acquire();
			
			
			setGSensor();
			
			//sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
			
			
			if(this.getString(R.string.hotel).equalsIgnoreCase("pch")){
			//
			// // system admin
			try {
				// Initialize Device Policy Manager service and our receiver
				// class
				devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
				myDeviceAdmin = new ComponentName(this, ESDDeviceAdminReceiver.class);

				// Activate device administration
				if (!devicePolicyManager.isAdminActive(myDeviceAdmin)) {
					Log.v(myLOG, "Samsung: enabling application as device admin");
					try {
						Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
						intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, myDeviceAdmin);
						intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "ESD BSP");
						startActivityForResult(intent, ACTIVATION_REQUEST);

					} catch (Exception e) {
						Log.v(myLOG, "Exception:" + e);
					}
				} else {
					Log.v(myLOG, "Samsung: Application already has device admin privileges");
				}

			} catch (Exception e) {
			}
			}
			//

			//
			// try {
			// // by pass the async send cmd
			// StrictMode.ThreadPolicy policy = new
			// StrictMode.ThreadPolicy.Builder().permitAll().build();
			// StrictMode.setThreadPolicy(policy);
			//
			// } catch (Exception e) {}

			try {
				// set mas base once

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.mas_base)));
			} catch (Exception e) {
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				curroompage = bedroompage;

				setContentView(R.layout.roomcontrol_all);

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
				curroompage = bedroompage;

				setContentView(R.layout.roomcontrol_all_pch);

				// if (settings.getBoolean("hasCurtain", false)) {
				// setContentView(R.layout.roomcontrol_all_pchcurtain);
				//
				// } else {
				//
				// }

			} else {
				curroompage = 5;

				setContentView(R.layout.roomcontrol_all_pch);
			}

			try {

				pchpage_3 = (SlidingPanel) findViewById(R.id.popup_3);
				pchpage_4 = (SlidingPanel) findViewById(R.id.popup_4);
				pchpage_5 = (SlidingPanel) findViewById(R.id.popup_5);
				pchpage_6 = (SlidingPanel) findViewById(R.id.popup_6);

			} catch (Exception e) {
			}

			// for Flurry log
			final Map<String, String> map = new HashMap<String, String>();
			map.put("Room", MainApplication.getMAS().getData("data_myroom"));

			FlurryAgent.logEvent("RoomControl", map);

			MainApplication.setCurrentActivity(this);

			try {
				int_rcSetHour = Integer.parseInt(MainApplication.getContext().getString(R.string.default_alarm_h));
				int_rcSetHourMAS = int_rcSetHour;

				int_rcSetMinute = Integer.parseInt(MainApplication.getContext().getString(R.string.default_alarm_m));
				int_rcSetMinuteMAS = int_rcSetMinute;

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// the middle layouts of RoomControl
				pch_rc_top = (LinearLayout) findViewById(R.id.pch_rc_top);
				pch_rc_bottom = (LinearLayout) findViewById(R.id.pch_rc_bottom);

				pch_rc_top.setVisibility(View.VISIBLE);
				pch_rc_bottom.setVisibility(View.VISIBLE);

				/*
				 * pbh_rc_top = (LinearLayout)findViewById(R.id.pbh_rc_top);
				 * pbh_rc_bottom =
				 * (LinearLayout)findViewById(R.id.pbh_rc_bottom);
				 * 
				 * pch_rc_top.setVisibility(View.GONE);
				 * pch_rc_bottom.setVisibility(View.GONE);
				 * 
				 * pbh_rc_top.setVisibility(View.GONE);
				 * pbh_rc_bottom.setVisibility(View.GONE);
				 */

			} catch (Exception e) {
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				// myCurLayout = pbhpage;

				curroompage = 0;

				/*
				 * pbh_rc_top.setVisibility(View.VISIBLE);
				 * pbh_rc_bottom.setVisibility(View.VISIBLE);
				 * 
				 * pch_rc_top.removeAllViews(); pch_rc_bottom.removeAllViews();
				 */

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				alarmPopAnimShow = AnimationUtils.loadAnimation(this, R.anim.popup_show);
				alarmPopAnimHide = AnimationUtils.loadAnimation(this, R.anim.popup_hide);

				curroompage = 1;

				/*
				 * pbh_rc_top.setVisibility(View.VISIBLE);
				 * pbh_rc_bottom.setVisibility(View.VISIBLE);
				 */

				/*
				 * pch_rc_top.setVisibility(View.VISIBLE);
				 * pch_rc_bottom.setVisibility(View.VISIBLE);
				 * 
				 * pbh_rc_top.removeAllViews(); pbh_rc_bottom.removeAllViews();
				 */

			}

			loadRCItem();
			loadRCLabel();

			try {
				// MainApplication.getMAS().setCurRoom(isBR);

				switchLayout(MainApplication.getMAS().getCurRoom());

			} catch (Exception e) {
			}

			addRunnableCall();

			// LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
			// new IntentFilter("change_white_msg"));

			
			DataCacheManager.getInstance().startLoadData();

			CheckMessageUpdateManager.getInstance().startLoadData();
			

			// Helper.showInternetWarningToast();

		} catch (Exception e) {
			Log.v(myLOG, e.toString());
			e.printStackTrace();
		}

		/*
		 * String kk = null; if(kk.equalsIgnoreCase("ghqaga")){ Log.i(myLOG,
		 * "yaeh"); }
		 */

		/*
		 * Handler handler = new Handler();
		 * 
		 * handler.postDelayed(new Runnable(){
		 * 
		 * @Override public void run(){ ArrayList<String> list = new
		 * ArrayList<String>();
		 * 
		 * list.get(11); }
		 * 
		 * }, 5000);
		 */

	}

	private void setGSensor() {
		try {

			// g-sensor
			sensorMan = (SensorManager) this.getSystemService(this.SENSOR_SERVICE);
			accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			mAccel = 0.00f;
			mAccelCurrent = SensorManager.GRAVITY_EARTH;
			mAccelLast = SensorManager.GRAVITY_EARTH;
			
			sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

		} catch (Exception e) {
		}
	}

	///////////////////////////////////////////////////////////////////

	private void addRunnableCall() {
		try {
			timeoutDelay = Integer.parseInt(MainApplication.getContext().getString(R.string.mainscreentimeoutvalue))
					* 1000;

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

			guiHandler.removeCallbacks(guiRunnable);
			guiHandler.postDelayed(guiRunnable, guiDelay);

			/*
			 * // after all loaded , then load the area // as this cannot be
			 * placed in loop // so the lang cannot be reflect
			 * 
			 * 
			 * 
			 * 
			 * if ((GlobalValue.getInstance().getAllDictArray().size() == 0) ||
			 * MainApplication.getMAS().getData("data_roomtype") == "") { final
			 * Handler handler = new Handler(); handler.postDelayed(new
			 * Runnable() {
			 * 
			 * @Override public void run() { //handler.removeCallbacks(this);
			 * 
			 * 
			 * try { //Log.v(myLOG,"s:" +
			 * MainApplication.getMAS().getData("data_roomtype"));
			 * 
			 * 
			 * isSuite = Integer.parseInt(MainApplication.getMAS().getData(
			 * "data_roomtype") + "") - 1;
			 * 
			 * 
			 * } catch (Exception e) {Log.i(myLOG,e.toString());}
			 * 
			 * 
			 * 
			 * rcFootRoomArea_logic();
			 * 
			 * 
			 * 
			 * } }, 500);
			 * 
			 * return; }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeRunnableCall() {
		try {
			timeoutHandler.removeCallbacks(timeoutRunnable);
			
			
			guiHandler.removeCallbacks(guiRunnable);

		} catch (Exception e) {
		}
	}

	private void handleSleep() {
		try {

			timeoutHandler.removeCallbacks(timeoutRunnable);
			timeoutHandler.postDelayed(timeoutRunnable, timeoutDelay);

		} catch (Exception e) {
		}

	}

	private Runnable timeoutRunnable = new Runnable() {

		public void run() {
			try {
				timeoutHandler.removeCallbacks(timeoutRunnable);

				sleepBSP();

			} catch (Exception e) {
			}

		}
	};

	////////////////////////////////////////////////////////

	private Runnable loadLabelRunnable = new Runnable() {

		public void run() {
			try {
				// loadLabelHandler.removeCallbacks(loadLabelRunnable);

				loadRCLabel();

			} catch (Exception e) {
			}

		}
	};

	////////////////////////////////////////////////////////

	private void loadRCLabel() {
		try {
			// Log.i(myLOG, "loadRCLabel fire");


			try {
				if (GlobalValue.getInstance().getAllDictArray().size() == 0) {

					try {
						loadLabelHandler.removeCallbacks(loadLabelRunnable);
					} catch (Exception e) {
					}

					loadLabelHandler.postDelayed(loadLabelRunnable, loadLabelDelay);

					return;
				}
			} catch (Exception e) {
			}

			// change all label
			try {
				try {
					headRC.setText(MainApplication.getLabel("RC").toUpperCase());
				} catch (Exception e) {
				}

				try {
					headService.setText(MainApplication.getLabel("Services").toUpperCase());
				} catch (Exception e) {
				}

				try {
					headTV.setText(MainApplication.getLabel("TV").toUpperCase());
				} catch (Exception e) {
				}

				try {
					headRadio.setText(MainApplication.getLabel("Radio").toUpperCase());
				} catch (Exception e) {
				}

				try {
					headLang.setText(MainApplication.getLabel("Lang").toUpperCase());
				} catch (Exception e) {
				}

				try {
					headWeather.setText(MainApplication.getLabel("Weather").toUpperCase());
				} catch (Exception e) {
				}

				try {
					msgText.setText(MainApplication.getLabel("Messages"));
				} catch (Exception e) {
				}

				try {

					footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>"
							+ MainApplication.getMAS().getData("data_myroom")));
				} catch (Exception e) {
				}

			} catch (Exception e) {
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				if ((MainApplication.getMAS().getCurRoom() == Integer
						.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
						|| (MainApplication.getMAS().getCurRoom() == Integer
								.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

					try {
						rcMasterSwitchLabel.setText(MainApplication.getLabel("MasterSwitch"));
					} catch (Exception e) {
					}

				}

				try {
					rcBedroomLightsLabel.setText(MainApplication.getLabel("BedroomLight"));
				} catch (Exception e) {
				}

				try {
					// pch button
					rcAlarmLabel.setText(MainApplication.getLabel("SetAlarm"));
				} catch (Exception e) {
				}
			}

			try {
				rcBedroomTempLabel.setText(MainApplication.getLabel("Temperature"));
			} catch (Exception e) {
			}

			try {

				rcNightLightLabel.setText(MainApplication.getLabel("NightLight"));
			} catch (Exception e) {
			}

			try {
				rcCurtainsLabel.setText(MainApplication.getLabel("Curtain"));
			} catch (Exception e) {
			}

			try {
				rcFanLabel.setText(MainApplication.getLabel("Fan"));
			} catch (Exception e) {
			}

			try {
				rcVCLabel.setText(MainApplication.getLabel("VC"));
			} catch (Exception e) {
			}

			try {
				rcMURLabel.setText(MainApplication.getLabel("MUR"));
			} catch (Exception e) {
			}

			try {
				rcDNDLabel.setText(MainApplication.getLabel("DND"));
			} catch (Exception e) {
			}

			try {
				rcSetAlarmLabel.setText(MainApplication.getLabel("SetAlarm"));

				if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

					rcSnoozeLabel.setText(MainApplication.getLabel("Snooze"));
					rcSetHourLabel.setText(MainApplication.getLabel("SetHour"));
					rcSetMinuteLabel.setText(MainApplication.getLabel("SetMinute"));

				} else {

					rcBigOffLabel.setText(MainApplication.getLabel("AlarmOff").toUpperCase());

					// rcBigOffLabel.setText((MainApplication.getLabel("AlarmOff")).trim()
					// == "" ? "Alarm Off" :
					// MainApplication.getLabel("AlarmOff"));
					rcBigSnoozeLabel.setText(MainApplication.getLabel("Snooze").toUpperCase());
				}
			} catch (Exception e) {
			}

			try {

				// kkkkkk
				rcFootRoomArea_logic();

			} catch (Exception e) {
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/////////////////////////////////////////////////////////////////////////////

	private void wakeBSPLightOnly() {
		try {

			if (MainApplication.mWakeLock != null) {
				MainApplication.mWakeLock.release();
			}

			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getClass().getName());
			MainApplication.mWakeLock.acquire();

			unlockScreen();

			handleSleep();

			Window mWindow = MainApplication.getCurrentActivity().getWindow();
			WindowManager.LayoutParams lp = mWindow.getAttributes();
			lp.screenBrightness = (float) 1;
			mWindow.setAttributes(lp);

			try {

				devicePolicyManager.resetPassword("", 0);
			} catch (Exception e) {
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/////////////////////////////////////////////////////////

	private void unlockScreen() {
		try {

			Window window = MainApplication.getCurrentActivity().getWindow();

			window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
			window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
			window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		} catch (Exception e) {
		}

	}

	/////////////////////////////////////////////////////////

	@SuppressWarnings("deprecation")
	public void wakeBSP() {
		
		//MainApplication.setCurrentActivity(this);
		try {

			if (MainApplication.mWakeLock != null) {
				MainApplication.mWakeLock.release();
			}

			MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getClass().getName());
			MainApplication.mWakeLock.acquire();

			unlockScreen();

			Window mWindow = MainApplication.getCurrentActivity().getWindow();
			WindowManager.LayoutParams lp = mWindow.getAttributes();
			lp.screenBrightness = (float) 1;
			mWindow.setAttributes(lp);

			// new add to wake the panel - 20151102, janice request
/*			myCMD = "SET%20WAKEPANEL";
			MainApplication.getMAS().sendMASCmd(myCMD, 0);*/

			
			
			// try {
			//
			// devicePolicyManager.resetPassword("", 0);
			// } catch (Exception e) {}

			//
			// try {
			// MainApplication.getWakeLock().acquire();
			//
			// } catch (Exception e) {}

			MainApplication.setScreenSta(1);

			try {
				if (btnTouchWake == null) {
					btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
				}
				btnTouchWake.setVisibility(View.GONE);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// try {
			//
			// kcm = KnoxCustomManager.getInstance();
			//
			// kcm.setOverlayTransparency(0.0f); // add overlay
			// kcm.setLcdBacklightState(true); // turn on backlight
			//
			// Log.v(myLOG, "lcd:" + kcm.getLcdBacklightState() +"");
			//
			//
			// } catch (Exception e) {
			// Log.v(myLOG,"samsung:" + e.toString());
			// }
			//

			// refresh
			
			if (int_AlarmPop < 2) { // 2 is alarm 
				checkFootAreaInit = 0;
			}

			handleSleep();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sleepBSP() {
		try {

			MainApplication.setScreenSta(0);

			Log.v(myLOG, "************************");
			Log.v(myLOG, "sleeping fire");
			Log.v(myLOG, "************************");
			
			
			ActivityManager am = (ActivityManager)this.getSystemService(Context.ACTIVITY_SERVICE);

			int sizeStack =  am.getRunningTasks(2).size();

			for(int i = 0;i < sizeStack;i++){

			    ComponentName cn = am.getRunningTasks(2).get(i).topActivity;
			    Log.d(myLOG, cn.getClassName());
			}
			
			

			try {
	

				if(this.getString(R.string.hotel).equalsIgnoreCase("pch") || this.getString(R.string.hotel).equalsIgnoreCase("pbh")){
					devicePolicyManager.lockNow();
				}
				
				
				// samsung part

				try {

					if(this.getString(R.string.hotel).equalsIgnoreCase("pch")){
					
						kcm = KnoxCustomManager.getInstance();
						kcm.setWakeUpMode(true);
						
						Log.v(myLOG, "kcm true");
						
					}

				} catch (Exception e) {
					
					Log.v(myLOG, "kcm :" + e.toString());
				}					

			} catch (Exception e) {
			}

			 try {
				
			 //MainApplication.getWakeLock().release();
			
			} catch (Exception e) {}

			try {
				int_AlarmPop = 0;

				alarmPopup.setVisibility(View.GONE);

				showRCLayout();

			} catch (Exception e) {
			}

			try {
				if (btnTouchWake == null) {
					btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
				}

				btnTouchWake.setVisibility(View.VISIBLE);
				btnTouchWake.bringToFront();

			} catch (Exception e) {
			}

			Window mWindow = MainApplication.getCurrentActivity().getWindow();
			WindowManager.LayoutParams lp = mWindow.getAttributes();
			lp.screenBrightness = (float) myBrightness;
			mWindow.setAttributes(lp);

		} catch (Exception e) {
			Log.i(myLOG, "sleeping issue = " + e.toString());
		}
	}

	/////////////////////////////////////////////////////////////////////////////

	private void loadFindView() {

		// just do it
		try {
			try {

				headAlarmClock = null;

				rcSetHour = null;
				rcSetMinute = null;
				headAlarm = null;
				rcSetAlarm = null;
				rcSnooze = null;

				rcHeadBigClockSign = null;
				rcHeadBigClock = null;

				rcSetAlarmLabel = null;
				rcSnoozeLabel = null;
				rcSetHourLabel = null;
				rcSetMinuteLabel = null;

			} catch (Exception e) {
			}

			try {
				alarmPopup = null;
			} catch (Exception e) {
			}

			try {

				rcMasterSwitch = null;
				rcMasterSwitchLabel = null;
			} catch (Exception e) {
			}

			try {
				rcBedroomLightsLabel = null;
				rcBedroomLightsTop = null;
				rcBedroomLightsBase = null;
				rcBedroomLightsDisplay = null;

				rcBedroomLightsMinus = null;
				rcBedroomLightsPlus = null;

			} catch (Exception e) {
			}

			try {

				rcNightLightALL = null;
				rcNightLight = null;
				rcNightLightLabel = null;

			} catch (Exception e) {
			}

			try {
				rcCurtainsTop = null;
				rcCurtainsBase = null;
				rcCurtainsClose = null;
				rcCurtainsOpen = null;
				rcCurtainsLabel = null;

			} catch (Exception e) {
			}

			try {
				rcBedroomTempButton = null;
				rcBedroomTempType = null;
				rcBedroomTempMinus = null;
				rcBedroomTempPlus = null;
				rcBedroomTempLabel = null;
				rcBedroomTempDisplay = null;

			} catch (Exception e) {
			}

			try {
				rcFanTop = null;
				rcFanBase = null;
				rcFanDisplay = null;
				rcFanMinus = null;
				rcFanPlus = null;
				rcFanLabel = null;

			} catch (Exception e) {
			}
		} catch (Exception e) {
		}

		if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

			try {

				headAlarmClock = (TextView) findViewById(R.id.headAlarmClock);

				rcSetHour = (LinearLayout) findViewById(R.id.rcSetHour);
				rcSetMinute = (LinearLayout) findViewById(R.id.rcSetMinute);
				headAlarm = (ImageView) findViewById(R.id.headAlarm);
				rcSetAlarm = (LinearLayout) findViewById(R.id.rcSetAlarm);
				rcSnooze = (LinearLayout) findViewById(R.id.rcSnooze);

				rcSetAlarmLabel = (MyTextView) findViewById(R.id.rcSetAlarmLabel);
				rcSnoozeLabel = (MyTextView) findViewById(R.id.rcSnoozeLabel);
				rcSetHourLabel = (MyTextView) findViewById(R.id.rcSetHourLabel);
				rcSetMinuteLabel = (MyTextView) findViewById(R.id.rcSetMinuteLabel);

				/*
				 * rcHeadBigClockSign = (MyTextView)
				 * findViewById(R.id.headClockSign_alarmpage); rcHeadBigClock =
				 * (MyTextView) findViewById(R.id.headClock_alarmpage);
				 */

			} catch (Exception e) {
			}

			try {

				rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL);
				rcNightLight = (ImageView) findViewById(R.id.rcNightLight);
				rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel);

			} catch (Exception e) {
			}

			try {
				rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton);
				rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType);
				rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus);
				rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus);
				rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel);
				rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay);

			} catch (Exception e) {
			}

			try {

				rcFanTop = (ImageView) findViewById(R.id.rcFanTop);
				rcFanBase = (ImageView) findViewById(R.id.rcFanBase);
				rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay);
				rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus);
				rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus);
				rcFanLabel = (TextView) findViewById(R.id.rcFanLabel);

			} catch (Exception e) {
			}

		} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

			try {

				// headAlarmClock = (TextView)
				// findViewById(R.id.headAlarmClock_pch);

				// rcSetHour = (LinearLayout) findViewById(R.id.rcSetHour_pch);
				// rcSetMinute = (LinearLayout)
				// findViewById(R.id.rcSetMinute_pch);
				// headAlarm = (ImageView) findViewById(R.id.headAlarm_pch);
				rcSetAlarm = (LinearLayout) findViewById(R.id.rcSetAlarm_pch);

				rcBigSnooze = (RelativeLayout) findViewById(R.id.rcBigSnooze_pch);
				rcBigOff = (RelativeLayout) findViewById(R.id.rcBigOff_pch);

				// rcHeadBigClockSign = (MyTextView)
				// findViewById(R.id.headClockSign_alarmpage_pch);
				// rcHeadBigClock = (MyTextView)
				// findViewById(R.id.headClock_alarmpage_pch);

				rcSetAlarmLabel = (MyTextView) findViewById(R.id.rcSetAlarmLabel_pch);

				rcBigSnoozeLabel = (MyTextView) findViewById(R.id.rcBigSnoozeLabel_pch);
				rcBigOffLabel = (MyTextView) findViewById(R.id.rcBigOffLabel_pch);

				// rcSetHourLabel = (MyTextView)
				// findViewById(R.id.rcSetHourLabel_pch);
				// rcSetMinuteLabel = (MyTextView)
				// findViewById(R.id.rcSetMinuteLabel_pch);

			} catch (Exception e) {
			}

			try {
				alarmPopup = (SlidingPanel) findViewById(R.id.popup_window_pch);
				alarmPopupAlert = (SlidingPanel) findViewById(R.id.popupAlarmAlert_window_pch);

			} catch (Exception e) {
			}

			// if
			// (this.getString(R.string.hotelcurtain).equalsIgnoreCase("pchcurtain"))
			// {

			if ((settings.getBoolean("hasCurtain", false))
					|| this.getString(R.string.hotelcurtain).equalsIgnoreCase("okcurtain")) {

				if (curroompage == bedroompage) {

					try {
						rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch6);
						rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch6);
					} catch (Exception e) {
					}

					try {
						rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch6);
						rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch6);
						rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch6);
						rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch6);

						rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch6);
						rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch6);

					} catch (Exception e) {
					}

					try {

						rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch6);
						rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch6);
						rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch6);

					} catch (Exception e) {
					}

					try {
						rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch6);
						rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch6);
						rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch6);
						rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch6);
						rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch6);

					} catch (Exception e) {
					}

					try {
						rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch6);
						rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch6);
						rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch6);
						rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch6);
						rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch6);
						rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch6);

					} catch (Exception e) {
					}

					try {
						rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch6);
						rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch6);
						rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch6);
						rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch6);
						rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch6);
						rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch6);

					} catch (Exception e) {
					}

				} else {

					/*
					 * try { rcMasterSwitch = (ImageView)
					 * findViewById(R.id.rcMasterSwitch_pch4);
					 * rcMasterSwitchLabel = (TextView)
					 * findViewById(R.id.rcMasterSwitchLabel_pch4); } catch
					 * (Exception e) {}
					 */

					try {
						rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch4);
						rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch4);
						rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch4);
						rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch4);

						rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch4);
						rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch4);

					} catch (Exception e) {
					}

					/*
					 * try {
					 * 
					 * rcNightLightALL = (LinearLayout)
					 * findViewById(R.id.rcNightLightALL_pch4); rcNightLight =
					 * (ImageView) findViewById(R.id.rcNightLight_pch4);
					 * rcNightLightLabel = (TextView)
					 * findViewById(R.id.rcNightLightLabel_pch4);
					 * 
					 * } catch (Exception e) {}
					 */

					try {
						rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch4);
						rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch4);
						rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch4);
						rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch4);
						rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch4);

					} catch (Exception e) {
					}

					try {
						rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch4);
						rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch4);
						rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch4);
						rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch4);
						rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch4);
						rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch4);

					} catch (Exception e) {
					}

					try {
						rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch4);
						rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch4);
						rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch4);
						rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch4);
						rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch4);
						rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch4);

					} catch (Exception e) {
					}

				}

			} else {

				if (curroompage == bedroompage) {
					Log.v(myLOG, "bedroom create");

					try {
						rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch);
						rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch);
					} catch (Exception e) {

					}

					try {
						rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch);
						rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch);
						rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch);
						rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch);

						rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch);
						rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch);

					} catch (Exception e) {
					}

					try {

						rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch);
						rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch);
						rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch);

					} catch (Exception e) {
					}

					/*
					 * try { rcCurtainsTop = (ImageView)
					 * findViewById(R.id.rcCurtainsTop_pch); rcCurtainsBase =
					 * (ImageView) findViewById(R.id.rcCurtainsBase_pch);
					 * rcCurtainsClose = (ImageView)
					 * findViewById(R.id.rcCurtainsClose_pch); rcCurtainsLabel =
					 * (TextView) findViewById(R.id.rcCurtainsLabel_pch);
					 * 
					 * } catch (Exception e) {}
					 */

					try {
						rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch);
						rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch);
						rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch);
						rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch);
						rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch);
						rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch);

					} catch (Exception e) {
					}

					try {
						rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch);
						rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch);
						rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch);
						rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch);
						rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch);
						rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch);

					} catch (Exception e) {
					}

				} else {

					/*
					 * try { rcMasterSwitch = (ImageView)
					 * findViewById(R.id.rcMasterSwitch_pch3);
					 * rcMasterSwitchLabel = (TextView)
					 * findViewById(R.id.rcMasterSwitchLabel_pch3); } catch
					 * (Exception e) {}
					 */

					try {
						rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch3);
						rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch3);
						rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch3);
						rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch3);

						rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch3);
						rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch3);

					} catch (Exception e) {
					}

					/*
					 * try {
					 * 
					 * rcNightLightALL = (LinearLayout)
					 * findViewById(R.id.rcNightLightALL_pch3); rcNightLight =
					 * (ImageView) findViewById(R.id.rcNightLight_pch3);
					 * rcNightLightLabel = (TextView)
					 * findViewById(R.id.rcNightLightLabel_pch3);
					 * 
					 * } catch (Exception e) {}
					 */

					/*
					 * try { rcCurtainsTop = (ImageView)
					 * findViewById(R.id.rcCurtainsTop_pch3); rcCurtainsBase =
					 * (ImageView) findViewById(R.id.rcCurtainsBase_pch3);
					 * rcCurtainsClose = (ImageView)
					 * findViewById(R.id.rcCurtainsClose_pch3); rcCurtainsLabel
					 * = (TextView) findViewById(R.id.rcCurtainsLabel_pch3);
					 * 
					 * } catch (Exception e) {}
					 */

					try {
						rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch3);
						rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch3);
						rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch3);
						rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch3);
						rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch3);
						rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch3);

					} catch (Exception e) {
					}

					try {
						rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch3);
						rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch3);
						rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch3);
						rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch3);
						rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch3);
						rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch3);

					} catch (Exception e) {
					}

				}
			}

		}

	}

	//////////////////

	private void killAllLayout() {

		try {

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				//

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				pchpage_3.setVisibility(View.GONE);
				pchpage_4.setVisibility(View.GONE);
				pchpage_5.setVisibility(View.GONE);
				pchpage_6.setVisibility(View.GONE);
			}

		} catch (Exception e) {
		}
	}

	private void showRCLayout() {
		try {

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				//

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				if (int_AlarmPop == 0) {

					if ((settings.getBoolean("hasCurtain", false))
							|| this.getString(R.string.hotelcurtain).equalsIgnoreCase("okcurtain")) {

						if (curroompage == bedroompage) {
							pchpage_3.setVisibility(View.GONE);
							pchpage_4.setVisibility(View.GONE);
							pchpage_5.setVisibility(View.GONE);

							pchpage_6.setVisibility(View.VISIBLE);

						} else {

							pchpage_3.setVisibility(View.GONE);
							pchpage_6.setVisibility(View.GONE);
							pchpage_5.setVisibility(View.GONE);

							pchpage_4.setVisibility(View.VISIBLE);
						}

					} else {

						if (curroompage == bedroompage) {
							pchpage_3.setVisibility(View.GONE);
							pchpage_4.setVisibility(View.GONE);
							pchpage_6.setVisibility(View.GONE);

							pchpage_5.setVisibility(View.VISIBLE);

						} else {

							pchpage_4.setVisibility(View.GONE);
							pchpage_6.setVisibility(View.GONE);
							pchpage_5.setVisibility(View.GONE);

							pchpage_3.setVisibility(View.VISIBLE);
						}

					}

				}

			} else {
				
				pchpage_3.setVisibility(View.GONE);
				pchpage_4.setVisibility(View.GONE);
				pchpage_5.setVisibility(View.GONE);
				pchpage_6.setVisibility(View.GONE);
				
			}

			try {
				// just load whatever
				loadFindView();

			} catch (Exception e) {
			}

		} catch (Exception e) {
		}

	}

	private void loadRCItem() {

		try {

			// try {
			// vf = (ViewFlipper)findViewById(R.id.vf);
			// //
			// } catch (Exception e) {}
			//
			//
			//
			// vf.setDisplayedChild(myCurLayout);

			try {
				showRCLayout();

			} catch (Exception e) {
			}

			try {
				if (MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")) {

					prepareAlarmSelectionList();

				}
			} catch (Exception e) {
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
				// for alarm set message

				try {
					errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
					errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
					errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);

					errorMessageDialogCloseImage.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							switch (v.getId()) {
							case R.id.errorMessageDialogCloseImage:
								errorMessageDialogLayout.setVisibility(View.GONE);

								break;

							default:
								break;
							}
						}
					});

				} catch (Exception e) {
				}

			}

			msgImage = (ImageView) findViewById(R.id.msgImage);
			msgText = (MyTextView) findViewById(R.id.msgText);

			msgText.setText(MainApplication.getLabel("Messages"));

			loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
			loadingProgressBar.setVisibility(View.GONE);

			msgImage.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						String dateString = MainApplication.getMAS().getData("year") + "-"
								+ MainApplication.getMAS().getData("month") + "-"
								+ MainApplication.getMAS().getData("day") + " "
								+ MainApplication.getMAS().getData("hour") + ":"
								+ MainApplication.getMAS().getData("minute");
						Log.i(myLOG, "dateString = " + dateString);

						Context context = getApplicationContext();
						CharSequence text = "The store date = " + dateString;
						int duration = Toast.LENGTH_LONG;

						Toast toast = Toast.makeText(context, text, duration);
						// toast.show();

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						Date date = null;
						try {
							date = format.parse(dateString);
							System.out.println(date);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (date != null) {
							settings.edit().putLong("lastMessageVisitTime", date.getTime()).commit();

							Log.i(myLOG, "stored UpdateTime =" + date.getTime());
						}

						Intent intent = new Intent(MainApplication.getContext(), MessageActivity.class);

						intent.putExtra("titleId", "Messages");

						MainApplication.getCurrentActivity().startActivity(intent);

					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}
			});

			layoutRC = (LinearLayout) findViewById(R.id.rcpage);
			layoutRC.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						// handleSleep();

					} catch (Exception e) {
					}

					return false;
				}
			});

			/////////////////////////////////////////////////

			btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
			btnTouchWake.setVisibility(View.GONE);

			btnTouchWake.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						Log.v(myLOG, "Touch Wake");

						try {
							// MainApplication.getCurrentActivity().dispatchTouchEvent(arg1);
						} catch (Exception e) {
						}

						// you have touch
						wakeBSP();
						// handleSleep();

					} catch (Exception e) {
					}

					return false;
				}

			});

			//////////////////////////////////////////////////////////

			headRC = (MyTextView) findViewById(R.id.headRoomControl);
			headRC.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						if (MainApplication.getCurrentActivity().getString(R.string.hotel).equalsIgnoreCase("pch")) {
							int_AlarmPop = 0;

							alarmPopup.setVisibility(View.GONE);
							alarmPopupAlert.setVisibility(View.GONE);

							// refresh the gui update
							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelay);

							showRCLayout();

						}

						MainApplication.playClickSound(headRC);

					} catch (Exception e) {
					}

					return false;
				}

			});

			headService = (MyTextView) findViewById(R.id.headService);
			// headService.setVisibility(View.GONE); // tmp add by william (
			// confirm with andrew at 2014-11-29 )

			headService.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {
						
						Intent intent = new Intent(getApplicationContext(), ServiceMainActivity.class);
						startActivity(intent);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						
						MainApplication.getCurrentActivity().startActivity(
								intent);
						MainApplication.getCurrentActivity().finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			headTV = (MyTextView) findViewById(R.id.headTV);

			// for PBH, there is TV function
			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				headTV.setVisibility(View.GONE);
			}

			if (this.getString(R.string.hastvpage).equalsIgnoreCase("notv")) {
				headTV.setVisibility(View.GONE);
			}

			headTV.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						MainApplication.playClickSound(headTV);

						Intent intent = new Intent(getApplicationContext(), TVActivity.class);
						startActivity(intent);

					} catch (Exception e) {
						Log.i("RoomControlActivity", e.toString());
					}

					return false;
				}

			});

			headRadio = (MyTextView) findViewById(R.id.headRadio);
			headRadio.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						MainApplication.playClickSound(headRadio);

						Intent intent = new Intent(MainApplication.getContext(), RadioActivity.class);
						MainApplication.getCurrentActivity().startActivity(intent);

					} catch (Exception e) {
					}

					return false;
				}

			});

			headLang = (MyTextView) findViewById(R.id.headLang);
			headLang.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						MainApplication.playClickSound(headLang);

						Intent intent = new Intent(MainApplication.getContext(), LangActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						MainApplication.getCurrentActivity().startActivity(intent);
						RoomControlActivity.this.finish();

					} catch (Exception e) {
					}

					return false;
				}

			});

			/////////////////

			headWeather = (MyTextView) findViewById(R.id.headWeather);
			headWeather.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub

					try {

						MainApplication.playClickSound(headWeather);

						Intent intent = new Intent(MainApplication.getContext(), WeatherActivity.class);
						MainApplication.getCurrentActivity().startActivity(intent);

					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}

			});

			///////////////////////////////////////////////////////////

			if (footRoomArea == null) {

				footRoomArea = (Spinner) findViewById(R.id.footRoomArea);
			}
			// footRoomArea = (MyTextView) findViewById(R.id.footRoomArea);
			// footRoomArea.setText("");

			/*
			 * footRoomArea.setOnTouchListener( new OnTouchListener() {
			 * 
			 * @Override public boolean onTouch(View v, MotionEvent event) {
			 * 
			 * 
			 * 
			 * if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
			 * try { guiHandler.removeCallbacks(guiRunnable); } catch (Exception
			 * e) {}
			 * 
			 * MainApplication.playClickSound(footRoomArea);
			 * 
			 * } else if (event.getAction() ==
			 * android.view.MotionEvent.ACTION_UP) {
			 * 
			 * //handleSleep();
			 * 
			 * isBR = (isBR + 1) % 2;
			 * 
			 * //MainApplication.setCurArea((isBR == 0 ? 1 : 0));
			 * 
			 * MainApplication.getMAS().setCurRoom(isBR); switchLayout(isBR);
			 * 
			 * rcFootRoomArea_logic();
			 * 
			 * // make the ui will update once addRunnableCall();
			 * 
			 * }
			 * 
			 * return true; } } );
			 */

			//////////////////////////////////////////////////////

			try {

				/*
				 * rcSetAlarmLabel = (MyTextView)
				 * findViewById(R.id.rcSetAlarmLabel);
				 * rcSetAlarmLabel.setText("");
				 * 
				 * rcSnoozeLabel = (MyTextView)
				 * findViewById(R.id.rcSnoozeLabel); rcSnoozeLabel.setText("");
				 * 
				 * rcSetHourLabel = (MyTextView)
				 * findViewById(R.id.rcSetHourLabel);
				 * rcSetHourLabel.setText("");
				 * 
				 * rcSetMinuteLabel = (MyTextView)
				 * findViewById(R.id.rcSetMinuteLabel);
				 * rcSetMinuteLabel.setText("");
				 */

			} catch (Exception e) {
			}

			/////////////////////////////////////////////////////

			try {

				if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
					rcHeadClockSign = (MyTextView) findViewById(R.id.headClockSign);
				}
				if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
					rcHeadClockSign = (MyTextView) findViewById(R.id.headClockSign_pch);

				}
				rcHeadClockSign.setText("");

				rcHeadClock = (MyTextView) findViewById(R.id.headClock);
				if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
					rcHeadClock = (MyTextView) findViewById(R.id.headClock_pch);

				}
				rcHeadClock.setText("00:00");

				try {
					String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
					myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
					myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);

					if (myHeadClockHour == 0) {
						myHeadClockHour = 12;
						myHeadClockSign = "am";
					} else if (myHeadClockHour == 12) {
						myHeadClockHour = myHeadClockHour;
						myHeadClockSign = "pm";
					} else if (myHeadClockHour > 12) {
						myHeadClockHour = myHeadClockHour - 12;
						myHeadClockSign = "pm";
					} else {
						myHeadClockSign = "am";
					}

					// rcHeadClock.setText(com.hshgroup.bspcontrol.MainActivity.data_curtime);
					rcHeadClock.setText(MainApplication.twoDigitMe(myHeadClockHour) + ":"
							+ MainApplication.twoDigitMe(myHeadClockMin));
					rcHeadClockSign.setText(myHeadClockSign);

					if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						rcHeadBigClock.setText(MainApplication.twoDigitMe(myHeadClockHour) + ":"
								+ MainApplication.twoDigitMe(myHeadClockMin));
						rcHeadBigClockSign.setText(myHeadClockSign);

					}

				} catch (Exception e) {
					// Log.i("jam","error = " + e.toString());
				}

			} catch (Exception e) {
			}

			try {
				footRoom = (MyTextView) findViewById(R.id.footRoomNum);
				// footRoom.setText("ROOM");

			} catch (Exception e) {
			}

			try {

				// vc and dnd
				rcVCLabel = (TextView) findViewById(R.id.rcVCLabel);
				// rcVCLabel.setText("");

				rcVC = (LinearLayout) findViewById(R.id.rcVC);
				rcVC.setClickable(true);

				rcVC.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcVC.setPressed(true);
							MainApplication.playClickSound(rcVC);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOn", int_rcVC + "");
							FlurryAgent.logEvent("Valet", map);

							int_rcVC = (int_rcVC + 1) % 2;

							if (int_rcVC == 0) {
								myCMD = "SET%20VALET%20OFF";
							} else {
								myCMD = "SET%20VALET%20ON";
							}

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcVC_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							rcVC.setPressed(false);
						}

						return true;

					}

				});
			} catch (Exception e) {
			}

			try {
				rcMURLabel = (TextView) findViewById(R.id.rcMURLabel);
				// rcMURLabel.setText("");

				rcMUR = (LinearLayout) findViewById(R.id.rcMUR);
				rcMUR.setClickable(true);

				rcMUR.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcMUR.setPressed(true);
							MainApplication.playClickSound(rcMUR);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							int_rcMUR = (int_rcMUR + 1) % 2;

							if (int_rcMUR == 0) {
								myCMD = "SET%20MUR%20OFF";
							} else {
								myCMD = "SET%20MUR%20ON";
							}

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOn", int_rcMUR + "");

							FlurryAgent.logEvent("MakeUpRoom", map);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcMUR_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							rcMUR.setPressed(false);
						}

						return true;
					}
				});
			} catch (Exception e) {
			}

			try {
				rcDNDLabel = (TextView) findViewById(R.id.rcDNDLabel);
				// rcDNDLabel.setText("");

				rcDND = (LinearLayout) findViewById(R.id.rcDND);
				rcDND.setClickable(true);

				rcDND.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcDND.setPressed(true);
							MainApplication.playClickSound(rcDND);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							int_rcDND = (int_rcDND + 1) % 2;

							if (int_rcDND == 0) {
								myCMD = "SET%20DND%20OFF";
							} else {
								myCMD = "SET%20DND%20ON";
							}

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOn", int_rcDND + "");

							FlurryAgent.logEvent("PrivacyPlease", map);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcDND_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							rcDND.setPressed(false);
						}

						return true;
					}
				});

			} catch (Exception e) {
			}

			// --- room control ----------------

			// put the find view by id outside this loop

			////////////////////////////////////////

			try {

				// pch alarm
				if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

					// Hide the popup initially.....

					if (int_AlarmPop == 1) {

						alarmPopup.setVisibility(View.VISIBLE);
						alarmPopup.bringToFront();

						// alarmPopup.startAnimation( alarmPopAnimShow );
					} else if (int_AlarmPop == 0) {
						// alarmPopup.startAnimation( alarmPopAnimHide );
						int_AlarmPop = 0;

						alarmPopup.setVisibility(View.GONE);

					}

					// alarmPopup.setVisibility(View.GONE);

					rcAlarmLabel = (TextView) findViewById(R.id.rcAlarmLabel);
					// rcAlarmLabel.setText("");

					rcAlarm = (LinearLayout) findViewById(R.id.rcAlarmBottomBarButton);

					rcAlarm.setClickable(true);
					rcAlarm.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							guiHandler.removeCallbacks(guiRunnable);

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								// rcAlarm.setPressed(true);
								MainApplication.playClickSound(rcAlarm);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								// for Flurry log
								/*
								 * final Map<String, String> map = new
								 * HashMap<String, String>(); map.put("Room",
								 * MainApplication.getMAS().getData(
								 * "data_myroom")); map.put("alarm",
								 * "just whatever"); FlurryAgent.logEvent(
								 * "alarm pch button", map);
								 */

								int_AlarmPop = (int_AlarmPop + 1) % 2;

								if (int_AlarmPop == 1) {

									killAllLayout();

									try {

										offMessage();
									} catch (Exception e) {
									}

									// stop the gui changing during set
									guiHandler.removeCallbacks(guiRunnable);

									// just in case
									alarmPopupAlert.setVisibility(View.GONE);

									alarmPopup.setVisibility(View.VISIBLE);
									alarmPopup.bringToFront();

									// alarmPopup.startAnimation(
									// alarmPopAnimShow );
								} else {
									// alarmPopup.startAnimation(
									// alarmPopAnimHide );

									alarmPopup.setVisibility(View.GONE);

									guiHandler.removeCallbacks(guiRunnable);
									guiHandler.postDelayed(guiRunnable, guiDelayLong);

									showRCLayout();

								}

								// alarmPopup.setVisibility(View.GONE);

								handleSleep();

								// rcAlarm.setPressed(false);
							}

							return true;
						}
					});
				}

			} catch (Exception e) {
			}

			try {

				if ((MainApplication.getMAS().getCurRoom() == Integer
						.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
						|| (MainApplication.getMAS().getCurRoom() == Integer
								.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

					if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

						rcMasterSwitch.setClickable(true);
						rcMasterSwitch.setOnTouchListener(new OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {

								guiHandler.removeCallbacks(guiRunnable);

								if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

									rcMasterSwitch.setPressed(true);
									MainApplication.playClickSound(rcMasterSwitch);

								} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

									int_rcMasterSwitch = (int_rcMasterSwitch + 1) % 2;

									if (int_rcMasterSwitch == 0) {
										myCMD = "SET%20ROOMMASTERLIGHT%20OFF";
									} else {
										myCMD = "SET%20ROOMMASTERLIGHT%20ON";
									}
									guiHandler.removeCallbacks(guiRunnable);
									guiHandler.postDelayed(guiRunnable, guiDelayLong);

									// handleSleep();

									rcMasterSwitch_logic();

									MainApplication.getMAS().sendMASCmd(myCMD, 1);

									rcMasterSwitch.setPressed(false);
								}

								return true;
							}

						});

					}

				}

			} catch (Exception e) {
			}

			try {

				rcBedroomTempButton.setClickable(true);
				rcBedroomTempButton.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBedroomTempButton.setPressed(true);
							MainApplication.playClickSound(rcBedroomTempButton);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							if (int_rcFan > 0) {
								int_rcBedroomTempCF = (int_rcBedroomTempCF + 1) % 2;

								// for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS().getData("data_myroom"));
								map.put("changeToF", int_rcBedroomTempCF + "");

								FlurryAgent.logEvent("Temperature", map);

								if (int_rcBedroomTempCF == 0) {

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = f2c(ff_rcBedroomTemp);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									}

									myCMD = "SET%20TEMPCF%20C";
								} else {

									ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									}

									myCMD = "SET%20TEMPCF%20F";
								}
								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

							} else {
								if (int_rcFan == 0) {
									int_rcFan = 3;

									myCMD = "SET%20FAN%20HIGH";
								} else {
									int_rcFan = 0;

									myCMD = "SET%20FAN%20OFF";
								}

								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								// handleSleep();

								rcFan_logic();
							}

							rcBedroomTempCF_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcBedroomTempButton.setPressed(false);
						}

						return true;
					}

				});

				rcBedroomTempType.setClickable(true);
				rcBedroomTempType.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBedroomTempType.setPressed(true);
							MainApplication.playClickSound(rcBedroomTempType);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							if (int_rcFan > 0) {
								int_rcBedroomTempCF = (int_rcBedroomTempCF + 1) % 2;

								// for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS().getData("data_myroom"));
								map.put("changeToF", int_rcBedroomTempCF + "");

								FlurryAgent.logEvent("DegreeType", map);

								if (int_rcBedroomTempCF == 0) {

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = f2c(ff_rcBedroomTemp);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									}

									myCMD = "SET%20TEMPCF%20C";
								} else {

									ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									}

									myCMD = "SET%20TEMPCF%20F";
								}
								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

							}

							// handleSleep();

							rcBedroomTempCF_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcBedroomTempType.setPressed(false);
						}

						return true;
					}

				});

				rcBedroomTempMinus.setClickable(true);
				rcBedroomTempMinus.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBedroomTempMinus.setPressed(true);
							MainApplication.playClickSound(rcBedroomTempMinus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("TempMinus", map);

							if (int_rcFan > 0) {

								if (int_rcBedroomTempCF == 0) {
									// c
									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = (ff_rcBedroomTemp - 0.5f);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg - 1.0f);
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									}
								} else {

									// f
									// pch value is based on c to calculate

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = (ff_rcBedroomTemp - 1);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg - 1);
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;

										if (int_rcBedroomTempCF == 1) {
											ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

										}
									}

								}

								if (ff_rcBedroomTempOrg < ACMin) {
									ff_rcBedroomTempOrg = ACMin;

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;

										if (int_rcBedroomTempCF == 1) {
											ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

										}
									}
								}

								if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
									myCMD = "SET%20TEMPNEW%20" + (ff_rcBedroomTempOrg * 10);
								} else if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
									myCMD = "SET%20TEMP%20" + Math.round((ff_rcBedroomTempOrg));
								}

							} else {
								if (int_rcFan == 0) {
									int_rcFan = 3;
								} else {
									int_rcFan = 0;
								}

								myCMD = "SET%20FAN%20TOGGLE";

								rcFan_logic();
							}

							// handleSleep();

							rcBedroomTempCF_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcBedroomTempMinus.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}

				});

				rcBedroomTempPlus.setClickable(true);
				rcBedroomTempPlus.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBedroomTempPlus.setPressed(true);
							MainApplication.playClickSound(rcBedroomTempPlus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("TempPlus", map);

							if (int_rcFan > 0) {

								if (int_rcBedroomTempCF == 0) {
									// c
									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = (ff_rcBedroomTemp + 0.5f);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg + 1.0f);
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									}
								} else {

									// f
									// pch value is based on c to calculate

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
										ff_rcBedroomTemp = (ff_rcBedroomTemp + 1);
										ff_rcBedroomTempOrg = ff_rcBedroomTemp;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg + 1);
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;

										if (int_rcBedroomTempCF == 1) {
											ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

										}
									}

								}

								if (ff_rcBedroomTempOrg > ACMax) {
									ff_rcBedroomTempOrg = ACMax;

									if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

										ff_rcBedroomTemp = ff_rcBedroomTempOrg;
									} else if (RoomControlActivity.this.getString(R.string.hotel)
											.equalsIgnoreCase("pch")) {
										ff_rcBedroomTemp = ff_rcBedroomTempOrg;

										if (int_rcBedroomTempCF == 1) {
											ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

										}
									}

								}

								// data_temperature = ""+ff_rcBedroomTemp;

								if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
									myCMD = "SET%20TEMPNEW%20" + (ff_rcBedroomTempOrg * 10);
								} else if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
									myCMD = "SET%20TEMP%20" + Math.round((ff_rcBedroomTempOrg));
								}

							} else {
								if (int_rcFan == 0) {
									int_rcFan = 3;
								} else {
									int_rcFan = 0;
								}

								myCMD = "SET%20FAN%20TOGGLE";

								rcFan_logic();
							}

							// handleSleep();

							rcBedroomTempCF_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcBedroomTempPlus.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}

				});

				rcBedroomTempCF_logic();

			} catch (Exception e) {
				e.printStackTrace();
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				try {

					rcBedroomLightsTop.setClickable(true);
					rcBedroomLightsBase.setClickable(true);
					rcBedroomLightsDisplay.setActivated(true);

					rcBedroomLightsDisplayDrawable = (ClipDrawable) rcBedroomLightsDisplay.getDrawable();
					rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * int_rcBedroomLights); // ClipDrawable
																													// 0:
																													// blank,
																													// 10000:
																													// whole

					rcBedroomLightsTop.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								rcBedroomLightsTop.setPressed(true);
								rcBedroomLightsBase.setPressed(true);

								MainApplication.playClickSound(rcBedroomLightsTop);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								if (int_rcBedroomLights == 0) {
									int_rcBedroomLights = 5;
								} else {
									int_rcBedroomLights = 0;
								}

								myCMD = "SET%20ROOMLIGHT%20TOGGLE";

								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								rcBedroomLights_logic();

								MainApplication.getMAS().sendMASCmd(myCMD, 0);

								rcBedroomLightsTop.setPressed(false);
								rcBedroomLightsBase.setPressed(false);
							}

							return true;
						}

					});

					rcBedroomLightsBase.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								rcBedroomLightsTop.setPressed(true);
								rcBedroomLightsBase.setPressed(true);

								MainApplication.playClickSound(rcBedroomLightsTop);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								if (int_rcBedroomLights == 0) {
									int_rcBedroomLights = 5;
								} else {
									int_rcBedroomLights = 0;
								}

								myCMD = "SET%20ROOMLIGHT%20TOGGLE";

								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								rcBedroomLights_logic();

								MainApplication.getMAS().sendMASCmd(myCMD, 0);

								rcBedroomLightsTop.setPressed(false);
								rcBedroomLightsBase.setPressed(false);
							}

							return true;
						}

					});

					rcBedroomLightsMinus.setClickable(true);
					rcBedroomLightsMinus.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								rcBedroomLightsMinus.setPressed(true);
								MainApplication.playClickSound(rcBedroomLightsTop);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								int_rcBedroomLights = (int_rcBedroomLights - 1);
								if (int_rcBedroomLights < 0) {
									int_rcBedroomLights = 0;
								}

								// myCMD = "SET%20ROOMLIGHT%20DOWN";
								myCMD = "SET%20ROOMLIGHT%20" + int_rcBedroomLights;

								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								rcBedroomLights_logic();

								MainApplication.getMAS().sendMASCmd(myCMD, 0);

								rcBedroomLightsMinus.setPressed(false);
							}

							return true;
						}

					});

					rcBedroomLightsPlus.setClickable(true);
					rcBedroomLightsPlus.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								rcBedroomLightsPlus.setPressed(true);
								MainApplication.playClickSound(rcBedroomLightsTop);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								int_rcBedroomLights = (int_rcBedroomLights + 1);
								if (int_rcBedroomLights > 5) {
									int_rcBedroomLights = 5;
								}

								// myCMD = "SET%20ROOMLIGHT%20UP";
								myCMD = "SET%20ROOMLIGHT%20" + int_rcBedroomLights;

								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								rcBedroomLights_logic();

								MainApplication.getMAS().sendMASCmd(myCMD, 0);

								rcBedroomLightsPlus.setPressed(false);
							}

							return true;
						}

					});

				} catch (Exception e) {
					e.printStackTrace();
				}
				////////////////////////////////////////
			}

			try {

				if ((MainApplication.getMAS().getCurRoom() == Integer
						.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
						|| (MainApplication.getMAS().getCurRoom() == Integer
								.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

					rcNightLight.setClickable(true);

					rcNightLight.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							guiHandler.removeCallbacks(guiRunnable);

							if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

								rcNightLight.setPressed(true);
								MainApplication.playClickSound(rcNightLight);

							} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

								int_rcNightLight = (int_rcNightLight + 1) % 2;

								// for Flurry log
								final Map<String, String> map = new HashMap<String, String>();
								map.put("Room", MainApplication.getMAS().getData("data_myroom"));
								map.put("TurnOn", int_rcNightLight + "");

								FlurryAgent.logEvent("Nightlight", map);

								if (int_rcNightLight == 0) {
									myCMD = "SET%20NIGHTLIGHT%20OFF";
								} else {
									myCMD = "SET%20NIGHTLIGHT%20ON";
								}
								guiHandler.removeCallbacks(guiRunnable);
								guiHandler.postDelayed(guiRunnable, guiDelayLong);

								// handleSleep();

								rcNightLight_logic();

								MainApplication.getMAS().sendMASCmd(myCMD, 1);

								rcNightLight.setPressed(false);
							}

							return true;
						}

					});

				}

			} catch (Exception e) {
				Log.v(myLOG, "Nightlight:" + e.toString());
			}

			////////////////////////////////////////

			try {

				rcFanTop.setClickable(true);
				rcFanBase.setClickable(true);
				rcFanDisplay.setActivated(true);
				rcFanDisplayDrawable = (ClipDrawable) rcFanDisplay.getDrawable();
				rcFanDisplayDrawable.setLevel(fanspeedBase + levelStep * int_rcFan); // ClipDrawable
																						// 0:
																						// blank,
																						// 10000:
																						// whole

				rcFanTop.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcFanTop.setPressed(true);
							rcFanBase.setPressed(true);

							MainApplication.playClickSound(rcFanTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOff", int_rcFan + "");

							FlurryAgent.logEvent("FanSpeed", map);

							if (int_rcFan == 0) {
								int_rcFan = 3;

								myCMD = "SET%20FAN%20HIGH";
							} else {
								int_rcFan = 0;

								myCMD = "SET%20FAN%20OFF";
							}

							// handleSleep();

							rcFan_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcFanTop.setPressed(false);
							rcFanBase.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}

				});

				rcFanBase.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcFanTop.setPressed(true);
							rcFanBase.setPressed(true);

							MainApplication.playClickSound(rcFanTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							if (int_rcFan == 0) {
								int_rcFan = 3;

								myCMD = "SET%20FAN%20HIGH";
							} else {
								int_rcFan = 0;

								myCMD = "SET%20FAN%20OFF";
							}

							// handleSleep();

							rcFan_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcFanTop.setPressed(false);
							rcFanBase.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}

				});

				rcFanMinus.setClickable(true);
				rcFanMinus.setActivated(true);

				rcFanMinus.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcFanMinus.setPressed(true);

							MainApplication.playClickSound(rcFanMinus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("FanMinus", map);

							int_rcFan = int_rcFan - 1;
							if (int_rcFan < 0) {
								int_rcFan = 0;
							}

							if (int_rcFan == 3) {
								myCMD = "SET%20FAN%20HIGH";
							} else if (int_rcFan == 2) {
								myCMD = "SET%20FAN%20MID";
							} else if (int_rcFan == 1) {
								myCMD = "SET%20FAN%20LOW";
							} else if (int_rcFan == 0) {
								myCMD = "SET%20FAN%20OFF";
							}

							// handleSleep();

							rcFan_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcFanMinus.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}
				});

				rcFanPlus.setClickable(true);
				rcFanPlus.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcFanPlus.setPressed(true);

							MainApplication.playClickSound(rcFanPlus);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("FanPlus", map);

							int_rcFan = int_rcFan + 1;
							if (int_rcFan > 3) {
								int_rcFan = 3;
							}

							if (int_rcFan == 3) {
								myCMD = "SET%20FAN%20HIGH";
							} else if (int_rcFan == 2) {
								myCMD = "SET%20FAN%20MID";
							} else if (int_rcFan == 1) {
								myCMD = "SET%20FAN%20LOW";
							} else if (int_rcFan == 0) {
								myCMD = "SET%20FAN%20OFF";
							}

							// handleSleep();

							rcFan_logic();

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcFanPlus.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);
						}

						return true;
					}

				});

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

				rcCurtainsTop.setClickable(true);
				rcCurtainsBase.setClickable(true);

				rcCurtainsTop.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcCurtainsTop.setPressed(true);
							rcCurtainsBase.setPressed(true);

							MainApplication.playClickSound(rcCurtainsTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							int_rcCurtains = (int_rcCurtains + 1) % 2;

							myCMD = "SET%20CURTAIN%20TOGGLE";

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcCurtains_logic();

						}

						return true;
					}

				});

				rcCurtainsBase.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcCurtainsTop.setPressed(true);
							rcCurtainsBase.setPressed(true);

							MainApplication.playClickSound(rcCurtainsTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							int_rcCurtains = (int_rcCurtains + 1) % 2;

							myCMD = "SET%20CURTAIN%20TOGGLE";

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcCurtains_logic();

						}

						return true;
					}

				});

				rcCurtainsClose.setClickable(true);
				rcCurtainsClose.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcCurtainsClose.setPressed(true);

							MainApplication.playClickSound(rcCurtainsTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// int_rcCurtains = (int_rcCurtains + 1) % 2;

							myCMD = "SET%20CURTAIN%20CLOSE";

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcCurtains_logic();

							rcCurtainsClose.setPressed(false);

						}

						return true;
					}

				});

				rcCurtainsOpen.setClickable(true);
				rcCurtainsOpen.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcCurtainsOpen.setPressed(true);

							MainApplication.playClickSound(rcCurtainsTop);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							// int_rcCurtains = (int_rcCurtains + 1) % 2;

							myCMD = "SET%20CURTAIN%20OPEN";

							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							rcCurtains_logic();

							rcCurtainsOpen.setPressed(false);

						}

						return true;
					}

				});

			} catch (Exception e) {
			}

			try {
				alarmSetting();
			} catch (Exception e) {
			}

			////////////////////////////////////////

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	////////////////////////////////////////////////////////

	public void rcVC_logic() {
		try {

			if (int_rcVC == 0) {
				// rcVCLabel.setTextColor(getResources().getColor(R.color.lightgrey));
				rcVCLabel.setTextColor(getResources().getColor(R.color.white));
				rcVC.setActivated(false);

				int_rcVC = 0;
			} else {
				rcVCLabel.setTextColor(getResources().getColor(R.color.white));
				rcDNDLabel.setTextColor(getResources().getColor(R.color.white));

				rcVC.setActivated(true);

				int_rcDND = 0;
				rcDND.setActivated(false);
			}

		} catch (Exception e) {
		}
	}

	public void rcMUR_logic() {
		try {

			if (int_rcMUR == 0) {
				// rcVCLabel.setTextColor(getResources().getColor(R.color.lightgrey));
				rcMURLabel.setTextColor(getResources().getColor(R.color.white));
				rcMUR.setActivated(false);

				int_rcMUR = 0;
			} else {
				rcMURLabel.setTextColor(getResources().getColor(R.color.white));
				rcDNDLabel.setTextColor(getResources().getColor(R.color.white));

				rcMUR.setActivated(true);

				int_rcDND = 0;
				rcDND.setActivated(false);
			}

		} catch (Exception e) {
		}
	}

	public void rcDND_logic() {
		try {

			if (int_rcDND == 0) {
				rcDNDLabel.setTextColor(getResources().getColor(R.color.white));
				rcDND.setActivated(false);

				int_rcDND = 0;
			} else {
				rcDNDLabel.setTextColor(getResources().getColor(R.color.white));
				rcVCLabel.setTextColor(getResources().getColor(R.color.white));
				rcMURLabel.setTextColor(getResources().getColor(R.color.white));

				rcDND.setActivated(true);

				int_rcVC = 0;
				rcVC.setActivated(false);

				int_rcMUR = 0;
				rcMUR.setActivated(false);
			}

		} catch (Exception e) {
		}
	}

	public void rcMasterSwitch_logic() {
		try {

			if ((MainApplication.getMAS().getCurRoom() == Integer
					.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
					|| (MainApplication.getMAS().getCurRoom() == Integer
							.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

				if (int_rcMasterSwitch == 0) {
					// rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));
					// rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));

					rcMasterSwitch.setActivated(false);

					try {

						rcBedroomLightsTop.setActivated(false);
						rcBedroomLightsBase.setActivated(false);

					} catch (Exception e) {
					}

					int_rcBedroomLights = 0;
				} else {
					// rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));
					// rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));

					try {

						rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));

					} catch (Exception e) {
					}

					rcMasterSwitch.setActivated(true);

					try {
						rcBedroomLightsTop.setActivated(true);
						rcBedroomLightsBase.setActivated(true);
					} catch (Exception e) {
					}

					int_rcBedroomLights = 5;

					int_rcNightLight = 0;

					try {
						rcNightLight.setActivated(false);
					} catch (Exception e) {
					}
				}

				try {
					rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * int_rcBedroomLights); // ClipDrawable
																													// 0:
																													// blank,
																													// 10000:
																													// whole
				} catch (Exception e) {
				}

			}

		} catch (Exception e) {
		}
	}

	public void rcBedroomLights_logic() {
		try {

			if (int_rcBedroomLights == 0) {
				// rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));
				// rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));

				rcBedroomLightsTop.setActivated(false);
				rcBedroomLightsBase.setActivated(false);

				try {
					int_rcMasterSwitch = 0;
					rcMasterSwitch.setActivated(false);
				} catch (Exception e) {
				}

			} else {
				// rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));
				// rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));

				try {
					rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));
				} catch (Exception e) {
				}

				rcBedroomLightsTop.setActivated(true);
				rcBedroomLightsBase.setActivated(true);

				if ((MainApplication.getMAS().getCurRoom() == Integer
						.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
						|| (MainApplication.getMAS().getCurRoom() == Integer
								.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

					try {
						int_rcMasterSwitch = 1;
						rcMasterSwitch.setActivated(true);
					} catch (Exception e) {
					}

					try {
						int_rcNightLight = 0;
						rcNightLight.setActivated(false);
					} catch (Exception e) {
					}

				}
			}

			try {
				rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * int_rcBedroomLights); // ClipDrawable
																												// 0:
																												// blank,
																												// 10000:
																												// whole
			} catch (Exception e) {
			}

		} catch (Exception e) {
		}
	}

	public void rcNightLight_logic() {

		try {

			if ((MainApplication.getMAS().getCurRoom() == Integer
					.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
					|| (MainApplication.getMAS().getCurRoom() == Integer
							.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

				rcNightLightALL.setVisibility(View.VISIBLE);
			} else {

				rcNightLightALL.setVisibility(View.GONE);
			}

			if ((MainApplication.getMAS().getCurRoom() == Integer
					.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
					|| (MainApplication.getMAS().getCurRoom() == Integer
							.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

				try {

					if (int_rcNightLight == 0) {
						rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));

						rcNightLight.setActivated(false);
					} else {
						rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));

						if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

							try {
								rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));
							} catch (Exception e) {
							}

							try {
								rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));
							} catch (Exception e) {
							}
						}
						rcNightLight.setActivated(true);

						if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

							try {

								rcBedroomLightsTop.setActivated(false);
								rcBedroomLightsBase.setActivated(false);

							} catch (Exception e) {
							}

							int_rcMasterSwitch = 0;
							int_rcBedroomLights = 0;

							try {
								rcMasterSwitch.setActivated(false);
							} catch (Exception e) {
							}
						}
					}

					try {
						rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * int_rcBedroomLights); // ClipDrawable
																														// 0:
																														// blank,
																														// 10000:
																														// whole
					} catch (Exception e) {
					}

				} catch (Exception e) {
				}

			}

		} catch (Exception e) {
		}

	}

	//////////////////////////////////////////////////////////////////////////

	public float c2f(float c) {
		float f = 0f;

		// pch
		if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
			f = 0;
			f = Math.round((int) (c * 9 / 5)) + 32;
			return f;
		}

		if (c == 15) {
			f = 59.0f;
		} else if (c == 15.5) {
			f = 60.0f;
		} else if (c == 16) {
			f = 61.0f;
		} else if (c == 16.5) {
			f = 62.0f;
		} else if (c == 17) {
			f = 63.0f;
		} else if (c == 17.5) {
			f = 64.0f;
		} else if (c == 18) {
			f = 64.0f;
		} else if (c == 18.5) {
			f = 65.0f;
		} else if (c == 19) {
			f = 66.0f;
		} else if (c == 19.5) {
			f = 67.0f;
		} else if (c == 20) {
			f = 68.0f;
		} else if (c == 20.5) {
			f = 69.0f;
		} else if (c == 21) {
			f = 70.0f;
		} else if (c == 21.5) {
			f = 71.0f;
		} else if (c == 22) {
			f = 72.0f;
		} else if (c == 22.5) {
			f = 73.0f;
		} else if (c == 23) {
			f = 73.0f;
		} else if (c == 23.5) {
			f = 74.0f;
		} else if (c == 24) {
			f = 75.0f;
		} else if (c == 24.5) {
			f = 76.0f;
		} else if (c == 25) {
			f = 77.0f;
		} else if (c == 25.5) {
			f = 78.0f;
		} else if (c == 26) {
			f = 79.0f;
		} else if (c == 26.5) {
			f = 80.0f;
		} else if (c == 27) {
			f = 81.0f;
		} else if (c == 27.5) {
			f = 82.0f;
		} else if (c == 28) {
			f = 82.0f;
		}

		return f;
	}

	public float f2c(float f) {
		float c = 0;

		if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
			// pch only have c value

			// c = Math.round((f - 32) * 5/9);
			c = Math.round((int) ((f - 32) * 5 / 9));
			return c;
		}

		if (f == 59) {
			c = 15.0f;
		} else if (f == 60) {
			c = 15.5f;
		} else if (f == 61) {
			c = 16.0f;
		} else if (f == 62) {
			c = 17.0f;
		} else if (f == 63) {
			c = 17.5f;
		} else if (f == 64) {
			c = 18.0f;
		} else if (f == 65) {
			c = 18.5f;
		} else if (f == 66) {
			c = 19.0f;
		} else if (f == 67) {
			c = 19.5f;
		} else if (f == 68) {
			c = 20.0f;
		} else if (f == 69) {
			c = 20.5f;
		} else if (f == 70) {
			c = 21.0f;
		} else if (f == 71) {
			c = 21.5f;
		} else if (f == 72) {
			c = 22.0f;
		} else if (f == 73) {
			c = 23.0f;
		} else if (f == 74) {
			c = 23.5f;
		} else if (f == 75) {
			c = 24.0f;
		} else if (f == 76) {
			c = 24.5f;
		} else if (f == 77) {
			c = 25.0f;
		} else if (f == 78) {
			c = 25.5f;
		} else if (f == 79) {
			c = 26.0f;
		} else if (f == 80) {
			c = 26.5f;
		} else if (f == 81) {
			c = 27.0f;
		} else if (f == 82) {
			c = 28.0f;
		}

		return c;

	}

	public void rcBedroomTempCF_logic() {
		// Log.i(myLOG, "rcBedroomTempCF_logic fire");
		try {

			if (int_rcFan > 0) {

				rcBedroomTempLabel.setTextColor(getResources().getColor(R.color.white));

				rcBedroomTempButton.setActivated(true);

				if (int_rcBedroomTempCF == 0) {
					// c
					if (RoomControlActivity.this.getString(R.string.hotelac).equalsIgnoreCase("pbhsp")) {
						ACMin = 15;
					} else {
						ACMin = 18;
					}

					ACMax = 28;

					ff_rcBedroomTempValue = ff_rcBedroomTemp;

					if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
						rcBedroomTempDisplay.setText(Html.fromHtml("" + ff_rcBedroomTempValue + "&#176;C"));
					} else if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						rcBedroomTempDisplay.setText(Html.fromHtml("" + Math.round(ff_rcBedroomTempValue) + "&#176;C"));
					}

					if (ff_rcBedroomTemp <= 20) {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.blue));
					} else if (ff_rcBedroomTemp > 25) {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.red));
					} else {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.white));
					}

				} else {
					// f

					if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
						if (RoomControlActivity.this.getString(R.string.hotelac).equalsIgnoreCase("pbhsp")) {
							ACMin = 59;
						} else {
							ACMin = 64;
						}

						ACMax = 82;
					} else if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						ACMin = 18;
						ACMax = 28;
					}

					// int_rcBedroomTempValue = Math.round(int_rcBedroomTemp *
					// 9/5) + 32;
					ff_rcBedroomTempValue = ff_rcBedroomTemp;

					if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
						rcBedroomTempDisplay.setText(Html.fromHtml("" + ff_rcBedroomTempValue + "&#176;F"));
					} else if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						rcBedroomTempDisplay.setText(Html.fromHtml("" + Math.round(ff_rcBedroomTempValue) + "&#176;F"));
					}

					if (ff_rcBedroomTemp <= 68) {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.blue));
					} else if (ff_rcBedroomTemp > 77) {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.red));
					} else {

						rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.white));
					}

				}
			} else {
				rcBedroomTempLabel.setTextColor(getResources().getColor(R.color.white));

				rcBedroomTempButton.setActivated(false);
				rcBedroomTempDisplay.setText(Html.fromHtml(""));
			}

		} catch (Exception e) {
		}

	}

	public void rcCurtains_logic() {

		try {

			rcCurtainsTop.setActivated(true);
			rcCurtainsBase.setActivated(true);

			rcCurtainsTop.setPressed(false);
			rcCurtainsBase.setPressed(false);

			Handler tmp = new Handler();
			tmp.postDelayed(new Runnable() {
				public void run() {
					try {
						// rcCurtainsLabel.setTextColor(getResources().getColor(R.color.white));

						rcCurtainsTop.setActivated(false);
						rcCurtainsBase.setActivated(false);

					} catch (Exception e) {
					}
				}
			}, 700);

		} catch (Exception e) {
		}

	}

	/////////////////////////////////////////////////////////////////////
	
	
	

	public void switchLayout(int pos) {

		try {

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
				//

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				if (pos == Integer.parseInt(MainApplication.getContext().getString(R.string.mas_base))) {

					curroompage = bedroompage;

				} else if (pos == Integer.parseInt(MainApplication.getContext().getString(R.string.maslr_base))) {

					curroompage = otherroompage;
					
				} else if (pos == Integer.parseInt(MainApplication.getContext().getString(R.string.massr_base))) {

					curroompage = otherroompage;						

				} else if (pos == Integer.parseInt(MainApplication.getContext().getString(R.string.massb_base))) {

					curroompage = bedroompage;

				} else if (pos == Integer.parseInt(MainApplication.getContext().getString(R.string.masdr_base))) {

					curroompage = otherroompage;

				}

			}

			// Log.v(myLOG,"cur page: " + curroompage);

			loadRCItem();
			// showRCLayout();

		} catch (Exception e) {

			// Log.v(myLOG,"switch layout: " + e.toString());
		}

	}
	

	private void checkRoomArea(String zone) {

		try {
			Log.v(myLOG, "area:" + zone);

			if (zone.equalsIgnoreCase(MainApplication.getLabel("Bedroom Control"))) {
				Log.v(myLOG, "br");

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.mas_base)));
			} else if (zone.equalsIgnoreCase(MainApplication.getLabel("Living Room Control"))) {
				Log.v(myLOG, "lr");

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.maslr_base)));
			} else if (zone.equalsIgnoreCase(MainApplication.getLabel("Study Room Control"))) {

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.massr_base)));				
				
			} else if (zone.equalsIgnoreCase(MainApplication.getLabel("Second Bedroom Control"))) {

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.massb_base)));
			} else if (zone.equalsIgnoreCase(MainApplication.getLabel("Dining Room Control"))) {

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.masdr_base)));
			} else {
				Log.v(myLOG, "others");

				MainApplication.getMAS()
						.setCurRoom(Integer.parseInt(MainApplication.getContext().getString(R.string.mas_base)));
			}

		} catch (Exception e) {
		}

	}

	private void createFootRoomArea() {

		try {

			List<String> list = new ArrayList<String>();

			switch (isSuite) {
			case 0:
				break;
			case 1:
				list.add(MainApplication.getLabel("Bedroom Control"));
				list.add(MainApplication.getLabel("Living Room Control"));
				break;
			case 2:
				list.add(MainApplication.getLabel("Bedroom Control"));
				list.add(MainApplication.getLabel("Living Room Control"));
				list.add(MainApplication.getLabel("Dining Room Control"));
				break;
			case 3:
				list.add(MainApplication.getLabel("Bedroom Control"));
				list.add(MainApplication.getLabel("Second Bedroom Control"));
				list.add(MainApplication.getLabel("Living Room Control"));
				list.add(MainApplication.getLabel("Dining Room Control"));
				break;
				
			case 11: // PBJ add 10 
				list.add(MainApplication.getLabel("Bedroom Control"));
				list.add(MainApplication.getLabel("Second Bedroom Control"));
				list.add(MainApplication.getLabel("Living Room Control"));
				list.add(MainApplication.getLabel("Study Room Control"));
				
				break;
				
			default:
				break;
			}

			if (isSuite == 0) {
				footRoomArea.setVisibility(View.GONE);

			} else {
				footRoomArea.setVisibility(View.VISIBLE);

				try {

					if (dataAdapter != null) {
						dataAdapter.clear();
					}

				} catch (Exception e) {
				}

				dataAdapter = new MySpinnerAdapter(this, android.R.layout.simple_spinner_item, list);
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

				footRoomArea.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

						if (checkFootAreaInit > 1) {
							
							if (int_AlarmPop < 2) {
								Log.v(myLOG, "SELECT:" + parent.getSelectedItem().toString());
	
								checkRoomArea(parent.getSelectedItem().toString());
								MainApplication.getMAS().setSelectedArea(pos);
	
								switchLayout(MainApplication.getMAS().getCurRoom());
							
							}

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelay);

						} else {
							checkFootAreaInit = 2;

						}

						// MainApplication.getMAS().setCurRoom(pos);
						// switchLayout(pos);

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}

				});

				footRoomArea.setAdapter(dataAdapter);
				dataAdapter.notifyDataSetChanged();

				// footRoomArea.setSelection(MainApplication.getMAS().getCurRoom());
			}

		} catch (Exception e) {
		}

	}

	public void rcFootRoomArea_logic() {
		try {

			try {
				if (MainApplication.getScreenSta() == 1) { // on screen

					if (checkFootAreaInit < 1) {
						checkFootAreaInit = checkFootAreaInit + 1;

						createFootRoomArea();

						footRoomArea.setSelection(MainApplication.getMAS().getSelectedArea());
						switchLayout(MainApplication.getMAS().getCurRoom());
					}

				}

				// footRoomArea.setSelection(MainApplication.getMAS().getSelectedArea());

				// Log.v(myLOG, "suite num:" + isSuite);
				// footRoomArea.setVisibility(View.GONE);

				/*
				 * if (isSuite == 0) { footRoomArea.setVisibility(View.GONE);
				 * 
				 * } else { footRoomArea.setVisibility(View.VISIBLE);
				 * 
				 * List<String> list = new ArrayList<String>();
				 * list.add(MainApplication.getLabel("Bedroom Control"));
				 * list.add(MainApplication.getLabel("Living Room Control"));
				 * 
				 * 
				 * MySpinnerAdapter dataAdapter = new MySpinnerAdapter(this,
				 * android.R.layout.simple_spinner_item,list);
				 * 
				 * dataAdapter.setDropDownViewResource(android.R.layout.
				 * simple_spinner_dropdown_item);
				 * 
				 * footRoomArea.setOnItemSelectedListener(new
				 * OnItemSelectedListener() {
				 * 
				 * @Override public void onItemSelected(AdapterView<?> parent,
				 * View view, int pos,long id) {
				 * 
				 * if (pos == 0) { isBR = 1; } else { isBR = 0; }
				 * 
				 * MainApplication.setCurArea(pos); switchLayout(pos);
				 * 
				 * }
				 * 
				 * @Override public void onNothingSelected(AdapterView<?>
				 * parent) {
				 * 
				 * }
				 * 
				 * });
				 * 
				 * footRoomArea.setAdapter(dataAdapter);
				 * footRoomArea.setSelection(MainApplication.getCurArea()); }
				 */

				if (isSuite == 0) {

					footRoomArea.setVisibility(View.GONE);

				} else {
					footRoomArea.setVisibility(View.VISIBLE);

				}

			} catch (Exception e) {
			}

			try {

				/*
				 * if (isBR == 1) {
				 * footRoomArea.setText(MainApplication.getLabel(
				 * "Bedroom Control"));
				 * 
				 * } else { footRoomArea.setText(MainApplication.getLabel(
				 * "Living Room Control"));
				 * 
				 * }
				 */

			} catch (Exception e) {
			}

			rcNightLight_logic();

		} catch (Exception e) {
		}

	}

	public void rcFan_logic() {

		try {

			if (int_rcFan == 0) {
				rcFanLabel.setTextColor(getResources().getColor(R.color.white));

				rcFanTop.setActivated(false);
				rcFanBase.setActivated(false);
			} else {
				rcFanLabel.setTextColor(getResources().getColor(R.color.white));

				rcFanTop.setActivated(true);
				rcFanBase.setActivated(true);
			}

			rcFanDisplayDrawable.setLevel(fanspeedBase + levelStep * int_rcFan); // ClipDrawable
																					// 0:
																					// blank,
																					// 10000:
																					// whole

			rcBedroomTempCF_logic();

		} catch (Exception e) {
		}
	}

	public void rcAlarmGUI_logic() {
		try {

			try {

				if (int_rcSetAlarm == 0) {

					if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
						headAlarm.setVisibility(View.INVISIBLE);
					}

					rcSetAlarm.setActivated(false);

					if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						rcAlarm.setPressed(false); // bottom bar button

						alarmPopupAlert.setVisibility(View.GONE);

						int_AlarmPop = 0;
					}

				} else {
					if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {
						headAlarm.setVisibility(View.VISIBLE);
					}

					rcSetAlarm.setActivated(true);

					if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
						rcAlarm.setPressed(true);
					}

				}

			} catch (Exception e) {
			}

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

				if (int_rcSetHour == 0) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "a";
				} else if (int_rcSetHour == 12) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "p";
				} else if (int_rcSetHour > 12) {
					myHeadAlarmHour = int_rcSetHour - 12;
					myHeadAlarmSign = "p";
				} else {
					myHeadAlarmHour = int_rcSetHour;
					myHeadAlarmSign = "a";
				}

				int_rcSetHourMAS = int_rcSetHour;

				headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
						+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);

			} else if (this.getString(R.string.hotel).equalsIgnoreCase("pch")) {

				try {
					if (int_rcSetHourMAS == 0) {
						int_rcSetHour = 12;
						apmList.setValue(0);

					} else if (int_rcSetHourMAS < 12) {
						apmList.setValue(0);
					} else if (int_rcSetHourMAS == 12) {
						apmList.setValue(1);
					} else {
						int_rcSetHour = int_rcSetHourMAS - 12;
						apmList.setValue(1);
					}

					hrList.setValue(int_rcSetHour);
					minList.setValue(int_rcSetMinute);

				} catch (Exception e) {
				}

			}

		} catch (Exception e) {

			// Log.v(myLOG,"alarm:" + e.toString());
		}

	}

	////////////////////////////////////////////////////////

	@SuppressLint("NewApi")
	private Runnable guiRunnable = new Runnable() {

		public void run() {
			try {

				guiHandler.removeCallbacks(guiRunnable);

				// Log.v(myLOG,"GUI start");

				try {
					// Log.v(myLOG,"suite:" +
					// MainApplication.getMAS().getData("data_roomtype"));
					isSuite = Integer.parseInt(MainApplication.getMAS().getData("data_roomtype") + "") - 1;

					/*
					 * if (isSuite !=
					 * Integer.parseInt(MainApplication.getMAS().getData(
					 * "data_roomtype") + "") - 1) { isSuite =
					 * Integer.parseInt(MainApplication.getMAS().getData(
					 * "data_roomtype") + "") - 1;
					 * 
					 * rcFootRoomArea_logic();
					 * 
					 * }
					 */

				} catch (Exception e) {
					Log.i(myLOG, e.toString());
				}

				try {
					// battery
					ImageView imgBB = (ImageView) findViewById(R.id.headBattery);
					imgBB.setImageDrawable((getResources().getDrawable(getResources().getIdentifier(
							"drawable/battery_" + MainApplication.getBatteryChecker().getBatteryLevel(), "drawable",
							getPackageName()))));
					
					Button fakeLanButton = (Button) findViewById(R.id.fakeLanButton);
					
					fakeLanButton.setLongClickable(true);
					fakeLanButton.setOnLongClickListener(new OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {

							Log.i(myLOG, "fakeLanButton clicked");
							
							Intent intent = new Intent(MainApplication.getContext(),
									LangActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							MainApplication.getCurrentActivity().startActivity(intent);
							
							RoomControlActivity.this.finish();	
							return false;
						}	        	
			        });

				} catch (Exception e) {
					Log.i(myLOG, "battery error" + e.toString());
					e.printStackTrace();
				}

				try {

					if (MainApplication.getBatteryChecker().getProximityFire() == 1) {
						wakeBSP();

						Log.v(myLOG, "proximity");
					}

					if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {

						//wakeBSPLightOnly();
						wakeBSP();

						if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
							// if (int_AlarmPop == 0) {
							int_AlarmPop = 2;

							killAllLayout();

							if (alarmPopup != null) {
								alarmPopup.setVisibility(View.GONE);
							}

							alarmPopupAlert.setVisibility(View.VISIBLE);
							alarmPopupAlert.bringToFront();

							// alarmPopup.startAnimation( alarmPopAnimShow );
							// }
							// alarmPopup.startAnimation( alarmPopAnimShow );
						}

						// int_rcSetAlarm = 1;

						Log.v(myLOG, "alarm wake");

					} else {

						// after alarm wake to snooze
						if (int_AlarmPop == 2) {
							int_AlarmPop = 0;

							showRCLayout();
						}

					}

				} catch (Exception e) {
				}

				// all gui change
				try {
					try {
						String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
						myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
						myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);

						if (myHeadClockHour == 0) {
							myHeadClockHour = 12;
							myHeadClockSign = "am";
						} else if (myHeadClockHour == 12) {
							myHeadClockHour = myHeadClockHour;
							myHeadClockSign = "pm";
						} else if (myHeadClockHour > 12) {
							myHeadClockHour = myHeadClockHour - 12;
							myHeadClockSign = "pm";
						} else {
							myHeadClockSign = "am";
						}

						rcHeadClock.setText(Html.fromHtml("<font>" + MainApplication.twoDigitMe(myHeadClockHour) + ":"
								+ MainApplication.twoDigitMe(myHeadClockMin) + "</font><small>" + myHeadClockSign
								+ "</small>"));

						rcHeadClock.setText(MainApplication.twoDigitMe(myHeadClockHour) + ":"
								+ MainApplication.twoDigitMe(myHeadClockMin));
						rcHeadClockSign.setText(myHeadClockSign);

						// if(RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")){
						// rcHeadBigClock.setText(MainApplication.twoDigitMe(myHeadClockHour)
						// + ":" + MainApplication.twoDigitMe(myHeadClockMin));
						// rcHeadBigClockSign.setText(myHeadClockSign);
						//
						//
						//
						// }
						//

					} catch (Exception e) {
						Log.i(myLOG, e.toString());
					}

				} catch (Exception e) {
					Log.i(myLOG, e.toString());
				}

				try {

					try {
						if (MainApplication.getMAS().getData("data_valet").equalsIgnoreCase("ON")) {
							int_rcVC = 1;
						} else {
							int_rcVC = 0;
						}
						rcVC_logic();
					} catch (Exception e) {
					}

					try {
						if (MainApplication.getMAS().getData("data_mur").equalsIgnoreCase("ON")) {
							int_rcMUR = 1;
						} else {
							int_rcMUR = 0;
						}
						rcMUR_logic();
					} catch (Exception e) {
					}

					try {
						if (MainApplication.getMAS().getData("data_dnd").equalsIgnoreCase("ON")) {
							int_rcDND = 1;
						} else {
							int_rcDND = 0;
						}
						rcDND_logic();
					} catch (Exception e) {
					}

					try {
						if ((MainApplication.getMAS().getCurRoom() == Integer
								.parseInt(MainApplication.getContext().getString(R.string.mas_base)))
								|| (MainApplication.getMAS().getCurRoom() == Integer
										.parseInt(MainApplication.getContext().getString(R.string.massb_base)))) {

							if (MainApplication.getMAS().getData("data_nightlight").equalsIgnoreCase("ON")) {
								int_rcNightLight = 1;
							} else {
								int_rcNightLight = 0;
							}
							rcNightLight_logic();

						}

					} catch (Exception e) {
					}

					try {
						// pch
						if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
							int_rcBedroomLights = Integer.parseInt(MainApplication.getMAS().getData("data_roomlight"));
							rcBedroomLights_logic();
						}
					} catch (Exception e) {
					}

					try {
						// pch
						// int_rcBedroomTemp =
						// Integer.parseInt(MainApplication.getMAS().getData("data_temperature"));
						// if(RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")){
						ff_rcBedroomTempOrg = Float.parseFloat(MainApplication.getMAS().getData("data_temperature"));

						// just use to calc
						// pch is always use c value

						ff_rcBedroomTemp = ff_rcBedroomTempOrg;

						// }
					} catch (Exception e) {
						// Log.i("RoomControl", "error 3 = " + e.toString());
					}

					try {

						if (MainApplication.getMAS().getData("data_temperature_cf").equalsIgnoreCase("C")) {
							int_rcBedroomTempCF = 0;
						} else {
							int_rcBedroomTempCF = 1;

							if (RoomControlActivity.this.getString(R.string.hotel).equalsIgnoreCase("pch")) {
								ff_rcBedroomTemp = c2f(ff_rcBedroomTempOrg);

							}
						}
						rcBedroomTempCF_logic();

					} catch (Exception e) {
					}

					try {
						if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("HIGH")) {
							int_rcFan = 3;
						} else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("MID")) {
							int_rcFan = 2;
						} else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("LOW")) {
							int_rcFan = 1;
						} else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("OFF")) {
							int_rcFan = 0;
						}
						rcFan_logic();
					} catch (Exception e) {
					}

					//////////// alarm
					try {

						if (MainApplication.getMAS().getData("data_alarm").equalsIgnoreCase("ON")) {
							int_rcSetAlarm = 1;

							int_rcSetHourMAS = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_hr"));
							int_rcSetMinuteMAS = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_min"));

							int_rcSetHour = int_rcSetHourMAS;
							int_rcSetMinute = int_rcSetMinuteMAS;

							int_rcSnooze = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_snooze"));

						} else {
							int_rcSetAlarm = 0;

							// int_rcSetHour = 6;
							// int_rcSetMinute = 0;

							int_rcSnooze = 5;

						}
						rcAlarmGUI_logic();

					} catch (Exception e) {
					}

					// loadRCLabel();

					/*
					 * if (myCurLang !=
					 * MainApplication.getMAS().getData("data_language")) {
					 * myCurLang =
					 * MainApplication.getMAS().getData("data_language");
					 * 
					 * loadRCLabel();
					 * 
					 * // for the zone select rcFootRoomArea_logic(); } else {
					 * 
					 * // still load once loadRCLabel(); }
					 */

					if (checkCurLang != MainApplication.getMAS().getData("data_language")) {

						checkCurLang = MainApplication.getMAS().getData("data_language");

						checkFootAreaInit = 0; // try to refresh
					}

					loadRCLabel();

					// sleepBSP();
				} catch (Exception e) {
					// Log.i("RoomControl", "error 4 = " + e.toString());
				}

				/////////////////////////////////////////////////////////////////

				// Log.v(myLOG,"GUI end");

				guiHandler.removeCallbacks(guiRunnable);
				guiHandler.postDelayed(guiRunnable, guiDelay);

			} catch (Exception e) {
				// Log.i("RoomControl", "error 5 = " + e.toString());
			}
		}
	};

	//////////////////////////////////////////////////////////

	public void alarmSetting() {

		try {

			if (this.getString(R.string.hotel).equalsIgnoreCase("pbh")) {

				rcAlarm = (LinearLayout) findViewById(R.id.rcAlarmBottomBarButton);
				rcAlarm.setVisibility(View.GONE);

				// headAlarmClock.setText(MainApplication.twoDigitMe(int_rcSetHour)
				// + ":" + MainApplication.twoDigitMe(int_rcSetMinute));

				if (int_rcSetHour == 0) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "a";
				} else if (int_rcSetHour == 12) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "p";
				} else if (int_rcSetHour > 12) {
					myHeadAlarmHour = int_rcSetHour - 12;
					myHeadAlarmSign = "p";
				} else {
					myHeadAlarmHour = int_rcSetHour;
					myHeadAlarmSign = "a";
				}

				int_rcSetHourMAS = int_rcSetHour;

				headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
						+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);

				rcSetHour.setClickable(true);
				rcSetHour.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {
							guiHandler.removeCallbacks(guiRunnable);

							autoHHHandler.removeCallbacks(autoHHRunnable);
							autoHHHandler.postDelayed(autoHHRunnable, autoHHDelay);

						} catch (Exception e) {
						}

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcSetHour.setActivated(true);
							rcSetHour.setPressed(true);

							MainApplication.playClickSound(rcSetHour);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							autoHHHandler.removeCallbacks(autoHHRunnable);

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							FlurryAgent.logEvent("Hours", map);

							guiHandler.removeCallbacks(guiRunnable);

							if (autoSetAlarmDoMe == 0) {
								int_rcSetHour = (int_rcSetHour + 1);
								if (int_rcSetHour >= 24) {
									int_rcSetHour = 0;
								}

								if (int_rcSetHour == 0) {
									myHeadAlarmHour = 12;
									myHeadAlarmSign = "a";
								} else if (int_rcSetHour == 12) {
									myHeadAlarmHour = 12;
									myHeadAlarmSign = "p";
								} else if (int_rcSetHour > 12) {
									myHeadAlarmHour = int_rcSetHour - 12;
									myHeadAlarmSign = "p";
								} else {
									myHeadAlarmHour = int_rcSetHour;
									myHeadAlarmSign = "a";
								}

								int_rcSetHourMAS = int_rcSetHour;

								headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);
							}
							autoSetAlarmDoMe = 0;

							rcSetHour.setActivated(false);
							rcSetHour.setPressed(false);

							int_rcSetAlarm = 1;

							if (int_rcSetAlarm == 0) {

								myCMD = "SET%20ALARM%20OFF";
							} else {
								myCMD = "SET%20ALARM%20ON%20" + MainApplication.twoDigitMe(int_rcSetHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute);
							}

							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							// handleSleep();

							rcAlarmGUI_logic();
						}

						return true;
					}
				});

				rcSetMinute.setClickable(true);
				rcSetMinute.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {
							guiHandler.removeCallbacks(guiRunnable);

							autoMMHandler.removeCallbacks(autoMMRunnable);
							autoMMHandler.postDelayed(autoMMRunnable, autoMMDelay);

						} catch (Exception e) {
						}

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcSetMinute.setActivated(true);
							rcSetMinute.setPressed(true);

							MainApplication.playClickSound(rcSetMinute);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							autoMMHandler.removeCallbacks(autoMMRunnable);

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							FlurryAgent.logEvent("Mintues", map);

							if (autoSetAlarmDoMe == 0) {
								int_rcSetMinute = (int_rcSetMinute + 1);
								if (int_rcSetMinute >= 60) {
									int_rcSetMinute = 0;
								}

								headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);

							}
							autoSetAlarmDoMe = 0;

							rcSetMinute.setActivated(false);
							rcSetMinute.setPressed(false);

							int_rcSetAlarm = 1;

							if (int_rcSetAlarm == 0) {

								myCMD = "SET%20ALARM%20OFF";
							} else {
								myCMD = "SET%20ALARM%20ON%20" + MainApplication.twoDigitMe(int_rcSetHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute);
							}

							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							// handleSleep();

							rcAlarmGUI_logic();
						}

						return true;
					}
				});

				rcSetAlarm.setClickable(true);
				rcSetAlarm.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcSetAlarm.setActivated(true);
							rcSetAlarm.setPressed(true);

							MainApplication.playClickSound(rcSetAlarm);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							rcSetAlarm.setActivated(false);
							rcSetAlarm.setPressed(false);

							int_rcSetAlarm = (int_rcSetAlarm + 1) % 2;

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOn", int_rcSetAlarm + "");
							FlurryAgent.logEvent("EnableAlarm", map);

							guiHandler.removeCallbacks(guiRunnable);

							if (int_rcSetAlarm == 0) {
								myCMD = "SET%20ALARM%20OFF";

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

							} else {
								myCMD = "SET%20ALARM%20ON%20" + MainApplication.twoDigitMe(int_rcSetHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute);
							}

							Log.v(myLOG, myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcAlarmGUI_logic();
						}

						return true;
					}
				});

				rcSnooze.setClickable(true);
				rcSnooze.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcSnooze.setActivated(true);
							rcSnooze.setPressed(true);

							MainApplication.playClickSound(rcSnooze);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							rcSnooze.setActivated(false);
							rcSnooze.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("Snooze", map);

							if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

								String[] snoozeParts = MainApplication.getMAS().getData("data_alarm_settime")
										.split(":");
								int snoozeHour = Integer.parseInt(snoozeParts[0]);
								int snoozeMinute = Integer.parseInt(snoozeParts[1])
										+ Integer.parseInt(MainApplication.getMAS().getData("data_alarm_snooze"));

								if (snoozeMinute > 59) {
									snoozeMinute = snoozeMinute - 60;

									snoozeHour = snoozeHour + 1;
									if (snoozeHour > 23) {
										snoozeHour = 0;
									}
								}

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

								myCMD = "SET%20ALARMMARK%20" + MainApplication.twoDigitMe(snoozeHour) + ":"
										+ MainApplication.twoDigitMe(snoozeMinute);

								MainApplication.getMAS().sendMASCmd(myCMD, 1);

							}

							// handleSleep();

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

						}

						return true;
					}
				});

			} else {

				//// PCH alarm

				rcAlarm = (LinearLayout) findViewById(R.id.rcAlarmBottomBarButton);
				rcAlarm.setClickable(true);

				rcSetAlarm.setClickable(true);
				rcSetAlarm.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcSetAlarm.setActivated(true);
							rcSetAlarm.setPressed(true);

							MainApplication.playClickSound(rcSetAlarm);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							rcSetAlarm.setActivated(false);
							rcSetAlarm.setPressed(false);

							int_rcSetAlarm = (int_rcSetAlarm + 1) % 2;

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));
							map.put("TurnOn", int_rcSetAlarm + "");
							FlurryAgent.logEvent("EnableAlarm", map);

							guiHandler.removeCallbacks(guiRunnable);

							if (int_rcSetAlarm == 0) {
								showMessage(MainApplication.getLabel("alarm.thankyou.cancel"));

								myCMD = "SET%20ALARM%20OFF";

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

							} else {

								if (apmList.getValue() == 0) {
									if (hrList.getValue() == 12) {
										int_rcSetHour = 0;
									} else {
										int_rcSetHour = hrList.getValue();
									}
								} else {
									if (hrList.getValue() == 12) {
										int_rcSetHour = 12;
									} else {
										int_rcSetHour = hrList.getValue() + 12;
									}
								}

								int_rcSetHourMAS = int_rcSetHour;
								int_rcSetMinute = minList.getValue();
								int_rcSetMinuteMAS = int_rcSetMinute;

								showMessage(MainApplication.getLabel("alarm.thankyou.msg"));

								myCMD = "SET%20ALARM%20ON%20" + MainApplication.twoDigitMe(int_rcSetHour) + ":"
										+ MainApplication.twoDigitMe(int_rcSetMinute);
							}

							Log.v(myLOG, myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcAlarmGUI_logic();
						}

						return true;
					}
				});

				/// for the big button press

				rcBigOff.setClickable(true);
				rcBigOff.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBigOff.setActivated(true);
							rcBigOff.setPressed(true);

							MainApplication.playClickSound(rcBigOff);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							rcBigOff.setActivated(false);
							rcBigOff.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);

							int_rcSetAlarm = 0;
							myCMD = "SET%20ALARM%20OFF";

							try {
								MainApplication.getAlarmChecker().stopAlarm();

							} catch (Exception e) {
							}

							Log.v(myLOG, myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 1);

							//
							// MainApplication.getMAS().sendMASCmd(myCMD,0);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// handleSleep();

							rcAlarmGUI_logic();

							// resume the rc
							int_AlarmPop = 0;
							alarmPopupAlert.setVisibility(View.GONE);

							showRCLayout();
						}

						return true;
					}
				});

				rcBigSnooze.setClickable(true);
				rcBigSnooze.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						guiHandler.removeCallbacks(guiRunnable);

						if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

							rcBigSnooze.setActivated(true);
							rcBigSnooze.setPressed(true);

							MainApplication.playClickSound(rcBigSnooze);

						} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

							rcBigSnooze.setActivated(false);
							rcBigSnooze.setPressed(false);

							guiHandler.removeCallbacks(guiRunnable);

							// for Flurry log
							final Map<String, String> map = new HashMap<String, String>();
							map.put("Room", MainApplication.getMAS().getData("data_myroom"));

							FlurryAgent.logEvent("Snooze", map);

							if (MainApplication.getAlarmChecker().getAlarmStatus() == 1) {

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

								String[] snoozeParts = MainApplication.getMAS().getData("data_alarm_settime")
										.split(":");
								int snoozeHour = Integer.parseInt(snoozeParts[0]);
								int snoozeMinute = Integer.parseInt(snoozeParts[1])
										+ Integer.parseInt(MainApplication.getMAS().getData("data_alarm_snooze"));

								if (snoozeMinute > 59) {
									snoozeMinute = snoozeMinute - 60;

									snoozeHour = snoozeHour + 1;
									if (snoozeHour > 23) {
										snoozeHour = 0;
									}
								}

								try {
									MainApplication.getAlarmChecker().stopAlarm();

								} catch (Exception e) {
								}

								myCMD = "SET%20ALARMMARK%20" + MainApplication.twoDigitMe(snoozeHour) + ":"
										+ MainApplication.twoDigitMe(snoozeMinute);

								MainApplication.getMAS().sendMASCmd(myCMD, 1);

								//
								// MainApplication.getMAS().sendMASCmd(myCMD,0);

							}

							// handleSleep();

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

							// resume the rc
							int_AlarmPop = 0;
							alarmPopupAlert.setVisibility(View.GONE);

							showRCLayout();

						}

						return true;
					}
				});
			}

		} catch (Exception e) {
			// Log.i("Room Control","error 2 = " + e.toString());
		}

	}

	// for pch alarm msgbox use
	private void showMessage(String msg) {
		try {

			errorMessageDialogLayout.setVisibility(View.VISIBLE);
			errorMessageDialogLabel.setText(msg);
		} catch (Exception e) {
		}
	}

	private void offMessage() {
		try {
			errorMessageDialogLayout.setVisibility(View.GONE);

		} catch (Exception e) {
		}

	}

	/////////////////////////////////////////////////////////////////

	// PCH version alarm

	private void prepareAlarmSelectionList() {

		List<String> hrArray;
		List<String> minArray;
		List<String> apmArray;

		String[] strHrArray;
		String[] strMinArray;
		String[] strApmArray;

		try {

			hrList = (MyNumberPicker) this.findViewById(R.id.hrList);

			hrList.setMinValue(1);
			hrList.setMaxValue(12);
			hrList.setWrapSelectorWheel(false);

			hrList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			hrList.setScaleX(2f);
			hrList.setScaleY(2f);
			hrList.setFormatter(new TwoDigitFormatter());

			hrList.setShowDividers(0);

			// hrList.setValue(int_rcSetHour);

			hrList.setOnValueChangedListener(new OnValueChangeListener() {

				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					// TODO Auto-generated method stub

					try {
						// guiHandler.removeCallbacks(guiRunnable);

						if (apmList.getValue() == 0) {
							if (newVal == 12) {
								int_rcSetHour = 0;
							} else {
								int_rcSetHour = newVal;
							}
						} else {
							if (newVal == 12) {
								int_rcSetHour = 12;
							} else {
								int_rcSetHour = newVal + 12;
							}
						}

						int_rcSetHourMAS = int_rcSetHour;

						// int_rcSetAlarm = 1;
						int_rcSetMinute = minList.getValue();
						int_rcSetMinuteMAS = int_rcSetMinute;

						String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
								+ MainApplication.twoDigitMe(int_rcSetMinute);

						if (int_rcSetAlarm == 1) {
							myCMD = "SET%20ALARM%20ON%20" + myAlarm;

							// Log.v(myLOG,myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

						}

						// guiHandler.postDelayed(guiRunnable, guiDelayLong);

						rcAlarmGUI_logic();

					} catch (Exception e) {
					}
				}
			});

			minList = (MyNumberPicker) this.findViewById(R.id.minList);
			minList.setMinValue(0);
			minList.setMaxValue(59);
			minList.setWrapSelectorWheel(false);

			minList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			minList.setScaleX(2f);
			minList.setScaleY(2f);
			minList.setFormatter(new TwoDigitFormatter());

			minList.setShowDividers(0);

			// minList.setValue(int_rcSetMinute);

			minList.setOnValueChangedListener(new OnValueChangeListener() {

				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					// TODO Auto-generated method stub

					try {
						// guiHandler.removeCallbacks(guiRunnable);

						if (apmList.getValue() == 0) {
							if (hrList.getValue() == 12) {
								int_rcSetHour = 0;
							} else {
								int_rcSetHour = hrList.getValue();
							}
						} else {
							if (hrList.getValue() == 12) {
								int_rcSetHour = 12;
							} else {
								int_rcSetHour = hrList.getValue() + 12;
							}
						}

						int_rcSetHourMAS = int_rcSetHour;

						// int_rcSetAlarm = 1;
						int_rcSetMinute = newVal;
						int_rcSetMinuteMAS = int_rcSetMinute;

						String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
								+ MainApplication.twoDigitMe(int_rcSetMinute);

						if (int_rcSetAlarm == 1) {
							myCMD = "SET%20ALARM%20ON%20" + myAlarm;

							// Log.v(myLOG,myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

						}

						// guiHandler.postDelayed(guiRunnable, guiDelayLong);

						rcAlarmGUI_logic();

					} catch (Exception e) {
					}
				}
			});

			apmList = (MyNumberPicker) this.findViewById(R.id.apmList);
			apmList.setMinValue(0);
			apmList.setMaxValue(1);
			apmList.setDisplayedValues(new String[] { "AM", "PM" });

			apmList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			apmList.setScaleX(2f);
			apmList.setScaleY(2f);

			apmList.setOnValueChangedListener(new OnValueChangeListener() {

				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					// TODO Auto-generated method stub

					try {
						// guiHandler.removeCallbacks(guiRunnable);

						if (newVal == 0) {
							if (hrList.getValue() == 12) {
								int_rcSetHour = 0;
							} else {
								int_rcSetHour = hrList.getValue();
							}
						} else {
							if (hrList.getValue() == 12) {
								int_rcSetHour = 12;
							} else {
								int_rcSetHour = hrList.getValue() + 12;
							}
						}

						int_rcSetHourMAS = int_rcSetHour;

						// int_rcSetAlarm = 1;
						int_rcSetMinute = minList.getValue();
						int_rcSetMinuteMAS = int_rcSetMinute;

						String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
								+ MainApplication.twoDigitMe(int_rcSetMinute);

						if (int_rcSetAlarm == 1) {
							myCMD = "SET%20ALARM%20ON%20" + myAlarm;

							// Log.v(myLOG,myCMD);
							MainApplication.getMAS().sendMASCmd(myCMD, 0);

							guiHandler.removeCallbacks(guiRunnable);
							guiHandler.postDelayed(guiRunnable, guiDelayLong);

						}

						// guiHandler.postDelayed(guiRunnable, guiDelayLong);

						rcAlarmGUI_logic();

					} catch (Exception e) {
					}
				}
			});

			if (int_rcSetHourMAS == 0) {
				int_rcSetHour = 12;
				apmList.setValue(0);

			} else if (int_rcSetHourMAS < 12) {
				apmList.setValue(0);
			} else if (int_rcSetHourMAS == 12) {
				apmList.setValue(1);
			} else {
				int_rcSetHour = int_rcSetHourMAS - 12;
				apmList.setValue(1);
			}

			hrList.setValue(int_rcSetHour);
			minList.setValue(int_rcSetMinute);

		} catch (Exception e) {
		}

	}

	/////////////////////////////////////////////////////////////////

	// PBH version alarm

	private Runnable autoHHRunnable = new Runnable() {

		public void run() {
			try {

				// Log.v("aaaa","cpu here");

				autoHHHandler.removeCallbacks(autoHHRunnable);

				MainApplication.playClickSound(rcSetHour);

				autoSetAlarmDoMe = 0;
				int_rcSetHour = (int_rcSetHour + 1);
				if (int_rcSetHour >= 24) {
					int_rcSetHour = 0;
				}

				if (int_rcSetHour == 0) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "a";
				} else if (int_rcSetHour == 12) {
					myHeadAlarmHour = 12;
					myHeadAlarmSign = "p";
				} else if (int_rcSetHour > 12) {
					myHeadAlarmHour = int_rcSetHour - 12;
					myHeadAlarmSign = "p";
				} else {
					myHeadAlarmHour = int_rcSetHour;
					myHeadAlarmSign = "a";
				}

				int_rcSetHourMAS = int_rcSetHour;

				headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
						+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);
				autoSetAlarmDoMe = 1;

				autoHHHandler.removeCallbacks(autoHHRunnable);
				autoHHHandler.postDelayed(autoHHRunnable, autoHHDelay);

				// handleSleep();

			} catch (Exception e) {
			}
		}
	};

	private Runnable autoMMRunnable = new Runnable() {

		public void run() {
			try {

				autoMMHandler.removeCallbacks(autoMMRunnable);

				MainApplication.playClickSound(rcSetMinute);

				autoSetAlarmDoMe = 0;
				int_rcSetMinute = (int_rcSetMinute + 1);
				if (int_rcSetMinute >= 60) {
					int_rcSetMinute = 0;
				}

				headAlarmClock.setText(MainApplication.twoDigitMe(myHeadAlarmHour) + ":"
						+ MainApplication.twoDigitMe(int_rcSetMinute) + myHeadAlarmSign);
				autoSetAlarmDoMe = 1;

				autoMMHandler.removeCallbacks(autoMMRunnable);
				autoMMHandler.postDelayed(autoMMRunnable, autoMMDelay);

				// handleSleep();

			} catch (Exception e) {
			}
		}
	};

	//////////////////////////////////////////////////////

	public void showLoading() {
		Log.i(myLOG, "showLoading fire");
		loadingProgressBar.setVisibility(View.VISIBLE);
		// loadingProgressBar.bringToFront();
		btnTouchWake.bringToFront();
	}

	public void hideLoading() {
		Log.i(myLOG, "hideLoading fire");
		loadingProgressBar.setVisibility(View.GONE);

		// for the case that no internet, we have to show "no interest" after we
		// got the dictionary from local file
		Helper.showInternetWarningToast();
		
		if(MainApplication.getLabel("Services").equalsIgnoreCase("")){
			// force reload the MainApplication
			
			Log.i("RoomControl","ForceReload start");
			DataCacheManager.getInstance().forceReload();
		}
		
	}

	//////////////////////////////////////////////////////////

	
//	private void clearReferences() {
//		try {
//			Activity currActivity = MainApplication.getCurrentActivity();
//
//			if (currActivity != null && currActivity.equals(this)) {
//				MainApplication.setCurrentActivity(null);
//			}
//		} catch (Exception e) {
//		}
//	}

	
	/////////////////////////////////////////////////////////
	/// FOR G-Sensor
	/////////////////////////////////////////////////////////

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub

		try {
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				mGravity = event.values.clone();
				// Shake detection
				float x = mGravity[0];
				float y = mGravity[1];
				float z = mGravity[2];
				mAccelLast = mAccelCurrent;
				mAccelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
				float delta = mAccelCurrent - mAccelLast;

				mAccel = mAccel * 0.9f + delta;
				// Make this higher or lower according to how much
				// motion you want to detect
				if (mAccel > 1.75) { // default is 3

					// seems move
					wakeBSP();

					Log.v(myLOG, "G Fire");
				}
			}

		} catch (Exception e) {
		}

	}

}
