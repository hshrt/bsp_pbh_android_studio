package com.hsh.esdbsp.ui;

import android.widget.NumberPicker.Formatter;




public class TwoDigitFormatter implements Formatter { 
	
	@Override
	public String format(int value) { 
		return String.format("%02d", value); 
	} 
}