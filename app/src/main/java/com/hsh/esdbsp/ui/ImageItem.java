package com.hsh.esdbsp.ui;

import com.hsh.esdbsp.MainApplication;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.ImageView;


public class ImageItem {
	private int image;
	private String imageStr;
	
	private String title;
	private String cmd;

	public ImageItem(int image, String title, String cmd) {
		super();
		this.image = image;
		this.title = title;
		this.cmd = cmd;
	}	
	
	public ImageItem(String image, String title, String cmd) {
		super();
		this.imageStr = image;
		this.title = title;
		this.cmd = cmd;
	}	
	
	public String getImageStr() {
		return imageStr;
	}

	public int getImage() {
		return image;
	}

	public String getTitle() {
		return title;
	}
	
	public String getCmd() {
		return cmd;
	}	

	public void setTitle(String title) {
		this.title = title;
	}
}