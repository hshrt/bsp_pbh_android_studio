package com.hsh.esdbsp.ui;

import com.hsh.esdbsp.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class MyTextView extends TextView {


    public MyTextView(Context context) {
      super(context);
      
      
      
      Typeface face=Typeface.createFromAsset(context.getAssets(), "minion.otf"); 
      this.setTypeface(face); 
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
        String typefaceName = a.getString( R.styleable.MyTextView_typeface);
        
        if(typefaceName != null) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), String.format("%s.otf", typefaceName));
            this.setTypeface(tf);
        } else {
        	Typeface tf=Typeface.createFromAsset(context.getAssets(), "minion.otf"); 
        	this.setTypeface(tf);
        }
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
        String typefaceName = a.getString( R.styleable.MyTextView_typeface);
        
        if(typefaceName != null) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), String.format("%s.otf", typefaceName));
            this.setTypeface(tf);
        } else {        
        	Typeface tf=Typeface.createFromAsset(context.getAssets(), "minion.otf"); 
        	this.setTypeface(tf);
        }
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
        
       
    }

}