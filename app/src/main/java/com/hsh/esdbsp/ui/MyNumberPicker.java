

package com.hsh.esdbsp.ui;

import com.hsh.esdbsp.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import android.widget.NumberPicker;


public class MyNumberPicker extends NumberPicker {

    public MyNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        
        try {
	        if(child instanceof EditText) {
	            ((EditText) child).setTextSize(36);
	        } else if(child instanceof TextView) {
	            ((TextView) child).setTextSize(36);
	        }
        
        } catch (Exception e) {}
        
    }
}