package com.hsh.esdbsp.ui;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.hshservice.model.GeneralItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class GridViewAdapter extends ArrayAdapter  <ImageItem> implements ListAdapter{
	private Context context;
	private int layoutResourceId;
	private ArrayList<ImageItem> data = new ArrayList<ImageItem>();
	
	
	//////////////////////////////////////////////////////
	
	
	private HashMap<Integer, ImageView> views;
	
	ViewHolder holder = null;
	
	
	//////////////////////////////////////////////////////
	

	public GridViewAdapter(Context context, int layoutResourceId,
			ArrayList data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		
		views = new HashMap<Integer, ImageView>();
		
		MainApplication.getRadio().setTVData(data);
	}
	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public ImageItem getItem(int position){
		return data.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		
		
		

		try {
			if (row == null) {
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				row = inflater.inflate(layoutResourceId, parent, false);
				holder = new ViewHolder();
				//holder.imageTitle = (TextView) row.findViewById(R.id.text);
				
				holder.image = (ImageView) row.findViewById(R.id.channelicon);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}
	
			ImageItem item = data.get(position);
			
			//holder.imageTitle.setText(item.getTitle());
			//holder.image.setImageBitmap(item.getImage());
			
			//holder.image.setBackgroundResource(item.getImage());
			
//			DisplayImageOptions options = new DisplayImageOptions.Builder()
//			.showImageOnLoading(R.drawable.bbg)
//			.showImageForEmptyUri(R.drawable.bbg)
//			.showImageOnFail(R.drawable.bbg)
//			.cacheInMemory(true)
//			.cacheOnDisk(true)
//			.considerExifParams(true)
//			.bitmapConfig(Bitmap.Config.RGB_565)
//			.build();
			
			//String imageLink = "drawable://R.drawable." +item.getImageStr();
			//Log.i("gridVIewadapter",imageLink);
			
			//ImageLoader.getInstance().displayImage(imageLink, holder.image, options);

			
			
//			  Bundle b = new Bundle ();
//			  b.putInt("file", item.getImage());
//			  b.putInt("pos", position);
//			  
//			
//			  new LoadImage().execute(b);
			  
			
			holder.image.setImageResource(item.getImage());
			
			
			/*
			holder.image.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					// TODO Auto-generated method stub
					
					try {						
						//Toast.makeText(MainApplication.getContext(), item.getTitle() + "",Toast.LENGTH_SHORT).show();
						
						MainApplication.playClickSound(arg0);
						MainApplication.getMAS().sendMASCmd(item.getCmd(), 0);
						
					} catch (Exception e) {
						Log.v("TVIcon",e.toString());
					}
					
					return false;
				}
				
			});
			*/
			
			
		} catch (Exception e) {}

		return row;
	}

	
	
	static class ViewHolder {
		TextView imageTitle;
		ImageView image;
	}
	
	

	
	


	private class LoadImage extends AsyncTask<Bundle, Void, Bundle> {

		LoadImage() {
			// TODO Auto-generated constructor stub
		}
	
		@Override
		protected Bundle doInBackground(Bundle... b) {
			
			Bundle bundle = new Bundle();
			
			try {
			
				// get the file that was passed from the bundle..
				int file = b[0].getInt("file");
				Log.v("grid","img file:" + file);
				
				
				URL UrlImage;
				Bitmap bm=null;
				try {
	//			 UrlImage = new URL (file);
	//			 HttpURLConnection connection;
	//			 connection = (HttpURLConnection) UrlImage.openConnection(); 
	//			 
	//			 bm= BitmapFactory.decodeStream(connection.getInputStream());
					
					
					//bm = BitmapFactory.decodeResource(MainApplication.getCurrentActivity().getResources(),file);
				
				} catch (Exception e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
				}   
		
		
				// now that we have the bitmap (bm), we'll
				// create another bundle to pass to 'onPostExecute'.
				// this is the method that is called at the end of 
				// our task. like a callback function..
				// this time, we're not passing the filename to this
				// method, but the actual bitmap, not forgetting to
				// pass the same position along..
				
				
				//bundle.putParcelable("bm", bm);
				bundle.putInt("pos", b[0].getInt("pos"));
				bundle.putInt("file", file);
				
			
			} catch (Exception e) {
				e.printStackTrace();
				
			}
			
			return bundle;
		}
	
		@Override
		protected void onPostExecute(Bundle result) {
			
			
			
			try {
				super.onPostExecute(result);
				
				// just a test to make sure that the position and
				// file name are matching before and after the
				// image has loaded..
				Log.d("test", "*after: " + result.getInt("pos") );
				
				// here's where the photo gets put into the
				// appropriate ImageView. we're retrieving the
				// ImageView from the HashMap according to
				// the position..
	
				
//				ImageView view = views.get(result.getInt("pos"));
//				
//				// then we set the bitmap into that view. and that's it.
//				view.setImageBitmap((Bitmap) result.getParcelable("bm"));
				
				
				//holder.image.setImageBitmap((Bitmap) result.getParcelable("bm"));

				holder.image.setImageResource(result.getInt("file"));
			
			} catch (Exception e) {
				
				Log.v("grid log","grid:" + e.toString());
			}
			
			
		}
		
	}
	
	
}







