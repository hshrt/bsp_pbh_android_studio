package com.hsh.esdbsp;

import com.hsh.esdbsp.activity.RoomControlActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class StartupBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Log.v("Startup","Starting...");
			
			Intent startupIntent = new Intent(context, RoomControlActivity.class); // substitute with your launcher class
			startupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(startupIntent);
			
			Log.v("Startup","Finish Start");
		
		} catch (Exception e) {
			
			Log.v("Startup",e.toString());
		}
	}
	
}

