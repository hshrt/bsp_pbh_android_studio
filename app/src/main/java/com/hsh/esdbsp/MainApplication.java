package com.hsh.esdbsp;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.baidu.apistore.sdk.ApiStoreSDK;
import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.comm.AlarmChecker;
import com.hsh.esdbsp.comm.BatteryChecker;
import com.hsh.esdbsp.comm.MASComm;
import com.hsh.esdbsp.comm.OnTopChecker;
import com.hsh.esdbsp.comm.RadioComm;
import com.hsh.esdbsp.comm.WeatherComm;
import com.hsh.esdbsp.ui.GridViewAdapter;
import com.hsh.esdbsp.ui.ImageItem;
import com.hsh.hshservice.global.DataCacheManager;
import com.hsh.hshservice.global.GlobalValue;
import com.hsh.hshservice.model.Dictionary;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

/*@ReportsCrashes(
 // This is required for backward compatibility but not used
 formUri = "http://www.backendofyourchoice.com/reportpath"
 )*/

/*@ReportsCrashes(
 formKey = "", // This is required for backward compatibility but not used
 formUri = "http://www.backendofyourchoice.com/reportpath"
 )*/

// william test
public class MainApplication extends MultiDexApplication {

	static String TAG = "MainApplication";

	// //////////////////////////

	// share perferences
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;

	private static String myLOG = "ESDBSP";

	private static int curScreenSta = 1;

	private static Context mContext;
	private static AssetManager mAssetManager;
	private static Activity mCurrentActivity;

	private static MASComm mMAS = null;

	private static BatteryChecker mBatteryChecker = null;
	private static AlarmChecker mAlarmChecker = null;
	private static OnTopChecker mOnTopChecker = null;

	private static WeatherComm mWeather = null;
	private static RadioComm mRadio = null;

	private boolean isInSetting;

	// for bsp sleep
	public static PowerManager powerManager;
	public static WakeLock mWakeLock;
	

	// tv grid
	private static GridViewAdapter customGridAdapter = null;

	// ///////////////////////////////////////////

	// for the room control page footbar
	private static int curRoomArea = 0;

	// ///////////////////////////////////////////

	static Handler moveTopHandler = new Handler();
	static int moveTopDelay = 30000;

	// ///////////////////////////////////////////
	// for lang

	private static String fullLangJSON = "";

	// ///////////////////////////////////////////

	public static Boolean useLocalFile = false;

	// ///////////////////////////////////////////
	


	// //////////////////////////////////////////

	public static Activity getCurrentActivity() {
		return mCurrentActivity;
	}

	public static void setCurrentActivity(Activity a) {
		mCurrentActivity = a;
	}

	// ///////////////////////////////////////////

	public static Context getContext() {
		return mContext;
	}

	public static AssetManager getAsset() {
		return mAssetManager;
	}

	// ///////////////////////////////////////////

	public static void setScreenSta(int sta) {
		curScreenSta = sta;
	}

	public static int getScreenSta() {
		return curScreenSta;
	}

	// ///////////////////////////////////////////

	/*
	 * public static void setCurArea(int area) { curRoomArea = area; }
	 * 
	 * 
	 * public static int getCurArea() { return curRoomArea; }
	 */
	// ///////////////////////////////////////////

	public static MASComm getMAS() {
		if (mMAS == null) {
			mMAS = new MASComm();
		}

		return mMAS;
	}

	public static OnTopChecker getOnTopChecker() {
		if (mOnTopChecker == null) {
			mOnTopChecker = new OnTopChecker();
		}

		return mOnTopChecker;

	}


	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public static BatteryChecker getBatteryChecker() {
		if (mBatteryChecker == null) {
			mBatteryChecker = new BatteryChecker();
		}

		return mBatteryChecker;

	}

	public static AlarmChecker getAlarmChecker() {
		if (mAlarmChecker == null) {
			mAlarmChecker = new AlarmChecker();
		}

		return mAlarmChecker;
	}

	// ////////////////////////////////////////////

	private static void getLangJSON() {
		try {

			InputStream is = getAsset().open("lang.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(is,
					"UTF-8"));

			String line = "";
			fullLangJSON = "";

			while ((line = in.readLine()) != null) {
				fullLangJSON = fullLangJSON + line;
			}

		} catch (Exception e) {
		}

	}

	private static void getLangJSON2() {
		Log.i("MainApplication", "getLangJSON2 fire");
		int ch;
		StringBuffer fileContent = new StringBuffer("");
		FileInputStream fis;
		try {
			fis = mCurrentActivity.openFileInput("dict.json");
			try {
				while ((ch = fis.read()) != -1)
					fileContent.append((char) ch);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Log.i("MainApplication", "fileContent = " + new String(fileContent));

		fullLangJSON = new String(fileContent);
		Log.i("MainApplication", "FullLangJSON = " + fullLangJSON);
	}

	public static String getLabel(String s) {
		// new

		String r = "";

		ArrayList<Dictionary> allDict = GlobalValue.getInstance()
				.getAllDictArray();
		Dictionary dedicatedDict = null;

		try {
			// Log.i("MainApplication", "HAllo ");
			// Log.i("MainApplication", "fullJSonLang = " + fullLangJSON);
			if (allDict == null) {

				// get the dictionary from cache.
			}

			for (Dictionary d : allDict) {
				if (d.getDictId().equalsIgnoreCase(s)) {
					dedicatedDict = d;
					break;
				} else if (d.getKey().equalsIgnoreCase(s)) {
					dedicatedDict = d;
					break;
				}
			}

			if (dedicatedDict == null) {
				r = " ";
				return r;
			}

			// r = jrmain.get(mMAS.getData("data_language") == "" ? "E" :
			// mMAS.getData("data_language")).toString();
			// r = dedicatedDict.getLang("E").toString();

			r = dedicatedDict.getLang(
					mMAS.getData("data_language") == "" ? "E" : mMAS
							.getData("data_language")).toString();
			
			  /*r =
			  dedicatedDict.getLang(DataCacheManager.getInstance().getLang()
			  ).toString();*/
			
			//Log.i(TAG, "currentActivity"+MainApplication.getCurrentActivity());
			
			if (MainApplication.getCurrentActivity().getString(R.string.hotel)
					.equalsIgnoreCase("PHK_R")) {
				r = dedicatedDict.getLang(
						DataCacheManager.getInstance().getLang()).toString();
			}

			if (r.equalsIgnoreCase("")) {
				r = dedicatedDict.getLang("E").toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return r;

		// Original
		/*
		 * String r = "";
		 * 
		 * try {
		 * 
		 * if ((fullLangJSON == "") || (fullLangJSON == null)) { getLangJSON();
		 * }
		 * 
		 * JSONObject jr = new JSONObject(new String(fullLangJSON)); JSONObject
		 * jrmain = new JSONObject(jr.get(s).toString());
		 * 
		 * r = jrmain.get(mMAS.getData("data_language") == "" ? "E" :
		 * mMAS.getData("data_language")).toString();
		 * 
		 * } catch (Exception e) {}
		 * 
		 * return r;
		 */

	}

	public static String getEngLabel(String s) {
		// new

		String r = "";

		Log.i(TAG, "string label = " + s);

		ArrayList<Dictionary> allDict = GlobalValue.getInstance()
				.getAllDictArray();
		Dictionary dedicatedDict = null;

		try {
			// Log.i("MainApplication", "HAllo ");
			// Log.i("MainApplication", "fullJSonLang = " + fullLangJSON);
			if (allDict == null) {

				// get the dictionary from cache.
			}

			for (Dictionary d : allDict) {
				if (d.getDictId().equalsIgnoreCase(s)) {
					dedicatedDict = d;
					break;
				} else if (d.getKey().equalsIgnoreCase(s)) {
					dedicatedDict = d;
					break;
				}
			}

			if (dedicatedDict == null) {
				r = " ";
				return r;
			}

			r = dedicatedDict.getLang("E").toString();

		} catch (Exception e) {
			Log.i("getLabel", e.toString());
		}

		try {
			if (r.equalsIgnoreCase("")) {
				r = dedicatedDict.getLang("E").toString();
			}
		} catch (Exception e) {
			// there is something wrong about the dictionary file
			// try to force reload
			// we will force an get data from server
			// DataCacheManager.getInstance().forceReload();

		}

		return r;
	}

	// //////////////////////////////////////////

	public static RadioComm getRadio() {
		if (mRadio == null) {
			mRadio = new RadioComm();
		}

		return mRadio;
	}

	public static RotateAnimation getAnim() {

		RotateAnimation anim = new RotateAnimation(0.0f, 360.0f,
				Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF,
				.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		return anim;

	}

	// //////////////////////////////////////////

	public static WeatherComm getWeather() {
		if (mWeather == null) {
			mWeather = new WeatherComm();
		}

		return mWeather;
	}

	// //////////////////////////////////////////

	// just parse to two digit
	public static String twoDigitMe(int ii) {

		if (ii < 10)
			return "0" + ii;
		else
			return "" + ii;

	}

	public static void playClickSound(View v) {
		try {

			v.playSoundEffect(SoundEffectConstants.CLICK);
		} catch (Exception e) {
		}
	}

	public static void brightUp() {
		try {
			Window mWindow = MainApplication.getCurrentActivity().getWindow();
			WindowManager.LayoutParams lp = mWindow.getAttributes();
			lp.screenBrightness = (float) 1;
			mWindow.setAttributes(lp);

		} catch (Exception e) {
		}
	}

	// //////////////////////////////////////////

	// tv grid

	public static void setTVGrid(GridViewAdapter a) {

		customGridAdapter = a;
	}

	public static GridViewAdapter getTVGrid() {

		return customGridAdapter;
	}

	// /////////////////////////////////////////

	@Override
	public void onCreate() {
		try {
			
			//baidu API SDK
			ApiStoreSDK.init(this, getString(R.string.baidu_api_key));
			
			super.onCreate();

			PackageInfo pInfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			String version = pInfo.versionName;

			FlurryAgent.setVersionName(version);

			// configure Flurry
			FlurryAgent.setLogEnabled(true);
			// init Flurry
			FlurryAgent.init(this, getString(R.string.flurry_key));

			settings = getSharedPreferences(PREFS_NAME, 0);

			// start the data loading looping, the reason to delay half second
			// is because need wait for setup Wifi first
			// DataCacheManager.getInstance().startLoadData();


			mContext = this;
			mAssetManager = this.getAssets();

			loadSetting();

			// setWiFi(mContext); // need this for pbh

			mMAS = new MASComm();
			mWeather = new WeatherComm();
			mRadio = new RadioComm();

			mBatteryChecker = new BatteryChecker();
			mAlarmChecker = new AlarmChecker();

			Log.i("Mainapp", "oncreate");

			mMAS.getMASFeed();
			mWeather.getWeatherFeed();

			// Create global configuration and initialize ImageLoader with this
			// configuration

			// ImageLoaderConfiguration config = new
			// ImageLoaderConfiguration.Builder(getApplicationContext()).build();

			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					getApplicationContext())
					.threadPriority(Thread.NORM_PRIORITY - 2)
					.denyCacheImageMultipleSizesInMemory()
					.diskCacheFileNameGenerator(new Md5FileNameGenerator())
					.diskCacheSize(200 * 1024 * 1024)
					// 300 Mb
					.memoryCacheSize(150 * 1024 * 1024)
					.tasksProcessingOrder(QueueProcessingType.LIFO)
					.writeDebugLogs()
					// Remove for release app
					.imageDownloader(
							new BaseImageDownloader(MainApplication
									.getContext(), 10 * 1000, 40 * 1000)) // this
																			// line
																			// of
																			// code
																			// is
																			// for
																			// how
																			// long
																			// the
																			// timeout
																			// should
																			// be
					.build();

			ImageLoader.getInstance().init(config);

			// /// move top

			moveTopHandler.removeCallbacks(moveTopRunnable);
			moveTopHandler.postDelayed(moveTopRunnable, moveTopDelay);

		} catch (Exception e) {
		}

	}

	// //////////////////////////////////////////////////////////

	private Runnable moveTopRunnable = new Runnable() {

		public void run() {
			try {

				moveTopHandler.removeCallbacks(moveTopRunnable);

				moveTop();

				moveTopHandler.postDelayed(moveTopRunnable, moveTopDelay);

			} catch (Exception e) {
			}
		}
	};

	// ////////////////////////////////////////////////////////////

	@SuppressLint("NewApi")
	@Override
	public void onTrimMemory(int level) {
		super.onTrimMemory(level);

		Log.i(TAG, "onTrimMemory level = " + level);
		// Log.i(TAG, "before release: getMemoryClass() = " +
		// ActivityManager.getMemoryClass());

		final Runtime runtime = Runtime.getRuntime();
		final long usedMemInMB = (runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
		final long maxHeapSizeInMB = runtime.maxMemory() / 1048576L;

		Log.i(TAG, "before clear Image cache");
		Log.i(TAG, "usedMemInMB = " + usedMemInMB);
		Log.i(TAG, "maxHeapSizeInMB = " + maxHeapSizeInMB);

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("TrimMemory Level", level + "");
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));

		FlurryAgent.logEvent("RoomControl", map);

		// don't compare with == as intermediate stages also can be reported,
		// always better to check >= or <=
		if (level >= ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL) {
			try {
				// clear image cache in memory
				ImageLoader.getInstance().clearMemoryCache();

				final Runtime runtimeAfter = Runtime.getRuntime();
				final long usedMemInMBAfter = (runtimeAfter.totalMemory() - runtimeAfter
						.freeMemory()) / 1048576L;
				final long maxHeapSizeInMBAfter = runtimeAfter.maxMemory() / 1048576L;

				Log.i(TAG, "after clear Image cache");
				Log.i(TAG, "usedMemInMB = " + usedMemInMBAfter);
				Log.i(TAG, "maxHeapSizeInMB = " + maxHeapSizeInMBAfter);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		Log.i(TAG, "after clear Image cache");
		Log.i(TAG, "usedMemInMB = " + usedMemInMB);
		Log.i(TAG, "maxHeapSizeInMB = " + maxHeapSizeInMB);

		// ////////////

		// moveTop();

	}

	// //////////////////////////////////////////////////////////////////////////

	private void moveTop() {

		try {

			// Log.v(myLOG, MainApplication.getOnTopChecker().getPackageName());

			if (!MainApplication
					.getOnTopChecker()
					.getPackageName()
					.equalsIgnoreCase(
							MainApplication.getContext().getPackageName())) {
				Log.v(myLOG, "not top"
						+ MainApplication.getOnTopChecker().getPackageName());

				// Intent intent = new Intent("android.intent.action.MAIN");
				// intent.setComponent(ComponentName.unflattenFromString("com.hsh.esdbsp"));
				// intent.addCategory("android.intent.category.LAUNCHER");

				moveTopHelper();

				// startActivity(intent);

			} else {
				Log.v(myLOG, MainApplication.getOnTopChecker().getPackageName());
			}

		} catch (Exception e) {
			Log.v(myLOG, e.toString());

			// not sure
			moveTopHelper();
		}

	}

	private void moveTopHelper() {

		try {
			Intent launchBat = getBaseContext().getPackageManager()
					.getLaunchIntentForPackage(
							MainApplication.getContext().getPackageName());
			// launchBat.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			launchBat.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(launchBat);

		} catch (Exception e) {
		}
	}

	// //////////////////////////////////////////////////////////////////////////

	private void loadSetting() {
		try {

			// Settings.System.putInt(getContentResolver(),
			// Settings.System.DIM_SCREEN, 0);
			// Settings.System.putInt(getContentResolver(),
			// Settings.System.SCREEN_OFF_TIMEOUT, 65535);

			// http://developer.android.com/reference/android/provider/Settings.Secure.html
			// offFlightMode(MainApplication.getContext());

			// brightness
			Settings.System.putInt(getContentResolver(),
					Settings.System.SCREEN_BRIGHTNESS, 255);
			Settings.System.putInt(getContentResolver(),
					Settings.System.SCREEN_BRIGHTNESS_MODE,
					Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

			// disable auto rotate
			Settings.System.putInt(this.getContentResolver(),
					Settings.System.ACCELEROMETER_ROTATION, 0); // 1 is enable

			// sync
			ContentResolver.setMasterSyncAutomatically(false);

			AudioManager audioManager = (AudioManager) getSystemService(MainApplication
					.getContext().AUDIO_SERVICE);
			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

			
			
			// keep system running
			powerManager = (PowerManager) getSystemService(MainApplication
					.getContext().POWER_SERVICE);
			mWakeLock = powerManager.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
			// WakeLock mWakeLock =
			// powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
			// getClass().getName());

			if ((mWakeLock != null) && // we have a WakeLock
					(mWakeLock.isHeld() == false)) { // but we don't hold it
				mWakeLock.acquire();
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static boolean isFlightModeEnabled(Context context) {
		return Settings.System.getInt(context.getContentResolver(),
				Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
	}

	public static void offFlightMode(Context context) {

		try {

			Settings.System.putInt(context.getContentResolver(),
					Settings.Global.AIRPLANE_MODE_ON, 0);
			Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
			intent.putExtra("state", false);
			context.sendBroadcast(intent);

		} catch (Exception e) {
		}

	}

	// ///////////////////////////////////////////////////////////

	public void setWiFi(Context context) {

		try {

			String networkSSID = this.getString(R.string.my_ssid);
			String networkPass = this.getString(R.string.my_key);

			WifiConfiguration conf = new WifiConfiguration();
			conf.SSID = "\"" + networkSSID + "\"";

			conf.preSharedKey = "\"" + networkPass + "\"";

			conf.hiddenSSID = true;

			conf.status = WifiConfiguration.Status.ENABLED;
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			conf.allowedPairwiseCiphers
					.set(WifiConfiguration.PairwiseCipher.TKIP);
			conf.allowedPairwiseCiphers
					.set(WifiConfiguration.PairwiseCipher.CCMP);
			conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);

			WifiManager wifiManager = (WifiManager) context
					.getSystemService(Context.WIFI_SERVICE);
			wifiManager.setWifiEnabled(true);
			wifiManager.addNetwork(conf);

			List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
			for (WifiConfiguration i : list) {

				if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {

					wifiManager.disconnect();
					if (wifiManager.enableNetwork(i.networkId, true)) {

						// finally quit
						try {

							SharedPreferences prefs = PreferenceManager
									.getDefaultSharedPreferences(getBaseContext());
							boolean previouslyStarted = prefs
									.getBoolean(
											getString(R.string.pref_previously_started),
											false);
							if (!previouslyStarted) {
								SharedPreferences.Editor edit = prefs.edit();
								edit.putBoolean(
										getString(R.string.pref_previously_started),
										Boolean.TRUE);
								edit.commit();

							} else {
								try {

								} catch (Exception e) {
								}
							}

						} catch (Exception e) {
						}

						Log.v(myLOG, "wifi ok connected!");
					} else {
						Log.v(myLOG, "wifi not connect!!");
					}

					wifiManager.reconnect();

					break;
				}
			}

		} catch (Exception e) {
		}

	}

	public boolean isInSetting() {
		return isInSetting;
	}

	public void setInSetting(boolean isInSetting) {
		this.isInSetting = isInSetting;
	}

	// ///////////////////////////////////////////////////////////

}
