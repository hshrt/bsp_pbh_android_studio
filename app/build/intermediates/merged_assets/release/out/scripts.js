// Stop Event Propagation
function stopEvent(ev) {
	ev.stopPropagation();
}

// Alert setup
function alertSetup() {
	var a = document.getElementById('alert');
	var aHeader = document.getElementById('alertHeader');
	var aBody = document.getElementById('alertBody');
	var aControls = document.getElementById('alertControls');
	var aHeight = 0;
	var aHeaderHeight = 0;
	var aBodyHeightOrigin = 0;
	var aBodyHeight = 0;
	var aControlsHeight = 0;
	aBody.style.height = "auto"; //reset
	aBodyHeightOrigin = parseInt(window.getComputedStyle(aBody, null).getPropertyValue("height"));
	aHeight = parseInt(window.getComputedStyle(a, null).getPropertyValue("height"));

	if (aHeader){
		if((window.getComputedStyle(aHeader, null).getPropertyValue("display")) != 'none'){
			aHeaderHeight = aHeader.offsetHeight + parseInt(window.getComputedStyle(aHeader, null).getPropertyValue("margin-top")) + parseInt(window.getComputedStyle(aHeader, null).getPropertyValue("margin-bottom"));
		}
	}

	if(aControls){
		aControlsHeight = aControls.offsetHeight + parseInt(window.getComputedStyle(aControls, null).getPropertyValue("margin-top")) + parseInt(window.getComputedStyle(aControls, null).getPropertyValue("margin-bottom"));
	}

	if (aHeight > (window.innerHeight)){
		aHeight = window.innerHeight - parseInt(window.getComputedStyle(a, null).getPropertyValue("border-top-width")) - parseInt(window.getComputedStyle(a, null).getPropertyValue("border-bottom-width"));
	}
	else {
		a.style.height = "auto"; //reset
	}

	aBodyHeight = aHeight - aHeaderHeight - aControlsHeight;
	aBody.style.height = aBodyHeight + "px";

	if (aBodyHeight < aBodyHeightOrigin){
		a.classList.add('alert-scaled');
	}
	else {
		a.classList.remove('alert-scaled');
	}
}
